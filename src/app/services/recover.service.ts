import { Injectable } from '@angular/core';
import { recover } from '../models/global/recover';

@Injectable({
    providedIn: 'root'
})
export class RecoverService {
    public recoverInf: recover = new recover();

    constructor() {}

    getRecover(format:string, type:string, user_name: string){
        this.recoverInf.format = format;
        this.recoverInf.type = type;
        this.recoverInf.object = this.getObjectGrid(format, type);
        this.recoverInf.columns = this.getColumnsGrid(format, type);
        this.recoverInf.filter = this.getFilterGrid(format, type, user_name);

        if (type == 'R') {
            this.recoverInf.title = 'Folios Creados'
        } else if (type == 'V') {
            this.recoverInf.title = 'Folios en Visto Bueno'
        } else if (type == 'A') {
            this.recoverInf.title = 'Folios en Autorización'
        } else if (type == 'UP') {
            this.recoverInf.title = 'Usuarios Pendientes'
        } else if (type == 'CP') {
            this.recoverInf.title = 'Contrataciones Pendientes'
        } else if (type == 'RP') {
            this.recoverInf.title = 'Renovaciones Pendientes'
        }

        return this.recoverInf;
    }

    getObjectGrid(format:string, type:string) {
        switch(format) {
            case 'DRH-FRM-PER':
                return '41';
            case 'DRH-FRM-BPR':
                return '65';
            case 'DRH-FRM-CTR':
                switch(type) {
                    case 'CP':
                        return '63';
                    case 'RP':
                        return '';
                    default:
                        return '67';
                }
            case 'ADM-FRM-USR':
                return '41';
            case 'DRM-FRM-REQ':
                return '15';
            default:
                return '';
        }
    }

    getColumnsGrid(format:string, type:string) {
        switch(format) {
            case 'DRH-FRM-PER':
                if(type == 'R') {
                    return [
                        { headerName: 'Región', field: 'DSREG', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Nº Folio', field: 'FOLIO', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Fecha', field: 'FECHA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Estatus', field: 'STDSC', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Adscripción', field: 'ADSCR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Puesto', field: 'PUEST', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Funcionario', field: 'NOMBR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true }
                    ];
                } else {
                    return [
                        { headerName: 'Región', field: 'DSREG', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Nº Folio', field: 'FOLIO', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Fecha', field: 'FECHA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Adscripción', field: 'ADSCR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Puesto', field: 'PUEST', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Funcionario', field: 'NOMBR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true }
                    ];
                }
            case 'DRH-FRM-BPR':
                if(type == 'R') {
                    return [
                        { headerName: 'Nº Folio', field: 'FOLIO', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Fecha', field: 'FECHA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Estatus', field: 'STDSC', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Región', field: 'DSREG', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Adscripción', field: 'ADSCR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Puesto', field: 'PUEST', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Funcionario', field: 'NOMBR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Fecha de Baja', field: 'FCBAJ', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Causa de Baja', field: 'CAUBA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true }
                    ];
                } else {
                    return [
                        { headerName: 'Nº Folio', field: 'FOLIO', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Fecha', field: 'FECHA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Región', field: 'DSREG', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Adscripción', field: 'ADSCR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Puesto', field: 'PUEST', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Funcionario', field: 'NOMBR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Fecha de Baja', field: 'FCBAJ', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Causa de Baja', field: 'CAUBA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true }
                    ];
                }
            case 'DRH-FRM-CTR':
                if(type == 'CP') {
                    return [
                        { headerName: 'Región', field: 'DSREG', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Solicitud', field: 'FOLIO', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Nº Empleado', field: 'NUEMP', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Funcionario', field: 'NOMBR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Fecha Ingreso', field: 'FCING', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Fecha Término', field: 'FCTER', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true }
                    ];
                } else {
                    return [
                        { headerName: 'Folio', field: 'FOLIO', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Tipo de Contratación', field: 'TIPCT', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Región', field: 'DSREG', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Nº Empleado', field: 'NUEMP', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Funcionario', field: 'NOMBR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Fecha Ingreso', field: 'FCING', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Fecha Término', field: 'FCTER', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true }
                    ];
                }
            case 'ADM-FRM-USR':
                if(type == 'R') {
                    return [
                        { headerName: 'Email', field: 'EMAIL', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Región', field: 'DSREG', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Adscripción', field: 'ADSCR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Puesto', field: 'PUEST', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Funcionario', field: 'NOMBR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true }
                    ];
                } else {
                    return [
                        { headerName: 'Región', field: 'DSREG', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Nº Folio', field: 'FOLIO', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Fecha', field: 'FECHA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Adscripción', field: 'ADSCR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Puesto', field: 'PUEST', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Funcionario', field: 'NOMBR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true }
                    ];
                }
            case 'DRM-FRM-REQ':
                if(type == 'R') {
                    return [
                        { headerName: 'Folio', field: 'FOLIO', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Fecha', field: 'FECHA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Estatus', field: 'STDSC', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Partida', field: 'PRTSP', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Contrato', field: 'NMCON', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Monto', field: 'TOTAL', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true, cellStyle: { 'text-align': 'right' } }
                    ];
                } else {
                    return [
                        { headerName: 'Folio', field: 'FOLIO', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Fecha', field: 'FECHA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Usuario', field: 'NOMBR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Partida', field: 'PRTSP', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Contrato', field: 'NMCON', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                        { headerName: 'Monto', field: 'TOTAL', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true, cellStyle: { 'text-align': 'right' } }
                    ];
                }
            default:
                return [];
        }
    }

    getFilterGrid(format:string, type: string, user_name: string) {
        switch(format) {
            case 'DRH-FRM-PER':
                if(type == 'R') {
                    return 'USUAR=¯' + user_name + '¯';
                } else if(type == 'V') {
                    return 'STTUS=¯2¯ AND VBHPU=¯' + user_name + '¯';
                } else if(type == 'A') {
                    return 'STTUS=¯3¯ AND AUTOU=¯' + user_name + '¯';
                }
            case 'DRH-FRM-BPR':
                if(type == 'R') {
                    return 'USUAR=¯' + user_name + '¯';
                } else if(type == 'V') {
                    return 'STTUS=¯2¯ AND VBHPU=¯' + user_name + '¯';
                } else if(type == 'A') {
                    return 'STTUS=¯3¯ AND AUTOU=¯' + user_name + '¯';
                }
            case 'DRH-FRM-CTR':
                if(type == 'R') {
                    return 'USUAR=¯' + user_name + '¯ AND STTUS=4';
                } else if(type == 'CP') {
                    return 'EJERC=' + sessionStorage.getItem('year');
                }
            case 'ADM-FRM-USR':
                if(type == 'R') {
                    return 'A.STTUS=¯4¯ AND LEN(EMAIL) > 0';
                } else if(type == 'UP') {
                    return 'A.STTUS=¯4¯ AND LEN(EMAIL) = 0 AND LEN(CAUBA) = 0';
                }
            case 'DRM-FRM-REQ':
                if(type == 'R') {
                    return 'EJERC=' + sessionStorage.getItem('year') + ' AND USUAR=¯' + user_name + '¯';
                } else if(type == 'V') {
                    return 'EJERC=' + sessionStorage.getItem('year') + ' AND STTUS=¯2¯ AND VBSPU=¯' + user_name + '¯';
                } else if(type == 'A') {
                    return 'EJERC=' + sessionStorage.getItem('year') + ' AND STTUS=¯3¯ AND AUTOU=¯' + user_name + '¯';
                }
            default:
                return '';
        }
    }
}