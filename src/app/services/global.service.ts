import { Injectable } from '@angular/core';
import { WBService } from './wb.service';
import swal from 'sweetalert2';
import { address } from '../models/global/address';

@Injectable({
    providedIn: 'root'
})
export class GlobalService {
    public ad: address;

    constructor(private wbService: WBService) {}

    getDateUTC(date:string){
        let timeDiference:number = new Date().getTimezoneOffset();
        let fecha:Array<string> = date.split('/');
        let fechaUTC:Date = new Date(Date.UTC(Number(fecha[2]), Number(fecha[1])-1, Number(fecha[0]),0,timeDiference,0,0));

        return fechaUTC;
    }

    messageSwal(title: string, content: string, type: any){
        swal.fire(title, content, type);
    }

    getAddressJSON(ads: any) {
        this.ad = new address();

        this.ad.place = this.isGooglePlace(ads);

        if(this.ad.place) {
            this.ad.address = ads.formatted_address;
            this.ad.placeId = ads.place_id;
            this.ad.geometry = this.isGeometry(ads);

            if(this.ad.geometry) {
                this.ad.longitud = ads.geometry.location.lng();
                this.ad.latitude = ads.geometry.location.lat();
            }

            this.ad.components = this.haveComponents(ads);

            if(this.ad.components) {
                for(let x=0; x < ads.address_components.length; x++) {
                    if(ads.address_components[x].types && ads.address_components[x].types[0]) {
                        let type = ads.address_components[x].types && ads.address_components[x].types[0];

                        switch(type) {
                            case 'street_number':
                                this.ad.streetNumber = ads.address_components[x].types && ads.address_components[x].short_name;
                                break;
                            case 'route':
                                this.ad.street = ads.address_components[x].types && ads.address_components[x].long_name;
                                break;
                            case 'sublocality_level_1':
                                this.ad.sublocality = ads.address_components[x].types && ads.address_components[x].long_name;
                                break;
                            case 'locality':
                                this.ad.city = ads.address_components[x].types && ads.address_components[x].long_name;
                                break;
                            case 'administrative_area_level_1':
                                this.ad.state = ads.address_components[x].types && ads.address_components[x].long_name;
                                this.ad.stateShort = ads.address_components[x].types && ads.address_components[x].short_name;
                                break;
                            case 'country':
                                this.ad.country = ads.address_components[x].types && ads.address_components[x].long_name;
                                this.ad.countryShort = ads.address_components[x].types && ads.address_components[x].short_name;
                                break;
                            case 'administrative_area_level_2':
                                this.ad.district = ads.address_components[x].types && ads.address_components[x].long_name;
                                break;
                            case 'postal_code':
                                this.ad.postalCode = ads.address_components[x].types && ads.address_components[x].short_name;
                            default:
                                break;
                        }
                    }
                }
            }
        }

        return this.ad;
    }

    protected isGooglePlace(place){
        if (!place) return false;
        return !!place.place_id;
    }

    protected isGeometry(place){
        if (!place.geometry) return false;
        return !!place.geometry;
    }

    protected haveComponents(place){
        if(!place.address_components) return false;

        return place.address_components.length > 0;
    }
}