import { Injectable } from '@angular/core';
import { Observable, throwError, forkJoin } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpRequest, HttpEvent, HttpResponse } from '@angular/common/http';
import { EnvService } from '../models/environment/environment.model';
import { AuthService } from './auth.service';

@Injectable({
    providedIn: 'root'
})
export class WBService {
    private urlEndpoint;

    constructor(private http: HttpClient, private authService: AuthService, public env: EnvService) {
        this.urlEndpoint = this.env.url + 'api';
    }

    getMenu(): Observable<any> {
        let user_name = this.authService.user.user_name;

        return this.http.get<any>(this.urlEndpoint + '/menu?user_name=' + user_name)
    }

    getCount(object: string, find: string, parm?: string): Observable<any> {
        let params = parm ? parm:'';

        return this.http.get<any>(this.urlEndpoint + '/countInf?Object=' + object + '&Find=' + find + '&Params=' +params)
    }

    getInformation(object: string, find: string, parm?: string): Observable<any> {
        let params = parm ? parm:'';

        return this.http.get<any>(this.urlEndpoint + '/information?Object=' + object + '&Find=' + find + '&Params=' +params)
    }

    getInformationParallel(object: Array<string>, find: Array<string>, parm?: Array<string>): Observable<any> {
        let http = [];
        
        for (let x = 0; x < object.length; x ++) {
            let params = parm[x] ? parm[x]:'';

            http.push(this.http.get<any>(this.urlEndpoint + '/information?Object=' + object[x] + '&Find=' + find[x] + '&Params=' +params));
        }

        return forkJoin(http);
    }

    saveInformationForms(type: string, object: string, folio: string, status: string, data: string): Observable<any> {
        let user_name = this.authService.user.user_name;

        return this.http.post<any>(this.urlEndpoint + '/save/' + type + '?object=' + object + '&user_name=' + user_name + '&folio=' + folio + '&status=' + status, data)
    }

    updateInformationForms(object: string, order: string, filter: string, data: string): Observable<any> {
        let user_name = this.authService.user.user_name;

        return this.http.put<any>(this.urlEndpoint + '/save?object=' + object + '&user_name=' + user_name + '&order=' + order + '&filter=' + filter, data)
    }

    getInformationForms(object: string, find: string): Observable<any> {
        let user_name = this.authService.user.user_name;

        return this.http.get<any>(this.urlEndpoint + '/recover?object=' + object + '&find=' + find + '&user_name=' + user_name)
    }

    uploadFile(file: File, object: string, folio: string): Observable<HttpEvent<{}>> {
        let user_name = this.authService.user.user_name;
        
        let formData = new FormData();

        formData.append("file", file);
        formData.append("user_name", user_name);
        formData.append("object", object);
        formData.append("folio", folio);

        let token = sessionStorage.getItem('token');

        const req = new HttpRequest('POST', this.urlEndpoint + '/upload/file', formData, {reportProgress: true});

        return this.http.request(req);
    }

    showFile(file: string, object: string, folio: string, type: string): Observable<Blob> {
        let user_name = this.authService.user.user_name;

        return this.http.get(this.urlEndpoint + '/show/file?file=' + file + '&user_name=' + user_name + '&object=' + object + '&folio=' + folio + '&type=' + type, { responseType: 'blob'});
    }

    createFile(option: string, object: string, folio: string): Observable<Blob> {
        let user_name = this.authService.user.user_name;

        return this.http.get(this.urlEndpoint + '/create/file/'+option+'?user_name=' + user_name + '&object=' + object + '&folio=' + folio, {responseType: 'blob'});
    }

    downloadFile(option: string, object: string, folio: string): Observable<Blob> {
        let user_name = this.authService.user.user_name;

        return this.http.get(this.urlEndpoint + '/download/file/'+option+'?user_name=' + user_name + '&object=' + object + '&folio=' + folio, {responseType: 'blob'});
    }

    getXML(file: string, object: string, folio: string): Observable<any> {
        let user_name = this.authService.user.user_name;

        return this.http.get<any>(this.urlEndpoint + '/reader/xml?user_name=' + user_name + '&object=' + object + '&folio=' + folio + '&file=' + file)
    }

    uploadForm(file: File, object: string, folio: string): Observable<HttpEvent<{}>> {
        let user_name = this.authService.user.user_name;
        
        let formData = new FormData();

        formData.append("file", file);
        formData.append("user_name", user_name);
        formData.append("object", object);
        formData.append("folio", folio);

        const req = new HttpRequest('POST', this.urlEndpoint + '/upload/form', formData, {reportProgress: true});

        return this.http.request(req);
    }

    getReportStructure(report: string): Observable<any> {
        return this.http.get<any>(this.urlEndpoint + '/report/structure?Report=' + report)
    }

    getReportData(report: string, find: string, parm: string): Observable<any> {
        let user_name = this.authService.user.user_name;

        return this.http.get<any>(this.urlEndpoint + '/report/data?Report=' + report + '&Find=' + find + '&Params=' + parm + '&user_name=' + user_name)
    }

    saveVersionsReport(report: string, version: string, data: string): Observable<any> {
        let user_name = this.authService.user.user_name;

        return this.http.post<any>(this.urlEndpoint + '/report/version?Report=' + report + '&user_name=' + user_name + '&version=' + version, data)
    }

    getVersions(report: string): Observable<any> {
        let user_name = this.authService.user.user_name;

        return this.http.get<any>(this.urlEndpoint + '/report/versions?Report=' + report + '&user_name=' + user_name)
    }

    deleteVersion(report: string, version: string): Observable<any> {
        let user_name = this.authService.user.user_name;

        return this.http.delete<any>(this.urlEndpoint + '/report/version?Report=' + report + '&user_name=' + user_name + '&version=' + version)
    }

    saveCatalogs(option: string, object: string, folio: string, params: string, data: string): Observable<any> {
        let user_name = this.authService.user.user_name;

        return this.http.post<any>(this.urlEndpoint + '/catalogs/save/' +option+ '?object=' + object + '&user_name=' + user_name + '&folio=' + folio + '&params=' + params, data)
    }

    deleteCatalogs(object: string, folio: string): Observable<any> {
        let user_name = this.authService.user.user_name;

        return this.http.delete<any>(this.urlEndpoint + '/catalogs/delete?object=' + object + '&user_name=' + user_name + '&folio=' + folio)
    }
} 