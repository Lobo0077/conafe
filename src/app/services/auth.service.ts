import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { login } from '../models/pages/auth/login.model';
import { EnvService } from '../models/environment/environment.model';
import * as jwt_decode from 'jwt-decode';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private urlEndpoint;

    constructor(private http: HttpClient, public env: EnvService) {
        this.urlEndpoint = this.env.url;
    }

    public get user():any{
        if(sessionStorage.getItem('user')){
            return JSON.parse(sessionStorage.getItem('user'));
        }else{
            return null;
        }
    }

    public get token():any{
        return sessionStorage.getItem('token');
    }

    public get session():boolean {
        if(sessionStorage.getItem('user')){
            return true;
        }else{
            return false;
        }
    }

    login(log: login): Observable<any> {
        const credentials = btoa('WorkBookApp:fKnTYJpPrOWSrg4m1IXR');

        const httpHeaders = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded',
                                             Authorization: 'Basic ' + credentials});

        const params = new URLSearchParams();

        params.set('grant_type', 'password');
        params.set('username', log.username);
        params.set('password', log.password);

        return this.http.post<any>(this.urlEndpoint + 'oauth/token', params.toString(), {headers: httpHeaders});
    }

    saveStorage(token: string):void {
        let payload = jwt_decode(token);

        sessionStorage.setItem('user', JSON.stringify(payload));
        sessionStorage.setItem('token', token);
    }

    hasRole(role:string):boolean{
        if(this.user && this.user.authorities.includes(role)){
            return true;
        }
        return false;
    }
    
    tokenExpired():boolean {
        if(this.user) {
            let user = this.user;
            let now = new Date().getTime()/1000

            if(user.exp < now) {
                return true;
            } else {
                return false;
            }
        } else {
            return true;
        }
    }
}
