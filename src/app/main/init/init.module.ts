import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { InitComponent } from './init.component';

import { InitGuard } from '../../guards/init.guard';

const routes = [
    {
        path     : 'init',
        component: InitComponent,
        canActivate: [InitGuard]
    }
];

@NgModule({
    declarations: [
        InitComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,

        FuseSharedModule
    ],
    exports     : [
        InitComponent
    ]
})

export class InitModule
{
}
