import { Component, ViewEncapsulation } from '@angular/core';
import { login } from '../../../../models/pages/auth/login.model';
import { FuseConfigService } from '@fuse/services/config.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { fuseAnimations } from '@fuse/animations';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';

import { Router } from '@angular/router';
import { AuthService } from '../../../../services/auth.service';
import { WBService } from '../../../../services/wb.service';
import { GlobalService } from '../../../../services/global.service';

import { FuseNavigation } from '@fuse/types';

@Component({
    selector     : 'login',
    templateUrl  : './login.component.html',
    styleUrls    : ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class LoginComponent
{
    public log: login;
    public navigation: FuseNavigation[] = [];

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _fuseNavigationService: FuseNavigationService,
        private _router: Router,
        private _authService: AuthService,
        private _wbService: WBService,
        private _globalService: GlobalService,
        private _fuseSplashScreenService: FuseSplashScreenService
    )
    {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };

        this.log = new login('', '');
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    login() {
        this._fuseSplashScreenService.show();

        this._authService.login(this.log).subscribe(response => {
            this._authService.saveStorage(response.access_token);

            this._wbService.getMenu().subscribe(response => {
                
                this.navigation = response.children;
                
                if(this._fuseNavigationService.getNavigation('main')) {
                    this._fuseNavigationService.unregister('main');
                }

                this._fuseNavigationService.register('main', this.navigation);
                this._fuseNavigationService.setCurrentNavigation('main');

                this._fuseSplashScreenService.hide();
                this._router.navigate(['/init']);
              })
        }, error => {
            console.log(error);
            this._fuseSplashScreenService.hide();
            this._globalService.messageSwal('Datos Incorrectos', 'Tu usuario o contraseña es incorrecta', 'error');
        }
        );
    }
}
