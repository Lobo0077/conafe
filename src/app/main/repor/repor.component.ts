import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { buttonGrid } from "../../custom/grid/button/button.grid.component";
import { currencyGrid } from "../../custom/grid/label/currency.grid.component";

import { AuthService } from '../../services/auth.service';
import { GlobalService } from '../../services/global.service';
import { WBService } from '../../services/wb.service';

import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import { VERSION } from '../../models/repor/version/version.model';

import "ag-grid-enterprise";
import * as moment from 'moment';
import swal from 'sweetalert2';

@Component({
    selector: 'repor',
    templateUrl: './repor.component.html',
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es-MX' },
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
    ]
})
export class ReporComponent implements OnInit {
    public modul: string;
    public rpctg: string;
    public repor: string;
    public object: string;
    public expor: Array<string>;
    public param: boolean;
    public params: any;
    public filter: string;
    public version: VERSION = new VERSION();

    public versions = [];
    private gridApi: any;
    public columnDefs = [];
    public rowData = [];

    constructor(private activatedRoute: ActivatedRoute,
        private wbService: WBService,
        private authService: AuthService,
        private globalService: GlobalService) { }

    ngOnInit() {
        this.activatedRoute.params.subscribe(routeParams => {
            this.object = routeParams['repor'];
            this.version = new VERSION();
            this.loadRepor(this.object);
        });
    }

    loadRepor(repor) {
        let user: string = this.authService.user.user_name;
        let regio: string = this.authService.user.office;
        let profile: string = this.authService.user.profile;
        let year: string = sessionStorage.getItem('year');

        this.filter = 'EJERC=¯' + year + '¯';

        if (profile == 'B') {
            this.filter += ' AND USUAR=¯' + user + '¯';
        } else if (profile == 'R') {
            this.filter += ' AND REGIO=¯' + regio + '¯';
        }

        this.wbService.getReportStructure(repor).subscribe(response => {
            this.columnDefs = this.preColumns(response.columns);

            this.modul = response.modul;
            this.rpctg = response.rpctg;
            this.repor = response.repor;
            this.expor = response.expor;
            this.param = response.param;

            this.rowData = [];

            if (!this.param) {
                this.getData(repor, '');
            } else {
                this.params = response.params;
            }
        })
    }

    getData(repor, params) {
        let filters = this.gridApi.api.getFilterModel(),
            orders = this.gridApi.api.getSortModel();

        this.rowData = [];

        this.wbService.getReportData(repor, this.filter, params).subscribe(response => {
            this.rowData = response.data;

            setTimeout(() => {
                if (filters) {
                    this.gridApi.api.setFilterModel(filters);
                } else {
                    this.gridApi.api.setFilterModel(null);
                }

                if (orders) {
                    this.gridApi.api.setSortModel(orders);
                } else {
                    this.gridApi.api.setSortModel(null);
                }
            }, 10)
        })
    }

    onGridReady(params) {
        this.gridApi = params;
        console.clear();
    }

    preColumns(data: Array<any>) {
        for (let x = 0; x < data.length; x++) {
            if (data[x].children) {
                for (let z = 0; z < data[x].children.length; z++) {
                    data[x].children[z] = this.formColumns(data[x].children[z]);
                }
            } else {
                data[x] = this.formColumns(data[x]);
            }
        }

        return data;
    }

    formColumns(data) {
        if (data.filter == 'agDateColumnFilter') {
            data.filterParams = { comparator: function (filterLocalDateAtMidnight, cellValue) { var dateAsString = cellValue; if (dateAsString == null) return -1; var dateParts = dateAsString.split('/'); var cellDate = new Date(Number(dateParts[2]), Number(dateParts[1]) - 1, Number(dateParts[0])); if (filterLocalDateAtMidnight.getTime() == cellDate.getTime()) { return 0; } if (cellDate < filterLocalDateAtMidnight) { return -1; } if (cellDate > filterLocalDateAtMidnight) { return 1; } }, browserDatePicker: true }
        }

        if (data.cellRendererFramework) {
            if (data.cellRendererFramework == 'buttonGrid') {
                data.cellRendererFramework = buttonGrid;
            } else if (data.cellRendererFramework == 'currencyGrid') {
                data.cellRendererFramework = currencyGrid;
            }
        }

        if (data.cellRendererParams) {
            if(data.cellRendererParams.indexOf('download,') == 0) {
                data.cellRendererParams = { options: data.cellRendererParams, onClick: this.onBtnDownload.bind(this) };
            }
        }

        if (data.cellStyle) {
            let style = JSON.parse(data.cellStyle);

            data.cellStyle = function () { return style }
        }

        return data;
    }

    onBtnDownload(e) {
        if(e.option=='1') {
            this.downloadForm(e.rowData.FORMA, e.rowData[e.field]);
        } else if(e.option=='2') {
            this.showFile(e.rowData.FORMA, e.rowData[e.field], e.rowData[e.column]);
        } else if(e.option=='3') {
            this.downloadFile(e.rowData.FORMA, e.rowData[e.field], e.rowData[e.column]);
        }
    }

    downloadForm(forma: string, folio: string) {
        this.wbService.downloadFile('OK', forma, folio).subscribe(x => {

            var newBlob = new Blob([x], { type: 'application/pdf' });

            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(newBlob);
                return;
            }

            const data = window.URL.createObjectURL(newBlob);

            var link = document.createElement('a');
            link.href = data;
            link.download = folio + '-OK.pdf';

            link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

            setTimeout(function () {
                window.URL.revokeObjectURL(data);
                link.remove();
            }, 100);
        });
    }

    showFile(forma: string, folio: string, file: string) {
        this.wbService.showFile(file, forma, folio, 'application/pdf').subscribe(x => {
            var newBlob = new Blob([x], { type: 'application/pdf' });

            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(newBlob);
                return;
            }

            const data = window.URL.createObjectURL(newBlob);

            window.open(data, '_blank');
        });
    }

    downloadFile(forma: string, folio: string, file: string) {
        this.wbService.showFile(file, forma, folio, 'text/xml').subscribe(x => {
            var newBlob = new Blob([x], { type: 'text/xml' });

            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(newBlob);
                return;
            }

            const data = window.URL.createObjectURL(newBlob);

            var link = document.createElement('a');
            link.href = data;
            link.download = file;

            link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

            setTimeout(function () {
                window.URL.revokeObjectURL(data);
                link.remove();
            }, 100);
        });
    }

    exportXLS() {
        var params = {
            columnKeys: this.expor,
            fileName: this.repor,
            sheetName: 'Reporte'
        };

        this.gridApi.api.exportDataAsExcel(params);
    }

    saveVersion() {
        let columns = JSON.stringify(this.gridApi.columnApi.getColumnState()),
            filters = JSON.stringify(this.gridApi.api.getFilterModel()),
            orders = JSON.stringify(this.gridApi.api.getSortModel());

        this.version.COLUM = columns,
            this.version.FILTE = filters,
            this.version.ORDBY = orders;

        if (!this.version.VERSI) {
            swal.fire({
                title: 'Nombre de la versión a guardar',
                input: 'text',
                showCancelButton: true,
                allowOutsideClick: true,
                allowEscapeKey: true,
                inputValidator: (value) => {
                    if (!value) {
                        return 'Necesitas ingresar el nombre de la versión'
                    }

                    return new Promise((resolve) => {
                        this.version.DESCR = value;
                        this.updateVersion();
                        resolve();
                    })
                }
            })
        } else {
            this.updateVersion();
        }
    }

    saveAsVersion() {
        let columns = JSON.stringify(this.gridApi.columnApi.getColumnState()),
            filters = JSON.stringify(this.gridApi.api.getFilterModel()),
            orders = JSON.stringify(this.gridApi.api.getSortModel());

        this.version.COLUM = columns,
            this.version.FILTE = filters,
            this.version.ORDBY = orders;

        swal.fire({
            title: 'Nombre de la nueva versión a guardar',
            input: 'text',
            showCancelButton: true,
            allowOutsideClick: true,
            allowEscapeKey: true,
            inputValidator: (value) => {
                if (!value) {
                    return 'Necesitas ingresar el nombre de la versión'
                }

                return new Promise((resolve) => {
                    this.version.VERSI = '';
                    this.version.DESCR = value;
                    this.updateVersion();
                    resolve();
                })
            }
        })
    }

    updateVersion() {
        this.wbService.saveVersionsReport(this.object, this.version.VERSI, JSON.stringify(this.version)).subscribe(
            response => {
                if (!this.version.VERSI) {
                    this.version.VERSI = response.VERSI;
                    this.globalService.messageSwal('Información Almacenada', 'Tu versión fue almacenada con el folio ' + response.VERSI, 'success');
                } else {
                    this.globalService.messageSwal('Información Actualizada', 'Tu versión fue actualizada', 'success');
                }

            },
            error => {
                this.globalService.messageSwal('Error!!', 'Tu versión no pudo ser almacenada favor de intentarlo nuevamente', 'error');
                console.log(error);
            })
    }

    process() {
        let param: string = '';

        for (let x = 0; x < this.params.length; x++) {
            if (this.params[x].option == 3) {
                if (moment(this.params[x].value).format('DD/MM/YYYY') != "Invalid date") {
                    param += "¯" + moment(this.params[x].value).format('DD/MM/YYYY') + "¯";
                } else {
                    param += "¯¯"
                }
            } else {
                param += "¯" + this.params[x].value + "¯";
            }

            if (x != this.params.length - 1) {
                param += ",";
            }
        }

        param = "¯" + this.authService.user.user_name + "¯," + param;
        this.getData(this.object, param);
    }

    openVersion() {
        this.wbService.getVersions(this.object).subscribe(response => {
            if (response.data != null) {
                this.versions = response.data;

                const options = {};

                for (let x = 0; x < this.versions.length; x++) {
                    let key: string = this.versions[x].VERSI;
                    let value: string = this.versions[x].DESCR;

                    options[key] = value;
                }

                swal.fire({
                    title: 'Seleccionar versión a recuperar',
                    input: 'select',
                    inputOptions: options,
                    inputPlaceholder: 'Selecciona',
                    showCancelButton: true,
                    allowOutsideClick: true,
                    allowEscapeKey: true,
                    inputValidator: (value) => {
                        return new Promise((resolve) => {
                            this.getVersion(value);
                            resolve();
                        })
                    }
                })
            } else {
                this.globalService.messageSwal('Sin Versiones', 'No existen versiones para recuperar', 'warning');
            }
        });
    }

    getVersion(ver) {
        let res = this.versions.find(version => version.VERSI === ver);

        this.version = res;

        let columns = this.version.COLUM;
        let filters = this.version.FILTE;
        let orders = this.version.ORDBY;

        if (columns) {
            this.gridApi.columnApi.setColumnState(JSON.parse(columns));
        } else {
            this.gridApi.columnApi.resetColumnState();
        }

        if (filters) {
            this.gridApi.api.setFilterModel(JSON.parse(filters));
        } else {
            this.gridApi.api.setFilterModel(null);
        }

        if (orders) {
            this.gridApi.api.setSortModel(JSON.parse(orders));
        } else {
            this.gridApi.api.setSortModel(null);
        }
    }

    clear() {
        this.gridApi.columnApi.resetColumnState();
        this.gridApi.api.setFilterModel(null);
        this.gridApi.api.setSortModel(null);
    }

    deleteVersion() {
        swal.fire({
            title: 'Eliminar Versión',
            text: '¿Estás seguro de eliminar la versión?',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            if (result.value) {
                this.wbService.deleteVersion(this.object, this.version.VERSI).subscribe(
                    response => {
                        this.globalService.messageSwal('Versión Eliminada', 'Tu versión fue eliminada', 'success');
                        this.version = new VERSION();
                        this.clear();
                    },
                    error => {
                        this.globalService.messageSwal('Error!!', 'Tu versión no pudo ser eliminada favor de intentarlo nuevamente', 'error');
                        console.log(error);
                    })
            }
        })
    }
}