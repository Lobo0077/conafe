import { Component, OnInit, ViewChild } from '@angular/core';

import { AuthService } from '../../../../services/auth.service';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';

import { currencyGrid } from "../../../../custom/grid/label/currency.grid.component";
import { setupMaster } from 'cluster';

@Component({
    selector: 'rep_drh_pla',
    templateUrl: './pla.component.html',
    styleUrls: ['./pla.component.scss']
})
export class REPDRHPLAComponent implements OnInit {
    private gridApiP: any;
    public rowDataP = [];
    public columnDefsP = [];
    public footerDataP = [];

    public tpOptions = {
        onFilterChanged: event => this.filterChangedP(event),
        alignedGrids: [],
        defaultColDef: {
            editable: true,
            sortable: true,
            resizable: true,
            filter: true,
            flex: 1,
            minWidth: 100
        }
        ,
        suppressHorizontalScroll: true
    };

    public bpOptions = {
        alignedGrids: [],
        defaultColDef: {
            editable: true,
            sortable: true,
            resizable: true,
            filter: true,
            flex: 1,
            minWidth: 100
        }
    };

    private gridApiE: any;
    public rowDataE = [];
    public columnDefsE = [];
    public footerDataE = [];

    public teOptions = {
        onFilterChanged: event => this.filterChangedE(event),
        alignedGrids: [],
        defaultColDef: {
            editable: true,
            sortable: true,
            resizable: true,
            filter: true,
            flex: 1,
            minWidth: 100
        }
        ,
        suppressHorizontalScroll: true
    };

    public beOptions = {
        alignedGrids: [],
        defaultColDef: {
            editable: true,
            sortable: true,
            resizable: true,
            filter: true,
            flex: 1,
            minWidth: 100
        }
    };

    private gridApiH: any;
    public rowDataH = [];
    public columnDefsH = [];
    public footerDataH = [];

    public thOptions = {
        onFilterChanged: event => this.filterChangedH(event),
        alignedGrids: [],
        defaultColDef: {
            editable: true,
            sortable: true,
            resizable: true,
            filter: true,
            flex: 1,
            minWidth: 100
        }
        ,
        suppressHorizontalScroll: true
    };

    public bhOptions = {
        alignedGrids: [],
        defaultColDef: {
            editable: true,
            sortable: true,
            resizable: true,
            filter: true,
            flex: 1,
            minWidth: 100
        }
    };

    private gridApiR: any;
    public rowDataR = [];
    public columnDefsR = [];
    public footerDataR = [];

    public trOptions = {
        onFilterChanged: event => this.filterChangedR(event),
        alignedGrids: [],
        defaultColDef: {
            editable: true,
            sortable: true,
            resizable: true,
            filter: true,
            flex: 1,
            minWidth: 100
        }
        ,
        suppressHorizontalScroll: true
    };

    public brOptions = {
        alignedGrids: [],
        defaultColDef: {
            editable: true,
            sortable: true,
            resizable: true,
            filter: true,
            flex: 1,
            minWidth: 100
        }
    };

    constructor(private wbService: WBService,
        private authService: AuthService,
        private globalService: GlobalService) {
        this.columnDefsP = [
            { headerName: 'PUESTO', field: 'DESCR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
            { headerName: 'CLAVE DEL PUESTO', field: 'PUEST', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
            { headerName: 'AUTORIZADAS', field: 'AUTOR', suppressMenu: true, filter: 'agNumberColumnFilter', sortable: true, cellStyle: { 'text-align': 'center' } },
            { headerName: 'OCUPADAS', field: 'OCUPA', suppressMenu: true, filter: 'agNumberColumnFilter', sortable: true, cellStyle: { 'text-align': 'center' } },
            { headerName: 'PERMISOS', field: 'PERMI', suppressMenu: true, filter: 'agNumberColumnFilter', sortable: true, cellStyle: { 'text-align': 'center' } },
            { headerName: 'VACANTES', field: 'VACAN', suppressMenu: true, filter: 'agNumberColumnFilter', sortable: true, cellStyle: { 'text-align': 'center' } }
        ];

        this.tpOptions.alignedGrids.push(this.bpOptions);
        this.bpOptions.alignedGrids.push(this.tpOptions);

        this.columnDefsE = [
            { headerName: 'PUESTO', field: 'DESCR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
            { headerName: 'NIVEL', field: 'NIVEL', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
            { headerName: 'PUESTO', field: 'DESC2', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
            { headerName: 'SALARIO', field: 'SMEIN', suppressMenu: true, filter: 'agNumberColumnFilter', sortable: true, cellStyle: { 'text-align': 'right' }, cellRendererFramework: currencyGrid },
            { headerName: 'AUTORIZADAS', field: 'AUTOR', suppressMenu: true, filter: 'agNumberColumnFilter', sortable: true, cellStyle: { 'text-align': 'center' } },
            { headerName: 'OCUPADAS', field: 'OCUPA', suppressMenu: true, filter: 'agNumberColumnFilter', sortable: true, cellStyle: { 'text-align': 'center' } },
            { headerName: 'VACANTES', field: 'VACAN', suppressMenu: true, filter: 'agNumberColumnFilter', sortable: true, cellStyle: { 'text-align': 'center' } }
        ];

        this.teOptions.alignedGrids.push(this.beOptions);
        this.beOptions.alignedGrids.push(this.teOptions);

        this.columnDefsH = [
            { headerName: 'CLAVE DEL PUESTO', field: 'PUEST', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
            { headerName: 'NIVEL', field: 'NIVEL', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
            { headerName: 'PUESTO', field: 'DESCR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
            { headerName: 'SALARIO', field: 'SMEIN', suppressMenu: true, filter: 'agNumberColumnFilter', sortable: true, cellStyle: { 'text-align': 'right' }, cellRendererFramework: currencyGrid },
            { headerName: 'AUTORIZADAS', field: 'AUTOR', suppressMenu: true, filter: 'agNumberColumnFilter', sortable: true, cellStyle: { 'text-align': 'center' } },
            { headerName: 'OCUPADAS', field: 'OCUPA', suppressMenu: true, filter: 'agNumberColumnFilter', sortable: true, cellStyle: { 'text-align': 'center' } },
            { headerName: 'VACANTES', field: 'VACAN', suppressMenu: true, filter: 'agNumberColumnFilter', sortable: true, cellStyle: { 'text-align': 'center' } }
        ];

        this.thOptions.alignedGrids.push(this.bhOptions);
        this.bhOptions.alignedGrids.push(this.thOptions);

        this.columnDefsR = [
            { headerName: 'CONCEPTO PLAZAS', field: 'DESCR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
            { headerName: 'OCUPADAS', field: 'OCUPA', suppressMenu: true, filter: 'agNumberColumnFilter', sortable: true, cellStyle: { 'text-align': 'center' } },
            { headerName: 'VACANTES', field: 'VACAN', suppressMenu: true, filter: 'agNumberColumnFilter', sortable: true, cellStyle: { 'text-align': 'center' } },
            { headerName: 'PERMISOS', field: 'PERMI', suppressMenu: true, filter: 'agNumberColumnFilter', sortable: true, cellStyle: { 'text-align': 'center' } },
            { headerName: 'AUTORIZADAS', field: 'AUTOR', suppressMenu: true, filter: 'agNumberColumnFilter', sortable: true, cellStyle: { 'text-align': 'center' } },
            { headerName: 'FEMENINO', field: 'OCUFM', suppressMenu: true, filter: 'agNumberColumnFilter', sortable: true, cellStyle: { 'text-align': 'center' } },
            { headerName: 'MASCULINO', field: 'OCUMS', suppressMenu: true, filter: 'agNumberColumnFilter', sortable: true, cellStyle: { 'text-align': 'center' } },
            { headerName: 'BASE SINDICALIZADOS', field: 'SINBS', suppressMenu: true, filter: 'agNumberColumnFilter', sortable: true, cellStyle: { 'text-align': 'center' } },
            { headerName: 'CONFIANZA SINDICALIZADOS', field: 'SINCN', suppressMenu: true, filter: 'agNumberColumnFilter', sortable: true, cellStyle: { 'text-align': 'center' } }
        ];

        this.trOptions.alignedGrids.push(this.brOptions);
        this.brOptions.alignedGrids.push(this.trOptions);
    }

    ngOnInit() {
        this.wbService.getReportData('DRH-REP-PLP', '', '').subscribe(response => {
            this.rowDataP = response.data;

            let aut = 0,
                ocu = 0,
                per = 0,
                vac = 0;

            this.rowDataP.forEach(element => {
                aut += element.AUTOR;
                ocu += element.OCUPA;
                per += element.PERMI;
                vac += element.VACAN;
            })

            this.footerDataP = [
                {
                    DESCR: 'Total',
                    PUEST: '',
                    AUTOR: aut,
                    OCUPA: ocu,
                    PERMI: per,
                    VACAN: vac
                }
            ];
        })

        this.wbService.getReportData('DRH-REP-PLE', '', '').subscribe(response => {
            this.rowDataE = response.data;

            let aut = 0,
                ocu = 0,
                vac = 0;

            this.rowDataE.forEach(element => {
                aut += element.AUTOR;
                ocu += element.OCUPA;
                vac += element.VACAN;
            })

            this.footerDataE = [
                {
                    DESCR: 'Total',
                    NIVEL: '',
                    DESC2: '',
                    SMEIN: '',
                    AUTOR: aut,
                    OCUPA: ocu,
                    VACAN: vac
                }
            ];
        })

        this.wbService.getReportData('DRH-REP-PLH', '', '').subscribe(response => {
            this.rowDataH = response.data;

            let aut = 0,
                ocu = 0,
                vac = 0;

            this.rowDataH.forEach(element => {
                aut += element.AUTOR;
                ocu += element.OCUPA;
                vac += element.VACAN;
            })

            this.footerDataH = [
                {
                    PUEST: 'Total',
                    NIVEL: '',
                    DESCR: '',
                    SMEIN: '',
                    AUTOR: aut,
                    OCUPA: ocu,
                    VACAN: vac
                }
            ];
        })

        this.wbService.getReportData('DRH-REP-PLA', '', '').subscribe(response => {
            this.rowDataR = response.data;

            let ocu = 0,
                vac = 0,
                per = 0,
                aut = 0,
                fem = 0,
                mas = 0,
                bsn = 0,
                csn = 0;

            this.rowDataR.forEach(element => {
                ocu += element.OCUPA;
                vac += element.VACAN;
                per += element.PERMI;
                aut += element.AUTOR;
                fem += element.OCUFM;
                mas += element.OCUMS;
                bsn += element.SINBS;
                csn += element.SINCN;
            })

            this.footerDataR = [
                {
                    DESCR: 'Total',
                    OCUPA: ocu,
                    VACAN: vac,
                    PERMI: per,
                    AUTOR: aut,
                    OCUFM: fem,
                    OCUMS: mas,
                    SINBS: bsn,
                    SINCN: csn
                }
            ];
        })
    }

    onGridReadyP(params) {
        this.gridApiP = params;
        console.clear();
    }

    filterChangedP(event) {
        let aut = 0,
            ocu = 0,
            per = 0,
            vac = 0;

        this.gridApiP.api.forEachNodeAfterFilter(element => {
            if (element.data) {
                aut += element.data.AUTOR;
                ocu += element.data.OCUPA;
                per += element.data.PERMI;
                vac += element.data.VACAN;
            }
        });

        this.footerDataP = [
            {
                DESCR: 'Total',
                PUEST: '',
                AUTOR: aut,
                OCUPA: ocu,
                PERMI: per,
                VACAN: vac
            }
        ];
    }

    onGridReadyE(params) {
        this.gridApiE = params;
        console.clear();
    }

    filterChangedE(event) {
        let aut = 0,
            ocu = 0,
            vac = 0;

        this.gridApiE.api.forEachNodeAfterFilter(element => {
            aut += element.data.AUTOR;
            ocu += element.data.OCUPA;
            vac += element.data.VACAN;
        })

        this.footerDataE = [
            {
                DESCR: 'Total',
                NIVEL: '',
                DESC2: '',
                SMEIN: '',
                AUTOR: aut,
                OCUPA: ocu,
                VACAN: vac
            }
        ];
    }

    onGridReadyH(params) {
        this.gridApiH = params;
        console.clear();
    }

    filterChangedH(event) {
        let aut = 0,
            ocu = 0,
            vac = 0;

        this.gridApiH.api.forEachNodeAfterFilter(element => {
            aut += element.data.AUTOR;
            ocu += element.data.OCUPA;
            vac += element.data.VACAN;
        })

        this.footerDataH = [
            {
                PUEST: 'Total',
                NIVEL: '',
                DESCR: '',
                SMEIN: '',
                AUTOR: aut,
                OCUPA: ocu,
                VACAN: vac
            }
        ];
    }

    onGridReadyR(params) {
        this.gridApiR = params;
        console.clear();
    }

    filterChangedR(event) {
        let ocu = 0,
            vac = 0,
            per = 0,
            aut = 0,
            fem = 0,
            mas = 0,
            bsn = 0,
            csn = 0;

        this.gridApiR.api.forEachNodeAfterFilter(element => {
            ocu += element.data.OCUPA;
            vac += element.data.VACAN;
            per += element.data.PERMI;
            aut += element.data.AUTOR;
            fem += element.data.OCUFM;
            mas += element.data.OCUMS;
            bsn += element.data.SINBS;
            csn += element.data.SINCN;
        })

        this.footerDataR = [
            {
                DESCR: 'Total',
                OCUPA: ocu,
                VACAN: vac,
                PERMI: per,
                AUTOR: aut,
                OCUFM: fem,
                OCUMS: mas,
                SINBS: bsn,
                SINCN: csn
            }
        ];
    }
}