import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef, Inject } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import { AuthService } from '../../../../services/auth.service';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';

import { DialogRecover } from '../../../../custom/dialogs/recover/recover.component';
import { DialogConfirm } from '../../../../custom/dialogs/confirm/confirm.component'

import { F_DRM_FRM_REQ, names } from '../../../../models/forms/DRM/FRM/REQ/drm.frm.req.model';
import { CustomErrorStateMatcher } from '../../../../models/global/error';

@Component({
  selector: 'drm_req',
  templateUrl: './req.component.html',
  styleUrls: ['./req.component.scss'],
  providers: [
    { provide: MAT_DATE_LOCALE, useValue: 'es-MX' },
    { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
    { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
  ]
})
export class DRMREQComponent implements OnInit, OnDestroy {
  @ViewChild('groupForm', { static: false }) groupF: NgForm;

  public format: string;
  public error = new CustomErrorStateMatcher();

  public f_drm_frm_req: F_DRM_FRM_REQ = new F_DRM_FRM_REQ();
  public names: names = new names();

  public tiposRequisicion: Array<any>;
  public regiones: Array<any>;
  public adscripciones: Array<any>;

  public partidas: Array<any>;
  public filteredP: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
  public PFilterCtrl: FormControl = new FormControl();

  public tiposContratacion: Array<any>;

  public ctrped: Array<any>;
  public filteredCP: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
  public CPFilterCtrl: FormControl = new FormControl();

  public funcionarios: Array<any>;
  public filteredAU: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
  public AUFilterCtrl: FormControl = new FormControl();
  public filteredVB: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
  public VBFilterCtrl: FormControl = new FormControl();

  public perVB: boolean = false;
  public countVB: Number = 0;
  public perAU: boolean = false;
  public countAU: Number = 0;

  protected _onDestroy = new Subject<void>();

  // Private
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   *
   */
  constructor(public authService: AuthService,
    private wbService: WBService,
    private globalService: GlobalService,
    public dialog: MatDialog,
    private cdr: ChangeDetectorRef,
    private _fuseSplashScreenService: FuseSplashScreenService) {

    this.format = 'DRM-FRM-REQ';
    this.f_drm_frm_req.STTUS = '1',
      this.f_drm_frm_req.EJERC = sessionStorage.getItem('year'),
      this.f_drm_frm_req.USUAR = this.authService.user.user_name,
      this.f_drm_frm_req.REGIO = this.authService.user.office;

    this.wbService.getInformation('10', 'PUEST=¯' + this.authService.user.job + '¯').subscribe(response => {
      if (response.data != null) {
        this.names.ELPUE = response.data[0].DESCR;
      }
    })

    // Set the private defaults
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  ngAfterContentChecked() {
    this.cdr.detectChanges();
  }

  ngOnInit() {
    this.getPermission();

    this.wbService.getInformation('1', '').subscribe(response => {
      this.f_drm_frm_req.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
    })

    this.wbService.getInformation('12', '').subscribe(response => {
      this.tiposRequisicion = response.data;
    })

    this.wbService.getInformation('3', 'REGIO=¯' + this.authService.user.office + '¯').subscribe(response => {
      this.regiones = response.data;
    })

    this.wbService.getInformation('4', '').subscribe(response => {
      this.adscripciones = response.data;
    })

    this.wbService.getInformation('9', '').subscribe(response => {
      this.partidas = response.data;
      this.filteredP.next(this.partidas)
    })

    this.PFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterP();
      })

    this.wbService.getInformation('13', '').subscribe(response => {
      this.tiposContratacion = response.data;
    })

    this.wbService.getInformation('14', '').subscribe(response => {
      this.ctrped = response.data;
      this.filteredCP.next(this.ctrped)
    })

    this.CPFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterCP();
      })

    this.wbService.getInformation('5', '').subscribe(response => {
      this.funcionarios = response.data;
      this.filteredVB.next(this.funcionarios)
      this.filteredAU.next(this.funcionarios)
    })

    this.AUFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterAU();
      })

    this.VBFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterVB();
      })
  }

  ngOnDestroy() {
    this._onDestroy.next();
    this._onDestroy.complete();
  }

  getPermission() {
    this.wbService.getInformation('0', 'TYPPE=¯permission¯ AND PAREN=¯' + this.format + '¯', '¯' + this.authService.user.user_name + '¯').subscribe(response => {
      if (response.data) {
        response.data.forEach(element => {
          if (element.PERMI == 1) {
            this.perVB = element.ACCES;
          } else if (element.PERMI == 2) {
            this.perAU = element.ACCES;
          }
        });

        if (this.perVB) {
          this.wbService.getCount('15', 'EJERC=' + sessionStorage.getItem('year') + 'AND STTUS=¯2¯ AND VBSPU=¯' + this.authService.user.user_name + '¯').subscribe(response => {
            if (response.count) {
              this.countVB = response.count;
            } else {
              this.countVB = 0;
            }
          })
        } else {
          this.countVB = 0;
        }

        if (this.perAU) {
          this.wbService.getCount('15', 'EJERC=' + sessionStorage.getItem('year') + 'AND STTUS=¯3¯ AND AUTOU=¯' + this.authService.user.user_name + '¯').subscribe(response => {
            if (response.count) {
              this.countAU = response.count;
            } else {
              this.countAU = 0;
            }
          })
        } else {
          this.countAU = 0;
        }
      }
    })
  }

  clear(type) {
    let folio = this.f_drm_frm_req.FOLIO,
        fecha = this.f_drm_frm_req.FECHA;

    this.f_drm_frm_req = new F_DRM_FRM_REQ();
    this.names = new names();

    if (type == 'C') {
        this.f_drm_frm_req.FOLIO = folio;

        if (folio.length == 30) {
            this.wbService.getInformation('1', '').subscribe(response => {
                this.f_drm_frm_req.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
            })
        } else {
            this.f_drm_frm_req.FECHA = fecha;
        }
    } else {
        this.wbService.getInformation('1', '').subscribe(response => {
            this.f_drm_frm_req.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
        })
    }

    this.f_drm_frm_req.STTUS = '1',
    this.f_drm_frm_req.EJERC = sessionStorage.getItem('year'),
    this.f_drm_frm_req.USUAR = this.authService.user.user_name,
    this.f_drm_frm_req.REGIO = this.authService.user.office;
    this.puesto(this.f_drm_frm_req.USUAR, 'ELPUE');
}

  openDialog(option, type) {
    if (option == 'R') {
        const dialogRef = this.dialog.open(DialogRecover, {
            width: '1000px',
            height: '650px',
            data: { type: type, format: this.format }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result && result.FOLIO) {
                this.getInformation(result);
            }
        });
    } else {
        const dialogRef = this.dialog.open(DialogConfirm, {
            width: '1000px',
            height: '420px',
            data: { option: option, response: type }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result && result.option) {
                this.sendInformation(result);
            }
        });
    }
}

  getInformation(data) {
    this.wbService.getInformationForms(this.format, 'FOLIO=¯' + data.FOLIO + '¯').subscribe(response => {
      let rec_drm_frm_req = response.F_DRM_FRM_REQ;

      if (rec_drm_frm_req) {
        this.f_drm_frm_req.FOLIO = rec_drm_frm_req[0].FOLIO,
          this.f_drm_frm_req.STTUS = rec_drm_frm_req[0].STTUS,
          this.f_drm_frm_req.EJERC = rec_drm_frm_req[0].EJERC,
          this.f_drm_frm_req.USUAR = rec_drm_frm_req[0].USUAR,
          this.f_drm_frm_req.KEYUI = rec_drm_frm_req[0].KEYUI,
          this.f_drm_frm_req.FECHA = rec_drm_frm_req[0].FOLIO.length != 12 ? this.f_drm_frm_req.FECHA : this.globalService.getDateUTC(rec_drm_frm_req[0].FECHA),
          this.f_drm_frm_req.TPDRQ = rec_drm_frm_req[0].TPDRQ,
          this.f_drm_frm_req.REGIO = rec_drm_frm_req[0].REGIO,
          this.f_drm_frm_req.ADSCR = rec_drm_frm_req[0].ADSCR,
          this.f_drm_frm_req.TITUL = rec_drm_frm_req[0].TITUL,
          this.f_drm_frm_req.PRTSP = rec_drm_frm_req[0].PRTSP,
          this.f_drm_frm_req.TPDCT = rec_drm_frm_req[0].TPDCT,
          this.f_drm_frm_req.CTRPD = rec_drm_frm_req[0].CTRPD,
          this.f_drm_frm_req.DESCR = rec_drm_frm_req[0].DESCR,
          this.f_drm_frm_req.JUSTI = rec_drm_frm_req[0].JUSTI,
          this.f_drm_frm_req.CSEST = rec_drm_frm_req[0].CSEST,
          this.f_drm_frm_req.IIVAA = rec_drm_frm_req[0].IIVAA,
          this.f_drm_frm_req.DEREC = rec_drm_frm_req[0].DEREC,
          this.f_drm_frm_req.OTIMP = rec_drm_frm_req[0].OTIMP,
          this.f_drm_frm_req.TOTAL = rec_drm_frm_req[0].TOTAL,
          this.f_drm_frm_req.ENERO = rec_drm_frm_req[0].ENERO,
          this.f_drm_frm_req.FEBRE = rec_drm_frm_req[0].FEBRE,
          this.f_drm_frm_req.MARZO = rec_drm_frm_req[0].MARZO,
          this.f_drm_frm_req.ABRIL = rec_drm_frm_req[0].ABRIL,
          this.f_drm_frm_req.MAYOO = rec_drm_frm_req[0].MAYOO,
          this.f_drm_frm_req.JUNIO = rec_drm_frm_req[0].JUNIO,
          this.f_drm_frm_req.JULIO = rec_drm_frm_req[0].JULIO,
          this.f_drm_frm_req.AGOST = rec_drm_frm_req[0].AGOST,
          this.f_drm_frm_req.SEPTI = rec_drm_frm_req[0].SEPTI,
          this.f_drm_frm_req.OCTUB = rec_drm_frm_req[0].OCTUB,
          this.f_drm_frm_req.NOVIE = rec_drm_frm_req[0].NOVIE,
          this.f_drm_frm_req.DICIE = rec_drm_frm_req[0].DICIE,
          this.f_drm_frm_req.ANUAL = rec_drm_frm_req[0].ANUAL,
          this.f_drm_frm_req.VBSPU = rec_drm_frm_req[0].VBSPU,
          this.f_drm_frm_req.VBSPF = rec_drm_frm_req[0].STTUS!='1' ? this.globalService.getDateUTC(rec_drm_frm_req[0].VBSPF) : null,
          this.f_drm_frm_req.VBSPR = rec_drm_frm_req[0].VBSPR,
          this.f_drm_frm_req.VBSPC = rec_drm_frm_req[0].VBSPC,
          this.f_drm_frm_req.VBSPK = rec_drm_frm_req[0].STTUS!='1' ? rec_drm_frm_req[0].VBSPK:'',
          this.f_drm_frm_req.AUTOU = rec_drm_frm_req[0].AUTOU,
          this.f_drm_frm_req.AUTOF = rec_drm_frm_req[0].STTUS!='1' ? this.globalService.getDateUTC(rec_drm_frm_req[0].AUTOF) : null,
          this.f_drm_frm_req.AUTOR = rec_drm_frm_req[0].AUTOR,
          this.f_drm_frm_req.AUTOC = rec_drm_frm_req[0].AUTOC,
          this.f_drm_frm_req.AUTOK = rec_drm_frm_req[0].STTUS!='1' ? rec_drm_frm_req[0].AUTOK:'';

        this.puesto(this.f_drm_frm_req.USUAR, 'ELPUE');
        this.puesto(this.f_drm_frm_req.AUTOU, 'AUPUE');
        this.puesto(this.f_drm_frm_req.VBSPU, 'VSPUE');
      } else {
        this.globalService.messageSwal('Información no encontrada', 'La información con el folio ' + data.FOLIO + ' no se encontro en el servidor', 'error');
      }
    })
  }

  sendInformation(info) {
    let information,
      message;

    if (info.option == 'V') {
      information = [{ VBSPF: '|current_timestamp|', VBSPR: info.response, VBSPC: info.commit, VBSPK: '|UUID|', STTUS: info.status }]

      if (info.status == '1') {
        message = 'El folio: ' + this.f_drm_frm_req.FOLIO + ' fue habilitado para su edición';
      } else if (info.status == '3') {
        message = 'Información enviada para Autorización con el folio: ' + this.f_drm_frm_req.FOLIO;
      } else if (info.status == '6') {
        message = 'El folio: ' + this.f_drm_frm_req.FOLIO + ' fue cancelado';
      }

      this.wbService.updateInformationForms(this.format, '1', 'FOLIO=¯' + this.f_drm_frm_req.FOLIO + '¯', JSON.stringify(information)).subscribe(
        response => {
          if (response.message) {
            this.globalService.messageSwal('Información Almacenada', message, 'success');
            this.f_drm_frm_req.STTUS = info.status,
            this.f_drm_frm_req.VBSPR = info.response,
            this.f_drm_frm_req.VBSPC = info.commit;
            this.getPermission();

            if (info.status == '3') {
              this.createFile('OK');
            }
          }
        })
    } else if (info.option == 'A') {
      information = [{ AUTOF: '|current_timestamp|', AUTOR: info.response, AUTOC: info.commit, AUTOK: '|UUID|', STTUS: info.status }]

      if (info.status == '1') {
        message = 'El folio: ' + this.f_drm_frm_req.FOLIO + ' fue habilitado para su edición';
      } else if (info.status == '4') {
        message = 'Información Autorizada con el folio: ' + this.f_drm_frm_req.FOLIO;
      } else if (info.status == '6') {
        message = 'El folio: ' + this.f_drm_frm_req.FOLIO + ' fue cancelado';
      }

      this.wbService.updateInformationForms(this.format, '1', 'FOLIO=¯' + this.f_drm_frm_req.FOLIO + '¯', JSON.stringify(information)).subscribe(
        response => {
          if (response.message) {
            this.globalService.messageSwal('Información Almacenada', message, 'success');
            this.f_drm_frm_req.STTUS = info.status,
            this.f_drm_frm_req.AUTOR = info.response,
            this.f_drm_frm_req.AUTOC = info.commit;
            this.getPermission();

            if (info.status == '4') {
              this.createFile('OK');
            }
          }
        })
    }
  }

  protected filterP() {
    if (!this.partidas) {
      return;
    }

    let search = this.PFilterCtrl.value;
    if (!search) {
      this.filteredP.next(this.partidas.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredP.next(
      this.partidas.filter(partida => partida.DESCR.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterCP() {
    if (!this.ctrped) {
      return;
    }

    let search = this.CPFilterCtrl.value;
    if (!search) {
      this.filteredCP.next(this.ctrped.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredCP.next(
      this.ctrped.filter(cp => cp.DESCR.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterVB() {
    if (!this.funcionarios) {
      return;
    }

    let search = this.VBFilterCtrl.value;
    if (!search) {
      this.filteredVB.next(this.funcionarios.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredVB.next(
      this.funcionarios.filter(funcionario => funcionario.DESCR.toLowerCase().indexOf(search) > -1)
    );
  }

  protected filterAU() {
    if (!this.funcionarios) {
      return;
    }

    let search = this.AUFilterCtrl.value;
    if (!search) {
      this.filteredAU.next(this.funcionarios.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredAU.next(
      this.funcionarios.filter(funcionario => funcionario.DESCR.toLowerCase().indexOf(search) > -1)
    );
  }

  titular(adscr: string) {
    let ads = this.adscripciones.find(adscripcion => adscripcion.ADSCR === adscr);

    this.f_drm_frm_req.TITUL = ads.TITUL;
  }

  puesto(func: string, puest: string) {
    let res = this.funcionarios.find(funcionario => funcionario.CUSER === func);

    this.names[puest] = res.DSCPU;
  }

  costo() {
    this.f_drm_frm_req.IIVAA = this.f_drm_frm_req.CSEST * 0.16;
    this.f_drm_frm_req.TOTAL = this.f_drm_frm_req.CSEST +
      this.f_drm_frm_req.IIVAA +
      this.f_drm_frm_req.DEREC +
      this.f_drm_frm_req.OTIMP;
  }

  anual() {
    this.f_drm_frm_req.ANUAL = this.f_drm_frm_req.ENERO +
      this.f_drm_frm_req.FEBRE +
      this.f_drm_frm_req.MARZO +
      this.f_drm_frm_req.ABRIL +
      this.f_drm_frm_req.MAYOO +
      this.f_drm_frm_req.JUNIO +
      this.f_drm_frm_req.JULIO +
      this.f_drm_frm_req.AGOST +
      this.f_drm_frm_req.SEPTI +
      this.f_drm_frm_req.OCTUB +
      this.f_drm_frm_req.NOVIE +
      this.f_drm_frm_req.DICIE;
  }

  saveInformation(type: string, option: string) {
    this._fuseSplashScreenService.show();

    let folio: string = this.f_drm_frm_req.FOLIO,
      sttus: string = this.f_drm_frm_req.STTUS;

    let information = { 'F_DRM_FRM_REQ': this.f_drm_frm_req };

    this.wbService.saveInformationForms(type, this.format, folio, sttus, JSON.stringify(information)).subscribe(
      response => {
        this.getPermission();

        this.f_drm_frm_req.FOLIO = response.FOLIO;

        if (type == 'CF') {
          this.globalService.messageSwal('Información Almacenada', 'Información enviada para Visto Bueno con el folio: ' + response.FOLIO, 'success');
        } else {
          if (!folio) {
            this.globalService.messageSwal('Información Almacenada', 'Tu folio temporal es: ' + response.FOLIO, 'success');
          } else {
            this.globalService.messageSwal('Información Actualizada', 'Tu información con el folio ' + response.FOLIO + ' ha sido guarda', 'success');
          }
        }

        if (option == 'VP' || option == 'OK') {
          this.createFile(option);
        }

        if (type == 'CF') {
          this.f_drm_frm_req.STTUS = '2';
        }

        this._fuseSplashScreenService.hide();
      },
      error => {
        this._fuseSplashScreenService.hide();
        this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
        console.log(error);
      })
  }

  createFile(option: string) {
    let folio: string = this.f_drm_frm_req.FOLIO;

    this.wbService.createFile(option, this.format, folio).subscribe(x => {
      if (option == 'VP') {
        var newBlob = new Blob([x], { type: 'application/pdf' });

        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
          window.navigator.msSaveOrOpenBlob(newBlob);
          return;
        }

        const data = window.URL.createObjectURL(newBlob);

        var link = document.createElement('a');
        link.href = data;
        link.download = folio + '-' + option + '.pdf';

        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

        setTimeout(function () {
          window.URL.revokeObjectURL(data);
          link.remove();
        }, 100);
      }
    });
  }

  downloadFile(option: string) {
    let folio: string = this.f_drm_frm_req.FOLIO;

    this.wbService.downloadFile(option, this.format, folio).subscribe(x => {

      var newBlob = new Blob([x], { type: 'application/pdf' });

      if (window.navigator && window.navigator.msSaveOrOpenBlob) {
        window.navigator.msSaveOrOpenBlob(newBlob);
        return;
      }

      const data = window.URL.createObjectURL(newBlob);

      var link = document.createElement('a');
      link.href = data;
      link.download = folio + '-' + option + '.pdf';

      link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

      setTimeout(function () {
        window.URL.revokeObjectURL(data);
        link.remove();
      }, 100);
    });
  }

  valGnr(item, column) {
    if (this.groupF && this.groupF.controls[column]) {
      if (column == 'CTRPD') {
        if ((this.f_drm_frm_req.TPDCT == 'E' || this.f_drm_frm_req.TPDCT == 'M') && !item) {
          this.groupF.controls[column].setErrors({ 'cp': true });
          return true;
        } else {
          this.groupF.controls[column].setErrors(null);
          return false;
        }
      } else if (column == 'CSEST') {
        if (item <= 0) {
          this.groupF.controls[column].setErrors({ 'importe': true });
          return true;
        } else {
          this.groupF.controls[column].setErrors(null);
          return false;
        }
      } else if (column == 'TOTAL') {
        if (item != this.f_drm_frm_req.ANUAL) {
          this.groupF.controls[column].setErrors({ 'diferencia': true });
          return true;
        } else {
          this.groupF.controls[column].setErrors(null);
          return false;
        }
      }
    } else {
      return false;
    }
  }

  disabled() {
    if (this.f_drm_frm_req.STTUS == '1' && this.f_drm_frm_req.USUAR == this.authService.user.user_name) {
      return false;
    } else {
      return true;
    }
  }

  disabledSave() {
    if (this.groupF && this.groupF.controls['TOTAL']) {
      return (this.groupF.form.invalid || this.groupF.controls['TOTAL'].errors);
    } else {
      return true;
    }
  }
}