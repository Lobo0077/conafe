import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRippleModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { NgxDnDModule } from '@swimlane/ngx-dnd';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { CurrencyMaskModule } from "ngx-currency-mask";
import { NgCircleProgressModule } from 'ng-circle-progress';

import { AgGridModule } from 'ag-grid-angular';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';

import { DRMREQComponent } from './req/req.component';
import { DRMACPComponent } from './acp/acp.component';
import { DRMMDVComponent } from './mdv/mdv.component';

import { RoleGuard } from '../../../guards/role.guard';

const routes: Routes = [
    {
        path:      'req',
        component: DRMREQComponent,
        canActivate: [RoleGuard],
        data:{role: 'FORMS:DRM-FRM-REQ'}
    },
    {
        path:      'acp',
        component: DRMACPComponent,
        canActivate: [RoleGuard],
        data:{role: 'FORMS:DRM-FRM-ACP'}
    },
    {
        path:      'mdv',
        component: DRMMDVComponent,
        canActivate: [RoleGuard],
        data:{role: 'FORMS:DRM-FRM-MDV'}
    }
];

@NgModule({
    entryComponents: [
    ],
    declarations: [
        DRMREQComponent,
        DRMACPComponent,
        DRMMDVComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatBadgeModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSelectModule,
        MatTableModule,
        MatCardModule,
        MatToolbarModule,
        MatTooltipModule,
        MatDialogModule,
        MatProgressBarModule,
        MatAutocompleteModule,
        
        NgxDnDModule,

        NgxMatSelectSearchModule,
        CurrencyMaskModule,
        NgCircleProgressModule.forRoot({}),

        FuseSharedModule,
        FuseSidebarModule,

        AgGridModule.withComponents([])
    ]
})
export class ForDRMModule
{
}
