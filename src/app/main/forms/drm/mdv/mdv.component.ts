import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
    selector     : 'drm_mdv',
    templateUrl  : './mdv.component.html',
    styleUrls  : ['./mdv.component.scss']
})
export class DRMMDVComponent implements OnInit
{
    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     */
    constructor(
    )
    {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
    }
}
