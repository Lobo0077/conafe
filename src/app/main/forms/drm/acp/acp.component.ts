import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ReplaySubject, Subject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { HttpEventType } from '@angular/common/http';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { map, startWith } from 'rxjs/operators';

import { AuthService } from '../../../../services/auth.service';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';

import { F_DRM_FRM_ACP, P_DRM_FRM_ACP } from '../../../../models/forms/DRM/FRM/ACP/drm.frm.acp.model';
import { CustomErrorStateMatcher } from '../../../../models/global/error';
import * as moment from 'moment';

@Component({
    selector     : 'drm_acp',
    templateUrl  : './acp.component.html',
    styleUrls  : ['./acp.component.scss'],
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es-MX' },
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
      ]
})
export class DRMACPComponent implements OnInit, OnDestroy {
    @ViewChild('groupForm', {static: false}) groupF: NgForm;
    formChangesSubscription: any;

    public format: string;
    public error = new CustomErrorStateMatcher();
    public save:number=0;
    
    public f_drm_frm_acp: F_DRM_FRM_ACP = new F_DRM_FRM_ACP();
    public p_drm_frm_acp: P_DRM_FRM_ACP = new P_DRM_FRM_ACP();

    public regiones: Array<any>;

    public requisiciones: Array<any>;
    public filteredR: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public RFilterCtrl: FormControl = new FormControl();

    public tiposContratacion: Array<any>;
    public tipoAdjudicacion: Array<any>;
    public categoriaContratacion: Array<any>;
    public medioContratacion: Array<any>;
    public partidas: Array<any>;

    public proveedores: Array<any>;
    public filteredP: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);

    protected _onDestroy = new Subject<void>();

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     */
    constructor(private authService: AuthService,
                private wbService: WBService,
                private globalService: GlobalService,
                public dialog: MatDialog,
                private cdr: ChangeDetectorRef,
                private _fuseSplashScreenService: FuseSplashScreenService) {

        this.format = 'DRM-FRM-ACP';
        this.f_drm_frm_acp.STTUS = '1',
        this.f_drm_frm_acp.EJERC = sessionStorage.getItem('year'),
        this.f_drm_frm_acp.USUAR = this.authService.user.user_name,
        this.f_drm_frm_acp.REGIO = this.authService.user.office;

        this.save=1;
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    ngAfterContentChecked() {
        this.cdr.detectChanges();
    }

    ngOnInit() {
        this.wbService.getInformation('1', '').subscribe(response => {
            this.f_drm_frm_acp.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
          })

        this.wbService.getInformation('3', 'REGIO=¯'+this.authService.user.office+'¯').subscribe(response => {
            this.regiones = response.data;
        })

        this.wbService.getInformation('18', 'EJERC=¯'+this.f_drm_frm_acp.EJERC+'¯ AND REGIO=¯'+this.f_drm_frm_acp.REGIO+'¯').subscribe(response => {
            this.requisiciones = response.data;
            this.filteredR.next(this.requisiciones)
        })

        this.RFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
            this.filterR();
        })

        this.wbService.getInformation('13', '').subscribe(response => {
            this.tiposContratacion = response.data;
        })

        this.wbService.getInformation('19', '').subscribe(response => {
            this.tipoAdjudicacion = response.data;
        })

        this.wbService.getInformation('20', '').subscribe(response => {
            this.categoriaContratacion = response.data;
        })

        this.wbService.getInformation('21', '').subscribe(response => {
            this.medioContratacion = response.data;
        })

        this.wbService.getInformation('9', '').subscribe(response => {
            this.partidas = response.data;
        })

        this.wbService.getInformation('22', '').subscribe(response => {
            this.proveedores = response.data;

            if(this.proveedores != null) {
                if(this.groupF && this.groupF.form){
                    this.groupF.form.valueChanges.subscribe(form => {
                        this.filteredP.next(
                            this.proveedores.filter(proveedor => proveedor.DESCR.toLowerCase().indexOf(form.RRFCC ? form.RRFCC.toLowerCase():'') > -1)
                        );
                    })
                
                }
            }
        })
    }

    ngOnDestroy() {
        this._onDestroy.next();
        this._onDestroy.complete();
    }

    nuevo() {
        this.f_drm_frm_acp = new F_DRM_FRM_ACP();

        this.save=1;

        this.f_drm_frm_acp.STTUS = '1',
        this.f_drm_frm_acp.EJERC = sessionStorage.getItem('year'),
        this.f_drm_frm_acp.USUAR = this.authService.user.user_name,
        this.f_drm_frm_acp.REGIO = this.authService.user.office;

        this.wbService.getInformation('1', '').subscribe(response => {
            this.f_drm_frm_acp.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
        })
    }

    openDialog() {
        /*const dialogRef = this.dialog.open(DRMACPDialogRecover, {
          width: '1000px',
          height: '650px'
        });
    
        dialogRef.afterClosed().subscribe(result => {
          if(result && result.length > 0) {
            this.getInformation(result);
          }
        });*/
    }
  
    getInformation(fol: string) {
        this.wbService.getInformationForms(this.format, 'CTRPD=¯'+fol+'¯').subscribe(response => {
          let rec_drm_frm_acp = response.F_DRM_FRM_ACP;

            if(rec_drm_frm_acp){
                this.save=2;

                this.f_drm_frm_acp.CTRPD=rec_drm_frm_acp[0].CTRPD,
                this.f_drm_frm_acp.STTUS=rec_drm_frm_acp[0].STTUS,
                this.f_drm_frm_acp.EJERC=rec_drm_frm_acp[0].EJERC,
                this.f_drm_frm_acp.USUAR=rec_drm_frm_acp[0].USUAR,
                this.f_drm_frm_acp.FECHA=this.globalService.getDateUTC(rec_drm_frm_acp[0].FECHA),
                this.f_drm_frm_acp.REGIO=rec_drm_frm_acp[0].REGIO,
                this.f_drm_frm_acp.REQUI=rec_drm_frm_acp[0].REQUI,
                this.f_drm_frm_acp.TPDCT=rec_drm_frm_acp[0].TPDCT,
                this.f_drm_frm_acp.CTPDM=rec_drm_frm_acp[0].CTPDM,
                this.f_drm_frm_acp.PRTSP=rec_drm_frm_acp[0].PRTSP,
                this.f_drm_frm_acp.TPDAD=rec_drm_frm_acp[0].TPDAD,
                this.f_drm_frm_acp.CTDCT=rec_drm_frm_acp[0].CTDCT,
                this.f_drm_frm_acp.MDDCT=rec_drm_frm_acp[0].MDDCT,
                this.f_drm_frm_acp.RRFCC=rec_drm_frm_acp[0].RRFCC,
                this.f_drm_frm_acp.RZSOC=rec_drm_frm_acp[0].RZSOC,
                this.f_drm_frm_acp.FCINI=this.globalService.getDateUTC(rec_drm_frm_acp[0].FCINI),
                this.f_drm_frm_acp.FCTER=this.globalService.getDateUTC(rec_drm_frm_acp[0].FCTER),
                this.f_drm_frm_acp.TEMPO=rec_drm_frm_acp[0].TEMPO,
                this.f_drm_frm_acp.MNMIN=rec_drm_frm_acp[0].MNMIN,
                this.f_drm_frm_acp.MNMAX=rec_drm_frm_acp[0].MNMAX,
                this.f_drm_frm_acp.CTZC1=rec_drm_frm_acp[0].CTZC1,
                this.f_drm_frm_acp.CTZC2=rec_drm_frm_acp[0].CTZC2,
                this.f_drm_frm_acp.CTZC3=rec_drm_frm_acp[0].CTZC3,
                this.f_drm_frm_acp.DESCR=rec_drm_frm_acp[0].DESCR;

                this.p_drm_frm_acp.CTZC1=rec_drm_frm_acp[0].CTZC1 ? 100:0;
                this.p_drm_frm_acp.CTZC2=rec_drm_frm_acp[0].CTZC2 ? 100:0;
                this.p_drm_frm_acp.CTZC3=rec_drm_frm_acp[0].CTZC3 ? 100:0;

                this.valCtrpd(this.f_drm_frm_acp.CTRPD);
            } else{
                this.globalService.messageSwal('Información no encontrada', 'La información con el folio '+fol+ ' no se encontro en el servidor', 'error');
            }
        })
      }

    protected filterR() {
        if (!this.requisiciones) {
            return;
        }
        
        let search = this.RFilterCtrl.value;
        if (!search) {
            this.filteredR.next(this.requisiciones.slice());
            return;
        } else {
            search = search.toLowerCase();
        }
        
        this.filteredR.next(
            this.requisiciones.filter(requisicion => requisicion.DESCR.toLowerCase().indexOf(search) > -1)
        );
    }

    infoReq(req: string) {
        let res = this.requisiciones.find( requisicion => requisicion.FOLIO === req );

        this.f_drm_frm_acp.TPDCT = res.TPDCT,
        this.f_drm_frm_acp.CTPDM = res.CTRPD,
        this.f_drm_frm_acp.PRTSP = res.PRTSP;

        this.diffYear();

        this.wbService.getInformation('14', 'STTUS IN (2,3,4) AND CTRPD=¯'+this.f_drm_frm_acp.CTPDM+'¯').subscribe(response => {
            if(response.data != null) {
                this.f_drm_frm_acp.TPDAD=response.data[0].TPDAD,
                this.f_drm_frm_acp.CTDCT=response.data[0].CTDCT,
                this.f_drm_frm_acp.MDDCT=response.data[0].MDDCT,
                this.f_drm_frm_acp.RRFCC=response.data[0].RRFCC,
                this.f_drm_frm_acp.RZSOC=response.data[0].RZSOC,
                this.f_drm_frm_acp.TEMPO=response.data[0].TEMPO;
            }
        })
    }

    nombPro(rfc: string) {
        let res = this.proveedores.find( proveedor => proveedor.RRFCC === rfc );

        this.f_drm_frm_acp.RZSOC = res.RZSOC;
    }

    diffYear() {
        if(this.f_drm_frm_acp.TPDCT != 'M') {
            if(this.f_drm_frm_acp.FCINI && this.f_drm_frm_acp.FCTER) {
                if (moment(this.f_drm_frm_acp.FCINI).year() == moment(this.f_drm_frm_acp.FCTER).year()) {
                    this.f_drm_frm_acp.TEMPO = 'Anual';
                } else {
                    this.f_drm_frm_acp.TEMPO = 'Plurianual';
                }
            } else {
                this.f_drm_frm_acp.TEMPO = '';
            }
        } 
    }

    openInput(fieldName: string) {
        if(this.f_drm_frm_acp.STTUS == '1') {
          if(this.save==2 && this.f_drm_frm_acp.CTRPD) {
            document.getElementById(fieldName).click();
          } else {
            this.saveInformation('SI', fieldName);
          }
        }
    }
  
      fileChange(files: File[], field: string) {
        let folio: string = this.f_drm_frm_acp.CTRPD;
        this.p_drm_frm_acp[field] = 0;
    
        if (files.length > 0) {
          if(files[0].name.toLowerCase().endsWith('.pdf')) {
            this.f_drm_frm_acp[field] = files[0].name;
      
            this.wbService.uploadFile(files[0], this.format, folio).subscribe(
              event => {
                if(event.type === HttpEventType.UploadProgress) {
                  this.p_drm_frm_acp[field] = Math.round((event.loaded/event.total)*100);
                } else if(event.type === HttpEventType.Response) {
                  let response:any = event.body;
                  this.globalService.messageSwal('Subida Archivo', 'Tu archivo se ha subido con éxito: ' + files[0].name, 'success');
                }
              },
              error => {
                if(error.error){
                  if(error.error.file == 'prohibitus'){
                    this.f_drm_frm_acp[field] = error.error.file;
                    this.globalService.messageSwal('Archivo Bloqueado', error.error.message, 'error');
                  }else{
                    this.globalService.messageSwal('Error', error.error.message, 'error');
                  }
                } else{
                  console.log(error);
                }
              })
          } else {
            this.f_drm_frm_acp[field] = '';
            this.globalService.messageSwal('Extensión Invalida', 'Archivo adjunto no se pudo cargar, solo se permiten archivos pdf', 'warning');
          }
        } else {
          this.f_drm_frm_acp[field] = '';
        }
    }

    showFile(fieldName: string) {
        let folio: string = this.f_drm_frm_acp.CTRPD;
    
        this.wbService.showFile(this.f_drm_frm_acp[fieldName] ,this.format, folio, 'application/pdf').subscribe(x => {
          var newBlob = new Blob([x], { type: 'application/pdf' });
    
          if (window.navigator && window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveOrOpenBlob(newBlob);
              return;
          }
    
          const data = window.URL.createObjectURL(newBlob);
    
          window.open(data,'_blank');
        });
    }

    saveInformation(type: string, option: string) {
        this._fuseSplashScreenService.show();
  
        let folio: string = this.f_drm_frm_acp.CTRPD,
            sttus: string = this.f_drm_frm_acp.STTUS;
    
        let information = { 'F_DRM_FRM_ACP': this.f_drm_frm_acp} ;
    
        this.wbService.saveInformationForms(type, this.format, folio, sttus, JSON.stringify(information)).subscribe(
          response => {    
            this._fuseSplashScreenService.hide();
            this.save=2;

            this.globalService.messageSwal('Información Almacenada', 'Tu contrato ' + response.FOLIO + ' ha sido almacenado', 'success');

            if(type=='CL') {
                this.wbService.getInformation('18', 'EJERC=¯'+this.f_drm_frm_acp.EJERC+'¯ AND REGIO=¯'+this.f_drm_frm_acp.REGIO+'¯').subscribe(response => {
                    this.requisiciones = response.data;
                    this.filteredR.next(this.requisiciones)
                })

                this.f_drm_frm_acp.STTUS = '4';
            }

            if(option.length >= 5) {
                document.getElementById(option).click();
            }
          },
          error => {
            this._fuseSplashScreenService.hide();
            this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
            console.log(error);
          })
    }

    valCtrpd(ctrpd: string) {
        if(ctrpd && this.f_drm_frm_acp.STTUS == '1') {
            this.f_drm_frm_acp.CTRPD=this.f_drm_frm_acp.CTRPD.replace(/\//g,' ').replace(/\\/g,' ').replace(/\|/g,' ');
            ctrpd = this.f_drm_frm_acp.CTRPD;

            this.wbService.getInformation('14', 'STTUS IN (2,3,4) AND CTRPD=¯'+ctrpd+'¯').subscribe(response => {
                if(response.data != null){
                    this.groupF.controls['CTRPD'].setErrors({'exists': true});
                } else {
                    this.groupF.controls['CTRPD'].setErrors(null);
                }
            })
        }
    }

    valGnr(item, column) {
        if(this.groupF && this.groupF.controls[column]) {
            if(column == 'CTRPD') {
                return this.groupF.controls['CTRPD'].hasError('exists');
            } else if(column == 'FCINI') {
                if(!item) {
                    this.groupF.controls[column].setErrors({'fecha': true});
                    return true;
                } else{
                    this.groupF.controls[column].setErrors(null);
                    return false;
                }
            } else if(column == 'FCTER') {
                if(!item || item <= this.f_drm_frm_acp.FCINI) {
                    this.groupF.controls[column].setErrors({'fecha': true});
                    return true;
                } else{
                    this.groupF.controls[column].setErrors(null);
                    return false;
                }
            } else if(column == 'MNMIN') {
                if(item <= 0) {
                    this.groupF.controls[column].setErrors({'importe': true});
                    return true;
                } else{
                    this.groupF.controls[column].setErrors(null);
                    return false;
                }
            } else if(column == 'MNMAX') {
                if(item <= 0 || item < this.f_drm_frm_acp.MNMIN) {
                    this.groupF.controls[column].setErrors({'importe': true});
                    return true;
                } else{
                    this.groupF.controls[column].setErrors(null);
                    return false;
                }
            } else if(column == 'CTZC1' || column == 'CTZC2' || column == 'CTZC3') {
                if((this.f_drm_frm_acp[column].indexOf('prohibitus') >= 0 || this.f_drm_frm_acp[column].length <= 0)) {
                  this.groupF.controls[column].setErrors({'incorrect': true});
                  return true;
                } else {
                  this.groupF.controls[column].setErrors(null);
                  return false;
                }
              }
        } else {
          return false;
        }
    }

    clean() {
        let fecha = this.f_drm_frm_acp.FECHA;
    
        this.f_drm_frm_acp = new F_DRM_FRM_ACP();
  
        this.save=1;
        
        this.f_drm_frm_acp.STTUS = '1',
        this.f_drm_frm_acp.EJERC = sessionStorage.getItem('year'),
        this.f_drm_frm_acp.FECHA = fecha,
        this.f_drm_frm_acp.USUAR = this.authService.user.user_name,
        this.f_drm_frm_acp.REGIO = this.authService.user.office;
    }

    showErrorCTRPD() {
        if(this.groupF && this.groupF.controls['CTRPD']) {
            return this.groupF.controls['CTRPD'].hasError('exists');
        } else {
            return false;
        }
    }

    disabled() {
        if(this.f_drm_frm_acp.STTUS == '1') {
          return false;
        } else {
          return true;
        }
    }

    disabledSave() {
        if(this.groupF && this.groupF.controls['FCINI'] && this.groupF.controls['FCTER']) {
            return (this.groupF.form.invalid || this.groupF.controls['FCINI'].errors || this.groupF.controls['FCTER'].errors);
        } else {
            return true;
        }
    }
}