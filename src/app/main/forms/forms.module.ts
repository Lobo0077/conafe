import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';
import { CustomDialogModule } from '../../custom/dialogs/customDialogs.module';

const routes = [
    {
        path        : 'drf',
        loadChildren: './drf/f.drf.module#ForDRFModule'
    },
    {
        path        : 'drh',
        loadChildren: './drh/f.drh.module#ForDRHModule'
    },
    {
        path        : 'drm',
        loadChildren: './drm/f.drm.module#ForDRMModule'
    },
    {
        path        : 'adm',
        loadChildren: './adm/f.adm.module#ForADMModule'
    }
];

@NgModule({
    imports     : [
        RouterModule.forChild(routes),
        FuseSharedModule,
        CustomDialogModule
    ]
})
export class FormsModule
{
}
