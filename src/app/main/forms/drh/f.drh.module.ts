import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRippleModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';

import { NgxDnDModule } from '@swimlane/ngx-dnd';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { CurrencyMaskModule } from "ngx-currency-mask";
import { NgCircleProgressModule } from 'ng-circle-progress';

import { AgGridModule } from 'ag-grid-angular';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';

import { DRHPERComponent } from './per/per.component';
import { DRHMPRComponent } from './mpr/mpr.component';
import { DRHBPRComponent } from './bpr/bpr.component';
import { DRHCTRComponent } from './ctr/ctr.component';
import { DRHEXPComponent } from './exp/exp.component';

import { FilterPipe } from '../../../custom/filters/filter.pipe'

import { RoleGuard } from '../../../guards/role.guard';

const routes: Routes = [
    {
        path:      'per',
        component: DRHPERComponent,
        canActivate: [RoleGuard],
        data:{role: 'FORMS:DRH-FRM-PER'}
    },
    {
        path:      'mpr',
        component: DRHMPRComponent,
        canActivate: [RoleGuard],
        data:{role: 'FORMS:DRH-FRM-PER'}
    },
    {
        path:      'bpr',
        component: DRHBPRComponent,
        canActivate: [RoleGuard],
        data:{role: 'FORMS:DRH-FRM-PER'}
    },
    {
        path:      'ctr',
        component: DRHCTRComponent,
        canActivate: [RoleGuard],
        data:{role: 'FORMS:DRH-FRM-CTR'}
    },
    {
        path:      'exp',
        component: DRHEXPComponent,
        canActivate: [RoleGuard],
        data:{role: 'FORMS:DRH-FRM-EXP'}
    }
];

@NgModule({
    entryComponents: [
    ],
    declarations: [
        FilterPipe,
        DRHPERComponent,
        DRHMPRComponent,
        DRHBPRComponent,
        DRHCTRComponent,
        DRHEXPComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatBadgeModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSelectModule,
        MatTableModule,
        MatCardModule,
        MatToolbarModule,
        MatTooltipModule,
        MatDialogModule,
        
        NgxDnDModule,

        NgxMatSelectSearchModule,
        CurrencyMaskModule,
        NgCircleProgressModule.forRoot({}),

        FuseSharedModule,
        FuseSidebarModule,

        AgGridModule.withComponents([])
    ]
})
export class ForDRHModule
{
}
