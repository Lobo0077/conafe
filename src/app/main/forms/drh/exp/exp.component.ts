import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { MatDialog } from '@angular/material/dialog';
import { HttpEventType } from '@angular/common/http';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatTableDataSource } from '@angular/material';

import { AuthService } from '../../../../services/auth.service';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';

import { DialogRecover } from '../../../../custom/dialogs/recover/recover.component';
import { DialogConfirm } from '../../../../custom/dialogs/confirm/confirm.component'

import { NgxSpinnerService } from 'ngx-spinner';

import * as moment from 'moment';
import * as _ from 'lodash';

import { F_DRH_FRM_PER, F_DRH_FRM_PER_DFM, F_DRH_FRM_PER_DOC, P_DRH_FRM_PER_DOC, F_DRH_FRM_PER_TLB, F_DRH_FRM_PER_DCC, F_DRH_FRM_PER_IDI, aditional } from '../../../../models/forms/DRH/FRM/PER/drh.frm.per.model';
import { CustomErrorStateMatcher } from '../../../../models/global/error';

@Component({
    selector: 'drh_exp',
    templateUrl: './exp.component.html',
    styleUrls: ['./exp.component.scss'],
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es-MX' },
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
    ]
})
export class DRHEXPComponent implements OnInit, OnDestroy {
    @ViewChild('groupForm', { static: false }) groupF: NgForm;

    public format: string;
    public error = new CustomErrorStateMatcher();

    public f_drh_frm_per: F_DRH_FRM_PER = new F_DRH_FRM_PER();
    public f_drh_frm_per_dfm: F_DRH_FRM_PER_DFM[] = [];
    public f_drh_frm_per_idi: F_DRH_FRM_PER_IDI[] = [];
    public f_drh_frm_per_tlb: F_DRH_FRM_PER_TLB[] = [];
    public f_drh_frm_per_dcc: F_DRH_FRM_PER_DCC[] = [];
    public f_drh_frm_per_doc: F_DRH_FRM_PER_DOC[] = [];
    public p_drh_frm_per_doc: P_DRH_FRM_PER_DOC[] = [];
    public aditional: aditional = new aditional();

    public paises: Array<any>;
    public regiones: Array<any>;
    public adscripciones: Array<any>;
    public adscripcionesDisp: Array<any>;
    public puestos: Array<any>;
    public puestosDisp: Array<any>;
    public titulos: Array<any>;
    public docs: Array<any>;
    public estadoCivil: Array<any>;
    public genero: Array<any>;
    public escolaridad: Array<any>;
    public carreras: Array<any>;
    public campos: Array<any>;
    public areas: Array<any>;
    public nivelEstudio: Array<any>;
    public nivelIdioma: Array<any>;

    public sit_aca: Array<any>;
    public discapacidad: Array<any>;
    public hijos: Array<any> = [];
    public tipoPlaza: Array<any>;
    public tipoBaja: Array<any>;
    public tipoNombramiento: Array<any>;
    public tipoPago: Array<any>;
    public mediosPago: Array<any>;
    public pDispBajas: Array<any>;
    public pDisp: Array<any>;
    public plazas: Array<any>;
    public sustituye: Array<any>;
    public funcionarios: Array<any>;
    public filteredAU: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public AUFilterCtrl: FormControl = new FormControl();
    public filteredVB: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public VBFilterCtrl: FormControl = new FormControl();

    public editExp: boolean = false;
    
    protected _onDestroy = new Subject<void>();

    columnsToDocs = ['ID', 'TPDOC', 'REQUI', 'ENTRE', 'PPDFF'];
    dataSourceDocs;

    columnsToDFam = ['ID', 'NAMEE', 'PRIAP', 'SEGAP', 'FCNAC', 'EDADS', 'CCURP', 'PDFCR', 'PDFAN'];
    dataSourceDFam;

    columnsToIdi = ['ACTIONS', 'ID', 'IDIOM', 'REDAC', 'CONVE', 'COMPR'];
    dataSourceIdi;

    columnsToTLab = ['ACTIONS', 'ID', 'PAITL', 'ENTTL', 'EMPRE', 'PUEST', 'CAMEX', 'FCINI', 'FCTER'];
    dataSourceTLab;

    columnsToDcc = ['ACTIONS', 'ID', 'PAIDC', 'ENTDC', 'MATER', 'AREGN', 'INSTI', 'FCINI', 'FCTER', 'NVLES'];
    dataSourceDcc;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     */
    constructor(public authService: AuthService,
        private wbService: WBService,
        private globalService: GlobalService,
        public dialog: MatDialog,
        private cdr: ChangeDetectorRef,
        private spinner: NgxSpinnerService) {

        this.format = 'DRH-FRM-PER';
        this.f_drh_frm_per.STTUS = '1',
            this.f_drh_frm_per.USUAR = this.authService.user.user_name,
            this.f_drh_frm_per.DIAPA = '10 y 25';
            this.f_drh_frm_per.JORLB = 'Lunes a Viernes';
            this.f_drh_frm_per.TIPAG = 'Q';

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    ngAfterContentChecked() {
        this.cdr.detectChanges();
    }

    ngOnInit() {
        this.getPermission();

        this.wbService.getInformation('1', '').subscribe(response => {
            this.f_drh_frm_per.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
        })

        this.wbService.getInformation('70', '').subscribe(response => {
            this.paises = response.data;
        })

        this.wbService.getInformation('3', '').subscribe(response => {
            this.regiones = response.data;
        })

        this.wbService.getInformation('4', '').subscribe(response => {
            this.adscripciones = response.data;
        })

        this.wbService.getInformation('5', 'CAUBA=¯¯ AND CUSER IS NOT NULL AND A.STTUS=4').subscribe(response => {
            this.funcionarios = response.data;
            this.filteredVB.next(this.funcionarios)
            this.filteredAU.next(this.funcionarios)
            this.puesto(this.f_drh_frm_per.USUAR, 'ELPUE');
        })

        this.AUFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                this.filterAU();
            })

        this.VBFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                this.filterVB();
            })

        this.wbService.getInformation('10', '').subscribe(response => {
            this.puestos = response.data;
        })

        this.wbService.getInformation('40', '').subscribe(response => {
            this.titulos = response.data;
        })

        this.wbService.getInformation('50', '').subscribe(response => {
            this.docs = response.data;

            let index = 0;

            response.data.forEach(element => {
                index += 1

                this.f_drh_frm_per_doc.push({
                    ID: index,
                    ERROR: '',
                    TPDOC: element.TPDOC,
                    ENTRE: false,
                    PPDFF: '',
                    REQUI: element.REQUI
                })

                this.p_drh_frm_per_doc.push({
                    PPDFF: 0
                })
            });

            this.dataSourceDocs = new MatTableDataSource(this.f_drh_frm_per_doc);
        })

        this.wbService.getInformation('27', '').subscribe(response => {
            this.mediosPago = response.data;
        })

        this.wbService.getInformation('51', '').subscribe(response => {
            this.estadoCivil = response.data;
        })

        this.wbService.getInformation('52', '').subscribe(response => {
            this.genero = response.data;
        })

        this.wbService.getInformation('53', '').subscribe(response => {
            this.escolaridad = response.data;
        })

        this.wbService.getInformation('71', '').subscribe(response => {
            this.carreras = response.data;
        })

        this.wbService.getInformation('72', '').subscribe(response => {
            this.campos = response.data;
        })

        this.wbService.getInformation('73', '').subscribe(response => {
            this.areas = response.data;
        })

        this.wbService.getInformation('74', '').subscribe(response => {
            this.nivelEstudio = response.data;
        })

        this.wbService.getInformation('75', '').subscribe(response => {
            this.nivelIdioma = response.data;
        })

        this.wbService.getInformation('54', '').subscribe(response => {
            this.tipoPlaza = response.data;
        })

        this.wbService.getInformation('55', '').subscribe(response => {
            this.sit_aca = response.data;
        })

        this.wbService.getInformation('66', '').subscribe(response => {
            this.discapacidad = response.data;
        })

        this.wbService.getInformation('56', '').subscribe(response => {
            this.tipoBaja = response.data;
        })

        this.wbService.getInformation('57', '').subscribe(response => {
            this.tipoNombramiento = response.data;
        })

        this.wbService.getInformation('58', '').subscribe(response => {
            this.tipoPago = response.data;
        })

        this.wbService.getInformation('60', 'REGIO=¯' + this.authService.user.office + '¯').subscribe(response => {
            this.plazas = response.data;
        })

        this.f_drh_frm_per_idi.push({
            ID: 1,
            ERROR: '',
            IDIOM: 'ESPAÑOL',
            REDAC: 'A',
            CONVE: 'A',
            COMPR: 'A'
        })

        this.dataSourceIdi = new MatTableDataSource(this.f_drh_frm_per_idi);

        for (let x = 0; x <= 10; x++) {
            this.hijos.push({ n: x })
        }
    }

    ngOnDestroy() {
        this._onDestroy.next();
        this._onDestroy.complete();
    }

    getPermission() {
        this.wbService.getInformation('0', 'TYPPE=¯permission¯ AND PAREN=¯DRH-FRM-EXP¯', '¯' + this.authService.user.user_name + '¯').subscribe(response => {
            if (response.data) {
                response.data.forEach(element => {
                    if (element.PERMI == 1) {
                        this.editExp = element.ACCES;
                    }
                });
            }
        })
    }

    clear(type) {
        let nuemp = this.f_drh_frm_per.NUEMP,
            fecha = this.f_drh_frm_per.FECHA;

        this.f_drh_frm_per = new F_DRH_FRM_PER();
        this.aditional = new aditional();

        if (type == 'C') {
            this.f_drh_frm_per.NUEMP = nuemp;

            if (nuemp.length == 30) {
                this.wbService.getInformation('1', '').subscribe(response => {
                    this.f_drh_frm_per.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
                })
            } else {
                this.f_drh_frm_per.FECHA = fecha;
            }
        } else {
            this.wbService.getInformation('1', '').subscribe(response => {
                this.f_drh_frm_per.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
            })
        }

        this.f_drh_frm_per.STTUS = '1',
            this.f_drh_frm_per.USUAR = this.authService.user.user_name,
            this.f_drh_frm_per.DIAPA = '10 y 25';
        this.puesto(this.f_drh_frm_per.USUAR, 'ELPUE');

        this.f_drh_frm_per_idi = [{
            ID: 1,
            ERROR: '',
            IDIOM: 'ESPAÑOL',
            REDAC: 'A',
            CONVE: 'A',
            COMPR: 'A'
        }];

        this.dataSourceIdi = new MatTableDataSource(this.f_drh_frm_per_idi);

        this.f_drh_frm_per_tlb = [];

        this.dataSourceTLab = new MatTableDataSource(this.f_drh_frm_per_tlb);

        this.f_drh_frm_per_dcc = [];

        this.dataSourceDcc = new MatTableDataSource(this.f_drh_frm_per_dcc);

        this.f_drh_frm_per_dfm = [];

        this.dataSourceDFam = new MatTableDataSource(this.f_drh_frm_per_dfm);

        this.f_drh_frm_per_doc = [];
        this.p_drh_frm_per_doc = [];

        this.wbService.getInformation('50', '').subscribe(response => {
            this.docs = response.data;

            let index = 0;

            response.data.forEach(element => {
                index += 1

                this.f_drh_frm_per_doc.push({
                    ID: index,
                    ERROR: '',
                    TPDOC: element.TPDOC,
                    ENTRE: false,
                    PPDFF: '',
                    REQUI: element.REQUI
                })

                this.p_drh_frm_per_doc.push({
                    PPDFF: 0
                })
            });

            this.dataSourceDocs = new MatTableDataSource(this.f_drh_frm_per_doc);
        })
    }

    protected filterVB() {
        if (!this.funcionarios) {
            return;
        }

        let search = this.VBFilterCtrl.value;
        if (!search) {
            this.filteredVB.next(this.funcionarios.slice());
            return;
        } else {
            search = search.toLowerCase();
        }

        this.filteredVB.next(
            this.funcionarios.filter(funcionario => funcionario.DESCR.toLowerCase().indexOf(search) > -1)
        );
    }

    protected filterAU() {
        if (!this.funcionarios) {
            return;
        }

        let search = this.AUFilterCtrl.value;
        if (!search) {
            this.filteredAU.next(this.funcionarios.slice());
            return;
        } else {
            search = search.toLowerCase();
        }

        this.filteredAU.next(
            this.funcionarios.filter(funcionario => funcionario.DESCR.toLowerCase().indexOf(search) > -1)
        );
    }

    getPlaDisp() {
        if (this.f_drh_frm_per.REGOP && this.f_drh_frm_per.NOMBR) {
            this.wbService.getInformation('4', 'ADSCR in (select ADSCR from V_DRH_PDB where REGOP=¯' + this.f_drh_frm_per.REGOP + '¯ and NOMBT=¯' + this.f_drh_frm_per.NOMBR + '¯) or ADSCR in (select ADSCR from V_DRH_PDS where REGIO=¯' + this.f_drh_frm_per.REGOP + '¯ and NOMBR=¯' + this.f_drh_frm_per.NOMBR + '¯)').subscribe(response => {
                this.adscripcionesDisp = response.data;
            })

            if (this.f_drh_frm_per.ADSCR) {
                this.wbService.getInformation('10', 'PUEST in (select PUEST from V_DRH_PDB where REGOP=¯' + this.f_drh_frm_per.REGOP + '¯ and NOMBT=¯' + this.f_drh_frm_per.NOMBR + '¯ and ADSCR=¯' + this.f_drh_frm_per.ADSCR + '¯) or PUEST in (select PUEST from V_DRH_PDS where REGIO=¯' + this.f_drh_frm_per.REGOP + '¯ and NOMBR=¯' + this.f_drh_frm_per.NOMBR + '¯ and ADSCR=¯' + this.f_drh_frm_per.ADSCR + '¯)').subscribe(response => {
                    this.puestosDisp = response.data;
                })

                this.wbService.getInformation('61', 'REGOP=¯' + this.f_drh_frm_per.REGOP + '¯ and ADSCR=¯' + this.f_drh_frm_per.ADSCR + '¯').subscribe(response => {
                    this.pDispBajas = response.data;
                })

                this.wbService.getInformation('62', 'REGIO=¯' + this.f_drh_frm_per.REGOP + '¯ and ADSCR=¯' + this.f_drh_frm_per.ADSCR + '¯').subscribe(response => {
                    this.pDisp = response.data;
                })
            }
        }
    }

    puesto(func: string, puest: string) {
        let res = this.funcionarios.find(funcionario => funcionario.CUSER === func);

        if (res) {
            this.aditional[puest] = res.DSCPU;
        }
    }

    calcEdad(fcnac) {
        if (fcnac) {
            return Math.floor(moment(new Date()).diff(moment(fcnac, "MM/DD/YYYY"), 'years', true));
        }

        return 0
    }

    getChilds() {
        if (this.f_drh_frm_per.NMHIJ > 0) {
            if (this.f_drh_frm_per.NMHIJ >= this.f_drh_frm_per_dfm.length) {
                for (let x = this.f_drh_frm_per_dfm.length; x < this.f_drh_frm_per.NMHIJ; x++) {
                    this.f_drh_frm_per_dfm.push({
                        ID: x + 1,
                        ERROR: '',
                        NAMEE: '',
                        PRIAP: '',
                        SEGAP: '',
                        FCNAC: null,
                        EDADS: 0,
                        CCURP: '',
                        PDFCR: '',
                        PORCR: 0,
                        PDFAN: '',
                        PORAN: 0
                    })
                }
            } else {
                this.f_drh_frm_per_dfm.splice(this.f_drh_frm_per.NMHIJ,this.f_drh_frm_per_dfm.length)
            }
        }

        this.dataSourceDFam = new MatTableDataSource(this.f_drh_frm_per_dfm);
    }

    addRowIdi() {
        let index = this.f_drh_frm_per_idi.length + 1;

        this.f_drh_frm_per_idi.push({
            ID: index,
            ERROR: '',
            IDIOM: '',
            REDAC: '',
            CONVE: '',
            COMPR: ''
        })

        this.dataSourceIdi = new MatTableDataSource(this.f_drh_frm_per_idi);
    }

    removeRowIdi(index) {
        this.f_drh_frm_per_idi.splice(index, 1);

        this.f_drh_frm_per_idi.forEach(function (element, index) {
            element.ID = index + 1;
        })

        this.dataSourceIdi = new MatTableDataSource(this.f_drh_frm_per_idi);
    }

    addRowLab() {
        let index = this.f_drh_frm_per_tlb.length + 1;

        this.f_drh_frm_per_tlb.push({
            ID: index,
            ERROR: '',
            PAITL: '',
            ENTTL: '',
            EMPRE: '',
            PUEST: '',
            CAMEX: '',
            FCINI: null,
            FCTER: null
        })

        this.dataSourceTLab = new MatTableDataSource(this.f_drh_frm_per_tlb);
    }

    removeRowLab(index) {
        this.f_drh_frm_per_tlb.splice(index, 1);

        this.f_drh_frm_per_tlb.forEach(function (element, index) {
            element.ID = index + 1;
        })

        this.dataSourceTLab = new MatTableDataSource(this.f_drh_frm_per_tlb);
    }

    addRowDcc() {
        let index = this.f_drh_frm_per_dcc.length + 1;

        this.f_drh_frm_per_dcc.push({
            ID: index,
            ERROR: '',
            PAIDC: '',
            ENTDC: '',
            MATER: '',
            AREGN: '',
            INSTI: '',
            FCINI: null,
            FCTER: null,
            NVLES: ''
        })

        this.dataSourceDcc = new MatTableDataSource(this.f_drh_frm_per_dcc);
    }

    removeRowDcc(index) {
        this.f_drh_frm_per_dcc.splice(index, 1);

        this.f_drh_frm_per_dcc.forEach(function (element, index) {
            element.ID = index + 1;
        })

        this.dataSourceDcc = new MatTableDataSource(this.f_drh_frm_per_dcc);
    }

    infoPlaza() {
        this.limpia();

        if (this.f_drh_frm_per.NOMBR == 'H') {
            this.wbService.getInformation('60', 'PUEST=¯' + this.f_drh_frm_per.PUEST + '¯').subscribe(response => {
                if (response.data && response.data[0]) {
                    this.f_drh_frm_per.DESCR = response.data[0].DESC1,
                        this.f_drh_frm_per.DESC2 = response.data[0].DESC2;
                }
            })
        }
    }

    limpia() {
        this.f_drh_frm_per.SUSTI = '',
            this.f_drh_frm_per.SINDI = false,
            this.f_drh_frm_per.PLAZA = 0,
            this.f_drh_frm_per.DESCR = '',
            this.f_drh_frm_per.DESC2 = '',
            this.f_drh_frm_per.TIPPL = '',
            this.f_drh_frm_per.NIVEL = '',
            this.f_drh_frm_per.DRCTR = 0,
            this.f_drh_frm_per.NUMPG = 0,
            this.f_drh_frm_per.MONPR = 0,
            this.f_drh_frm_per.MONTT = 0,
            this.f_drh_frm_per.SUMEN = 0,
            this.f_drh_frm_per.COMGA = 0,
            this.f_drh_frm_per.SMEIN = 0;
    }

    impSueldo(plaex) {
        let regio = '',
            puest = '',
            plaza: number = 0;

        if (plaex && this.f_drh_frm_per.STTUS == '1') {
            let res = this.pDispBajas.find(user => user.NUEMP == this.f_drh_frm_per.SUSTI);

            regio = res.REGOP,
                puest = res.PUEST,
                plaza = res.PLAZA;
        } else {
            regio = this.f_drh_frm_per.REGIO,
                puest = this.f_drh_frm_per.PUEST,
                plaza = this.f_drh_frm_per.PLAZA;
        }

        if (this.NaN()) {
            this.wbService.getInformation('60', 'REGIO=¯' + regio + '¯ AND PUEST=¯' + puest + '¯ AND PLAZA=' + plaza).subscribe(response => {
                if (response.data && response.data[0]) {
                    this.f_drh_frm_per.PLAZA = plaza,
                        this.f_drh_frm_per.DESCR = response.data[0].DESC1,
                        this.f_drh_frm_per.DESC2 = response.data[0].DESC2,
                        this.f_drh_frm_per.TIPPL = response.data[0].TIPPL,
                        this.f_drh_frm_per.NIVEL = response.data[0].NIVEL,
                        this.f_drh_frm_per.SUMEN = response.data[0].SUMEN,
                        this.f_drh_frm_per.COMGA = response.data[0].COMGA,
                        this.f_drh_frm_per.SMEIN = response.data[0].SMEIN;

                    this.aditional.NOMBR = response.data[0].NOMBR,
                        this.aditional.ADSCR = response.data[0].ADSCR,
                        this.aditional.PUEST = response.data[0].PUEST,
                        this.aditional.PLAZA = response.data[0].PLAZA,
                        this.aditional.DESCR = response.data[0].DESC1,
                        this.aditional.DESC2 = response.data[0].DESC2,
                        this.aditional.TIPPL = response.data[0].TIPPL,
                        this.aditional.NIVEL = response.data[0].NIVEL,
                        this.aditional.SUMEN = response.data[0].SUMEN,
                        this.aditional.COMGA = response.data[0].COMGA,
                        this.aditional.SMEIN = response.data[0].SMEIN;

                    this.calcVigencia();
                } else {
                    this.f_drh_frm_per.PLAZA = 0,
                        this.f_drh_frm_per.DESCR = '',
                        this.f_drh_frm_per.DESC2 = '',
                        this.f_drh_frm_per.TIPPL = '',
                        this.f_drh_frm_per.NIVEL = '',
                        this.f_drh_frm_per.SUMEN = 0,
                        this.f_drh_frm_per.COMGA = 0,
                        this.f_drh_frm_per.SMEIN = 0;

                    this.aditional.PLAZA = 0,
                        this.aditional.DESCR = '',
                        this.aditional.DESC2 = '',
                        this.aditional.TIPPL = '',
                        this.aditional.NIVEL = '',
                        this.aditional.SUMEN = 0,
                        this.aditional.COMGA = 0,
                        this.aditional.SMEIN = 0;
                }
            })
        } else {
            this.wbService.getInformation('60', 'NUEMP=¯' + this.f_drh_frm_per.NUEMP + '¯').subscribe(response => {
                if (response.data && response.data[0]) {
                    this.aditional.NOMBR = response.data[0].NOMBR,
                        this.aditional.ADSCR = response.data[0].ADSCR,
                        this.aditional.PUEST = response.data[0].PUEST,
                        this.aditional.PLAZA = response.data[0].PLAZA,
                        this.aditional.DESCR = response.data[0].DESC1,
                        this.aditional.DESC2 = response.data[0].DESC2,
                        this.aditional.TIPPL = response.data[0].TIPPL,
                        this.aditional.NIVEL = response.data[0].NIVEL,
                        this.aditional.SUMEN = response.data[0].SUMEN,
                        this.aditional.COMGA = response.data[0].COMGA,
                        this.aditional.SMEIN = response.data[0].SMEIN;

                    this.calcVigencia();
                } else {
                    this.aditional.PLAZA = 0,
                        this.aditional.DESCR = '',
                        this.aditional.DESC2 = '',
                        this.aditional.TIPPL = '',
                        this.aditional.NIVEL = '',
                        this.aditional.SUMEN = 0,
                        this.aditional.COMGA = 0,
                        this.aditional.SMEIN = 0;
                }
            })
        }
    }

    calcVigencia() {
        if (this.f_drh_frm_per.FCING && this.f_drh_frm_per.NOMBR) {
            if (this.f_drh_frm_per.NOMBR != 'H') {
                let nombr = this.tipoNombramiento.find(element => element.NOMBR == this.f_drh_frm_per.NOMBR)

                if (nombr) {
                    this.f_drh_frm_per.FCTER = moment(this.f_drh_frm_per.FCING).add(nombr.VIGEN, 'M').toDate();
                }
            } else {
                if (this.f_drh_frm_per.DRCTR) {
                    this.f_drh_frm_per.FCTER = moment(this.f_drh_frm_per.FCING).add(this.f_drh_frm_per.DRCTR, 'M').toDate();
                } else {
                    this.f_drh_frm_per.FCTER = null;
                }
            }
        }
    }

    openDialog(option, type) {
        if (option == 'R') {
            let title = '',
                object = '41',
                columns = [],
                filter = '';

            if (type == 'R') {
                title = 'Empleados'
                columns = [
                    { headerName: 'Región', field: 'DSREG', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Fecha', field: 'FECHA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Estatus', field: 'STDSC', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Adscripción', field: 'ADSCR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Puesto', field: 'PUEST', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Nombramiento', field: 'NOMBT', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Funcionario', field: 'NOMBR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Nº Empleado', field: 'NUEMP', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true }
                ]
                filter = 'A.STTUS=4'
            }

            const dialogRef = this.dialog.open(DialogRecover, {
                width: '1000px',
                height: '650px',
                data: { title, columns, object, filter }
            });

            dialogRef.afterClosed().subscribe(result => {
                if (result && result.NUEMP) {
                    this.getInformation(result);
                }
            });
        } else {
            const dialogRef = this.dialog.open(DialogConfirm, {
                width: '1000px',
                height: '420px',
                data: { option: option, response: type }
            });

            dialogRef.afterClosed().subscribe(result => {
                if (result && result.option) {
                    this.sendInformation(result);
                }
            });
        }
    }

    getInformation(data) {
        this.wbService.getInformationForms(this.format, 'NUEMP=¯' + data.NUEMP + '¯').subscribe(response => {
            let rec_drh_frm_per = response.F_DRH_FRM_PER,
                rec_drh_frm_per_idi = response.F_DRH_FRM_PER_IDI,
                rec_drh_frm_per_tlb = response.F_DRH_FRM_PER_TLB,
                rec_drh_frm_per_dcc = response.F_DRH_FRM_PER_DCC,
                rec_drh_frm_per_dfm = response.F_DRH_FRM_PER_DFM,
                rec_drh_frm_per_doc = response.F_DRH_FRM_PER_DOC;

            if (rec_drh_frm_per) {
                this.f_drh_frm_per.NUEMP = rec_drh_frm_per[0].NUEMP,
                    this.f_drh_frm_per.STTUS = '1',
                    this.f_drh_frm_per.USUAR = rec_drh_frm_per[0].USUAR,
                    this.f_drh_frm_per.KEYUI = rec_drh_frm_per[0].KEYUI,
                    this.f_drh_frm_per.FECHA = isNaN(rec_drh_frm_per[0].NUEMP) ? this.f_drh_frm_per.FECHA : this.globalService.getDateUTC(rec_drh_frm_per[0].FECHA),
                    this.f_drh_frm_per.REGIO = rec_drh_frm_per[0].REGIO,
                    this.f_drh_frm_per.TITUL = rec_drh_frm_per[0].TITUL,
                    this.f_drh_frm_per.NAMEE = rec_drh_frm_per[0].NAMEE,
                    this.f_drh_frm_per.PRIAP = rec_drh_frm_per[0].PRIAP,
                    this.f_drh_frm_per.SEGAP = rec_drh_frm_per[0].SEGAP,
                    this.f_drh_frm_per.FCNAC = rec_drh_frm_per[0].FCNAC ? this.globalService.getDateUTC(rec_drh_frm_per[0].FCNAC) : null,
                    this.f_drh_frm_per.ESCOL = rec_drh_frm_per[0].ESCOL,
                    this.f_drh_frm_per.CARRE = rec_drh_frm_per[0].CARRE,
                    this.f_drh_frm_per.SITAC = rec_drh_frm_per[0].SITAC,
                    this.f_drh_frm_per.TTGRC = rec_drh_frm_per[0].TTGRC,
                    this.f_drh_frm_per.INSED = rec_drh_frm_per[0].INSED,
                    this.f_drh_frm_per.PINED = rec_drh_frm_per[0].PINED,
                    this.f_drh_frm_per.COMPU = rec_drh_frm_per[0].COMPU,
                    this.f_drh_frm_per.GENER = rec_drh_frm_per[0].GENER,
                    this.f_drh_frm_per.ESCIV = rec_drh_frm_per[0].ESCIV,
                    this.f_drh_frm_per.ESCON = rec_drh_frm_per[0].ESCON,
                    this.f_drh_frm_per.MADRE = rec_drh_frm_per[0].MADRE,
                    this.f_drh_frm_per.NMHIJ = rec_drh_frm_per[0].NMHIJ,
                    this.f_drh_frm_per.RRFCC = rec_drh_frm_per[0].RRFCC,
                    this.f_drh_frm_per.CCURP = rec_drh_frm_per[0].CCURP,
                    this.f_drh_frm_per.NACIO = rec_drh_frm_per[0].NACIO,
                    this.f_drh_frm_per.PAINA = rec_drh_frm_per[0].PAINA,
                    this.f_drh_frm_per.ENTNA = rec_drh_frm_per[0].ENTNA,
                    this.f_drh_frm_per.NMNSS = rec_drh_frm_per[0].NMNSS,
                    this.f_drh_frm_per.CALLE = rec_drh_frm_per[0].CALLE,
                    this.f_drh_frm_per.NMEXT = rec_drh_frm_per[0].NMEXT,
                    this.f_drh_frm_per.NMINT = rec_drh_frm_per[0].NMINT,
                    this.f_drh_frm_per.COLOC = rec_drh_frm_per[0].COLOC,
                    this.f_drh_frm_per.CODPO = rec_drh_frm_per[0].CODPO,
                    this.f_drh_frm_per.MUDEL = rec_drh_frm_per[0].MUDEL,
                    this.f_drh_frm_per.ENTFE = rec_drh_frm_per[0].ENTFE,
                    this.f_drh_frm_per.TELEF = rec_drh_frm_per[0].TELEF,
                    this.f_drh_frm_per.TECEL = rec_drh_frm_per[0].TECEL,
                    this.f_drh_frm_per.TEEME = rec_drh_frm_per[0].TEEME,
                    this.f_drh_frm_per.EMAIP = rec_drh_frm_per[0].EMAIP,
                    this.f_drh_frm_per.FOINE = rec_drh_frm_per[0].FOINE,
                    this.f_drh_frm_per.BANCO = rec_drh_frm_per[0].BANCO,
                    this.f_drh_frm_per.CTABA = rec_drh_frm_per[0].CTABA,
                    this.f_drh_frm_per.ENFCR = rec_drh_frm_per[0].ENFCR,
                    this.f_drh_frm_per.ENFER = rec_drh_frm_per[0].ENFER,
                    this.f_drh_frm_per.DISCA = rec_drh_frm_per[0].DISCA,
                    this.f_drh_frm_per.ADSCR = rec_drh_frm_per[0].ADSCR,
                    this.f_drh_frm_per.REGOP = rec_drh_frm_per[0].REGOP,
                    this.f_drh_frm_per.PUEST = rec_drh_frm_per[0].PUEST,
                    this.f_drh_frm_per.PLAEX = rec_drh_frm_per[0].PLAEX,
                    this.f_drh_frm_per.SUSTI = rec_drh_frm_per[0].SUSTI,
                    this.f_drh_frm_per.PLAZA = rec_drh_frm_per[0].PLAZA,
                    this.f_drh_frm_per.DESCR = rec_drh_frm_per[0].DESCR,
                    this.f_drh_frm_per.DESC2 = rec_drh_frm_per[0].DESC2,
                    this.f_drh_frm_per.NIVEL = rec_drh_frm_per[0].NIVEL,
                    this.f_drh_frm_per.TIPPL = rec_drh_frm_per[0].TIPPL,
                    this.f_drh_frm_per.NOMBR = rec_drh_frm_per[0].NOMBR,
                    this.f_drh_frm_per.DRCTR = rec_drh_frm_per[0].DRCTR,
                    this.f_drh_frm_per.NUMPG = rec_drh_frm_per[0].NUMPG,
                    this.f_drh_frm_per.MONPR = rec_drh_frm_per[0].MONPR,
                    this.f_drh_frm_per.MONTT = rec_drh_frm_per[0].MONTT,
                    this.f_drh_frm_per.SUMEN = rec_drh_frm_per[0].SUMEN,
                    this.f_drh_frm_per.COMGA = rec_drh_frm_per[0].COMGA,
                    this.f_drh_frm_per.SMEIN = rec_drh_frm_per[0].SMEIN,
                    this.f_drh_frm_per.SINDI = rec_drh_frm_per[0].SINDI,
                    this.f_drh_frm_per.JORLB = rec_drh_frm_per[0].JORLB,
                    this.f_drh_frm_per.HORAR = rec_drh_frm_per[0].HORAR,
                    this.f_drh_frm_per.HORDA = rec_drh_frm_per[0].HORDA,
                    this.f_drh_frm_per.HORAA = rec_drh_frm_per[0].HORAA,
                    this.f_drh_frm_per.HORDP = rec_drh_frm_per[0].HORDP,
                    this.f_drh_frm_per.HORAP = rec_drh_frm_per[0].HORAP,
                    this.f_drh_frm_per.EMAIL = rec_drh_frm_per[0].EMAIL,
                    this.f_drh_frm_per.EXTEN = rec_drh_frm_per[0].EXTEN,
                    this.f_drh_frm_per.JEFEI = rec_drh_frm_per[0].JEFEI,
                    this.f_drh_frm_per.FUNCI = rec_drh_frm_per[0].FUNCI,
                    this.f_drh_frm_per.FCING = rec_drh_frm_per[0].FCING ? this.globalService.getDateUTC(rec_drh_frm_per[0].FCING) : null,
                    this.f_drh_frm_per.FCINI = rec_drh_frm_per[0].FCINI ? this.globalService.getDateUTC(rec_drh_frm_per[0].FCINI) : null,
                    this.f_drh_frm_per.FCMOV = rec_drh_frm_per[0].FCMOV ? this.globalService.getDateUTC(rec_drh_frm_per[0].FCMOV) : null,
                    this.f_drh_frm_per.FCMOD = rec_drh_frm_per[0].FCMOD ? this.globalService.getDateUTC(rec_drh_frm_per[0].FCMOD) : null,
                    this.f_drh_frm_per.FCTER = rec_drh_frm_per[0].FCTER ? this.globalService.getDateUTC(rec_drh_frm_per[0].FCTER) : null,
                    this.f_drh_frm_per.FCBAJ = rec_drh_frm_per[0].FCBAJ ? this.globalService.getDateUTC(rec_drh_frm_per[0].FCBAJ) : null,
                    this.f_drh_frm_per.CAUBA = rec_drh_frm_per[0].CAUBA,
                    this.f_drh_frm_per.OBSER = rec_drh_frm_per[0].OBSER,
                    this.f_drh_frm_per.DIAPA = rec_drh_frm_per[0].DIAPA,
                    this.f_drh_frm_per.TIPAG = rec_drh_frm_per[0].TIPAG,
                    this.f_drh_frm_per.MDPAG = rec_drh_frm_per[0].MDPAG,
                    this.f_drh_frm_per.PAGAD = rec_drh_frm_per[0].PAGAD,
                    this.f_drh_frm_per.CRAMO = rec_drh_frm_per[0].CRAMO,
                    this.f_drh_frm_per.CLACO = rec_drh_frm_per[0].CLACO,
                    this.f_drh_frm_per.SUBAI = rec_drh_frm_per[0].SUBAI,
                    this.f_drh_frm_per.SUBAS = rec_drh_frm_per[0].SUBAS,
                    this.f_drh_frm_per.REMUT = rec_drh_frm_per[0].REMUT,
                    this.f_drh_frm_per.IMAGN = rec_drh_frm_per[0].IMAGN,
                    this.f_drh_frm_per.VBHPU = rec_drh_frm_per[0].VBHPU,
                    this.f_drh_frm_per.VBHPF = rec_drh_frm_per[0].STTUS != '1' && rec_drh_frm_per[0].VBHPF ? this.globalService.getDateUTC(rec_drh_frm_per[0].VBHPF) : null,
                    this.f_drh_frm_per.VBHPR = rec_drh_frm_per[0].VBHPR,
                    this.f_drh_frm_per.VBHPC = rec_drh_frm_per[0].VBHPC,
                    this.f_drh_frm_per.VBHPK = rec_drh_frm_per[0].STTUS != '1' ? rec_drh_frm_per[0].VBHPK : '',
                    this.f_drh_frm_per.AUTOU = rec_drh_frm_per[0].AUTOU,
                    this.f_drh_frm_per.AUTOF = rec_drh_frm_per[0].STTUS != '1' && rec_drh_frm_per[0].AUTOF ? this.globalService.getDateUTC(rec_drh_frm_per[0].AUTOF) : null,
                    this.f_drh_frm_per.AUTOR = rec_drh_frm_per[0].AUTOR,
                    this.f_drh_frm_per.AUTOC = rec_drh_frm_per[0].AUTOC,
                    this.f_drh_frm_per.AUTOK = rec_drh_frm_per[0].STTUS != '1' ? rec_drh_frm_per[0].AUTOK : '';

                this.f_drh_frm_per_idi = [];

                if (rec_drh_frm_per_idi) {
                    rec_drh_frm_per_idi.forEach(element => {
                        this.f_drh_frm_per_idi.push({
                            ID: element.ID,
                            ERROR: element.ERROR,
                            IDIOM: element.IDIOM,
                            REDAC: element.REDAC,
                            CONVE: element.CONVE,
                            COMPR: element.COMPR
                        })
                    });
                }

                this.dataSourceIdi = new MatTableDataSource(this.f_drh_frm_per_idi);

                this.f_drh_frm_per_tlb = [];

                if (rec_drh_frm_per_tlb) {
                    rec_drh_frm_per_tlb.forEach(element => {
                        this.f_drh_frm_per_tlb.push({
                            ID: element.ID,
                            ERROR: element.ERROR,
                            PAITL: element.PAITL,
                            ENTTL: element.ENTTL,
                            EMPRE: element.EMPRE,
                            PUEST: element.PUEST,
                            CAMEX: element.CAMEX,
                            FCINI: element.FCINI ? this.globalService.getDateUTC(element.FCINI) : null,
                            FCTER: element.FCTER ? this.globalService.getDateUTC(element.FCTER) : null
                        })
                    });
                }

                this.dataSourceTLab = new MatTableDataSource(this.f_drh_frm_per_tlb);

                this.f_drh_frm_per_dcc = [];

                if (rec_drh_frm_per_dcc) {
                    rec_drh_frm_per_dcc.forEach(element => {
                        this.f_drh_frm_per_dcc.push({
                            ID: element.ID,
                            ERROR: element.ERROR,
                            PAIDC: element.PAIDC,
                            ENTDC: element.ENTDC,
                            MATER: element.MATER,
                            AREGN: element.AREGN,
                            INSTI: element.INSTI,
                            FCINI: element.FCINI ? this.globalService.getDateUTC(element.FCINI) : null,
                            FCTER: element.FCTER ? this.globalService.getDateUTC(element.FCTER) : null,
                            NVLES: element.NVLES
                        })
                    });
                }

                this.dataSourceDcc = new MatTableDataSource(this.f_drh_frm_per_dcc);

                this.f_drh_frm_per_dfm = [];

                if (rec_drh_frm_per_dfm) {
                    rec_drh_frm_per_dfm.forEach(element => {
                        this.f_drh_frm_per_dfm.push({
                            ID: element.ID,
                            ERROR: element.ERROR,
                            NAMEE: element.NAMEE,
                            PRIAP: element.PRIAP,
                            SEGAP: element.SEGAP,
                            FCNAC: element.FCNAC ? this.globalService.getDateUTC(element.FCNAC) : null,
                            EDADS: this.calcEdad(element.FCNAC ? this.globalService.getDateUTC(element.FCNAC) : null),
                            CCURP: element.CCURP,
                            PDFCR: element.PDFCR,
                            PORCR: element.PDFCR ? 100 : 0,
                            PDFAN: element.PDFAN,
                            PORAN: element.PDFAN ? 100 : 0
                        })
                    });
                }

                this.dataSourceDFam = new MatTableDataSource(this.f_drh_frm_per_dfm);

                this.f_drh_frm_per_doc = [];
                this.p_drh_frm_per_doc = [];

                if (rec_drh_frm_per_doc) {
                    rec_drh_frm_per_doc.forEach(element => {
                        this.f_drh_frm_per_doc.push({
                            ID: element.ID,
                            ERROR: element.ERROR,
                            TPDOC: element.TPDOC,
                            ENTRE: element.ENTRE,
                            PPDFF: element.PPDFF,
                            REQUI: element.REQUI
                        })

                        this.p_drh_frm_per_doc.push({
                            PPDFF: element.PPDFF ? 100 : 0
                        });
                    });
                }

                this.docs.forEach(element => {
                    let valDoc

                    if (rec_drh_frm_per_doc) {
                        valDoc = rec_drh_frm_per_doc.filter(item => item.TPDOC == element.TPDOC)
                    }

                    if (!valDoc) {
                        let id = this.f_drh_frm_per_doc.length;

                        this.f_drh_frm_per_doc.push({
                            ID: id + 1,
                            ERROR: '',
                            TPDOC: element.TPDOC,
                            ENTRE: false,
                            PPDFF: '',
                            REQUI: element.REQUI
                        })

                        this.p_drh_frm_per_doc.push({
                            PPDFF: element.PPDFF ? 100 : 0
                        });
                    }
                });

                this.dataSourceDocs = new MatTableDataSource(this.f_drh_frm_per_doc);

                if (this.f_drh_frm_per.NOMBR != 'H') {
                    this.impSueldo(this.f_drh_frm_per.PLAEX);
                }

                if (this.f_drh_frm_per.SUSTI) {
                    this.wbService.getInformation('5', 'NUEMP=¯' + this.f_drh_frm_per.SUSTI + '¯').subscribe(response => {
                        this.sustituye = response.data;
                    })
                }

                if (this.NaN()) {
                    this.getPlaDisp()
                }

                this.getNacionalidad();
                this.aditional.EDADS = this.calcEdad(this.f_drh_frm_per.FCNAC);
                this.puesto(this.f_drh_frm_per.USUAR, 'ELPUE');
                this.puesto(this.f_drh_frm_per.VBHPU, 'VBPUE');
                this.puesto(this.f_drh_frm_per.AUTOU, 'AUPUE');
            } else {
                this.globalService.messageSwal('Información no encontrada', 'La información del empleado ' + data.NOMBR + ' no se encontro en el servidor', 'error');
            }
        })
    }

    sendInformation(info) {
        let information,
            message;

        if (info.option == 'V') {
            information = [{ VBHPF: '|current_timestamp|', VBHPR: info.response, VBHPC: info.commit, VBHPK: '|UUID|', STTUS: info.status }]

            if (info.status == '1') {
                message = 'El folio: ' + this.f_drh_frm_per.NUEMP + ' fue habilitado para su edición';
            } else if (info.status == '3') {
                message = 'Información enviada para Autorización con el folio: ' + this.f_drh_frm_per.NUEMP;
            } else if (info.status == '6') {
                message = 'El folio: ' + this.f_drh_frm_per.NUEMP + ' fue cancelado';
            }

            this.wbService.updateInformationForms(this.format, '1', 'NUEMP=¯' + this.f_drh_frm_per.NUEMP + '¯', JSON.stringify(information)).subscribe(
                response => {
                    if (response.message) {
                        this.globalService.messageSwal('Información Almacenada', message, 'success');
                        this.f_drh_frm_per.STTUS = info.status,
                            this.f_drh_frm_per.VBHPR = info.response,
                            this.f_drh_frm_per.VBHPC = info.commit;
                        this.getPermission();

                        if (info.status == '3') {
                            this.createFile('OK');
                        }
                    }
                })
        } else if (info.option == 'A') {
            information = [{ AUTOF: '|current_timestamp|', AUTOR: info.response, AUTOC: info.commit, AUTOK: '|UUID|', STTUS: info.status }]

            if (info.status == '1') {
                message = 'El folio: ' + this.f_drh_frm_per.NUEMP + ' fue habilitado para su edición';
            } else if (info.status == '4') {
                message = 'Información Autorizada con el folio: ' + this.f_drh_frm_per.NUEMP;
            } else if (info.status == '6') {
                message = 'El folio: ' + this.f_drh_frm_per.NUEMP + ' fue cancelado';
            }

            this.wbService.updateInformationForms(this.format, '1', 'NUEMP=¯' + this.f_drh_frm_per.NUEMP + '¯', JSON.stringify(information)).subscribe(
                response => {
                    if (response.message) {
                        this.globalService.messageSwal('Información Almacenada', message, 'success');
                        this.f_drh_frm_per.STTUS = info.status,
                            this.f_drh_frm_per.AUTOR = info.response,
                            this.f_drh_frm_per.AUTOC = info.commit;
                        this.getPermission();

                        if (info.status == '4') {
                            this.createFile('OK');
                        }
                    }
                })
        }
    }

    getNacionalidad() {
        const nacion = this.paises.find(item => item.PAISS == this.f_drh_frm_per.PAINA)

        if (nacion && this.f_drh_frm_per.STTUS == '1' && this.f_drh_frm_per.GENER) {
            if (this.f_drh_frm_per.GENER == 'F') {
                this.f_drh_frm_per.NACIO = nacion.NACIF
            } else {
                this.f_drh_frm_per.NACIO = nacion.NACIM
            }
        } else {
            this.f_drh_frm_per.NACIO = '';
        }
    }

    openInput(fieldName: string, type: string) {
        if (this.f_drh_frm_per.STTUS == '1' && (type == 'doc' || (type == 'dfm' && this.f_drh_frm_per.NMHIJ > 0))) {
            if (this.f_drh_frm_per.NUEMP) {
                document.getElementById(fieldName).click();
            } else {
                this.saveInformation('SI', fieldName);
            }
        }
    }

    fileChange(files: File[], field: string, index: number) {
        let nuemp: string = this.f_drh_frm_per.NUEMP;

        this.p_drh_frm_per_doc[index][field] = 0;

        if (files.length > 0) {
            let extension = files[0].name.toLowerCase();

            if (extension.endsWith('.pdf')) {

                if (field == 'PPDFF') {
                    this.f_drh_frm_per_doc[index][field] = files[0].name;
                } else {
                    this.f_drh_frm_per_dfm[index][field] = files[0].name;
                }

                this.wbService.uploadFile(files[0], this.format, nuemp).subscribe(
                    event => {
                        if (event.type === HttpEventType.UploadProgress) {
                            if (field == 'PPDFF') {
                                this.p_drh_frm_per_doc[index][field] = Math.round((event.loaded / event.total) * 100);
                            } else {
                                let f = field == 'PDFCR' ? 'PORCR' : 'PORAN'
                                this.f_drh_frm_per_dfm[index][f] = Math.round((event.loaded / event.total) * 100);
                            }
                        } else if (event.type === HttpEventType.Response) {
                            let response: any = event.body;
                            this.globalService.messageSwal('Subida Archivo', 'Tu archivo se ha subido con éxito: ' + files[0].name, 'success');
                        }
                    },
                    error => {
                        if (error.error) {
                            if (error.error.file == 'prohibitus') {
                                if (field == 'PPDFF') {
                                    this.f_drh_frm_per_doc[index][field] = error.error.file;
                                } else {
                                    this.f_drh_frm_per_dfm[index][field] = error.error.file;
                                }
                                this.globalService.messageSwal('Archivo Bloqueado', error.error.message, 'error');
                            } else {
                                this.globalService.messageSwal('Error', error.error.message, 'error');
                            }
                        } else {
                            console.log(error);
                        }
                    })
            } else {
                if (field == 'PPDFF') {
                    this.f_drh_frm_per_doc[index][field] = '';
                } else {
                    this.f_drh_frm_per_dfm[index][field] = '';
                }

                this.globalService.messageSwal('Extensión Invalida', 'Archivo adjunto no se pudo cargar, solo se permiten archivos pdf', 'warning');
            }
        } else {
            if (field == 'PPDFF') {
                this.f_drh_frm_per_doc[index][field] = '';
            } else {
                this.f_drh_frm_per_dfm[index][field] = '';
            }
        }
    }

    showFile(fieldName: string, index: number) {
        let nuemp: string = this.f_drh_frm_per.NUEMP;

        let file = fieldName == 'PPDFF' ? this.f_drh_frm_per_doc[index][fieldName] : this.f_drh_frm_per_dfm[index][fieldName];

        this.wbService.showFile(file, this.format, nuemp, 'application/pdf').subscribe(x => {
            var newBlob = new Blob([x], { type: 'application/pdf' });

            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(newBlob);
                return;
            }

            const data = window.URL.createObjectURL(newBlob);

            window.open(data, '_blank');
        });
    }

    valCURP(curp: string) {
        if (curp && this.f_drh_frm_per.STTUS == '1') {
            this.wbService.getInformation('41', 'A.STTUS IN (2,3,4) AND A.CCURP=¯' + this.f_drh_frm_per.CCURP + '¯').subscribe(response => {
                if (response.data != null) {
                    this.groupF.controls['CCURP'].setErrors({ 'exists': true });
                } else {
                    this.groupF.controls['CCURP'].setErrors(null);
                }
            })
        }
    }

    showErrorCURP() {
        if (this.groupF && this.groupF.controls['CCURP']) {
            return this.groupF.controls['CCURP'].hasError('exists');
        } else {
            return false;
        }
    }

    valCLABE(curp: string) {
        if (curp && this.f_drh_frm_per.STTUS == '1') {
            this.wbService.getInformation('41', 'A.STTUS IN (2,3,4) AND A.CTABA=¯' + this.f_drh_frm_per.CTABA + '¯').subscribe(response => {
                if (response.data != null) {
                    this.groupF.controls['CTABA'].setErrors({ 'exists': true });
                } else {
                    this.groupF.controls['CTABA'].setErrors(null);
                }
            })
        }
    }

    showErrorCLABE() {
        if (this.groupF && this.groupF.controls['CTABA']) {
            return this.groupF.controls['CTABA'].hasError('exists');
        } else {
            return false;
        }
    }

    valGnr(item, column) {
        if (this.groupF && this.groupF.controls[column]) {
            if (column == 'FCING' || column == 'FCNAC') {
                if (!item) {
                    this.groupF.controls[column].setErrors({ 'fecha': true });
                    this.groupF.form.setErrors({ 'fecha': true });
                    return true;
                } else {
                    this.groupF.controls[column].setErrors(null);
                    this.groupF.form.setErrors(null);
                    return false;
                }
            } else if (column == 'DRCTR' || column == 'NUMPG' || column == 'MONPR') {
                if (this.f_drh_frm_per.NOMBR == 'H') {
                    if (item <= 0) {
                        this.groupF.controls[column].setErrors({ 'mount': true });
                        this.groupF.form.setErrors({ 'mount': true });
                        return true;
                    } else {
                        this.groupF.controls[column].setErrors(null);
                        this.groupF.form.setErrors(null);
                        return false;
                    }
                } else {
                    this.groupF.controls[column].setErrors(null);
                    this.groupF.form.setErrors(null);
                    return false;
                }
            } else if (column == 'SUSTI') {
                if (this.f_drh_frm_per.PLAEX) {
                    if (!item) {
                        this.groupF.controls[column].setErrors({ 'sustituye': true });
                        this.groupF.form.setErrors({ 'sustituye': true });
                        return true;
                    } else {
                        this.groupF.controls[column].setErrors(null);
                        this.groupF.form.setErrors(null);
                        return false;
                    }
                } else {
                    this.groupF.controls[column].setErrors(null);
                    this.groupF.form.setErrors(null);
                    return false;
                }
            } else if (column == 'PLAZA') {
                if (!this.f_drh_frm_per.PLAEX) {
                    if (item <= 0) {
                        this.groupF.controls[column].setErrors({ 'plaza': true });
                        this.groupF.form.setErrors({ 'plaza': true });
                        return true;
                    } else {
                        this.groupF.controls[column].setErrors(null);
                        this.groupF.form.setErrors(null);
                        return false;
                    }
                } else {
                    this.groupF.controls[column].setErrors(null);
                    this.groupF.form.setErrors(null);
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    valDoc(item, element, column, index) {
        if (this.f_drh_frm_per.STTUS == '1') {
            if (this.groupF && this.groupF.controls[column + index]) {
                if (column == 'ENTRE') {
                    if (element.REQUI && !element.ENTRE) {
                        this.groupF.controls[column + index].setErrors({ 'entrega': true });
                        return true;
                    } else {
                        this.groupF.controls[column + index].setErrors(null);
                        return false;
                    }
                } else if (column == 'PPDFF') {
                    if (element.ENTRE && !element.PPDFF) {
                        this.groupF.controls[column + index].setErrors({ 'file': true });
                        return true;
                    } else if (item === 'prohibitus' || ((item.substring(item.lastIndexOf("."))).toLowerCase()) != '.pdf') {
                        this.groupF.controls[column + index].setErrors({ 'extension': true });
                        return true;
                    } else {
                        this.groupF.controls[column + index].setErrors(null);
                        return false;
                    }
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    valTlb(item, element, column, index) {
        if (this.f_drh_frm_per.STTUS == '1') {
            if (this.groupF && this.groupF.controls[column + 'L' + index]) {
                if (column == 'FCINI' || column == 'FCTER') {
                    if (!element[column]) {
                        this.groupF.controls[column + 'L' + index].setErrors({ 'fecha': true });
                        return true;
                    } else {
                        this.groupF.controls[column + 'L' + index].setErrors(null);
                        return false;
                    }
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    valDcc(item, element, column, index) {
        if (this.f_drh_frm_per.STTUS == '1') {
            if (this.groupF && this.groupF.controls[column + 'D' + index]) {
                if (column == 'FCINI' || column == 'FCTER') {
                    if (!element[column]) {
                        this.groupF.controls[column + 'D' + index].setErrors({ 'fecha': true });
                        return true;
                    } else {
                        this.groupF.controls[column + 'D' + index].setErrors(null);
                        return false;
                    }
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    valChilds(item, element, column, index) {
        if (this.f_drh_frm_per.STTUS == '1') {
            if (this.groupF && this.groupF.controls[column + index]) {
                if (column == 'FCNAC') {
                    if (!element.FCNAC) {
                        this.groupF.controls[column + index].setErrors({ 'fecha': true });
                        return true;
                    } else {
                        this.groupF.controls[column + index].setErrors(null);
                        return false;
                    }
                } else if (column == 'PDFCR' || column == 'PDFAN') {
                    if (!element[column]) {
                        this.groupF.controls[column + index].setErrors({ 'file': true });
                        return true;
                    } else if (item === 'prohibitus' || ((item.substring(item.lastIndexOf("."))).toLowerCase()) != '.pdf') {
                        this.groupF.controls[column + index].setErrors({ 'extension': true });
                        return true;
                    } else {
                        this.groupF.controls[column + index].setErrors(null);
                        return false;
                    }
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    saveInformation(type: string, option: string) {
        this.spinner.show();

        let nuemp: string = this.f_drh_frm_per.NUEMP,
            sttus: string = this.f_drh_frm_per.STTUS;

        let dfm = _.cloneDeep(this.f_drh_frm_per_dfm);

        dfm.forEach(item => { delete item.EDADS; delete item.PORCR; delete item.PORAN; });

        let information = {
            'F_DRH_FRM_PER': this.f_drh_frm_per,
            'F_DRH_FRM_PER_DOC': this.f_drh_frm_per_doc
        };

        if (dfm.length > 0) {
            information['F_DRH_FRM_PER_DFM'] = dfm
        }

        if (this.f_drh_frm_per_idi.length > 0) {
            information['F_DRH_FRM_PER_IDI'] = this.f_drh_frm_per_idi
        }

        if (this.f_drh_frm_per_tlb.length > 0) {
            information['F_DRH_FRM_PER_TLB'] = this.f_drh_frm_per_tlb
        }

        if (this.f_drh_frm_per_dcc.length > 0) {
            information['F_DRH_FRM_PER_DCC'] = this.f_drh_frm_per_dcc
        }

        this.wbService.saveInformationForms(type, this.format, nuemp, sttus, JSON.stringify(information)).subscribe(
            response => {
                this.globalService.messageSwal('Información Almacenada', 'La información ha sido guardada con el folio: ' + response.FOLIO, 'success');

                this.spinner.hide();
            },
            error => {
                this.spinner.hide();
                this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
                console.log(error);
            })
    }

    createFile(option: string) {
        let nuemp: string = this.f_drh_frm_per.NUEMP;

        this.wbService.createFile(option, this.format, nuemp).subscribe(x => {
            if (option == 'VP') {
                var newBlob = new Blob([x], { type: 'application/pdf' });

                if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                    window.navigator.msSaveOrOpenBlob(newBlob);
                    return;
                }

                const data = window.URL.createObjectURL(newBlob);

                var link = document.createElement('a');
                link.href = data;
                link.download = nuemp + '.pdf';

                link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

                setTimeout(function () {
                    window.URL.revokeObjectURL(data);
                    link.remove();
                }, 100);
            }
        });
    }

    downloadFile(option: string) {
        let nuemp: string = this.f_drh_frm_per.NUEMP;

        this.wbService.downloadFile(option, this.format, nuemp).subscribe(x => {

            var newBlob = new Blob([x], { type: 'application/pdf' });

            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(newBlob);
                return;
            }

            const data = window.URL.createObjectURL(newBlob);

            var link = document.createElement('a');
            link.href = data;
            link.download = nuemp + '.pdf';

            link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

            setTimeout(function () {
                window.URL.revokeObjectURL(data);
                link.remove();
            }, 100);
        });
    }

    disabled() {
        if (this.editExp && !this.NaN()) {
            return false
        } else {
            return true
        }
    }

    NaN() {
        return this.f_drh_frm_per.NUEMP ? isNaN(Number(this.f_drh_frm_per.NUEMP)) : true
    }
}