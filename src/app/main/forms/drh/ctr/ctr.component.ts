import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { MatDialog } from '@angular/material/dialog';
import { HttpEventType } from '@angular/common/http';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatTableDataSource } from '@angular/material';

import { AuthService } from '../../../../services/auth.service';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';

import { DialogRecover } from '../../../../custom/dialogs/recover/recover.component';
import { DialogConfirm } from '../../../../custom/dialogs/confirm/confirm.component'

import { NgxSpinnerService } from 'ngx-spinner';

import * as moment from 'moment';
import * as _ from 'lodash';

import { F_DRH_FRM_CTR, aditional } from '../../../../models/forms/DRH/FRM/CTR/drh.frm.ctr.model';
import { CustomErrorStateMatcher } from '../../../../models/global/error';

@Component({
    selector: 'drh_ctr',
    templateUrl: './ctr.component.html',
    styleUrls: ['./ctr.component.scss'],
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es-MX' },
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
    ]
})
export class DRHCTRComponent implements OnInit, OnDestroy {
    @ViewChild('groupForm', { static: false }) groupF: NgForm;

    public format: string;
    public error = new CustomErrorStateMatcher();

    public f_drh_frm_ctr: F_DRH_FRM_CTR = new F_DRH_FRM_CTR();
    public aditional: aditional = new aditional();

    public paises: Array<any>;
    public regiones: Array<any>;
    public adscripciones: Array<any>;
    public puestos: Array<any>;
    public perfiles: Array<any>;
    public titulos: Array<any>;
    public docs: Array<any>;
    public estadoCivil: Array<any>;
    public genero: Array<any>;
    public escolaridad: Array<any>;
    public sit_aca: Array<any>;
    public hijos: Array<any> = [];
    public tipoPlaza: Array<any>;
    public tipoBaja: Array<any>;
    public tipoNombramiento: Array<any>;
    public tipoPago: Array<any>;
    public mediosPago: Array<any>;
    public pDispBajas: Array<any>;
    public pDisp: Array<any>;
    public plazas: Array<any>;
    public funcionariosAll: Array<any>;
    public funcionarios: Array<any>;
    public filteredAU: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public AUFilterCtrl: FormControl = new FormControl();

    public countCP: Number = 0;
    public countRP: Number = 0;

    protected _onDestroy = new Subject<void>();

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     */
    constructor(public authService: AuthService,
        private wbService: WBService,
        private globalService: GlobalService,
        public dialog: MatDialog,
        private cdr: ChangeDetectorRef,
        private spinner: NgxSpinnerService) {

        this.format = 'DRH-FRM-CTR';
        this.f_drh_frm_ctr.STTUS = '1',
            this.f_drh_frm_ctr.EJERC = sessionStorage.getItem('year'),
            this.f_drh_frm_ctr.USUAR = this.authService.user.user_name;

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    ngAfterContentChecked() {
        this.cdr.detectChanges();
    }

    ngOnInit() {
        this.getCounts();

        this.wbService.getInformation('1', '').subscribe(response => {
            this.f_drh_frm_ctr.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
        })

        this.wbService.getInformation('70', '').subscribe(response => {
            this.paises = response.data;
        })

        this.wbService.getInformation('3', '').subscribe(response => {
            this.regiones = response.data;
        })

        this.wbService.getInformation('4', '').subscribe(response => {
            this.adscripciones = response.data;
        })

        this.wbService.getInformation('41', '').subscribe(response => {
            this.funcionariosAll = response.data;
        })

        this.wbService.getInformation('5', 'CAUBA=¯¯ AND CUSER IS NOT NULL AND A.STTUS=4').subscribe(response => {
            this.funcionarios = response.data;
            this.filteredAU.next(this.funcionarios)
            this.puesto(this.f_drh_frm_ctr.USUAR, 'ELPUE');
        })

        this.AUFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                this.filterAU();
            })

        this.wbService.getInformation('10', '').subscribe(response => {
            this.puestos = response.data;
        })

        this.wbService.getInformation('39', '').subscribe(response => {
            this.perfiles = response.data;
        })

        this.wbService.getInformation('40', '').subscribe(response => {
            this.titulos = response.data;
        })

        this.wbService.getInformation('27', '').subscribe(response => {
            this.mediosPago = response.data;
        })

        this.wbService.getInformation('51', '').subscribe(response => {
            this.estadoCivil = response.data;
        })

        this.wbService.getInformation('52', '').subscribe(response => {
            this.genero = response.data;
        })

        this.wbService.getInformation('53', '').subscribe(response => {
            this.escolaridad = response.data;
        })

        this.wbService.getInformation('54', '').subscribe(response => {
            this.tipoPlaza = response.data;
        })

        this.wbService.getInformation('55', '').subscribe(response => {
            this.sit_aca = response.data;
        })

        this.wbService.getInformation('56', '').subscribe(response => {
            this.tipoBaja = response.data;
        })

        this.wbService.getInformation('57', '').subscribe(response => {
            this.tipoNombramiento = response.data;
        })

        this.wbService.getInformation('58', '').subscribe(response => {
            this.tipoPago = response.data;
        })

        this.wbService.getInformation('60', 'REGIO=¯' + this.authService.user.office + '¯').subscribe(response => {
            this.plazas = response.data;
        })
    }

    ngOnDestroy() {
        this._onDestroy.next();
        this._onDestroy.complete();
    }

    getCounts() {
        this.wbService.getCount('63', 'EJERC=' + sessionStorage.getItem('year')).subscribe(response => {
            if (response.count) {
                this.countCP = response.count;
            } else {
                this.countCP = 0;
            }
        })
    }

    clear() {
        this.f_drh_frm_ctr = new F_DRH_FRM_CTR();
        this.aditional = new aditional();

        this.wbService.getInformation('1', '').subscribe(response => {
            this.f_drh_frm_ctr.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
        })

        this.f_drh_frm_ctr.STTUS = '1',
            this.f_drh_frm_ctr.EJERC = sessionStorage.getItem('year'),
            this.f_drh_frm_ctr.USUAR = this.authService.user.user_name;
        this.puesto(this.f_drh_frm_ctr.USUAR, 'ELPUE');
    }

    protected filterAU() {
        if (!this.funcionarios) {
            return;
        }

        let search = this.AUFilterCtrl.value;
        if (!search) {
            this.filteredAU.next(this.funcionarios.slice());
            return;
        } else {
            search = search.toLowerCase();
        }

        this.filteredAU.next(
            this.funcionarios.filter(funcionario => funcionario.DESCR.toLowerCase().indexOf(search) > -1)
        );
    }

    puesto(func: string, puest: string) {
        let res = this.funcionarios.find(funcionario => funcionario.CUSER === func);

        this.aditional[puest] = res.DSCPU;
    }

    calcEdad(fcnac) {
        if (fcnac) {
            return Math.floor(moment(new Date()).diff(moment(fcnac, "MM/DD/YYYY"), 'years', true));
        }

        return 0
    }

    limpia() {
        this.f_drh_frm_ctr.SUSTI = '',
            this.f_drh_frm_ctr.SINDI = false,
            this.f_drh_frm_ctr.PLAZA = 0,
            this.f_drh_frm_ctr.DESCR = '',
            this.f_drh_frm_ctr.DESC2 = '',
            this.f_drh_frm_ctr.TIPPL = '',
            this.f_drh_frm_ctr.NIVEL = '',
            this.f_drh_frm_ctr.NOMBR = '',
            this.f_drh_frm_ctr.SUMEN = 0,
            this.f_drh_frm_ctr.COMGA = 0,
            this.f_drh_frm_ctr.SMEIN = 0;
    }

    openDialog(option, type) {
        let title = '',
            object = '',
            columns = [],
            filter = '';

        if (type == 'R') {
            object = '67'
            title = 'Folios Creados'
            columns = [
                { headerName: 'Folio', field: 'FOLIO', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                { headerName: 'Tipo de Contratación', field: 'TIPCT', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                { headerName: 'Región', field: 'DSREG', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                { headerName: 'Nº Empleado', field: 'NUEMP', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                { headerName: 'Funcionario', field: 'NOMBR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                { headerName: 'Fecha Ingreso', field: 'FCING', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                { headerName: 'Fecha Término', field: 'FCTER', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true }
            ]
            filter = 'USUAR=¯' + this.authService.user.user_name + '¯ AND STTUS=4'
        } else if (type == 'CP') {
            object = '63'
            title = 'Contrataciones Pendientes'
            columns = [
                { headerName: 'Región', field: 'DSREG', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                { headerName: 'Nº Empleado', field: 'NUEMP', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                { headerName: 'Funcionario', field: 'NOMBR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                { headerName: 'Nombramiento', field: 'NOMBT', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                { headerName: 'Fecha Ingreso', field: 'FCING', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                { headerName: 'Fecha Término', field: 'FCTER', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true }
            ]
            filter = 'EJERC=' + sessionStorage.getItem('year')
        }

        const dialogRef = this.dialog.open(DialogRecover, {
            width: '1000px',
            height: '650px',
            data: { title, columns, object, filter }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result && (result.NUEMP || result.FOLIO)) {
                this.getInformation(result, type);
            }
        });
    }

    getInformation(data, type) {
        if (type == 'CP') {
            this.wbService.getInformationForms('DRH-FRM-PER', 'NUEMP=¯' + data.NUEMP + '¯').subscribe(response => {
                let rec_drh_frm_per = response.F_DRH_FRM_PER;

                if (rec_drh_frm_per) {
                    this.f_drh_frm_ctr.STTUS = '1',
                        this.f_drh_frm_ctr.EJERC = sessionStorage.getItem('year'),
                        this.f_drh_frm_ctr.USUAR = this.authService.user.user_name,
                        this.f_drh_frm_ctr.FECHA = this.globalService.getDateUTC(rec_drh_frm_per[0].FECHA),
                        this.f_drh_frm_ctr.REGIO = rec_drh_frm_per[0].REGIO,
                        this.f_drh_frm_ctr.SOLIC = rec_drh_frm_per[0].NUEMP,
                        this.f_drh_frm_ctr.TIPCT = 'N',
                        this.f_drh_frm_ctr.NUEMP = rec_drh_frm_per[0].NUEMP,
                        this.f_drh_frm_ctr.TITUL = rec_drh_frm_per[0].TITUL,
                        this.f_drh_frm_ctr.NAMEE = rec_drh_frm_per[0].NAMEE,
                        this.f_drh_frm_ctr.PRIAP = rec_drh_frm_per[0].PRIAP,
                        this.f_drh_frm_ctr.SEGAP = rec_drh_frm_per[0].SEGAP,
                        this.f_drh_frm_ctr.FCNAC = rec_drh_frm_per[0].FCNAC ? this.globalService.getDateUTC(rec_drh_frm_per[0].FCNAC) : null,
                        this.f_drh_frm_ctr.ESCOL = rec_drh_frm_per[0].ESCOL,
                        this.f_drh_frm_ctr.CARRE = rec_drh_frm_per[0].CARRE,
                        this.f_drh_frm_ctr.SITAC = rec_drh_frm_per[0].SITAC,
                        this.f_drh_frm_ctr.INSED = rec_drh_frm_per[0].INSED,
                        this.f_drh_frm_ctr.PINED = rec_drh_frm_per[0].PINED,
                        this.f_drh_frm_ctr.GENER = rec_drh_frm_per[0].GENER,
                        this.f_drh_frm_ctr.ESCIV = rec_drh_frm_per[0].ESCIV,
                        this.f_drh_frm_ctr.ESCON = rec_drh_frm_per[0].ESCON,
                        this.f_drh_frm_ctr.RRFCC = rec_drh_frm_per[0].RRFCC,
                        this.f_drh_frm_ctr.CCURP = rec_drh_frm_per[0].CCURP,
                        this.f_drh_frm_ctr.NACIO = rec_drh_frm_per[0].NACIO,
                        this.f_drh_frm_ctr.PAINA = rec_drh_frm_per[0].PAINA,
                        this.f_drh_frm_ctr.ENTNA = rec_drh_frm_per[0].ENTNA,
                        this.f_drh_frm_ctr.NMNSS = rec_drh_frm_per[0].NMNSS,
                        this.f_drh_frm_ctr.CALLE = rec_drh_frm_per[0].CALLE,
                        this.f_drh_frm_ctr.NMEXT = rec_drh_frm_per[0].NMEXT,
                        this.f_drh_frm_ctr.NMINT = rec_drh_frm_per[0].NMINT,
                        this.f_drh_frm_ctr.COLOC = rec_drh_frm_per[0].COLOC,
                        this.f_drh_frm_ctr.CODPO = rec_drh_frm_per[0].CODPO,
                        this.f_drh_frm_ctr.MUDEL = rec_drh_frm_per[0].MUDEL,
                        this.f_drh_frm_ctr.ENTFE = rec_drh_frm_per[0].ENTFE,
                        this.f_drh_frm_ctr.FOINE = rec_drh_frm_per[0].FOINE,
                        this.f_drh_frm_ctr.REGOP = rec_drh_frm_per[0].REGOP,
                        this.f_drh_frm_ctr.PLAEX = rec_drh_frm_per[0].PLAEX,
                        this.f_drh_frm_ctr.SUSTI = rec_drh_frm_per[0].SUSTI,
                        this.f_drh_frm_ctr.SINDI = rec_drh_frm_per[0].SINDI,
                        this.f_drh_frm_ctr.JORLB = rec_drh_frm_per[0].JORLB,
                        this.f_drh_frm_ctr.HORAR = rec_drh_frm_per[0].HORAR,
                        this.f_drh_frm_ctr.HORDA = rec_drh_frm_per[0].HORDA,
                        this.f_drh_frm_ctr.HORAA = rec_drh_frm_per[0].HORAA,
                        this.f_drh_frm_ctr.HORDP = rec_drh_frm_per[0].HORDP,
                        this.f_drh_frm_ctr.HORAP = rec_drh_frm_per[0].HORAP,
                        this.f_drh_frm_ctr.EMAIL = rec_drh_frm_per[0].EMAIL,
                        this.f_drh_frm_ctr.FUNCI = rec_drh_frm_per[0].FUNCI,
                        this.f_drh_frm_ctr.FCING = rec_drh_frm_per[0].FCING ? this.globalService.getDateUTC(rec_drh_frm_per[0].FCING) : null,
                        this.f_drh_frm_ctr.FCINI = rec_drh_frm_per[0].FCINI ? this.globalService.getDateUTC(rec_drh_frm_per[0].FCINI) : null,
                        this.f_drh_frm_ctr.FCTER = rec_drh_frm_per[0].FCTER ? this.globalService.getDateUTC(rec_drh_frm_per[0].FCTER) : null,
                        this.f_drh_frm_ctr.OBSER = rec_drh_frm_per[0].OBSER,
                        this.f_drh_frm_ctr.DIAPA = rec_drh_frm_per[0].DIAPA,
                        this.f_drh_frm_ctr.TIPAG = rec_drh_frm_per[0].TIPAG,
                        this.f_drh_frm_ctr.MDPAG = rec_drh_frm_per[0].MDPAG;

                        if (rec_drh_frm_per[0].NOMBR == 'H') {
                            this.f_drh_frm_ctr.ADSCR = rec_drh_frm_per[0].ADSCR,
                            this.f_drh_frm_ctr.PUEST = rec_drh_frm_per[0].PUEST,
                            this.f_drh_frm_ctr.PLAZA = rec_drh_frm_per[0].PLAZA,
                            this.f_drh_frm_ctr.DESCR = rec_drh_frm_per[0].DESCR,
                            this.f_drh_frm_ctr.DESC2 = rec_drh_frm_per[0].DESC2,
                            this.f_drh_frm_ctr.NIVEL = rec_drh_frm_per[0].NIVEL,
                            this.f_drh_frm_ctr.TIPPL = rec_drh_frm_per[0].TIPPL,
                            this.f_drh_frm_ctr.NOMBR = rec_drh_frm_per[0].NOMBR,
                            this.f_drh_frm_ctr.DRCTR = rec_drh_frm_per[0].DRCTR,
                            this.f_drh_frm_ctr.NUMPG = rec_drh_frm_per[0].NUMPG,
                            this.f_drh_frm_ctr.MONPR = rec_drh_frm_per[0].MONPR,
                            this.f_drh_frm_ctr.MONTT = rec_drh_frm_per[0].MONTT,
                            this.f_drh_frm_ctr.SUMEN = rec_drh_frm_per[0].SUMEN,
                            this.f_drh_frm_ctr.COMGA = rec_drh_frm_per[0].COMGA,
                            this.f_drh_frm_ctr.SMEIN = rec_drh_frm_per[0].SMEIN;
                        } else {
                            this.wbService.getInformation('60', 'NUEMP=¯' + data.NUEMP + '¯').subscribe(response => {
                                if (response.data && response.data[0]) {
                                    this.f_drh_frm_ctr.NOMBR = response.data[0].NOMBR,
                                    this.f_drh_frm_ctr.ADSCR = response.data[0].ADSCR,
                                    this.f_drh_frm_ctr.PUEST = response.data[0].PUEST,
                                    this.f_drh_frm_ctr.PLAZA = response.data[0].PLAZA,
                                    this.f_drh_frm_ctr.DESCR = response.data[0].DESC1,
                                    this.f_drh_frm_ctr.DESC2 = response.data[0].DESC2,
                                    this.f_drh_frm_ctr.TIPPL = response.data[0].TIPPL,
                                    this.f_drh_frm_ctr.NIVEL = response.data[0].NIVEL,
                                    this.f_drh_frm_ctr.SUMEN = response.data[0].SUMEN,
                                    this.f_drh_frm_ctr.COMGA = response.data[0].COMGA,
                                    this.f_drh_frm_ctr.SMEIN = response.data[0].SMEIN;
                                }
                            })
                        }

                    let nombr = this.tipoNombramiento.find(element => element.NOMBR == this.f_drh_frm_ctr.NOMBR)

                    if (nombr) {
                        this.f_drh_frm_ctr.MESCT = nombr.VIGEN;
                    }

                    this.aditional.EDADS = this.calcEdad(this.f_drh_frm_ctr.FCNAC);
                    this.puesto(this.f_drh_frm_ctr.USUAR, 'ELPUE');

                    this.wbService.getInformationForms(this.format, 'REGIO=¯' + data.REGIO + '¯ AND SOLIC=¯' + data.FOLIO + '¯').subscribe(response => {
                        let rec_drh_frm_ctr = response.F_DRH_FRM_CTR;

                        if (rec_drh_frm_ctr) {
                            this.f_drh_frm_ctr.FOLIO = rec_drh_frm_ctr[0].FOLIO,
                                this.f_drh_frm_ctr.FIRAU = rec_drh_frm_ctr[0].FIRAU,
                                this.f_drh_frm_ctr.AUTOU = rec_drh_frm_ctr[0].AUTOU;

                            this.puesto(this.f_drh_frm_ctr.AUTOU, 'AUPUE');
                        } else {
                            this.f_drh_frm_ctr.FOLIO = '',
                                this.f_drh_frm_ctr.FIRAU = false,
                                this.aditional.AUPUE = '';

                            this.getAut();
                        }
                    })
                } else {
                    this.globalService.messageSwal('Información no encontrada', 'La información del empleado ' + data.NOMBR + ' no se encontro en el servidor', 'error');
                }
            })
        } else if (type == 'R') {
            this.wbService.getInformationForms(this.format, 'FOLIO=¯' + data.FOLIO + '¯').subscribe(response => {
                let rec_drh_frm_ctr = response.F_DRH_FRM_CTR;

                if (rec_drh_frm_ctr) {
                    this.f_drh_frm_ctr.FOLIO = rec_drh_frm_ctr[0].FOLIO,
                        this.f_drh_frm_ctr.STTUS = rec_drh_frm_ctr[0].STTUS,
                        this.f_drh_frm_ctr.EJERC = rec_drh_frm_ctr[0].EJERC,
                        this.f_drh_frm_ctr.USUAR = rec_drh_frm_ctr[0].USUAR,
                        this.f_drh_frm_ctr.KEYUI = rec_drh_frm_ctr[0].KEYUI,
                        this.f_drh_frm_ctr.FECHA = rec_drh_frm_ctr[0].FECHA ? this.globalService.getDateUTC(rec_drh_frm_ctr[0].FECHA) : null,
                        this.f_drh_frm_ctr.REGIO = rec_drh_frm_ctr[0].REGIO,
                        this.f_drh_frm_ctr.SOLIC = rec_drh_frm_ctr[0].SOLIC,
                        this.f_drh_frm_ctr.TIPCT = rec_drh_frm_ctr[0].TIPCT,
                        this.f_drh_frm_ctr.MESCT = rec_drh_frm_ctr[0].MESCT,
                        this.f_drh_frm_ctr.FIRAU = rec_drh_frm_ctr[0].FIRAU,
                        this.f_drh_frm_ctr.NUEMP = rec_drh_frm_ctr[0].NUEMP,
                        this.f_drh_frm_ctr.TITUL = rec_drh_frm_ctr[0].TITUL,
                        this.f_drh_frm_ctr.NAMEE = rec_drh_frm_ctr[0].NAMEE,
                        this.f_drh_frm_ctr.PRIAP = rec_drh_frm_ctr[0].PRIAP,
                        this.f_drh_frm_ctr.SEGAP = rec_drh_frm_ctr[0].SEGAP,
                        this.f_drh_frm_ctr.FCNAC = rec_drh_frm_ctr[0].FCNAC ? this.globalService.getDateUTC(rec_drh_frm_ctr[0].FCNAC) : null,
                        this.f_drh_frm_ctr.ESCOL = rec_drh_frm_ctr[0].ESCOL,
                        this.f_drh_frm_ctr.CARRE = rec_drh_frm_ctr[0].CARRE,
                        this.f_drh_frm_ctr.SITAC = rec_drh_frm_ctr[0].SITAC,
                        this.f_drh_frm_ctr.INSED = rec_drh_frm_ctr[0].INSED,
                        this.f_drh_frm_ctr.PINED = rec_drh_frm_ctr[0].PINED,
                        this.f_drh_frm_ctr.GENER = rec_drh_frm_ctr[0].GENER,
                        this.f_drh_frm_ctr.ESCIV = rec_drh_frm_ctr[0].ESCIV,
                        this.f_drh_frm_ctr.ESCON = rec_drh_frm_ctr[0].ESCON,
                        this.f_drh_frm_ctr.RRFCC = rec_drh_frm_ctr[0].RRFCC,
                        this.f_drh_frm_ctr.CCURP = rec_drh_frm_ctr[0].CCURP,
                        this.f_drh_frm_ctr.NACIO = rec_drh_frm_ctr[0].NACIO,
                        this.f_drh_frm_ctr.ENTNA = rec_drh_frm_ctr[0].ENTNA,
                        this.f_drh_frm_ctr.NMNSS = rec_drh_frm_ctr[0].NMNSS,
                        this.f_drh_frm_ctr.CALLE = rec_drh_frm_ctr[0].CALLE,
                        this.f_drh_frm_ctr.NMEXT = rec_drh_frm_ctr[0].NMEXT,
                        this.f_drh_frm_ctr.NMINT = rec_drh_frm_ctr[0].NMINT,
                        this.f_drh_frm_ctr.COLOC = rec_drh_frm_ctr[0].COLOC,
                        this.f_drh_frm_ctr.CODPO = rec_drh_frm_ctr[0].CODPO,
                        this.f_drh_frm_ctr.MUDEL = rec_drh_frm_ctr[0].MUDEL,
                        this.f_drh_frm_ctr.ENTFE = rec_drh_frm_ctr[0].ENTFE,
                        this.f_drh_frm_ctr.FOINE = rec_drh_frm_ctr[0].FOINE,
                        this.f_drh_frm_ctr.ADSCR = rec_drh_frm_ctr[0].ADSCR,
                        this.f_drh_frm_ctr.REGOP = rec_drh_frm_ctr[0].REGOP,
                        this.f_drh_frm_ctr.PUEST = rec_drh_frm_ctr[0].PUEST,
                        this.f_drh_frm_ctr.PLAEX = rec_drh_frm_ctr[0].PLAEX,
                        this.f_drh_frm_ctr.SUSTI = rec_drh_frm_ctr[0].SUSTI,
                        this.f_drh_frm_ctr.PLAZA = rec_drh_frm_ctr[0].PLAZA,
                        this.f_drh_frm_ctr.DESCR = rec_drh_frm_ctr[0].DESCR,
                        this.f_drh_frm_ctr.DESC2 = rec_drh_frm_ctr[0].DESC2,
                        this.f_drh_frm_ctr.NIVEL = rec_drh_frm_ctr[0].NIVEL,
                        this.f_drh_frm_ctr.TIPPL = rec_drh_frm_ctr[0].TIPPL,
                        this.f_drh_frm_ctr.NOMBR = rec_drh_frm_ctr[0].NOMBR,
                        this.f_drh_frm_ctr.DRCTR = rec_drh_frm_ctr[0].DRCTR,
                        this.f_drh_frm_ctr.NUMPG = rec_drh_frm_ctr[0].NUMPG,
                        this.f_drh_frm_ctr.MONPR = rec_drh_frm_ctr[0].MONPR,
                        this.f_drh_frm_ctr.MONTT = rec_drh_frm_ctr[0].MONTT,
                        this.f_drh_frm_ctr.SUMEN = rec_drh_frm_ctr[0].SUMEN,
                        this.f_drh_frm_ctr.COMGA = rec_drh_frm_ctr[0].COMGA,
                        this.f_drh_frm_ctr.SMEIN = rec_drh_frm_ctr[0].SMEIN,
                        this.f_drh_frm_ctr.SINDI = rec_drh_frm_ctr[0].SINDI,
                        this.f_drh_frm_ctr.JORLB = rec_drh_frm_ctr[0].JORLB,
                        this.f_drh_frm_ctr.HORAR = rec_drh_frm_ctr[0].HORAR,
                        this.f_drh_frm_ctr.HORDA = rec_drh_frm_ctr[0].HORDA,
                        this.f_drh_frm_ctr.HORAA = rec_drh_frm_ctr[0].HORAA,
                        this.f_drh_frm_ctr.HORDP = rec_drh_frm_ctr[0].HORDP,
                        this.f_drh_frm_ctr.HORAP = rec_drh_frm_ctr[0].HORAP,
                        this.f_drh_frm_ctr.EMAIL = rec_drh_frm_ctr[0].EMAIL,
                        this.f_drh_frm_ctr.FUNCI = rec_drh_frm_ctr[0].FUNCI,
                        this.f_drh_frm_ctr.FCING = rec_drh_frm_ctr[0].FCING ? this.globalService.getDateUTC(rec_drh_frm_ctr[0].FCING) : null,
                        this.f_drh_frm_ctr.FCINI = rec_drh_frm_ctr[0].FCINI ? this.globalService.getDateUTC(rec_drh_frm_ctr[0].FCINI) : null,
                        this.f_drh_frm_ctr.FCTER = rec_drh_frm_ctr[0].FCTER ? this.globalService.getDateUTC(rec_drh_frm_ctr[0].FCTER) : null,
                        this.f_drh_frm_ctr.OBSER = rec_drh_frm_ctr[0].OBSER,
                        this.f_drh_frm_ctr.DIAPA = rec_drh_frm_ctr[0].DIAPA,
                        this.f_drh_frm_ctr.TIPAG = rec_drh_frm_ctr[0].TIPAG,
                        this.f_drh_frm_ctr.MDPAG = rec_drh_frm_ctr[0].MDPAG,
                        this.f_drh_frm_ctr.AUTOU = rec_drh_frm_ctr[0].AUTOU;

                    this.getNacionalidad();
                    this.aditional.EDADS = this.calcEdad(this.f_drh_frm_ctr.FCNAC);
                    this.puesto(this.f_drh_frm_ctr.USUAR, 'ELPUE');
                    this.puesto(this.f_drh_frm_ctr.AUTOU, 'AUPUE');
                } else {
                    this.globalService.messageSwal('Información no encontrada', 'La información del empleado ' + data.NOMBR + ' no se encontro en el servidor', 'error');
                }
            })
        }
    }

    saveInformation(type: string, option: string) {
        this.spinner.show();

        let folio: string = this.f_drh_frm_ctr.FOLIO,
            sttus: string = this.f_drh_frm_ctr.STTUS;

        let information = {
            'F_DRH_FRM_CTR': this.f_drh_frm_ctr
        };

        this.wbService.saveInformationForms(type, this.format, folio, sttus, JSON.stringify(information)).subscribe(
            response => {
                this.getCounts();

                this.f_drh_frm_ctr.FOLIO = response.FOLIO;

                if (type == 'CL') {
                    this.globalService.messageSwal('Información Almacenada', 'Información enviada con el folio: ' + response.FOLIO, 'success');
                    this.f_drh_frm_ctr.STTUS = '4';
                } else {
                    this.globalService.messageSwal('Información Actualizada', 'Tu información con el folio ' + response.FOLIO + ' ha sido guarda', 'success');
                }

                this.spinner.hide();

                if (option == 'VP' || option == 'OK') {
                    this.createFile(option);
                } else if (option.length >= 5) {
                    document.getElementById(option).click();
                }
            },
            error => {
                this.spinner.hide();
                this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
                console.log(error);
            })
    }

    getNacionalidad() {
        const nacion = this.paises.find(item => item.PAISS == this.f_drh_frm_ctr.PAINA)

        if (nacion && this.f_drh_frm_ctr.STTUS == '1' && this.f_drh_frm_ctr.GENER) {
            if (this.f_drh_frm_ctr.GENER == 'F') {
                this.f_drh_frm_ctr.NACIO = nacion.NACIF
            } else {
                this.f_drh_frm_ctr.NACIO = nacion.NACIM
            }
        } else {
            this.f_drh_frm_ctr.NACIO = '';
        }
    }

    createFile(option: string) {
        let folio: string = this.f_drh_frm_ctr.FOLIO;

        this.wbService.createFile(option, this.format, folio).subscribe(x => {
            var newBlob = new Blob([x], { type: 'application/pdf' });

            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(newBlob);
                return;
            }

            const data = window.URL.createObjectURL(newBlob);

            var link = document.createElement('a');
            link.href = data;
            link.download = folio + '-' + option + '.pdf';

            link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

            setTimeout(function () {
                window.URL.revokeObjectURL(data);
                link.remove();
            }, 100);
        });
    }

    downloadFile(option: string) {
        let folio: string = this.f_drh_frm_ctr.FOLIO;

        this.wbService.downloadFile(option, this.format, folio).subscribe(x => {

            var newBlob = new Blob([x], { type: 'application/pdf' });

            if (window.navigator && window.navigator.msSaveOrOpenBlob) {
                window.navigator.msSaveOrOpenBlob(newBlob);
                return;
            }

            const data = window.URL.createObjectURL(newBlob);

            var link = document.createElement('a');
            link.href = data;
            link.download = folio + '-' + option + '.pdf';

            link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));

            setTimeout(function () {
                window.URL.revokeObjectURL(data);
                link.remove();
            }, 100);
        });
    }

    disabled() {
        if (this.f_drh_frm_ctr.STTUS == '1') {
            return false;
        } else {
            return true;
        }
    }

    getAut() {
        if (this.f_drh_frm_ctr.FIRAU) {
            this.f_drh_frm_ctr.AUTOU = '';
            this.aditional.AUPUE = '';
        } else {
            this.wbService.getInformation('5', 'REGOP=¯09¯ and ADSCR=¯DAF¯ and PUEST=¯CFM3301561¯ and PLAZA=1 and CAUBA=¯¯').subscribe(response => {
                if (response.data) {
                    this.f_drh_frm_ctr.AUTOU = response.data[0].CUSER;
                    this.aditional.AUPUE = 'CFM3301561';
                } else {
                    this.f_drh_frm_ctr.FIRAU = true;
                    this.getAut();
                }
            })
        }
    }
}