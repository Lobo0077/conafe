import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { HttpEventType } from '@angular/common/http';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatTableDataSource } from '@angular/material';

import { AuthService } from '../../../../services/auth.service';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';

import { DialogRecover } from '../../../../custom/dialogs/recover/recover.component';
import { DialogConfirm } from '../../../../custom/dialogs/confirm/confirm.component'

import { NgxSpinnerService } from 'ngx-spinner';

import * as moment from 'moment';

import { F_DRH_FRM_BPR, aditional } from '../../../../models/forms/DRH/FRM/BPR/drh.frm.bpr.model';
import { CustomErrorStateMatcher } from '../../../../models/global/error';

@Component({
    selector: 'drh_bpr',
    templateUrl: './bpr.component.html',
    styleUrls: ['./bpr.component.scss'],
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es-MX' },
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
    ]
})
export class DRHBPRComponent implements OnInit, OnDestroy {
    @ViewChild('groupForm', { static: false }) groupF: NgForm;

    public format: string;
    public error = new CustomErrorStateMatcher();

    public f_drh_frm_bpr: F_DRH_FRM_BPR = new F_DRH_FRM_BPR();
    public aditional: aditional = new aditional();

    public regiones: Array<any>;
    public adscripciones: Array<any>;
    public puestos: Array<any>;
    public tipoPlaza: Array<any>;
    public tipoBaja: Array<any>;
    public tipoNombramiento: Array<any>;
    public funcBajas: Array<any>;
    public BJFilterCtrl: FormControl = new FormControl();
    public funcionarios: Array<any>;
    public filteredBJ: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public filteredAU: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public AUFilterCtrl: FormControl = new FormControl();
    public filteredVB: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public VBFilterCtrl: FormControl = new FormControl();

    public perVB: boolean = false;
    public countVB: Number = 0;
    public perAU: boolean = false;
    public countAU: Number = 0;

    protected _onDestroy = new Subject<void>();

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     */
    constructor(public authService: AuthService,
        private wbService: WBService,
        private globalService: GlobalService,
        public dialog: MatDialog,
        private cdr: ChangeDetectorRef,
        private spinner: NgxSpinnerService) {

        this.format = 'DRH-FRM-BPR';
        this.f_drh_frm_bpr.STTUS = '1',
        this.f_drh_frm_bpr.USUAR = this.authService.user.user_name,
        this.f_drh_frm_bpr.EJERC=Number(sessionStorage.getItem('year'));

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    ngAfterContentChecked() {
        this.cdr.detectChanges();
    }

    ngOnInit() {
        this.getPermission();

        this.wbService.getInformation('1', '').subscribe(response => {
            this.f_drh_frm_bpr.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
        })

        this.wbService.getInformation('3', '').subscribe(response => {
            this.regiones = response.data;
        })

        this.wbService.getInformation('4', '').subscribe(response => {
            this.adscripciones = response.data;
        })

        this.wbService.getInformation('5', 'CAUBA=¯¯ AND NUEMP != ¯¯ AND A.STTUS=4').subscribe(response => {
            this.funcBajas = response.data;
            this.filteredBJ.next(this.funcBajas)
        })

        this.BJFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                this.filterBJ();
            })

        this.wbService.getInformation('5', 'CAUBA=¯¯ AND CUSER IS NOT NULL AND A.STTUS=4').subscribe(response => {
            this.funcionarios = response.data;
            this.filteredVB.next(this.funcionarios)
            this.filteredAU.next(this.funcionarios)
            this.puesto(this.f_drh_frm_bpr.USUAR, 'ELPUE');
        })

        this.VBFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                this.filterVB();
            })

        this.AUFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                this.filterAU();
            })

        this.wbService.getInformation('10', '').subscribe(response => {
            this.puestos = response.data;
        })

        this.wbService.getInformation('54', '').subscribe(response => {
            this.tipoPlaza = response.data;
        })

        this.wbService.getInformation('56', '').subscribe(response => {
            this.tipoBaja = response.data;
        })

        this.wbService.getInformation('57', '').subscribe(response => {
            this.tipoNombramiento = response.data;
        })
    }

    ngOnDestroy() {
        this._onDestroy.next();
        this._onDestroy.complete();
    }

    getPermission() {
        this.wbService.getInformation('0', 'TYPPE=¯permission¯ AND PAREN=¯' + this.format + '¯', '¯' + this.authService.user.user_name + '¯').subscribe(response => {
            if (response.data) {
                response.data.forEach(element => {
                    if (element.PERMI == 1) {
                        this.perVB = element.ACCES;
                    } else if (element.PERMI == 2) {
                        this.perAU = element.ACCES;
                    }
                });

                if (this.perVB) {
                    this.wbService.getCount('65', 'STTUS=¯2¯ AND VBHPU=¯' + this.authService.user.user_name + '¯').subscribe(response => {
                        if (response.count) {
                            this.countVB = response.count;
                        } else {
                            this.countVB = 0;
                        }
                    })
                } else {
                    this.countVB = 0;
                }

                if (this.perAU) {
                    this.wbService.getCount('65', 'STTUS=¯3¯ AND AUTOU=¯' + this.authService.user.user_name + '¯').subscribe(response => {
                        if (response.count) {
                            this.countAU = response.count;
                        } else {
                            this.countAU = 0;
                        }
                    })
                } else {
                    this.countAU = 0;
                }
            }
        })
    }

    clear(type) {
        let folio = this.f_drh_frm_bpr.FOLIO,
            fecha = this.f_drh_frm_bpr.FECHA;

        this.f_drh_frm_bpr = new F_DRH_FRM_BPR();
        this.aditional = new aditional();

        if (type == 'C') {
            this.f_drh_frm_bpr.FOLIO = folio;

            if (folio.length == 30) {
                this.wbService.getInformation('1', '').subscribe(response => {
                    this.f_drh_frm_bpr.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
                })
            } else {
                this.f_drh_frm_bpr.FECHA = fecha;
            }
        } else {
            this.wbService.getInformation('1', '').subscribe(response => {
                this.f_drh_frm_bpr.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
            })
        }

        this.f_drh_frm_bpr.STTUS = '1',
        this.f_drh_frm_bpr.USUAR = this.authService.user.user_name,
        this.f_drh_frm_bpr.EJERC=Number(sessionStorage.getItem('year'));
        this.puesto(this.f_drh_frm_bpr.USUAR, 'ELPUE');
    }

    protected filterBJ() {
        if (!this.funcBajas) {
            return;
        }

        let search = this.BJFilterCtrl.value;
        if (!search) {
            this.filteredBJ.next(this.funcBajas.slice());
            return;
        } else {
            search = search.toLowerCase();
        }

        this.filteredBJ.next(
            this.funcBajas.filter(funcionario => funcionario.DESCR.toLowerCase().indexOf(search) > -1)
        );
    }

    protected filterVB() {
        if (!this.funcionarios) {
            return;
        }

        let search = this.VBFilterCtrl.value;
        if (!search) {
            this.filteredVB.next(this.funcionarios.slice());
            return;
        } else {
            search = search.toLowerCase();
        }

        this.filteredVB.next(
            this.funcionarios.filter(funcionario => funcionario.DESCR.toLowerCase().indexOf(search) > -1)
        );
    }

    protected filterAU() {
        if (!this.funcionarios) {
            return;
        }

        let search = this.AUFilterCtrl.value;
        if (!search) {
            this.filteredAU.next(this.funcionarios.slice());
            return;
        } else {
            search = search.toLowerCase();
        }

        this.filteredAU.next(
            this.funcionarios.filter(funcionario => funcionario.DESCR.toLowerCase().indexOf(search) > -1)
        );
    }

    puesto(func: string, puest: string) {
        let res = this.funcionarios.find(funcionario => funcionario.CUSER === func);

        this.aditional[puest] = res.DSCPU;
    }

    openDialog(option, type) {
        if (option == 'R') {
            let title = '',
                object = '65',
                columns = [],
                filter = '';

            if (type == 'R') {
                title = 'Folios Creados'
                columns = [
                    { headerName: 'Nº Folio', field: 'FOLIO', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Fecha', field: 'FECHA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Estatus', field: 'STDSC', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Región', field: 'DSREG', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Adscripción', field: 'ADSCR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Puesto', field: 'PUEST', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Funcionario', field: 'NOMBR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Fecha de Baja', field: 'FCBAJ', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Causa de Baja', field: 'CAUBA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true }
                ]
                filter = 'USUAR=¯' + this.authService.user.user_name + '¯'
            } else {
                title = type == 'V' ? 'Folios en Visto Bueno' : 'Folios en Autorización'
                columns = [
                    { headerName: 'Nº Folio', field: 'FOLIO', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Fecha', field: 'FECHA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Región', field: 'DSREG', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Adscripción', field: 'ADSCR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Puesto', field: 'PUEST', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Funcionario', field: 'NOMBR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Fecha de Baja', field: 'FCBAJ', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
                    { headerName: 'Causa de Baja', field: 'CAUBA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true }
                ]
                filter = type == 'V' ? 'STTUS=¯2¯ AND VBHPU=¯' + this.authService.user.user_name + '¯' : 'STTUS=¯3¯ AND AUTOU=¯' + this.authService.user.user_name + '¯'
            }

            const dialogRef = this.dialog.open(DialogRecover, {
                width: '1000px',
                height: '650px',
                data: { title, columns, object, filter }
            });

            dialogRef.afterClosed().subscribe(result => {
                if (result && result.FOLIO) {
                    this.getInformation(result);
                }
            });
        } else {
            const dialogRef = this.dialog.open(DialogConfirm, {
                width: '1000px',
                height: '420px',
                data: { option: option, response: type }
            });

            dialogRef.afterClosed().subscribe(result => {
                if (result && result.option) {
                    this.sendInformation(result);
                }
            });
        }
    }

    getInfo() {
        this.wbService.getInformationForms('DRH-FRM-PER', 'NUEMP=¯' + this.f_drh_frm_bpr.NUEMP + '¯').subscribe(response => {
            let rec_drh_frm_per = response.F_DRH_FRM_PER;

            if (rec_drh_frm_per) {
                this.f_drh_frm_bpr.REGIO=rec_drh_frm_per[0].REGIO,
                this.f_drh_frm_bpr.SINDI=rec_drh_frm_per[0].SINDI;

                this.getInfoPlaza();
            } else {
                this.globalService.messageSwal('Información no encontrada', 'La información del empleado ' + this.f_drh_frm_bpr.NUEMP + ' no se encontro en el servidor', 'error');
            }
        })
    }

    getInformation(data) {
        this.wbService.getInformationForms(this.format, 'FOLIO=¯' + data.FOLIO + '¯').subscribe(response => {
            let rec_drh_frm_bpr = response.F_DRH_FRM_BPR;
            
            if (rec_drh_frm_bpr) {
                this.f_drh_frm_bpr.FOLIO=rec_drh_frm_bpr[0].FOLIO,
                this.f_drh_frm_bpr.STTUS=rec_drh_frm_bpr[0].STTUS,
                this.f_drh_frm_bpr.EJERC=rec_drh_frm_bpr[0].EJERC,
                this.f_drh_frm_bpr.USUAR=rec_drh_frm_bpr[0].USUAR,
                this.f_drh_frm_bpr.KEYUI=rec_drh_frm_bpr[0].KEYUI,
                this.f_drh_frm_bpr.FECHA=rec_drh_frm_bpr[0].FOLIO.length == 30 ? this.f_drh_frm_bpr.FECHA : this.globalService.getDateUTC(rec_drh_frm_bpr[0].FECHA),
                this.f_drh_frm_bpr.NUEMP=rec_drh_frm_bpr[0].NUEMP,
                this.f_drh_frm_bpr.SINDI=rec_drh_frm_bpr[0].SINDI,
                this.f_drh_frm_bpr.FCBAJ=rec_drh_frm_bpr[0].FCBAJ ? this.globalService.getDateUTC(rec_drh_frm_bpr[0].FCBAJ):null,
                this.f_drh_frm_bpr.CAUBA=rec_drh_frm_bpr[0].CAUBA,
                this.f_drh_frm_bpr.JUSTI=rec_drh_frm_bpr[0].JUSTI,
                this.f_drh_frm_bpr.OBSER=rec_drh_frm_bpr[0].OBSER,
                this.f_drh_frm_bpr.VBHPU=rec_drh_frm_bpr[0].VBHPU,
                this.f_drh_frm_bpr.VBHPF=rec_drh_frm_bpr[0].VBHPF ? this.globalService.getDateUTC(rec_drh_frm_bpr[0].VBHPF):null,
                this.f_drh_frm_bpr.VBHPR=rec_drh_frm_bpr[0].VBHPR,
                this.f_drh_frm_bpr.VBHPC=rec_drh_frm_bpr[0].VBHPC,
                this.f_drh_frm_bpr.VBHPK=rec_drh_frm_bpr[0].VBHPK,
                this.f_drh_frm_bpr.AUTOU=rec_drh_frm_bpr[0].AUTOU,
                this.f_drh_frm_bpr.AUTOF=rec_drh_frm_bpr[0].AUTOF ? this.globalService.getDateUTC(rec_drh_frm_bpr[0].AUTOF):null,
                this.f_drh_frm_bpr.AUTOR=rec_drh_frm_bpr[0].AUTOR,
                this.f_drh_frm_bpr.AUTOC=rec_drh_frm_bpr[0].AUTOC,
                this.f_drh_frm_bpr.AUTOK=rec_drh_frm_bpr[0].AUTOK;

                if (this.f_drh_frm_bpr.STTUS != '1') {
                    this.f_drh_frm_bpr.REGIO=rec_drh_frm_bpr[0].REGIO,
                    this.f_drh_frm_bpr.ADSCR=rec_drh_frm_bpr[0].ADSCR,
                    this.f_drh_frm_bpr.REGOP=rec_drh_frm_bpr[0].REGOP,
                    this.f_drh_frm_bpr.PUEST=rec_drh_frm_bpr[0].PUEST,
                    this.f_drh_frm_bpr.PLAZA=rec_drh_frm_bpr[0].PLAZA,
                    this.f_drh_frm_bpr.DESCR=rec_drh_frm_bpr[0].DESCR,
                    this.f_drh_frm_bpr.DESC2=rec_drh_frm_bpr[0].DESC2,
                    this.f_drh_frm_bpr.NIVEL=rec_drh_frm_bpr[0].NIVEL,
                    this.f_drh_frm_bpr.TIPPL=rec_drh_frm_bpr[0].TIPPL,
                    this.f_drh_frm_bpr.NOMBR=rec_drh_frm_bpr[0].NOMBR,
                    this.f_drh_frm_bpr.SUMEN=rec_drh_frm_bpr[0].SUMEN,
                    this.f_drh_frm_bpr.COMGA=rec_drh_frm_bpr[0].COMGA,
                    this.f_drh_frm_bpr.SMEIN=rec_drh_frm_bpr[0].SMEIN;
                } else {
                    this.getInfoPlaza();
                }

                this.puesto(this.f_drh_frm_bpr.USUAR, 'ELPUE');
                this.puesto(this.f_drh_frm_bpr.VBHPU, 'VBPUE');
                this.puesto(this.f_drh_frm_bpr.AUTOU, 'AUPUE');
            } else {
                this.globalService.messageSwal('Información no encontrada', 'La información del empleado ' + data.NUEMP + ' no se encontro en el servidor', 'error');
            }
        })
    }

    getInfoPlaza() {
        this.wbService.getInformation('60', 'NUEMP=¯' + this.f_drh_frm_bpr.NUEMP + '¯').subscribe(response => {
            if (response.data && response.data[0]) {
                this.f_drh_frm_bpr.REGOP=response.data[0].REGIO,
                this.f_drh_frm_bpr.PUEST=response.data[0].PUEST,
                this.f_drh_frm_bpr.PLAZA=response.data[0].PLAZA,
                this.f_drh_frm_bpr.DESCR=response.data[0].DESC1,
                this.f_drh_frm_bpr.DESC2=response.data[0].DESC2,
                this.f_drh_frm_bpr.ADSCR=response.data[0].ADSCR,
                this.f_drh_frm_bpr.NIVEL=response.data[0].NIVEL,
                this.f_drh_frm_bpr.TIPPL=response.data[0].TIPPL,
                this.f_drh_frm_bpr.NOMBR=response.data[0].NOMBR,
                this.f_drh_frm_bpr.SUMEN=response.data[0].SUMEN,
                this.f_drh_frm_bpr.COMGA=response.data[0].COMGA,
                this.f_drh_frm_bpr.SMEIN=response.data[0].SMEIN;
            } else {
                this.f_drh_frm_bpr.REGOP='',
                this.f_drh_frm_bpr.PUEST='',
                this.f_drh_frm_bpr.PLAZA=0,
                this.f_drh_frm_bpr.DESCR='',
                this.f_drh_frm_bpr.DESC2='',
                this.f_drh_frm_bpr.ADSCR='',
                this.f_drh_frm_bpr.NIVEL='',
                this.f_drh_frm_bpr.TIPPL='',
                this.f_drh_frm_bpr.NOMBR='',
                this.f_drh_frm_bpr.SUMEN=0,
                this.f_drh_frm_bpr.COMGA=0,
                this.f_drh_frm_bpr.SMEIN=0;
            }
        })
    }

    sendInformation(info) {
        let information,
            message;

        if (info.option == 'V') {
            information = [{ VBHPF: '|current_timestamp|', VBHPR: info.response, VBHPC: info.commit, VBHPK: '|UUID|', STTUS: info.status }]

            if (info.status == '1') {
                message = 'El folio: ' + this.f_drh_frm_bpr.FOLIO + ' fue habilitado para su edición';
            } else if (info.status == '3') {
                message = 'Información enviada para Autorización con el folio: ' + this.f_drh_frm_bpr.FOLIO;
                
            } else if (info.status == '5') {
                message = 'El folio: ' + this.f_drh_frm_bpr.FOLIO + ' fue cancelado';

                if (this.f_drh_frm_bpr.NOMBR != 'H') {
                    let row = {
                        EJERC: sessionStorage.getItem('year'),
                        PAISS: 'MX',
                        REGIO: this.f_drh_frm_bpr.REGIO,
                        ADSCR: this.f_drh_frm_bpr.ADSCR,
                        PUEST: this.f_drh_frm_bpr.PUEST,
                        PLAZA: this.f_drh_frm_bpr.PLAZA,
                        DESCR: this.f_drh_frm_bpr.DESCR,
                        DESC2: this.f_drh_frm_bpr.DESC2,
                        NIVEL: this.f_drh_frm_bpr.NIVEL,
                        TIPPL: this.f_drh_frm_bpr.TIPPL,
                        NOMBR: this.f_drh_frm_bpr.NOMBR,
                        SUMEN: this.f_drh_frm_bpr.SUMEN,
                        COMGA: this.f_drh_frm_bpr.COMGA,
                        SMEIN: this.f_drh_frm_bpr.SMEIN,
                        NUEMP: this.f_drh_frm_bpr.NUEMP,
                        STTUS: 'A'
                    };

                    let info = { 'DRH-DPL': row };

                    this.wbService.saveCatalogs('M', 'DRH-DPL', row.PLAZA.toString(), '', JSON.stringify(info)).subscribe(
                        response => {
                            console.log(response);
                        },
                        error => {
                            this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
                            console.log(error);
                        })
                }                
            }

            this.wbService.updateInformationForms(this.format, '1', 'FOLIO=¯' + this.f_drh_frm_bpr.FOLIO + '¯', JSON.stringify(information)).subscribe(
                response => {
                    if (response.message) {
                        this.globalService.messageSwal('Información Almacenada', message, 'success');
                        this.f_drh_frm_bpr.STTUS = info.status,
                            this.f_drh_frm_bpr.VBHPR = info.response,
                            this.f_drh_frm_bpr.VBHPC = info.commit;
                        this.getPermission();

                        /*if (info.status == '3') {
                            console.log('Crea PDF');
                            //this.createFile('OK');
                        }*/
                    }
                })
        } else if (info.option == 'A') {
            information = [{ AUTOF: '|current_timestamp|', AUTOR: info.response, AUTOC: info.commit, AUTOK: '|UUID|', STTUS: info.status }]

            if (info.status == '1') {
                message = 'El folio: ' + this.f_drh_frm_bpr.FOLIO + ' fue habilitado para su edición';
            } else if (info.status == '4') {
                message = 'Información Autorizada con el folio: ' + this.f_drh_frm_bpr.FOLIO;

                if (this.f_drh_frm_bpr.NOMBR != 'H') {
                    let row = {
                        EJERC: sessionStorage.getItem('year'),
                        PAISS: 'MX',
                        REGIO: this.f_drh_frm_bpr.REGIO,
                        ADSCR: this.f_drh_frm_bpr.ADSCR,
                        PUEST: this.f_drh_frm_bpr.PUEST,
                        PLAZA: this.f_drh_frm_bpr.PLAZA,
                        DESCR: this.f_drh_frm_bpr.DESCR,
                        DESC2: this.f_drh_frm_bpr.DESC2,
                        NIVEL: this.f_drh_frm_bpr.NIVEL,
                        TIPPL: this.f_drh_frm_bpr.TIPPL,
                        NOMBR: this.f_drh_frm_bpr.NOMBR,
                        SUMEN: this.f_drh_frm_bpr.SUMEN,
                        COMGA: this.f_drh_frm_bpr.COMGA,
                        SMEIN: this.f_drh_frm_bpr.SMEIN,
                        NUEMP: this.f_drh_frm_bpr.NUEMP,
                        STTUS: 'B'
                    };

                    let info = { 'DRH-DPL': row };

                    this.wbService.saveCatalogs('M', 'DRH-DPL', row.PLAZA.toString(), '', JSON.stringify(info)).subscribe(
                        response => {
                            console.log(response);
                        },
                        error => {
                            this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
                            console.log(error);
                        })
                }
            } else if (info.status == '5') {
                message = 'El folio: ' + this.f_drh_frm_bpr.FOLIO + ' fue cancelado';

                if (this.f_drh_frm_bpr.NOMBR != 'H') {
                    let row = {
                        EJERC: sessionStorage.getItem('year'),
                        PAISS: 'MX',
                        REGIO: this.f_drh_frm_bpr.REGIO,
                        ADSCR: this.f_drh_frm_bpr.ADSCR,
                        PUEST: this.f_drh_frm_bpr.PUEST,
                        PLAZA: this.f_drh_frm_bpr.PLAZA,
                        DESCR: this.f_drh_frm_bpr.DESCR,
                        DESC2: this.f_drh_frm_bpr.DESC2,
                        NIVEL: this.f_drh_frm_bpr.NIVEL,
                        TIPPL: this.f_drh_frm_bpr.TIPPL,
                        NOMBR: this.f_drh_frm_bpr.NOMBR,
                        SUMEN: this.f_drh_frm_bpr.SUMEN,
                        COMGA: this.f_drh_frm_bpr.COMGA,
                        SMEIN: this.f_drh_frm_bpr.SMEIN,
                        NUEMP: this.f_drh_frm_bpr.NUEMP,
                        STTUS: 'A'
                    };

                    let info = { 'DRH-DPL': row };

                    this.wbService.saveCatalogs('M', 'DRH-DPL', row.PLAZA.toString(), '', JSON.stringify(info)).subscribe(
                        response => {
                            console.log(response);
                        },
                        error => {
                            this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
                            console.log(error);
                        })
                }
            }

            this.wbService.updateInformationForms(this.format, '1', 'FOLIO=¯' + this.f_drh_frm_bpr.FOLIO + '¯', JSON.stringify(information)).subscribe(
                response => {
                    if (response.message) {
                        this.globalService.messageSwal('Información Almacenada', message, 'success');
                        this.f_drh_frm_bpr.STTUS = info.status,
                            this.f_drh_frm_bpr.AUTOR = info.response,
                            this.f_drh_frm_bpr.AUTOC = info.commit;
                        this.getPermission();

                        if (info.status == '4') {
                            information = [{ FCBAJ: this.f_drh_frm_bpr.FCBAJ, CAUBA: this.f_drh_frm_bpr.CAUBA }]

                            this.wbService.updateInformationForms('DRH-FRM-PER', '1', 'NUEMP=¯' + this.f_drh_frm_bpr.NUEMP + '¯', JSON.stringify(information)).subscribe(
                                response => {
                                    console.log(response);
                                })
                        }
                        /*if (info.status == '4') {
                            console.log('Crea PDF');
                            //this.createFile('OK');
                        }*/
                    }
                })
        }
    }

    valGnr(item, column) {
        if (this.groupF && this.groupF.controls[column]) {
            if (column == 'FCBAJ') {
                if (!item) {
                    this.groupF.controls[column].setErrors({ 'fecha': true });
                    this.groupF.form.setErrors({ 'fecha': true });
                    return true;
                } else {
                    this.groupF.controls[column].setErrors(null);
                    this.groupF.form.setErrors(null);
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    saveInformation(type: string, option: string) {
        this.spinner.show();

        let folio: string = this.f_drh_frm_bpr.FOLIO,
            sttus: string = this.f_drh_frm_bpr.STTUS;

        let information = {
            'F_DRH_FRM_BPR': this.f_drh_frm_bpr
        };

        this.wbService.saveInformationForms(type, this.format, folio, sttus, JSON.stringify(information)).subscribe(
            response => {
                this.getPermission();

                this.f_drh_frm_bpr.FOLIO = response.FOLIO;

                if (type == 'CF') {
                    this.globalService.messageSwal('Información Almacenada', 'Información enviada para Visto Bueno con el folio: ' + response.FOLIO, 'success');
                    
                    if (this.f_drh_frm_bpr.NOMBR != 'H') {
                        let row = {
                            EJERC: sessionStorage.getItem('year'),
                            PAISS: 'MX',
                            REGIO: this.f_drh_frm_bpr.REGIO,
                            ADSCR: this.f_drh_frm_bpr.ADSCR,
                            PUEST: this.f_drh_frm_bpr.PUEST,
                            PLAZA: this.f_drh_frm_bpr.PLAZA,
                            DESCR: this.f_drh_frm_bpr.DESCR,
                            DESC2: this.f_drh_frm_bpr.DESC2,
                            NIVEL: this.f_drh_frm_bpr.NIVEL,
                            TIPPL: this.f_drh_frm_bpr.TIPPL,
                            NOMBR: this.f_drh_frm_bpr.NOMBR,
                            SUMEN: this.f_drh_frm_bpr.SUMEN,
                            COMGA: this.f_drh_frm_bpr.COMGA,
                            SMEIN: this.f_drh_frm_bpr.SMEIN,
                            NUEMP: this.f_drh_frm_bpr.NUEMP,
                            STTUS: 'PB'
                        };

                        let info = { 'DRH-DPL': row };

                        this.wbService.saveCatalogs('M', 'DRH-DPL', row.PLAZA.toString(), '', JSON.stringify(info)).subscribe(
                            response => {
                                console.log(response);
                            },
                            error => {
                                this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
                                console.log(error);
                            })
                    }
                } else {
                    if (!folio) {
                        this.globalService.messageSwal('Información Almacenada', 'Tu folio temporal es: ' + response.FOLIO, 'success');
                    } else {
                        this.globalService.messageSwal('Información Actualizada', 'Tu información con el folio ' + response.FOLIO + ' ha sido guarda', 'success');
                    }
                }

                /*if (option == 'VP' || option == 'OK') {
                    //this.createFile(option);
                    console.log('Se creo el PDF');
                } else if (option.length >= 5) {
                    document.getElementById(option).click();
                }*/

                if (type == 'CF') {
                    this.f_drh_frm_bpr.STTUS = '2';
                }

                this.spinner.hide();
            },
            error => {
                this.spinner.hide();
                this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
                console.log(error);
            })
    }

    disabled() {
        if (this.f_drh_frm_bpr.STTUS == '1') {
            return false;
        } else {
            return true;
        }
    }
}