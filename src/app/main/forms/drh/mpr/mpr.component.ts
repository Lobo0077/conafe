import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { HttpEventType } from '@angular/common/http';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatTableDataSource } from '@angular/material';

import { AuthService } from '../../../../services/auth.service';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';

import { DialogRecover } from '../../../../custom/dialogs/recover/recover.component';
import { DialogConfirm } from '../../../../custom/dialogs/confirm/confirm.component'

import * as moment from 'moment';

import { F_DRH_FRM_MPR, aditional } from '../../../../models/forms/DRH/FRM/MPR/drh.frm.mpr.model';
import { CustomErrorStateMatcher } from '../../../../models/global/error';

@Component({
    selector: 'drh_mpr',
    templateUrl: './mpr.component.html',
    styleUrls: ['./mpr.component.scss'],
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es-MX' },
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
    ]
})
export class DRHMPRComponent implements OnInit, OnDestroy {
    @ViewChild('groupForm', { static: false }) groupF: NgForm;

    public format: string;
    public error = new CustomErrorStateMatcher();

    public f_drh_frm_mpr: F_DRH_FRM_MPR = new F_DRH_FRM_MPR();
    public aditional: aditional = new aditional();

    public perVB: boolean = false;
    public countVB: Number = 0;
    public perAU: boolean = false;
    public countAU: Number = 0;

    protected _onDestroy = new Subject<void>();

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     */
    constructor(public authService: AuthService,
        private wbService: WBService,
        private globalService: GlobalService,
        public dialog: MatDialog,
        private cdr: ChangeDetectorRef,
        private _fuseSplashScreenService: FuseSplashScreenService) {

        this.format = 'DRH-FRM-MPR';
        this.f_drh_frm_mpr.STTUS = '1',
        this.f_drh_frm_mpr.USUAR = this.authService.user.user_name;

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    ngAfterContentChecked() {
        this.cdr.detectChanges();
    }

    ngOnInit() { }

    ngOnDestroy() {
        this._onDestroy.next();
        this._onDestroy.complete();
    }

    disabled() {
        if (this.f_drh_frm_mpr.STTUS == '1') {
            return false;
        } else {
            return true;
        }
    }
}