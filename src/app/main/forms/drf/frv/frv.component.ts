import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { MatTableDataSource } from '@angular/material';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { HttpEventType } from '@angular/common/http';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import { AuthService } from '../../../../services/auth.service';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';

import { F_DRF_FRM_FRV, F_DRF_FRM_FRV_COM, P_DRF_FRM_FRV, P_DRF_FRM_FRV_COM, names } from '../../../../models/forms/DRF/FRM/FRV/drf.frm.frv.model';
import { CustomErrorStateMatcher } from '../../../../models/global/error';

@Component({
    selector     : 'drf_frv',
    templateUrl  : './frv.component.html',
    styleUrls  : ['./frv.component.scss'],
    animations: [
      trigger('detailExpand', [
        state('collapsed', style({height: '0px', minHeight: '0'})),
        state('expanded', style({height: '*'})),
        transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
      ]),
    ],
    providers: [
      { provide: MAT_DATE_LOCALE, useValue: 'es-MX' },
      { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
      { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
    ]
})
export class DRFFRVComponent implements OnInit, OnDestroy {
    @ViewChild('groupForm', {static: false}) groupF: NgForm;

    public format: string;
    public error = new CustomErrorStateMatcher();

    public f_drf_frm_frv: F_DRF_FRM_FRV = new F_DRF_FRM_FRV();
    public p_drf_frm_frv: P_DRF_FRM_FRV = new P_DRF_FRM_FRV();
    public f_drf_frm_frv_com: F_DRF_FRM_FRV_COM[] = [];
    public p_drf_frm_frv_com: P_DRF_FRM_FRV_COM[] = [];
    public names: names = new names();

    public funcionarios: Array<any>;
    public regiones: Array<any>;
    public fondos: Array<any>;
    public movimientos: Array<any>;
    public adscripciones: Array<any>;

    public partidas: Array<any>;
    public filteredP: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public PFilterCtrl: FormControl = new FormControl();

    protected _onDestroy = new Subject<void>();

    columnsToDisplay = ['ACTIONS', 'ID', 'NMDOC', 'ARRSP', 'PRTSP', 'TOTFC'];
    dataSource;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     */
    constructor(private authService: AuthService,
                private wbService: WBService,
                private globalService: GlobalService,
                public dialog: MatDialog,
                private cdr: ChangeDetectorRef,
                private _fuseSplashScreenService: FuseSplashScreenService) {

        this.format = 'DRF-FRM-FRV';
        this.f_drf_frm_frv.STTUS = '1',
        this.f_drf_frm_frv.EJERC = sessionStorage.getItem('year'),
        this.f_drf_frm_frv.USUAR = this.authService.user.user_name,
        this.f_drf_frm_frv.REGIO = this.authService.user.office,
        this.names.ELNOM = this.authService.user.surname_first+' '+this.authService.user.surname_second+' '+this.authService.user.name;
        
        this.wbService.getInformation('10', 'PUEST=¯'+this.authService.user.job+'¯').subscribe(response => {
          if(response.data != null){
            this.names.ELPUE = response.data[0].DESCR;
          }
        })

        this.f_drf_frm_frv_com.push({
          ID:1,
          ERROR:'',
          XXMLL:'',
          PPDFF:'',
          NMDOC:'',
          FCDOC:null,
          RRFCC:'',
          NMRZS:'',
          CONCE:'',
          TOTFC:0,
          ARRSP:'',
          PRTSP:''
        });

        this.p_drf_frm_frv_com.push({
          XXMLL:0,
          PPDFF:0
        });

        this.dataSource  = new MatTableDataSource(this.f_drf_frm_frv_com);
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    ngAfterContentChecked() {
      this.cdr.detectChanges();
    }

    ngOnInit() {
      this.wbService.getInformation('1', '').subscribe(response => {
        this.f_drf_frm_frv.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
      })

      this.wbService.getInformation('3', 'REGIO=¯'+this.authService.user.office+'¯').subscribe(response => {
        this.regiones = response.data;
      })

      this.wbService.getInformation('16', 'REGIO=¯'+this.authService.user.office+'¯').subscribe(response => {
        this.fondos = response.data;
      })

      this.wbService.getInformation('5', '').subscribe(response => {
        this.funcionarios = response.data;
      })

      this.wbService.getInformation('17', '').subscribe(response => {
        this.movimientos = response.data;
      })

      this.wbService.getInformation('4', '').subscribe(response => {
        this.adscripciones = response.data;
      })

      this.wbService.getInformation('9', 'CAPIT IN (¯20000¯,¯30000¯)').subscribe(response => {
        this.partidas = response.data;

        this.partidas.splice(0, 0, {PRTSP:'DAC',DESCR:'Devolución a Caja'});

        this.filteredP.next(this.partidas)
      })

      this.PFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
        this.filterP();
      })
    }

    ngOnDestroy() {
      this._onDestroy.next();
      this._onDestroy.complete();
    }

    nuevo() {
      this.f_drf_frm_frv = new F_DRF_FRM_FRV();
      this.f_drf_frm_frv_com = [];
      this.p_drf_frm_frv_com = [];

      this.names = new names();

      this.f_drf_frm_frv.STTUS = '1',
      this.f_drf_frm_frv.EJERC = sessionStorage.getItem('year'),
      this.f_drf_frm_frv.USUAR = this.authService.user.user_name,
      this.f_drf_frm_frv.REGIO = this.authService.user.office,
      this.names.ELNOM = this.authService.user.surname_first+' '+this.authService.user.surname_second+' '+this.authService.user.name;
      
      this.wbService.getInformation('10', 'PUEST=¯'+this.authService.user.job+'¯').subscribe(response => {
        if(response.data != null){
          this.names.ELPUE = response.data[0].DESCR;
        }
      })

      this.wbService.getInformation('1', '').subscribe(response => {
        this.f_drf_frm_frv.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
      })

      this.f_drf_frm_frv_com.push({
        ID:1,
        ERROR:'',
        XXMLL:'',
        PPDFF:'',
        NMDOC:'',
        FCDOC:null,
        RRFCC:'',
        NMRZS:'',
        CONCE:'',
        TOTFC:0,
        ARRSP:'',
        PRTSP:''
      });

      this.p_drf_frm_frv_com.push({
        XXMLL:0,
        PPDFF:0
      });

      this.dataSource  = new MatTableDataSource(this.f_drf_frm_frv_com);
    }

    openDialog(): void {
      const dialogRef = this.dialog.open(DRFFRVDialogRecover, {
        width: '1000px',
        height: '650px'
      });
  
      dialogRef.afterClosed().subscribe(result => {
        if(result && result.length > 0) {
          this.getInformation(result);
        }
      });
    }

    getInformation(fol: string) {
      this.wbService.getInformationForms(this.format, 'FOLIO=¯'+fol+'¯').subscribe(response => {
        let rec_drf_frm_frv = response.F_DRF_FRM_FRV,
            rec_drf_frm_frv_com = response.F_DRF_FRM_FRV_COM;
  
        if(rec_drf_frm_frv && rec_drf_frm_frv_com){
          this.f_drf_frm_frv.FOLIO=rec_drf_frm_frv[0].FOLIO,
          this.f_drf_frm_frv.STTUS=rec_drf_frm_frv[0].STTUS,
          this.f_drf_frm_frv.EJERC=rec_drf_frm_frv[0].EJERC,
          this.f_drf_frm_frv.USUAR=rec_drf_frm_frv[0].USUAR,
          this.f_drf_frm_frv.FECHA=rec_drf_frm_frv[0].FOLIO.length != 12 ? this.f_drf_frm_frv.FECHA:this.globalService.getDateUTC(rec_drf_frm_frv[0].FECHA),
          this.f_drf_frm_frv.REGIO=rec_drf_frm_frv[0].REGIO,
          this.f_drf_frm_frv.FONDO=rec_drf_frm_frv[0].FONDO,
          this.f_drf_frm_frv.ENCAR=rec_drf_frm_frv[0].ENCAR,
          this.f_drf_frm_frv.TPMVF=rec_drf_frm_frv[0].TPMVF,
          this.f_drf_frm_frv.MNDFD=rec_drf_frm_frv[0].MNDFD,
          this.f_drf_frm_frv.OFSOL=rec_drf_frm_frv[0].OFSOL,
          this.f_drf_frm_frv.OFAPR=rec_drf_frm_frv[0].OFAPR,
          this.f_drf_frm_frv.TOTAL=rec_drf_frm_frv[0].TOTAL,
          this.f_drf_frm_frv.OBSER=rec_drf_frm_frv[0].OBSER;
  
          this.f_drf_frm_frv_com = [];
          this.p_drf_frm_frv_com = [];
  
          for(let x = 0; x < rec_drf_frm_frv_com.length; x++){
            this.f_drf_frm_frv_com.push({
              ID:rec_drf_frm_frv_com[x].ID,
              ERROR:rec_drf_frm_frv_com[x].ERROR,
              XXMLL:rec_drf_frm_frv_com[x].XXMLL,
              PPDFF:rec_drf_frm_frv_com[x].PPDFF,
              NMDOC:rec_drf_frm_frv_com[x].NMDOC,
              FCDOC:rec_drf_frm_frv_com[x].FCDOC ? this.globalService.getDateUTC(rec_drf_frm_frv_com[x].FCDOC):null,
              RRFCC:rec_drf_frm_frv_com[x].RRFCC,
              NMRZS:rec_drf_frm_frv_com[x].NMRZS,
              CONCE:rec_drf_frm_frv_com[x].CONCE,
              TOTFC:rec_drf_frm_frv_com[x].TOTFC,
              ARRSP:rec_drf_frm_frv_com[x].ARRSP,
              PRTSP:rec_drf_frm_frv_com[x].PRTSP
            });

            this.p_drf_frm_frv_com.push({
              XXMLL:rec_drf_frm_frv_com[x].XXMLL.length > 0 ? 100:0,
              PPDFF:rec_drf_frm_frv_com[x].PPDFF.length > 0 ? 100:0
            });
          }
    
          this.dataSource  = new MatTableDataSource(this.f_drf_frm_frv_com);

          this.infoFnd(this.f_drf_frm_frv.FONDO);
        }else{
          this.globalService.messageSwal('Información no encontrada', 'La información con el folio '+fol+ ' no se encontro en el servidor', 'error');
        }
      })
    }

    protected filterP() {
      if (!this.partidas) {
          return;
      }
      
      let search = this.PFilterCtrl.value;
      if (!search) {
          this.filteredP.next(this.partidas.slice());
          return;
      } else {
          search = search.toLowerCase();
      }
      
      this.filteredP.next(
          this.partidas.filter(partida => partida.DESCR.toLowerCase().indexOf(search) > -1)
      );
    }

    addRow() {
      let index = this.f_drf_frm_frv_com.length+1;
  
      this.f_drf_frm_frv_com.push({
        ID:index,
        ERROR:'',
        XXMLL:'',
        PPDFF:'',
        NMDOC:'',
        FCDOC:null,
        RRFCC:'',
        NMRZS:'',
        CONCE:'',
        TOTFC:0,
        ARRSP:'',
        PRTSP:''
      });

      this.p_drf_frm_frv_com.push({
        XXMLL:0,
        PPDFF:0
      });
  
      this.dataSource  = new MatTableDataSource(this.f_drf_frm_frv_com);
    }

    removeRow(index) {
      this.f_drf_frm_frv_com.splice(index,1);
  
      this.f_drf_frm_frv_com.forEach(function(element,index) {
        element.ID = index +1;
      })

      this.p_drf_frm_frv_com.splice(index,1);
  
      this.dataSource  = new MatTableDataSource(this.f_drf_frm_frv_com);
      this.updateAmounts();
    }

    updateAmounts() {
      let total: number = 0;
  
      this.f_drf_frm_frv_com.forEach(function(element) {
        total += element.TOTFC;
      })
      
      this.f_drf_frm_frv.TOTAL = total;
    }

    infoFnd(fnd: string) {
      let res = this.fondos.find( fondo => fondo.FONDO === fnd );
      
      this.f_drf_frm_frv.ENCAR = res.ENCAR,
      this.f_drf_frm_frv.MNDFD = res.MNDFD;

      let rsp = this.funcionarios.find( funcionario => funcionario.CUSER === this.f_drf_frm_frv.ENCAR );
      
      this.names.EFNOM = rsp.DESCR,
      this.names.EFPUE = rsp.DSCPU;
    }

    movimiento(tmv: string) {
      if(tmv=='A'){
        this.f_drf_frm_frv_com = [];
        this.p_drf_frm_frv_com = [];
        
        this.f_drf_frm_frv_com.push({
          ID:1,
          ERROR:'',
          XXMLL:'',
          PPDFF:'',
          NMDOC:'',
          FCDOC:null,
          RRFCC:'',
          NMRZS:'',
          CONCE:'',
          TOTFC:0,
          ARRSP:'',
          PRTSP:''
        });

        this.p_drf_frm_frv_com.push({
          XXMLL:0,
          PPDFF:0
        });

        this.dataSource  = new MatTableDataSource(this.f_drf_frm_frv_com);

        this.updateAmounts();
      }
    }

    openInput(fieldName: string) {
      if(this.f_drf_frm_frv.STTUS == '1') {
        if(this.f_drf_frm_frv.FOLIO) {
          document.getElementById(fieldName).click();
        } else {
          this.saveInformation('SI', fieldName);
        }
      }
    }

    fileChange(files: File[], field: string, index: number) {
      let folio: string = this.f_drf_frm_frv.FOLIO;
      this.p_drf_frm_frv_com[index][field] = 0;
  
      if (files.length > 0) {
        let extension = files[0].name.toLowerCase();
        
        if((field=='PPDFF' && extension.endsWith('.pdf')) || (field=='XXMLL' && extension.endsWith('.xml'))) {
          this.f_drf_frm_frv_com[index][field] = files[0].name;
    
          this.wbService.uploadFile(files[0], this.format, folio).subscribe(
            event => {
              if(event.type === HttpEventType.UploadProgress) {
                this.p_drf_frm_frv_com[index][field] = Math.round((event.loaded/event.total)*100);
              } else if(event.type === HttpEventType.Response) {
                let response:any = event.body;
                this.globalService.messageSwal('Subida Archivo', 'Tu archivo se ha subido con éxito: ' + files[0].name, 'success');

                if(field=='XXMLL' && extension.endsWith('.xml')){
                  this.wbService.getXML(files[0].name, this.format, folio).subscribe(response => {
                    this.infoXML(index, response);
                  })
                }
              }
            },
            error => {
              if(error.error){
                if(error.error.file == 'prohibitus'){
                  this.f_drf_frm_frv_com[index][field] = error.error.file;
                  this.globalService.messageSwal('Archivo Bloqueado', error.error.message, 'error');
                }else{
                  this.globalService.messageSwal('Error', error.error.message, 'error');
                }
              } else{
                console.log(error);
              }
            })
        } else {
          this.f_drf_frm_frv_com[index][field] = '';
          if(field=='PPDFF'){
            this.globalService.messageSwal('Extensión Invalida', 'Archivo adjunto no se pudo cargar, solo se permiten archivos pdf', 'warning');
          } else if(field=='XXMLL'){
            this.globalService.messageSwal('Extensión Invalida', 'Archivo adjunto no se pudo cargar, solo se permiten archivos xml', 'warning');
          }
        }
      } else {
        this.f_drf_frm_frv_com[index][field] = '';
      }
    }

    infoXML(index: number, result: any){
      if(result["cfdi:Comprobante"]){console.log(result["cfdi:Comprobante"]);
        if(result["cfdi:Comprobante"]["cfdi:Complemento"] && result["cfdi:Comprobante"]["cfdi:Complemento"][1] && result["cfdi:Comprobante"]["cfdi:Complemento"][1]["tfd:TimbreFiscalDigital"]){
          this.f_drf_frm_frv_com[index].NMDOC = result["cfdi:Comprobante"]["cfdi:Complemento"][1]["tfd:TimbreFiscalDigital"]["UUID"];

          var fecha = result["cfdi:Comprobante"]["cfdi:Complemento"][1]["tfd:TimbreFiscalDigital"]["FechaTimbrado"].substring(0,10).split('-');

          this.f_drf_frm_frv_com[index].FCDOC = this.globalService.getDateUTC(fecha[2]+'/'+fecha[1]+'/'+fecha[0]);
        }else{
          this.f_drf_frm_frv_com[index].NMDOC = '';
        }

        if(result["cfdi:Comprobante"]["cfdi:Emisor"] && result["cfdi:Comprobante"]["cfdi:Emisor"]["Rfc"]){
          this.f_drf_frm_frv_com[index].RRFCC = result["cfdi:Comprobante"]["cfdi:Emisor"]["Rfc"];
        }else{
          this.f_drf_frm_frv_com[index].RRFCC = '';
        }

        if(result["cfdi:Comprobante"]["cfdi:Emisor"] && result["cfdi:Comprobante"]["cfdi:Emisor"]["Nombre"]){
          this.f_drf_frm_frv_com[index].NMRZS = result["cfdi:Comprobante"]["cfdi:Emisor"]["Nombre"];
        }else{
          this.f_drf_frm_frv_com[index].NMRZS = '';
        }

        if(result["cfdi:Comprobante"]["Total"]){
          this.f_drf_frm_frv_com[index].TOTFC = result["cfdi:Comprobante"]["Total"];
        }else{
          this.f_drf_frm_frv_com[index].TOTFC = 0;
        }
      }

      this.updateAmounts();
    }

    showFile(fieldName: string, index: number) {
      let folio: string = this.f_drf_frm_frv.FOLIO;
  
      this.wbService.showFile(this.f_drf_frm_frv_com[index][fieldName] ,this.format, folio, 'application/pdf').subscribe(x => {
        var newBlob = new Blob([x], { type: 'application/pdf' });
  
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(newBlob);
            return;
        }
  
        const data = window.URL.createObjectURL(newBlob);
  
        window.open(data,'_blank');
      });
    }

    saveInformation(type: string, option: string) {
      this._fuseSplashScreenService.show();

      let folio: string = this.f_drf_frm_frv.FOLIO,
          sttus: string = this.f_drf_frm_frv.STTUS;
  
      let information = { 'F_DRF_FRM_FRV': this.f_drf_frm_frv,
                          'F_DRF_FRM_FRV_COM': this.f_drf_frm_frv_com} ;
  
      this.wbService.saveInformationForms(type, this.format, folio, sttus, JSON.stringify(information)).subscribe(
        response => {
          let old_folio = this.f_drf_frm_frv.FOLIO;
  
          this.f_drf_frm_frv.FOLIO = response.FOLIO;
  
          if(!folio) {
            this.globalService.messageSwal('Información Almacenada', 'Tu folio temporal es: ' + response.FOLIO, 'success');
          } else {
            if(type == 'CF'){
              if(old_folio != this.f_drf_frm_frv.FOLIO) {
                this.globalService.messageSwal('Información Almacenada', 'Tu información ha sido almacenada con el folio: ' + response.FOLIO, 'success');
              } else {
                this.globalService.messageSwal('Información Actualizada', 'Tu información con el folio ' + response.FOLIO + ' ha sido actualizada', 'success');
              }
            } else {
              this.globalService.messageSwal('Información Actualizada', 'Tu información con el folio ' + response.FOLIO + ' ha sido actualizada', 'success');
            }
          }

          if(option == 'VP' || option == 'OK'){
            this.createFile(option);
          } else if(option.length >= 5) {
            document.getElementById(option).click();
          }
  
          if(type == 'CF'){
            this.f_drf_frm_frv.STTUS = '2';
          }

          this._fuseSplashScreenService.hide();
        },
        error => {
          this._fuseSplashScreenService.hide();
          this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
          console.log(error);
        })
    }

    createFile(option: string) {
      let folio: string = this.f_drf_frm_frv.FOLIO;
  
      this.wbService.createFile(option ,this.format, folio).subscribe(x => {
  
        var newBlob = new Blob([x], { type: 'application/pdf' });
  
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(newBlob);
            return;
        }
  
        const data = window.URL.createObjectURL(newBlob);
  
        var link = document.createElement('a');
        link.href = data;
        link.download = folio + '-' + option + '.pdf';
  
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
  
        setTimeout(function () {
            window.URL.revokeObjectURL(data);
            link.remove();
        }, 100);
      });
    }

    downloadFile(option: string) {
      let folio: string = this.f_drf_frm_frv.FOLIO;
  
      this.wbService.downloadFile(option ,this.format, folio).subscribe(x => {
  
        var newBlob = new Blob([x], { type: 'application/pdf' });
  
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(newBlob);
            return;
        }
  
        const data = window.URL.createObjectURL(newBlob);
  
        var link = document.createElement('a');
        link.href = data;
        link.download = folio + '-' + option + '.pdf';
  
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
  
        setTimeout(function () {
            window.URL.revokeObjectURL(data);
            link.remove();
        }, 100);
      });
    }

    valDis() {
      if(this.f_drf_frm_frv.STTUS=='1') {
        if(this.groupF && this.groupF.form) {
          if(this.f_drf_frm_frv.TOTAL > this.f_drf_frm_frv.MNDFD) {
              this.groupF.form.setErrors({'disponible': true});
              return true;
          } else{
              this.groupF.form.setErrors(null);
              return false;
          }
        } else {
          return false;
        }
      } else {
        return false;
      }
    }

    valDet(item, column, index) {
      if(this.groupF && this.groupF.controls[column+index]) {
        if(this.f_drf_frm_frv.TPMVF != 'A'){
          if(column == 'XXMLL') {
            if(this.f_drf_frm_frv_com[index].PRTSP=='37201' || this.f_drf_frm_frv_com[index].PRTSP=='DAC'){
              this.groupF.controls[column+index].setErrors(null);
              return false;
            } else if(((item.substring(item.lastIndexOf("."))).toLowerCase())!='.xml') {
              this.groupF.controls[column+index].setErrors({'extension': true});
              return true;
            } else{
              this.groupF.controls[column+index].setErrors(null);
              return false;
            }
          } else if(column == 'PPDFF') {
            if(item === 'prohibitus' || ((item.substring(item.lastIndexOf("."))).toLowerCase())!='.pdf') {
              this.groupF.controls[column+index].setErrors({'extension': true});
              return true;
            } else{
              this.groupF.controls[column+index].setErrors(null);
              return false;
            }
          } else if(column == 'NMDOC' || column == 'FCDOC' || column == 'NMRZS') {
            if((this.f_drf_frm_frv_com[index].PRTSP=='37201' || this.f_drf_frm_frv_com[index].PRTSP=='DAC') && !item) {
              this.groupF.controls[column+index].setErrors({'value': true});
              return true;
            } else{
              this.groupF.controls[column+index].setErrors(null);
              return false;
            }
          } else if(column == 'CONCE' || column == 'ARRSP' || column == 'PRTSP') {
            if(!item) {
              this.groupF.controls[column+index].setErrors({'value': true});
              return true;
            } else{
              this.groupF.controls[column+index].setErrors(null);
              return false;
            }
          } else if(column == 'TOTFC') {
            if((this.f_drf_frm_frv_com[index].PRTSP=='37201' || this.f_drf_frm_frv_com[index].PRTSP=='DAC') && item <= 0) {
              this.groupF.controls[column+index].setErrors({'extension': true});
              return true;
            } else{
              this.groupF.controls[column+index].setErrors(null);
              return false;
            }
          }
        } else {
          this.groupF.controls[column+index].setErrors(null);
          return false;
        }
      } else {
        return false;
      }
    }

    clean() {
      let folio = this.f_drf_frm_frv.FOLIO,
          fecha = this.f_drf_frm_frv.FECHA;
  
      this.f_drf_frm_frv = new F_DRF_FRM_FRV();
      this.p_drf_frm_frv = new P_DRF_FRM_FRV();
      this.f_drf_frm_frv_com = [];
      this.f_drf_frm_frv_com = [];
  
      this.f_drf_frm_frv.STTUS = '1',
      this.f_drf_frm_frv.FOLIO = folio,
      this.f_drf_frm_frv.EJERC = sessionStorage.getItem('year'),
      this.f_drf_frm_frv.USUAR = this.authService.user.user_name,
      this.f_drf_frm_frv.REGIO = this.authService.user.office,
      this.names.ELNOM = this.authService.user.surname_first+' '+this.authService.user.surname_second+' '+this.authService.user.name,
      this.names.ELPUE = this.authService.user.job;

      if(folio.length != 12) {
        this.wbService.getInformation('1', '').subscribe(response => {
          this.f_drf_frm_frv.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
        })
      } else {
        this.f_drf_frm_frv.FECHA = fecha;
      }

      this.f_drf_frm_frv_com.push({
        ID:1,
        ERROR:'',
        XXMLL:'',
        PPDFF:'',
        NMDOC:'',
        FCDOC:null,
        RRFCC:'',
        NMRZS:'',
        CONCE:'',
        TOTFC:0,
        ARRSP:'',
        PRTSP:''
      });

      this.p_drf_frm_frv_com.push({
        XXMLL:0,
        PPDFF:0
      });
  
      this.dataSource  = new MatTableDataSource(this.f_drf_frm_frv_com);
    }

    disabled() {
      if(this.f_drf_frm_frv.STTUS == '1') {
        return false;
      } else {
        return true;
      }
    }
}

@Component({
  selector: 'drf_frv_dialog_recover',
  templateUrl: 'frv.dialog.recover.html',
})
export class DRFFRVDialogRecover implements OnInit {
    private gridApi: any;
    public rowSelection = "single";
    public format: string;
    public color = 'accent';
    public mode = 'determinate';
    public value = 0;
    public bufferValue = 100;

    columnDefs = [
        {headerName: 'Folio', field: 'FOLIO', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Fecha', field: 'FECHA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Estatus', field: 'STDSC', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Fondo', field: 'FONDO', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Tipo', field: 'TPMVF', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Monto', field: 'TOTAL', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true, cellStyle: {'text-align': 'right'} }
    ];

    rowData: any;

  constructor(private authService: AuthService,
              private wbService: WBService,
              private globalService: GlobalService,
              public dialogRef: MatDialogRef<DRFFRVDialogRecover>) {
        this.format = 'DRF-FRM-FRV';
    }

    ngOnInit() {
      this.loadData();
    }

    loadData() {
      this.wbService.getInformation('24', 'EJERC='+sessionStorage.getItem('year')+' AND USUAR=¯' + this.authService.user.user_name + '¯').subscribe(response => {
        this.rowData = response.data;
      })
    }

    onGridReady(params) {
      this.gridApi = params;
      this.gridApi.api.sizeColumnsToFit();      
    }

    onNoClick() {
      this.dialogRef.close();
    }

    recoverData() {
      let selectedNodes = this.gridApi.api.getSelectedNodes();

      if(selectedNodes.length <= 0){
        this.globalService.messageSwal('Advertencia', 'Selecciona un folio para recuperar', 'warning');
        return
      }

      this.dialogRef.close(selectedNodes[0].data.FOLIO);
    }

    showAdjuntar() {
      if(this.gridApi && this.gridApi.api){
        let selectedNodes = this.gridApi.api.getSelectedNodes();
        if(selectedNodes.length > 0){
          if(selectedNodes[0].data.STTUS == 2){
            return true;
          }
        }
      }
      
      return false;
    }

    adjuntarPDF() {
      this.value = 0;
      document.getElementById('PDFFR').click();
    }

    fileChange(files: File[], field: string) {
      let selectedNodes = this.gridApi.api.getSelectedNodes();

      let folio: string = selectedNodes[0].data.FOLIO;
  
      if (files.length > 0) {
        if(files[0].name.toLowerCase().endsWith('.pdf')) {
          this.wbService.uploadForm(files[0], this.format, folio).subscribe(
            event => {
              if(event.type === HttpEventType.UploadProgress) {
                this.value = Math.round((event.loaded/event.total)*100);
              } else if(event.type === HttpEventType.Response) {
                let response:any = event.body;

                this.gridApi.api.deselectAll();
                this.loadData();
                this.value = 0;

                this.globalService.messageSwal('Subida Archivo', 'Tu archivo se ha subido con éxito: ' + files[0].name, 'success');
              }
            },
            error => {
              this.value = 0;

              if(error.error){
                if(error.error.file == 'prohibitus'){
                  this.globalService.messageSwal('Archivo Bloqueado', error.error.message, 'error');
                }else{
                  this.globalService.messageSwal('Error', error.error.message, 'error');
                }
              } else{
                console.log(error);
              }
            })
        } else {
          this.globalService.messageSwal('Extensión Invalida', 'Archivo adjunto no se pudo cargar, solo se permiten archivos pdf', 'warning');
        }
      }
    }
}