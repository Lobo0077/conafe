import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { MatTableDataSource } from '@angular/material';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { HttpEventType } from '@angular/common/http';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import { AuthService } from '../../../../services/auth.service';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';

import { F_DRF_FRM_CDV, F_DRF_FRM_CDV_GCC, F_DRF_FRM_CDV_GSC, P_DRF_FRM_CDV_GCC, P_DRF_FRM_CDV_GSC, names } from '../../../../models/forms/DRF/FRM/CDV/drf.frm.cdv.model';
import { CustomErrorStateMatcher } from '../../../../models/global/error';

@Component({
    selector     : 'drf_cdv',
    templateUrl  : './cdv.component.html',
    styleUrls  : ['./cdv.component.scss'],
    providers: [
      { provide: MAT_DATE_LOCALE, useValue: 'es-MX' },
      { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
      { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
    ]
})
export class DRFCDVComponent implements OnInit, OnDestroy {
    @ViewChild('groupForm', {static: false}) groupF: NgForm;

    public format: string;
    public error = new CustomErrorStateMatcher();

    public f_drf_frm_cdv: F_DRF_FRM_CDV = new F_DRF_FRM_CDV();
    public f_drf_frm_cdv_gcc: F_DRF_FRM_CDV_GCC[] = [];
    public p_drf_frm_cdv_gcc: P_DRF_FRM_CDV_GCC[] = [];
    public f_drf_frm_cdv_gsc: F_DRF_FRM_CDV_GSC[] = [];
    public p_drf_frm_cdv_gsc: P_DRF_FRM_CDV_GSC[] = [];
    public names: names = new names();

    public funcionarios: Array<any>;
    public filteredRC: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public RCFilterCtrl: FormControl = new FormControl();

    public regiones: Array<any>;
    public adscripciones: Array<any>;
    public puestos: Array<any>;
    public comisiones: Array<any>;
    public lugares: Array<any>;
    public tipoViaticos: Array<any>;
    public medioPagos: Array<any>;

    public gcc: Array<any>;
    public filteredGCC: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public GCCFilterCtrl: FormControl = new FormControl();

    public gsc: Array<any>;
    public filteredGSC: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public GSCFilterCtrl: FormControl = new FormControl();

    protected _onDestroy = new Subject<void>();

    columnsToGCC = ['ACTIONS', 'ID', 'XXMLL', 'PPDFF', 'FACDV', 'NMDOC', 'FCDOC', 'RRFCC', 'CNGCC', 'TOTFC', 'IMACM'];
    dataSourceGCC;

    columnsToGSC = ['ACTIONS', 'ID', 'PPDFF', 'FECGS', 'CNGSC', 'IMPOR'];
    dataSourceGSC;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     */
    constructor(private authService: AuthService,
                private wbService: WBService,
                private globalService: GlobalService,
                public dialog: MatDialog,
                private cdr: ChangeDetectorRef,
                private _fuseSplashScreenService: FuseSplashScreenService) {

        this.format = 'DRF-FRM-CDV';
        this.f_drf_frm_cdv.STTUS = '1',
        this.f_drf_frm_cdv.EJERC = sessionStorage.getItem('year'),
        this.f_drf_frm_cdv.USUAR = this.authService.user.user_name,
        this.f_drf_frm_cdv.REGIO = this.authService.user.office,
        this.f_drf_frm_cdv.ADSCR = this.authService.user.adscription,
        this.f_drf_frm_cdv.PUEST = this.authService.user.job,
        this.f_drf_frm_cdv.NIVEL = this.authService.user.level,
        this.names.NUEMP = this.authService.user.n_emp,
        this.names.SLNOM = this.authService.user.surname_first+' '+this.authService.user.surname_second+' '+this.authService.user.name;
        
        this.wbService.getInformation('10', 'PUEST=¯'+this.authService.user.job+'¯').subscribe(response => {
          if(response.data != null){
            this.names.SLPUE = response.data[0].DESCR;
          }
        })

        this.f_drf_frm_cdv_gcc.push({
          ID:1,
          ERROR:true,
          XXMLL:'',
          PPDFF:'',
          FACDV:1,
          NMDOC:'',
          FCDOC:null,
          RRFCC:'',
          NMRZS:'',
          CNGCC:'',
          TOTFC:0,
          IMACM:0
        });

        this.p_drf_frm_cdv_gcc.push({
          XXMLL:0,
          PPDFF:0
        });

        this.f_drf_frm_cdv_gsc.push({
          ID:1,
          ERROR:false,
          PPDFF:'',
          FECGS:null,
          CNGSC:'',
          IMPOR:0
        });

        this.p_drf_frm_cdv_gsc.push({
          PPDFF:0
        });

        this.dataSourceGCC  = new MatTableDataSource(this.f_drf_frm_cdv_gcc);
        this.dataSourceGSC  = new MatTableDataSource(this.f_drf_frm_cdv_gsc);
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    ngAfterContentChecked() {
      this.cdr.detectChanges();
    }
    
    ngOnInit() {
      this.wbService.getInformation('1', '').subscribe(response => {
        this.f_drf_frm_cdv.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
      })

      this.wbService.getInformation('5', '').subscribe(response => {
        this.funcionarios = response.data;
        this.filteredRC.next(this.funcionarios)
      })

      this.RCFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterRC();
      })

      this.wbService.getInformation('3', 'REGIO=¯'+this.authService.user.office+'¯').subscribe(response => {
        this.regiones = response.data;
      })

      this.wbService.getInformation('4', '').subscribe(response => {
        this.adscripciones = response.data;
      })

      this.wbService.getInformation('10', '').subscribe(response => {
        this.puestos = response.data;
      })

      this.wbService.getInformation('29', 'EJERC='+sessionStorage.getItem('year')+' AND USUAR=¯' + this.authService.user.user_name + '¯ AND STTUS=3').subscribe(response => {
        this.comisiones = response.data;
      })

      this.wbService.getInformation('25', '').subscribe(response => {
        this.tipoViaticos = response.data;
      })

      this.wbService.getInformation('27', '').subscribe(response => {
        this.medioPagos = response.data;
      })

      this.wbService.getInformation('35', '').subscribe(response => {
        this.gcc = response.data;
        this.filteredGCC.next(this.gcc)
      })

      this.GCCFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGCC();
      })

      this.wbService.getInformation('36', '').subscribe(response => {
        this.gsc = response.data;
        this.filteredGSC.next(this.gsc)
      })

      this.GSCFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterGSC();
      })
    }

    ngOnDestroy() {
      this._onDestroy.next();
      this._onDestroy.complete();
    }

    clear(type) {
      let folio = this.f_drf_frm_cdv.FOLIO,
          compr = this.f_drf_frm_cdv.COMPR,
          fecha = this.f_drf_frm_cdv.FECHA;

      this.f_drf_frm_cdv = new F_DRF_FRM_CDV();
      this.f_drf_frm_cdv_gcc = [];
      this.p_drf_frm_cdv_gcc = [];
      this.f_drf_frm_cdv_gsc = [];
      this.p_drf_frm_cdv_gsc = [];

      this.names = new names();

      if(type == 'C') {
        this.f_drf_frm_cdv.FOLIO = folio;
        this.f_drf_frm_cdv.COMPR = compr;

        if(compr.length == 30) {
          this.wbService.getInformation('1', '').subscribe(response => {
            this.f_drf_frm_cdv.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
          })
        } else {
          this.f_drf_frm_cdv.FECHA = fecha;
        }
      } else {
        this.wbService.getInformation('1', '').subscribe(response => {
          this.f_drf_frm_cdv.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
        })
      }

      this.f_drf_frm_cdv.STTUS = '1',
      this.f_drf_frm_cdv.EJERC = sessionStorage.getItem('year'),
      this.f_drf_frm_cdv.USUAR = this.authService.user.user_name,
      this.f_drf_frm_cdv.REGIO = this.authService.user.office,
      this.f_drf_frm_cdv.ADSCR = this.authService.user.adscription,
      this.f_drf_frm_cdv.PUEST = this.authService.user.job,
      this.f_drf_frm_cdv.NIVEL = this.authService.user.level,
      this.names.NUEMP = this.authService.user.n_emp,
      this.names.SLNOM = this.authService.user.surname_first+' '+this.authService.user.surname_second+' '+this.authService.user.name;
      
      this.wbService.getInformation('10', 'PUEST=¯'+this.authService.user.job+'¯').subscribe(response => {
        if(response.data != null){
          this.names.SLPUE = response.data[0].DESCR;
        }
      })

      if(this.f_drf_frm_cdv.FOLIO) {
        this.selectFolio();
      }

      this.f_drf_frm_cdv_gcc.push({
        ID:1,
        ERROR:true,
        XXMLL:'',
        PPDFF:'',
        FACDV:1,
        NMDOC:'',
        FCDOC:null,
        RRFCC:'',
        NMRZS:'',
        CNGCC:'',
        TOTFC:0,
        IMACM:0
      });

      this.p_drf_frm_cdv_gcc.push({
        XXMLL:0,
        PPDFF:0
      });

      this.f_drf_frm_cdv_gsc.push({
        ID:1,
        ERROR:false,
        PPDFF:'',
        FECGS:null,
        CNGSC:'',
        IMPOR:0
      });

      this.p_drf_frm_cdv_gsc.push({
        PPDFF:0
      });

      this.dataSourceGCC  = new MatTableDataSource(this.f_drf_frm_cdv_gcc);
      this.dataSourceGSC  = new MatTableDataSource(this.f_drf_frm_cdv_gsc);
    }

    protected filterRC() {
      if (!this.funcionarios) {
        return;
      }
      
      let search = this.RCFilterCtrl.value;
      if (!search) {
        this.filteredRC.next(this.funcionarios.slice());
        return;
      } else {
        search = search.toLowerCase();
      }
      
      this.filteredRC.next(
        this.funcionarios.filter(funcionario => funcionario.DESCR.toLowerCase().indexOf(search) > -1)
      );
    }

    protected filterGCC() {
      if (!this.gcc) {
        return;
      }
      
      let search = this.GCCFilterCtrl.value;
      if (!search) {
        this.filteredGCC.next(this.gcc.slice());
        return;
      } else {
        search = search.toLowerCase();
      }
      
      this.filteredGCC.next(
        this.gcc.filter(gc => gc.DESCR.toLowerCase().indexOf(search) > -1)
      );
    }

    protected filterGSC() {
      if (!this.gsc) {
        return;
      }
      
      let search = this.GSCFilterCtrl.value;
      if (!search) {
        this.filteredGSC.next(this.gsc.slice());
        return;
      } else {
        search = search.toLowerCase();
      }
      
      this.filteredGSC.next(
        this.gsc.filter(gs => gs.DESCR.toLowerCase().indexOf(search) > -1)
      );
    }

    openDialog(): void {
      const dialogRef = this.dialog.open(DRFCDVDialogRecover, {
        width: '1000px',
        height: '650px'
      });
  
      dialogRef.afterClosed().subscribe(result => {
        if(result) {
          this.getInformation(result);
        }
      });
    }

    getInformation(data) {
      this.wbService.getInformationForms(this.format, 'FOLIO=¯'+data.FOLIO+'¯ AND COMPR=¯'+data.COMPR+'¯').subscribe(response => {
        let rec_drf_frm_cdv = response.F_DRF_FRM_CDV,
            rec_drf_frm_cdv_gcc = response.F_DRF_FRM_CDV_GCC,
            rec_drf_frm_cdv_gsc = response.F_DRF_FRM_CDV_GSC;
  
        if(rec_drf_frm_cdv && rec_drf_frm_cdv_gcc && rec_drf_frm_cdv_gsc){
          this.f_drf_frm_cdv.FOLIO=rec_drf_frm_cdv[0].FOLIO,
          this.f_drf_frm_cdv.COMPR=rec_drf_frm_cdv[0].COMPR,
          this.f_drf_frm_cdv.STTUS=rec_drf_frm_cdv[0].STTUS,
          this.f_drf_frm_cdv.EJERC=rec_drf_frm_cdv[0].EJERC,
          this.f_drf_frm_cdv.USUAR=rec_drf_frm_cdv[0].USUAR,
          this.f_drf_frm_cdv.FECHA=rec_drf_frm_cdv[0].FOLIO.length != 12 ? this.f_drf_frm_cdv.FECHA:this.globalService.getDateUTC(rec_drf_frm_cdv[0].FECHA),
          this.f_drf_frm_cdv.REGIO=rec_drf_frm_cdv[0].REGIO,
          this.f_drf_frm_cdv.ADSCR=rec_drf_frm_cdv[0].ADSCR,
          this.f_drf_frm_cdv.PUEST=rec_drf_frm_cdv[0].PUEST,
          this.f_drf_frm_cdv.NIVEL=rec_drf_frm_cdv[0].NIVEL,
          this.f_drf_frm_cdv.LUGAR=rec_drf_frm_cdv[0].LUGAR,
          this.f_drf_frm_cdv.NMPAG=rec_drf_frm_cdv[0].NMPAG,
          this.f_drf_frm_cdv.GSCCM=rec_drf_frm_cdv[0].GSCCM,
          this.f_drf_frm_cdv.GSSCM=rec_drf_frm_cdv[0].GSSCM,
          this.f_drf_frm_cdv.TTGTS=rec_drf_frm_cdv[0].TTGTS,
          this.f_drf_frm_cdv.DIFER=rec_drf_frm_cdv[0].DIFER,
          this.f_drf_frm_cdv.NMRIN=rec_drf_frm_cdv[0].NMRIN,
          this.f_drf_frm_cdv.MNRIN=rec_drf_frm_cdv[0].MNRIN,
          this.f_drf_frm_cdv.RMCMS=rec_drf_frm_cdv[0].RMCMS,
          this.f_drf_frm_cdv.OBSER=rec_drf_frm_cdv[0].OBSER,
          this.f_drf_frm_cdv.DIRIA=rec_drf_frm_cdv[0].DIRIA,
          this.f_drf_frm_cdv.PROPO=rec_drf_frm_cdv[0].PROPO,
          this.f_drf_frm_cdv.ACTIV=rec_drf_frm_cdv[0].ACTIV,
          this.f_drf_frm_cdv.RESUL=rec_drf_frm_cdv[0].RESUL,
          this.f_drf_frm_cdv.CONTR=rec_drf_frm_cdv[0].CONTR;

          this.f_drf_frm_cdv_gcc = [];
          this.p_drf_frm_cdv_gcc = [];
          this.f_drf_frm_cdv_gsc = [];
          this.p_drf_frm_cdv_gsc = [];
  
          for(let x = 0; x < rec_drf_frm_cdv_gcc.length; x++){
            this.f_drf_frm_cdv_gcc.push({
              ID:rec_drf_frm_cdv_gcc[x].ID,
              ERROR:rec_drf_frm_cdv_gcc[x].ERROR,
              XXMLL:rec_drf_frm_cdv_gcc[x].XXMLL,
              PPDFF:rec_drf_frm_cdv_gcc[x].PPDFF,
              FACDV:rec_drf_frm_cdv_gcc[x].FACDV,
              NMDOC:rec_drf_frm_cdv_gcc[x].NMDOC,
              FCDOC:rec_drf_frm_cdv_gcc[x].FCDOC ? this.globalService.getDateUTC(rec_drf_frm_cdv_gcc[x].FCDOC):null,
              RRFCC:rec_drf_frm_cdv_gcc[x].RRFCC,
              NMRZS:rec_drf_frm_cdv_gcc[x].NMRZS,
              CNGCC:rec_drf_frm_cdv_gcc[x].CNGCC,
              TOTFC:rec_drf_frm_cdv_gcc[x].TOTFC,
              IMACM:rec_drf_frm_cdv_gcc[x].IMACM
            });

            this.p_drf_frm_cdv_gcc.push({
              XXMLL:rec_drf_frm_cdv_gcc[x].XXMLL ? 100:0,
              PPDFF:rec_drf_frm_cdv_gcc[x].PPDFF ? 100:0
            });
          }

          for(let x = 0; x < rec_drf_frm_cdv_gsc.length; x++){
            this.f_drf_frm_cdv_gsc.push({
              ID:rec_drf_frm_cdv_gsc[x].ID,
              ERROR:rec_drf_frm_cdv_gsc[x].ERROR,
              PPDFF:rec_drf_frm_cdv_gsc[x].PPDFF,
              FECGS:rec_drf_frm_cdv_gsc[x].FECGS ? this.globalService.getDateUTC(rec_drf_frm_cdv_gsc[x].FECGS):null,
              CNGSC:rec_drf_frm_cdv_gsc[x].CNGSC,
              IMPOR:rec_drf_frm_cdv_gsc[x].IMPOR  
            });

            this.p_drf_frm_cdv_gsc.push({
              PPDFF:rec_drf_frm_cdv_gsc[x].PPDFF ? 100:0
            });
          }
          
          this.dataSourceGCC  = new MatTableDataSource(this.f_drf_frm_cdv_gcc);
          this.dataSourceGSC  = new MatTableDataSource(this.f_drf_frm_cdv_gsc);

          this.selectFolio();
          this.puesto(this.f_drf_frm_cdv.DIRIA)
        }else{
          this.globalService.messageSwal('Información no encontrada', 'La información de la comproción '+data.FOLIO+'-'+data.COMPR+ ' no se encontro en el servidor', 'error');
        }
      })
    }

    addRowGCC() {
      let index = this.f_drf_frm_cdv_gcc.length+1;
  
      this.f_drf_frm_cdv_gcc.push({
        ID:index,
        ERROR:true,
        XXMLL:'',
        PPDFF:'',
        FACDV:1,
        NMDOC:'',
        FCDOC:null,
        RRFCC:'',
        NMRZS:'',
        CNGCC:'',
        TOTFC:0,
        IMACM:0
      });

      this.p_drf_frm_cdv_gcc.push({
        XXMLL:0,
        PPDFF:0
      });
  
      this.dataSourceGCC  = new MatTableDataSource(this.f_drf_frm_cdv_gcc);
    }

    removeRowGCC(index) {
      this.f_drf_frm_cdv_gcc.splice(index,1);
      this.p_drf_frm_cdv_gcc.splice(index,1);
  
      this.f_drf_frm_cdv_gcc.forEach(function(element,index) {
        element.ID = index +1;
      })
  
      this.dataSourceGCC  = new MatTableDataSource(this.f_drf_frm_cdv_gcc);
      
      this.updateGCC();
    }

    addRowGSC() {
      let index = this.f_drf_frm_cdv_gsc.length+1;
  
      this.f_drf_frm_cdv_gsc.push({
        ID:index,
        ERROR:false,
        PPDFF:'',
        FECGS:null,
        CNGSC:'',
        IMPOR:0
      });

      this.p_drf_frm_cdv_gsc.push({
        PPDFF:0
      });
  
      this.dataSourceGSC  = new MatTableDataSource(this.f_drf_frm_cdv_gsc);
    }

    removeRowGSC(index) {
      this.f_drf_frm_cdv_gsc.splice(index,1);
      this.p_drf_frm_cdv_gsc.splice(index,1);
  
      this.f_drf_frm_cdv_gsc.forEach(function(element,index) {
        element.ID = index +1;
      })
  
      this.dataSourceGSC  = new MatTableDataSource(this.f_drf_frm_cdv_gsc);
      
      this.updateGSC();
    }

    updateGCC() {
      let total: number = 0;
  
      this.f_drf_frm_cdv_gcc.forEach(function(element) {
        total += element.IMACM;
      })
      
      this.f_drf_frm_cdv.GSCCM = total;

      this.updateTotal();
    }

    updateGSC() {
      let total: number = 0;
  
      this.f_drf_frm_cdv_gsc.forEach(function(element) {
        total += element.IMPOR;
      })
      
      this.f_drf_frm_cdv.GSSCM = total;

      this.updateTotal();
    }
    
    updateTotal() {
      this.f_drf_frm_cdv.TTGTS = this.f_drf_frm_cdv.GSCCM + this.f_drf_frm_cdv.GSSCM;
      this.f_drf_frm_cdv.DIFER = this.names.TTVIA - this.f_drf_frm_cdv.TTGTS;
      this.f_drf_frm_cdv.RMCMS = this.f_drf_frm_cdv.DIFER - this.f_drf_frm_cdv.MNRIN;
    }

    puesto(func: string) {
      let res = this.funcionarios.find( funcionario => funcionario.CUSER === func );
      
      this.names.RCNOM = res.DESCR;
      this.names.RCPUE = res.DSCPU;
    }

    selectFolio() {
      let res = this.comisiones.find( comision => comision.FOLIO === this.f_drf_frm_cdv.FOLIO );

      this.names.TPVIA = res.TPVIA,
      this.names.MDPAG = res.MDPAG,
      this.names.LBLMN = res.TPVIA == 'D' ? 'Monto Autorizado':'Monto Otorgado',
      this.names.PRINI = null,
      this.names.PRTER = null,
      this.names.TTVIA = 0,
      this.names.INFOR = false;

      this.wbService.getInformation('33', 'FOLIO=¯'+this.f_drf_frm_cdv.FOLIO+'¯').subscribe(response => {
        this.lugares = response.data;
        this.selectLugar();
      })
    }

    selectLugar() {
      if(this.f_drf_frm_cdv.LUGAR != null) {
        let res = this.lugares.find( lugar => lugar.FOLIO === this.f_drf_frm_cdv.FOLIO && lugar.ID === this.f_drf_frm_cdv.LUGAR );

        this.names.PRINI = res.PRINI ? this.globalService.getDateUTC(res.PRINI):null,
        this.names.PRTER = res.PRTER ? this.globalService.getDateUTC(res.PRTER):null,
        this.names.TTVIA = res.IMPCM,
        this.names.INFOR = res.INFOR;

        this.updateGCC();
        this.updateGSC();
      }
    }

    getReqcom(index, concep) {
      let res = this.gcc.find( gc => gc.CNGCC === concep );
      
      this.f_drf_frm_cdv_gcc[index].ERROR = res.RQCOM;

      if(!res.RQCOM) {
        this.f_drf_frm_cdv_gcc[index].XXMLL = '',
        this.p_drf_frm_cdv_gcc[index].XXMLL = 0,
        this.f_drf_frm_cdv_gcc[index].PPDFF = '',
        this.p_drf_frm_cdv_gcc[index].PPDFF = 0,
        this.f_drf_frm_cdv_gcc[index].FACDV = 1,
        this.f_drf_frm_cdv_gcc[index].NMDOC = '',
        this.f_drf_frm_cdv_gcc[index].FCDOC = null,
        this.f_drf_frm_cdv_gcc[index].RRFCC = '',
        this.f_drf_frm_cdv_gcc[index].NMRZS = '',
        this.f_drf_frm_cdv_gcc[index].TOTFC = 0,
        this.f_drf_frm_cdv_gcc[index].IMACM = 0;
      }

      this.updateGCC();
    }

    openInput(fieldName: string) {
      if(this.f_drf_frm_cdv.STTUS == '1' && this.f_drf_frm_cdv.FOLIO) {
        if(this.f_drf_frm_cdv.FOLIO && this.f_drf_frm_cdv.COMPR) {
          document.getElementById(fieldName).click();
        } else {
          this.saveInformation('SI', fieldName);
        }
      }
    }

    fileChange(files: File[], field: string, index: number, type: string) {
      let folio: string = this.f_drf_frm_cdv.FOLIO+'¦'+this.f_drf_frm_cdv.COMPR;
      
      if(type=='GCC') {
        this.p_drf_frm_cdv_gcc[index][field] = 0;
      } else if(type=='GSC') {
        this.p_drf_frm_cdv_gsc[index][field] = 0;
      }
  
      if (files.length > 0) {
        let extension = files[0].name.toLowerCase();
        
        if((field=='PPDFF' && extension.endsWith('.pdf')) || (field=='XXMLL' && extension.endsWith('.xml'))) {
          
          if(type=='GCC') {
            this.f_drf_frm_cdv_gcc[index][field] = files[0].name;
          } else if(type=='GSC') {
            this.f_drf_frm_cdv_gsc[index][field] = files[0].name;
          }
    
          this.wbService.uploadFile(files[0], this.format, folio).subscribe(
            event => {
              if(event.type === HttpEventType.UploadProgress) {
                if(type=='GCC') {
                  this.p_drf_frm_cdv_gcc[index][field] = Math.round((event.loaded/event.total)*100);
                } else if(type=='GSC') {
                  this.p_drf_frm_cdv_gsc[index][field] = Math.round((event.loaded/event.total)*100);
                }
              } else if(event.type === HttpEventType.Response) {
                let response:any = event.body;
                this.globalService.messageSwal('Subida Archivo', 'Tu archivo se ha subido con éxito: ' + files[0].name, 'success');

                if(field=='XXMLL' && extension.endsWith('.xml') && type=='GCC'){
                  this.wbService.getXML(files[0].name, this.format, folio).subscribe(response => {
                    this.infoXML(index, response);
                  })
                }
              }
            },
            error => {
              if(error.error){
                if(error.error.file == 'prohibitus'){
                  if(type=='GCC') {
                    this.f_drf_frm_cdv_gcc[index][field] = error.error.file;
                  } else if(type=='GSC') {
                    this.f_drf_frm_cdv_gsc[index][field] = error.error.file;
                  }
                  this.globalService.messageSwal('Archivo Bloqueado', error.error.message, 'error');
                }else{
                  this.globalService.messageSwal('Error', error.error.message, 'error');
                }
              } else{
                console.log(error);
              }
            })
        } else {
          if(type=='GCC') {
            this.f_drf_frm_cdv_gcc[index][field] = '';
          } else if(type=='GSC') {
            this.f_drf_frm_cdv_gsc[index][field] = '';
          }
          if(field=='PPDFF'){
            this.globalService.messageSwal('Extensión Invalida', 'Archivo adjunto no se pudo cargar, solo se permiten archivos pdf', 'warning');
          } else if(field=='XXMLL'){
            this.globalService.messageSwal('Extensión Invalida', 'Archivo adjunto no se pudo cargar, solo se permiten archivos xml', 'warning');
          }
        }
      } else {
        if(type=='GCC') {
          this.f_drf_frm_cdv_gcc[index][field] = '';
        } else if(type=='GSC') {
          this.f_drf_frm_cdv_gsc[index][field] = '';
        }
      }
    }

    infoXML(index: number, result: any){
      if(result["cfdi:Comprobante"]){console.log(result["cfdi:Comprobante"]);
        if(result["cfdi:Comprobante"]["cfdi:Complemento"] && result["cfdi:Comprobante"]["cfdi:Complemento"][1] && result["cfdi:Comprobante"]["cfdi:Complemento"][1]["tfd:TimbreFiscalDigital"]){
          this.f_drf_frm_cdv_gcc[index].NMDOC = result["cfdi:Comprobante"]["cfdi:Complemento"][1]["tfd:TimbreFiscalDigital"]["UUID"];

          var fecha = result["cfdi:Comprobante"]["cfdi:Complemento"][1]["tfd:TimbreFiscalDigital"]["FechaTimbrado"].substring(0,10).split('-');

          this.f_drf_frm_cdv_gcc[index].FCDOC = this.globalService.getDateUTC(fecha[2]+'/'+fecha[1]+'/'+fecha[0]);
        }else{
          this.f_drf_frm_cdv_gcc[index].NMDOC = '';
        }

        if(result["cfdi:Comprobante"]["cfdi:Emisor"] && result["cfdi:Comprobante"]["cfdi:Emisor"]["Rfc"]){
          this.f_drf_frm_cdv_gcc[index].RRFCC = result["cfdi:Comprobante"]["cfdi:Emisor"]["Rfc"];
        }else{
          this.f_drf_frm_cdv_gcc[index].RRFCC = '';
        }

        if(result["cfdi:Comprobante"]["cfdi:Emisor"] && result["cfdi:Comprobante"]["cfdi:Emisor"]["Nombre"]){
          this.f_drf_frm_cdv_gcc[index].NMRZS = result["cfdi:Comprobante"]["cfdi:Emisor"]["Nombre"];
        }else{
          this.f_drf_frm_cdv_gcc[index].NMRZS = '';
        }

        if(result["cfdi:Comprobante"]["Total"]){
          this.f_drf_frm_cdv_gcc[index].TOTFC = result["cfdi:Comprobante"]["Total"];
          this.f_drf_frm_cdv_gcc[index].IMACM = result["cfdi:Comprobante"]["Total"];
        }else{
          this.f_drf_frm_cdv_gcc[index].TOTFC = 0;
          this.f_drf_frm_cdv_gcc[index].IMACM = 0;
        }
      }

      this.updateGCC();
    }

    showFile(fieldName: string, index: number, type: string) {
      let folio: string = this.f_drf_frm_cdv.FOLIO+'¦'+this.f_drf_frm_cdv.COMPR;
  
      if(type=='GCC') {
        this.wbService.showFile(this.f_drf_frm_cdv_gcc[index][fieldName] ,this.format, folio, 'application/pdf').subscribe(x => {
          var newBlob = new Blob([x], { type: 'application/pdf' });
    
          if (window.navigator && window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveOrOpenBlob(newBlob);
              return;
          }
    
          const data = window.URL.createObjectURL(newBlob);
    
          window.open(data,'_blank');
        });
      } else if(type=='GSC') {
        this.wbService.showFile(this.f_drf_frm_cdv_gsc[index][fieldName] ,this.format, folio, 'application/pdf').subscribe(x => {
          var newBlob = new Blob([x], { type: 'application/pdf' });
    
          if (window.navigator && window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveOrOpenBlob(newBlob);
              return;
          }
    
          const data = window.URL.createObjectURL(newBlob);
    
          window.open(data,'_blank');
        });
      }
    }

    valRem() {
      if(this.f_drf_frm_cdv.STTUS=='1') {
        if(this.groupF && this.groupF.form && this.groupF.controls['RMCMS']) {
          if(this.f_drf_frm_cdv.RMCMS > 0) {
              this.groupF.controls['RMCMS'].setErrors({'remanente': true});
              this.groupF.form.setErrors({'remanente': true});
              return true;
          } else{
              this.groupF.controls['RMCMS'].setErrors(null);
              this.groupF.form.setErrors(null);
              return false;
          }
        } else {
          return false;
        }
      } else {
        return false;
      }
    }

    valGnr(column) {
      if(this.f_drf_frm_cdv.STTUS == '1') {
        if(this.groupF && this.groupF.controls[column]) {
          if(column == 'PROPO' || column == 'ACTIV' || column == 'RESUL' || column == 'CONTR') {
            if(this.names.INFOR && this.f_drf_frm_cdv[column].length <= 0) {
              this.groupF.controls[column].setErrors({'data': true});
              return true;
            } else {
              this.groupF.controls[column].setErrors(null);
              return false;
            }
          }
        } else{
          return false;
        }
      } else {
        return false;
      }
    }

    valGCC(item, element, column, index) {
      if(this.f_drf_frm_cdv.STTUS == '1') {
        if(this.groupF && this.groupF.controls[column+index]) {
          if(column == 'IMACM') {
            if((item > 0 && item <= this.f_drf_frm_cdv_gcc[index].TOTFC) || !this.f_drf_frm_cdv_gcc[index].ERROR) {
              this.groupF.controls[column+index].setErrors(null);
              return false;
            } else {
              this.groupF.controls[column+index].setErrors({'field': true});
              return true;
            }
          } else if(column == 'FACDV') {
            if(item > 0 || !this.f_drf_frm_cdv_gcc[index].ERROR) {
              this.groupF.controls[column+index].setErrors(null);
              return false;
            } else {
              this.groupF.controls[column+index].setErrors({'field': true});
              return true;
            }
          } else if(column == 'XXMLL') {
            if(((item.substring(item.lastIndexOf("."))).toLowerCase())!='.xml' && this.f_drf_frm_cdv_gcc[index].ERROR) {
              this.groupF.controls[column+index].setErrors({'extension': true});
              return true;
            } else{
              this.groupF.controls[column+index].setErrors(null);
              return false;
            }
          } else if(column == 'PPDFF') {
            if(item === 'prohibitus' || ((item.substring(item.lastIndexOf("."))).toLowerCase())!='.pdf' && this.f_drf_frm_cdv_gcc[index].ERROR) {
              this.groupF.controls[column+index].setErrors({'extension': true});
              return true;
            } else{
              this.groupF.controls[column+index].setErrors(null);
              return false;
            }
          }
        } else {
          return false;
        }
      } else {
        return false;
      }
    }

    valGSC(item, element, column, index) {
      if(this.f_drf_frm_cdv.STTUS == '1' && element.PPDFF) {
        if(this.groupF && this.groupF.controls[column+index]) {
          if(column == 'PDFGS') {
            if(item === 'prohibitus' || ((item.substring(item.lastIndexOf("."))).toLowerCase())!='.pdf') {
              this.groupF.controls[column+index].setErrors({'extension': true});
              return true;
            } else{
              this.groupF.controls[column+index].setErrors(null);
              return false;
            }
          } else if(column == 'FECGS' || column == 'CNGSC') {
            if(!item) {
              this.groupF.controls[column+index].setErrors({'data': true});
              return true;
            } else{
              this.groupF.controls[column+index].setErrors(null);
              return false;
            }
          } else if(column == 'IMPOR') {
            if(item <= 0) {
              this.groupF.controls[column+index].setErrors({'importe': true});
              return true;
            } else{
              this.groupF.controls[column+index].setErrors(null);
              return false;
            }
          }
        } else {
          return false;
        }
      } else {
        return false;
      }
    }

    saveInformation(type: string, option: string) {
      this._fuseSplashScreenService.show();

      let folio: string = this.f_drf_frm_cdv.FOLIO+'¦'+this.f_drf_frm_cdv.COMPR,
          compr: string = this.f_drf_frm_cdv.COMPR,
          sttus: string = this.f_drf_frm_cdv.STTUS;
  
      let information = { 'F_DRF_FRM_CDV': this.f_drf_frm_cdv,
                          'F_DRF_FRM_CDV_GCC': this.f_drf_frm_cdv_gcc,
                          'F_DRF_FRM_CDV_GSC': this.f_drf_frm_cdv_gsc};
  
      this.wbService.saveInformationForms(type, this.format, folio, sttus, JSON.stringify(information)).subscribe(
        response => {
          let old_folio = this.f_drf_frm_cdv.FOLIO;
  
          let fol = response.FOLIO.split('¦');

          this.f_drf_frm_cdv.COMPR = fol[1] ? fol[1]:'';
  
          if(!folio || !compr) {
            this.globalService.messageSwal('Información Almacenada', 'Tu comprobación temporal es: ' + response.FOLIO, 'success');
          } else {
            if(type == 'CF'){
              if(old_folio != this.f_drf_frm_cdv.FOLIO+'-'+this.f_drf_frm_cdv.COMPR) {
                this.globalService.messageSwal('Información Almacenada', 'Tu información ha sido almacenada con la comprobación: ' + response.FOLIO, 'success');
              } else {
                this.globalService.messageSwal('Información Actualizada', 'Tu información con la comprobación ' + response.FOLIO + ' ha sido actualizada', 'success');
              }
            } else {
              this.globalService.messageSwal('Información Actualizada', 'Tu información con la comprobación ' + response.FOLIO + ' ha sido actualizada', 'success');
            }
          }

          if(option == 'VP' || option == 'OK'){
            this.createFile(option);
          } else if(option.length >= 5) {
            document.getElementById(option).click();
          }
  
          if(type == 'CF'){
            this.f_drf_frm_cdv.STTUS = '2';
          }

          this._fuseSplashScreenService.hide();
        },
        error => {
          this._fuseSplashScreenService.hide();
          this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
          console.log(error);
        })
    }

    createFile(option: string) {
      let folio: string = this.f_drf_frm_cdv.FOLIO+'¦'+this.f_drf_frm_cdv.COMPR;
  
      this.wbService.createFile(option ,this.format, folio).subscribe(x => {
  
        var newBlob = new Blob([x], { type: 'application/pdf' });
  
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(newBlob);
            return;
        }
  
        const data = window.URL.createObjectURL(newBlob);
  
        var link = document.createElement('a');
        link.href = data;
        link.download = folio + '-' + option + '.pdf';
  
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
  
        setTimeout(function () {
            window.URL.revokeObjectURL(data);
            link.remove();
        }, 100);
      });
    }

    downloadFile(option: string) {
      let folio: string = this.f_drf_frm_cdv.FOLIO+'¦'+this.f_drf_frm_cdv.COMPR;
  
      this.wbService.downloadFile(option ,this.format, folio).subscribe(x => {
  
        var newBlob = new Blob([x], { type: 'application/pdf' });
  
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(newBlob);
            return;
        }
  
        const data = window.URL.createObjectURL(newBlob);
  
        var link = document.createElement('a');
        link.href = data;
        link.download = folio + '-' + option + '.pdf';
  
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
  
        setTimeout(function () {
            window.URL.revokeObjectURL(data);
            link.remove();
        }, 100);
      });
    }

    disabledSave() {
      if(this.f_drf_frm_cdv.STTUS == '1') {
        if(this.groupF && this.groupF.controls['FOLIO']) {
          return this.groupF.controls['FOLIO'].invalid;
        } else {
          return true
        }
      } else {
        return true;
      }
    }

    disabled() {
      if(this.f_drf_frm_cdv.STTUS == '1') {
        return false;
      } else {
        return true;
      }
    }
}

@Component({
  selector: 'drf_cdv_dialog_recover',
  templateUrl: 'cdv.dialog.recover.html',
})
export class DRFCDVDialogRecover implements OnInit {
    private gridApi: any;
    public rowSelection = "single";
    public format: string;
    public color = 'accent';
    public mode = 'determinate';
    public value = 0;
    public bufferValue = 100;

    columnDefs = [
        {headerName: 'Folio', field: 'FOLIO', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Comprobación', field: 'COMPR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Fecha', field: 'FECHA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Estatus', field: 'STDSC', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Lugar', field: 'PLACE', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true }
    ];

    rowData: any;

  constructor(private authService: AuthService,
              private wbService: WBService,
              private globalService: GlobalService,
              public dialogRef: MatDialogRef<DRFCDVDialogRecover>) {
        this.format = 'DRF-FRM-CDV';
    }

    ngOnInit() {
      this.loadData();
    }

    loadData() {
      this.wbService.getInformation('34', 'EJERC='+sessionStorage.getItem('year')+' AND USUAR=¯' + this.authService.user.user_name + '¯').subscribe(response => {
        this.rowData = response.data;
      })
    }

    onGridReady(params) {
      this.gridApi = params;    
    }

    onNoClick() {
      this.dialogRef.close();
    }

    recoverData() {
      let selectedNodes = this.gridApi.api.getSelectedNodes();

      if(selectedNodes.length <= 0){
        this.globalService.messageSwal('Advertencia', 'Selecciona un folio para recuperar', 'warning');
        return
      }

      this.dialogRef.close(selectedNodes[0].data);
    }

    showAdjuntar() {
      if(this.gridApi && this.gridApi.api){
        let selectedNodes = this.gridApi.api.getSelectedNodes();
        if(selectedNodes.length > 0){
          if(selectedNodes[0].data.STTUS == 2){
            return true;
          }
        }
      }
      
      return false;
    }

    adjuntarPDF() {
      this.value = 0;
      document.getElementById('PDFFR').click();
    }

    fileChange(files: File[], field: string) {
      let selectedNodes = this.gridApi.api.getSelectedNodes();

      let folio: string = selectedNodes[0].data.FOLIO+'-'+selectedNodes[0].data.COMPR;
  
      if (files.length > 0) {
        if(files[0].name.toLowerCase().endsWith('.pdf')) {
          this.wbService.uploadForm(files[0], this.format, folio).subscribe(
            event => {
              if(event.type === HttpEventType.UploadProgress) {
                this.value = Math.round((event.loaded/event.total)*100);
              } else if(event.type === HttpEventType.Response) {
                let response:any = event.body;

                this.gridApi.api.deselectAll();
                this.loadData();
                this.value = 0;

                this.globalService.messageSwal('Subida Archivo', 'Tu archivo se ha subido con éxito: ' + files[0].name, 'success');
              }
            },
            error => {
              this.value = 0;

              if(error.error){
                if(error.error.file == 'prohibitus'){
                  this.globalService.messageSwal('Archivo Bloqueado', error.error.message, 'error');
                }else{
                  this.globalService.messageSwal('Error', error.error.message, 'error');
                }
              } else{
                console.log(error);
              }
            })
        } else {
          this.globalService.messageSwal('Extensión Invalida', 'Archivo adjunto no se pudo cargar, solo se permiten archivos pdf', 'warning');
        }
      }
    }
}