import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { HttpEventType } from '@angular/common/http';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import { AuthService } from '../../../../services/auth.service';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';

import { F_DRF_FRM_GAC, names } from '../../../../models/forms/DRF/FRM/GAC/drf.frm.gac.model';
import { CustomErrorStateMatcher } from '../../../../models/global/error';


@Component({
    selector     : 'drf_gac',
    templateUrl  : './gac.component.html',
    styleUrls  : ['./gac.component.scss'],
    providers: [
      { provide: MAT_DATE_LOCALE, useValue: 'es-MX' },
      { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
      { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
    ]
})
export class DRFGACComponent implements OnInit, OnDestroy {
    @ViewChild('groupForm', {static: false}) groupF: NgForm;

    public format: string;
    public error = new CustomErrorStateMatcher();

    public f_drf_frm_gac: F_DRF_FRM_GAC = new F_DRF_FRM_GAC();
    public names: names = new names();

    public regiones: Array<any>;

    public ctrpd: Array<any>;
    public filteredCP: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public CPFilterCtrl: FormControl = new FormControl();

    public requisiciones: Array<any>;
    public partidas: Array<any>;

    public proveedores: Array<any>;
    public filteredP: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);

    public funcionarios: Array<any>;
    public filteredVB: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public VBFilterCtrl: FormControl = new FormControl();
    public filteredAU: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public AUFilterCtrl: FormControl = new FormControl();

    public autorizado: number = 0;
    public disponible: number = 0;

    protected _onDestroy = new Subject<void>();

    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     */
    constructor(private authService: AuthService,
                private wbService: WBService,
                private globalService: GlobalService,
                public dialog: MatDialog,
                private cdr: ChangeDetectorRef,
                private _fuseSplashScreenService: FuseSplashScreenService) {

        this.format = 'DRF-FRM-GAC';
        this.f_drf_frm_gac.STTUS = '1',
        this.f_drf_frm_gac.EJERC = sessionStorage.getItem('year'),
        this.f_drf_frm_gac.USUAR = this.authService.user.user_name,
        this.f_drf_frm_gac.REGIO = this.authService.user.office,
        this.names.ELNOM = this.authService.user.surname_first+' '+this.authService.user.surname_second+' '+this.authService.user.name;
        
        this.wbService.getInformation('10', 'PUEST=¯'+this.authService.user.job+'¯').subscribe(response => {
            if(response.data != null){
            this.names.ELPUE = response.data[0].DESCR;
            }
        })

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    ngOnInit() {
        this.wbService.getInformation('1', '').subscribe(response => {
            this.f_drf_frm_gac.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
        })
    
        this.wbService.getInformation('3', 'REGIO=¯'+this.authService.user.office+'¯').subscribe(response => {
            this.regiones = response.data;
        })

        this.wbService.getInformation('14', 'STTUS=3 AND REGIO=¯'+this.authService.user.office+'¯').subscribe(response => {
            this.ctrpd = response.data;

            this.ctrpd.splice(0, 0, {CTRPD:'SG',DESCR:'Servicio General'});

            this.filteredCP.next(this.ctrpd)
        })

        this.CPFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
            this.filterCP();
        })

        this.wbService.getInformation('9', '').subscribe(response => {
            this.partidas = response.data;
        })

        this.wbService.getInformation('37', '').subscribe(response => {
            this.proveedores = response.data;

            if(this.proveedores != null) {
              if(this.groupF && this.groupF.form){
                  this.groupF.form.valueChanges.subscribe(form => {
                      this.filteredP.next(
                          this.proveedores.filter(proveedor => proveedor.DESCR.toLowerCase().indexOf(form.RRFCC ? form.RRFCC.toLowerCase():'') > -1)
                      );
                  })
              
              }
            }
        })

        this.wbService.getInformation('5', '').subscribe(response => {
            this.funcionarios = response.data;
            this.filteredVB.next(this.funcionarios)
            this.filteredAU.next(this.funcionarios)
        })
      
        this.VBFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                this.filterVB();
        })
      
        this.AUFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                this.filterAU();
        })
    }

    ngOnDestroy() {
        this._onDestroy.next();
        this._onDestroy.complete();
    }

    clear(type) {
      let folio = this.f_drf_frm_gac.FOLIO,
          fecha = this.f_drf_frm_gac.FECHA;

      this.f_drf_frm_gac = new F_DRF_FRM_GAC();

      this.names = new names();

      if(type == 'C') {
        this.f_drf_frm_gac.FOLIO = folio;

        if(folio.length == 30) {
          this.wbService.getInformation('1', '').subscribe(response => {
            this.f_drf_frm_gac.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
          })
        } else {
          this.f_drf_frm_gac.FECHA = fecha;
        }
      } else {
        this.wbService.getInformation('1', '').subscribe(response => {
          this.f_drf_frm_gac.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
        })
      }

      this.f_drf_frm_gac.STTUS = '1',
      this.f_drf_frm_gac.EJERC = sessionStorage.getItem('year'),
      this.f_drf_frm_gac.USUAR = this.authService.user.user_name,
      this.f_drf_frm_gac.REGIO = this.authService.user.office,
      this.names.ELNOM = this.authService.user.surname_first+' '+this.authService.user.surname_second+' '+this.authService.user.name;
      
      this.wbService.getInformation('10', 'PUEST=¯'+this.authService.user.job+'¯').subscribe(response => {
          if(response.data != null){
          this.names.ELPUE = response.data[0].DESCR;
          }
      })
    }

    protected filterCP() {
        if (!this.ctrpd) {
          return;
        }
        
        let search = this.CPFilterCtrl.value;
        if (!search) {
          this.filteredCP.next(this.ctrpd.slice());
          return;
        } else {
          search = search.toLowerCase();
        }
        
        this.filteredCP.next(
          this.ctrpd.filter(cp => cp.DESCR.toLowerCase().indexOf(search) > -1)
        );
    }
  
    protected filterVB() {
        if (!this.funcionarios) {
          return;
        }
        
        let search = this.VBFilterCtrl.value;
        if (!search) {
          this.filteredVB.next(this.funcionarios.slice());
          return;
        } else {
          search = search.toLowerCase();
        }
        
        this.filteredVB.next(
          this.funcionarios.filter(funcionario => funcionario.DESCR.toLowerCase().indexOf(search) > -1)
        );
    }
  
    protected filterAU() {
        if (!this.funcionarios) {
          return;
        }
        
        let search = this.AUFilterCtrl.value;
        if (!search) {
          this.filteredAU.next(this.funcionarios.slice());
          return;
        } else {
          search = search.toLowerCase();
        }
        
        this.filteredAU.next(
          this.funcionarios.filter(funcionario => funcionario.DESCR.toLowerCase().indexOf(search) > -1)
        );
    }

    openDialog(): void {
      const dialogRef = this.dialog.open(DRFGACDialogRecover, {
        width: '1000px',
        height: '650px'
      });
  
      dialogRef.afterClosed().subscribe(result => {
        if(result) {
          this.getInformation(result);
        }
      });
    }

    getInformation(data) {
      this.wbService.getInformationForms(this.format, 'FOLIO=¯'+data.FOLIO+'¯').subscribe(response => {
        let rec_drf_frm_gac = response.F_DRF_FRM_GAC;
  
        if(rec_drf_frm_gac){
          this.f_drf_frm_gac.FOLIO=rec_drf_frm_gac[0].FOLIO,
          this.f_drf_frm_gac.STTUS=rec_drf_frm_gac[0].STTUS,
          this.f_drf_frm_gac.EJERC=rec_drf_frm_gac[0].EJERC,
          this.f_drf_frm_gac.USUAR=rec_drf_frm_gac[0].USUAR,
          this.f_drf_frm_gac.FECHA=rec_drf_frm_gac[0].FOLIO.length != 12 ? this.f_drf_frm_gac.FECHA:this.globalService.getDateUTC(rec_drf_frm_gac[0].FECHA),
          this.f_drf_frm_gac.REGIO=rec_drf_frm_gac[0].REGIO,
          this.f_drf_frm_gac.CTRPD=rec_drf_frm_gac[0].CTRPD,
          this.f_drf_frm_gac.REQUI=rec_drf_frm_gac[0].REQUI,
          this.f_drf_frm_gac.PRTSP=rec_drf_frm_gac[0].PRTSP,
          this.f_drf_frm_gac.RRFCC=rec_drf_frm_gac[0].RRFCC,
          this.f_drf_frm_gac.RZSOC=rec_drf_frm_gac[0].RZSOC,
          this.f_drf_frm_gac.IMPOR=rec_drf_frm_gac[0].IMPOR,
          this.f_drf_frm_gac.UTILI=rec_drf_frm_gac[0].UTILI,
          this.f_drf_frm_gac.VSTBO=rec_drf_frm_gac[0].VSTBO,
          this.f_drf_frm_gac.AUTOR=rec_drf_frm_gac[0].AUTOR;

          this.names.ELNOM = this.authService.user.surname_first+' '+this.authService.user.surname_second+' '+this.authService.user.name;
        
          this.wbService.getInformation('10', 'PUEST=¯'+this.authService.user.job+'¯').subscribe(response => {
            if(response.data != null){
              this.names.ELPUE = response.data[0].DESCR;
            }
          })

          this.puesto(this.f_drf_frm_gac.VSTBO, 'VBPUE');
          this.puesto(this.f_drf_frm_gac.AUTOR, 'AUPUE');

          this.getReq(this.f_drf_frm_gac.CTRPD);
          this.infoReq(this.f_drf_frm_gac.REQUI);
        }else{
          this.globalService.messageSwal('Información no encontrada', 'La información de la comproción '+data.FOLIO+'-'+data.COMPR+ ' no se encontro en el servidor', 'error');
        }
      })
    }

    getReq(ctrpd: string) {
      this.f_drf_frm_gac.PRTSP = '';

        if(this.f_drf_frm_gac.CTRPD != 'SG') {
            let res = this.ctrpd.find( cp => cp.CTRPD === this.f_drf_frm_gac.CTRPD );

            this.f_drf_frm_gac.RRFCC = res.RRFCC,
            this.f_drf_frm_gac.RZSOC = res.RZSOC;
        }

        this.wbService.getInformation('23', 'REGIO=¯'+this.authService.user.office+'¯ AND CTRPD=¯'+ctrpd+'¯').subscribe(response => {
          this.requisiciones = response.data;
        })
    }

    infoReq(req: string) {
        this.f_drf_frm_gac.PRTSP='',
        this.autorizado=0,
        this.disponible=0;
  
        this.wbService.getInformation('23', 'REGIO=¯'+this.authService.user.office+'¯ AND CTRPD=¯'+this.f_drf_frm_gac.CTRPD+'¯ AND FOLIO =¯'+req+'¯').subscribe(response => {
            if(response.data != null) {
              this.f_drf_frm_gac.PRTSP = response.data[0].PRTSP,
              this.autorizado=response.data[0].AUTOR,
              this.disponible=response.data[0].DISPO;
            }
        })
    }

    nombPro(rfc: string) {
        let res = this.proveedores.find( proveedor => proveedor.RRFCC === rfc );

        this.f_drf_frm_gac.RZSOC = res.RZSOC;
    }

    puesto(func: string, puest: string) {
        let res = this.funcionarios.find( funcionario => funcionario.CUSER === func );
        
        this.names[puest] = res.DSCPU;
    }

    saveInformation(type: string, option: string) {
      this._fuseSplashScreenService.show();

      let folio: string = this.f_drf_frm_gac.FOLIO,
          sttus: string = this.f_drf_frm_gac.STTUS;
  
      let information = { 'F_DRF_FRM_GAC': this.f_drf_frm_gac} ;
  
      this.wbService.saveInformationForms(type, this.format, folio, sttus, JSON.stringify(information)).subscribe(
        response => {
          let old_folio = this.f_drf_frm_gac.FOLIO;
  
          this.f_drf_frm_gac.FOLIO = response.FOLIO;
  
          if(!folio) {
            this.globalService.messageSwal('Información Almacenada', 'Tu folio temporal es: ' + response.FOLIO, 'success');
          } else {
            if(type == 'CF'){
              if(old_folio != this.f_drf_frm_gac.FOLIO) {
                this.globalService.messageSwal('Información Almacenada', 'Tu información ha sido almacenada con el folio: ' + response.FOLIO, 'success');
              } else {
                this.globalService.messageSwal('Información Actualizada', 'Tu información con el folio ' + response.FOLIO + ' ha sido actualizada', 'success');
              }
            } else {
              this.globalService.messageSwal('Información Actualizada', 'Tu información con el folio ' + response.FOLIO + ' ha sido actualizada', 'success');
            }
          }

          if(option == 'VP' || option == 'OK'){
            this.createFile(option);
          } else if(option.length >= 5) {
            document.getElementById(option).click();
          }
  
          if(type == 'CF'){
            this.f_drf_frm_gac.STTUS = '2';
          }

          this._fuseSplashScreenService.hide();
        },
        error => {
          this._fuseSplashScreenService.hide();
          this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
          console.log(error);
        })
    }

    createFile(option: string) {
      let folio: string = this.f_drf_frm_gac.FOLIO;
  
      this.wbService.createFile(option ,this.format, folio).subscribe(x => {
  
        var newBlob = new Blob([x], { type: 'application/pdf' });
  
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(newBlob);
            return;
        }
  
        const data = window.URL.createObjectURL(newBlob);
  
        var link = document.createElement('a');
        link.href = data;
        link.download = folio + '-' + option + '.pdf';
  
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
  
        setTimeout(function () {
            window.URL.revokeObjectURL(data);
            link.remove();
        }, 100);
      });
    }

    downloadFile(option: string) {
      let folio: string = this.f_drf_frm_gac.FOLIO;
  
      this.wbService.downloadFile(option ,this.format, folio).subscribe(x => {
  
        var newBlob = new Blob([x], { type: 'application/pdf' });
  
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(newBlob);
            return;
        }
  
        const data = window.URL.createObjectURL(newBlob);
  
        var link = document.createElement('a');
        link.href = data;
        link.download = folio + '-' + option + '.pdf';
  
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
  
        setTimeout(function () {
            window.URL.revokeObjectURL(data);
            link.remove();
        }, 100);
      });
    }

    valDis() {
        if(this.f_drf_frm_gac.STTUS=='1') {
          if(this.groupF && this.groupF.form && this.groupF.controls['disponible']) {
            if(this.f_drf_frm_gac.IMPOR > this.disponible) {
                this.groupF.controls['disponible'].setErrors({'importe': true});
                this.groupF.form.setErrors({'disponible': true});
                return true;
            } else{
                this.groupF.controls['disponible'].setErrors(null);
                this.groupF.form.setErrors(null);
                return false;
            }
          } else {
            return false;
          }
        } else {
          return false;
        }
    }

    valGnr(item, column) {
        if(this.groupF && this.groupF.controls[column]) {
            if(column == 'IMPOR') {
                if(item <= 0) {
                    this.groupF.controls[column].setErrors({'importe': true});
                    return true;
                } else{
                    this.groupF.controls[column].setErrors(null);
                    return false;
                }
            }
        } else {
          return false;
        }
    }

    disabled() {
        if(this.f_drf_frm_gac.STTUS == '1') {
          return false;
        } else {
          return true;
        }
    }
}

@Component({
  selector: 'drf_gac_dialog_recover',
  templateUrl: 'gac.dialog.recover.html',
})
export class DRFGACDialogRecover implements OnInit {
    private gridApi: any;
    public rowSelection = "single";
    public format: string;
    public color = 'accent';
    public mode = 'determinate';
    public value = 0;
    public bufferValue = 100;

    columnDefs = [
      {headerName: 'Folio', field: 'FOLIO', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
      {headerName: 'Fecha', field: 'FECHA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
      {headerName: 'Estatus', field: 'STDSC', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
      {headerName: 'Contrato', field: 'CTRPD', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
      {headerName: 'Proveedor', field: 'RZSOC', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
      {headerName: 'Monto', field: 'IMPOR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true, cellStyle: {'text-align': 'right'} }
    ];

    rowData: any;

  constructor(private authService: AuthService,
              private wbService: WBService,
              private globalService: GlobalService,
              public dialogRef: MatDialogRef<DRFGACDialogRecover>) {
        this.format = 'DRF-FRM-GAC';
    }

    ngOnInit() {
      this.loadData();
    }

    loadData() {
      this.wbService.getInformation('38', 'EJERC='+sessionStorage.getItem('year')+' AND USUAR=¯' + this.authService.user.user_name + '¯').subscribe(response => {
        this.rowData = response.data;
      })
    }

    onGridReady(params) {
      this.gridApi = params;    
    }

    onNoClick() {
      this.dialogRef.close();
    }

    recoverData() {
      let selectedNodes = this.gridApi.api.getSelectedNodes();

      if(selectedNodes.length <= 0){
        this.globalService.messageSwal('Advertencia', 'Selecciona un folio para recuperar', 'warning');
        return
      }

      this.dialogRef.close(selectedNodes[0].data);
    }

    showAdjuntar() {
      if(this.gridApi && this.gridApi.api){
        let selectedNodes = this.gridApi.api.getSelectedNodes();
        if(selectedNodes.length > 0){
          if(selectedNodes[0].data.STTUS == 2){
            return true;
          }
        }
      }
      
      return false;
    }

    adjuntarPDF() {
      this.value = 0;
      document.getElementById('PDFFR').click();
    }

    fileChange(files: File[], field: string) {
      let selectedNodes = this.gridApi.api.getSelectedNodes();

      let folio: string = selectedNodes[0].data.FOLIO;
  
      if (files.length > 0) {
        if(files[0].name.toLowerCase().endsWith('.pdf')) {
          this.wbService.uploadForm(files[0], this.format, folio).subscribe(
            event => {
              if(event.type === HttpEventType.UploadProgress) {
                this.value = Math.round((event.loaded/event.total)*100);
              } else if(event.type === HttpEventType.Response) {
                let response:any = event.body;

                this.gridApi.api.deselectAll();
                this.loadData();
                this.value = 0;

                this.globalService.messageSwal('Subida Archivo', 'Tu archivo se ha subido con éxito: ' + files[0].name, 'success');
              }
            },
            error => {
              this.value = 0;

              if(error.error){
                if(error.error.file == 'prohibitus'){
                  this.globalService.messageSwal('Archivo Bloqueado', error.error.message, 'error');
                }else{
                  this.globalService.messageSwal('Error', error.error.message, 'error');
                }
              } else{
                console.log(error);
              }
            })
        } else {
          this.globalService.messageSwal('Extensión Invalida', 'Archivo adjunto no se pudo cargar, solo se permiten archivos pdf', 'warning');
        }
      }
    }
}