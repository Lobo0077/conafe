import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { MatTableDataSource } from '@angular/material';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { HttpEventType } from '@angular/common/http';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import { AuthService } from '../../../../services/auth.service';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';

import { F_DRF_FRM_VIA, F_DRF_FRM_VIA_DES, F_DRF_FRM_VIA_BOL, P_DRF_FRM_VIA, names } from '../../../../models/forms/DRF/FRM/VIA/drf.frm.via.model';
import { CustomErrorStateMatcher } from '../../../../models/global/error';
import * as moment from 'moment';

declare var google;

@Component({
    selector     : 'drf_via',
    templateUrl  : './via.component.html',
    styleUrls  : ['./via.component.scss'],
    providers: [
      { provide: MAT_DATE_LOCALE, useValue: 'es-MX' },
      { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
      { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
    ]
})
export class DRFVIAComponent implements OnInit, OnDestroy {
    @ViewChild('groupForm', {static: false}) groupF: NgForm;

    public format: string;
    public error = new CustomErrorStateMatcher();

    public f_drf_frm_via: F_DRF_FRM_VIA = new F_DRF_FRM_VIA();
    public p_drf_frm_via: P_DRF_FRM_VIA = new P_DRF_FRM_VIA();
    public f_drf_frm_via_des: F_DRF_FRM_VIA_DES[] = [];
    public f_drf_frm_via_bol: F_DRF_FRM_VIA_BOL[] = [];
    public names: names = new names();

    public funcionarios: Array<any>;
    public filteredAU: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public AUFilterCtrl: FormControl = new FormControl();

    public regiones: Array<any>;
    public adscripciones: Array<any>;
    public puestos: Array<any>;
    public tipoViaticos: Array<any>;
    public paquetes: Array<any>;
    public medioPagos: Array<any>;
    public medioTransportes: Array<any>;
    public tabulador: Array<any>;
    public zme: Array<any>;
    public diasViaticados: Array<any>;

    protected _onDestroy = new Subject<void>();

    columnsToDestinies = ['ERROR', 'ACTIONS', 'ID', 'PLACE', 'DISTA', 'ZONNA', 'PRINI', 'PRTER', 'DIASS', 'CUODI', 'IMPCM'];
    dataSourceDestinies;

    columnsToTickets = ['ERROR', 'ACTIONS', 'ID', 'COMPA', 'VUELO', 'SALID', 'FECSL', 'HORSL', 'LLEGA', 'FECLL', 'HORLL', 'IMPOR'];
    dataSourceTickets;

    // Private
    private _unsubscribeAll: Subject<any>;

    options = {
      componentRestrictions: { country: 'MX' }
    }
    /**
     * Constructor
     *
     */
    constructor(private authService: AuthService,
                private wbService: WBService,
                private globalService: GlobalService,
                public dialog: MatDialog,
                private cdr: ChangeDetectorRef,
                private _fuseSplashScreenService: FuseSplashScreenService) {

        this.format = 'DRF-FRM-VIA';
        this.f_drf_frm_via.STTUS = '1',
        this.f_drf_frm_via.EJERC = sessionStorage.getItem('year'),
        this.f_drf_frm_via.USUAR = this.authService.user.user_name,
        this.f_drf_frm_via.REGIO = this.authService.user.office,
        this.f_drf_frm_via.ADSCR = this.authService.user.adscription,
        this.f_drf_frm_via.PUEST = this.authService.user.job,
        this.f_drf_frm_via.NIVEL = this.authService.user.level,
        this.names.NUEMP = this.authService.user.n_emp,
        this.names.SLNOM = this.authService.user.surname_first+' '+this.authService.user.surname_second+' '+this.authService.user.name;
        
        this.wbService.getInformation('10', 'PUEST=¯'+this.authService.user.job+'¯').subscribe(response => {
          if(response.data != null){
            this.names.SLPUE = response.data[0].DESCR;
          }
        })

        this.f_drf_frm_via_des.push({
          ID:1,
          ERROR:'',
          PLACE:'',
          LATDS:0,
          LNGDS:0,
          CIUDS:'',
          ESTDS:'',
          PAIDS:'',
          DISTA:0,
          ZONNA:'',
          PRINI:null,
          PRTER:null,
          DIASS:0,
          CUODI:0,
          IMPCM:0
        });

        this.f_drf_frm_via_bol.push({
          ID:1,
          ERROR:'',
          COMPA:'',
          VUELO:'',
          SALID:'',
          FECSL:null,
          HORSL:null,
          LLEGA:'',
          FECLL:null,
          HORLL:null,
          IMPOR:0
        });

        this.dataSourceDestinies  = new MatTableDataSource(this.f_drf_frm_via_des);
        this.dataSourceTickets  = new MatTableDataSource(this.f_drf_frm_via_bol);
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    ngAfterContentChecked() {
      this.cdr.detectChanges();
    }

    ngOnInit() {
      this.wbService.getInformation('1', '').subscribe(response => {
        this.f_drf_frm_via.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
      })

      this.wbService.getInformation('5', '').subscribe(response => {
        this.funcionarios = response.data;
        this.filteredAU.next(this.funcionarios)
      })

      this.AUFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterAU();
      })

      this.wbService.getInformation('3', 'REGIO=¯'+this.authService.user.office+'¯').subscribe(response => {
        this.regiones = response.data;

        if(this.regiones != null) {
          this.f_drf_frm_via.CIUPR = this.regiones[0].CIUDA,
          this.f_drf_frm_via.ESTPR = this.regiones[0].ESTAD,
          this.f_drf_frm_via.PAIPR = this.regiones[0].PAISS,
          this.f_drf_frm_via.LATPR = this.regiones[0].LATIT,
          this.f_drf_frm_via.LNGPR = this.regiones[0].LONGI;
        }
      })

      this.wbService.getInformation('4', '').subscribe(response => {
        this.adscripciones = response.data;
      })

      this.wbService.getInformation('10', '').subscribe(response => {
        this.puestos = response.data;
      })

      this.wbService.getInformation('25', '').subscribe(response => {
        this.tipoViaticos = response.data;
      })

      this.wbService.getInformation('26', '').subscribe(response => {
        this.paquetes = response.data;
      })

      this.wbService.getInformation('27', '').subscribe(response => {
        this.medioPagos = response.data;
      })

      this.wbService.getInformation('28', '').subscribe(response => {
        this.medioTransportes = response.data;
      })

      if(this.authService.user.level && this.authService.user.level.length > 0) {
        if(isNaN(this.authService.user.level.substring(0,1))) {
          this.wbService.getInformation('30', 'GRPJR=¯'+this.authService.user.level.substring(0,1)+'¯').subscribe(response => {
            this.tabulador = response.data;
          })
        } else {
          this.wbService.getInformation('30', 'GRPJR=¯¯').subscribe(response => {
            this.tabulador = response.data;
          })          
        }
      } else {
        this.wbService.getInformation('30', 'GRPJR=¯¯').subscribe(response => {
          this.tabulador = response.data;
        })
      }

      this.wbService.getInformation('31', '').subscribe(response => {
        this.zme = response.data;
      })

      this.wbService.getInformation('32', 'USUAR=¯'+this.authService.user.user_name+'¯ AND EJERC=¯'+sessionStorage.getItem('year')+'¯').subscribe(response => {
        this.diasViaticados = response.data;

        this.names.TDIAS = this.diasViaticados != null ? this.diasViaticados[0].TTDIA:0;
      })
    }

    ngOnDestroy() {
      this._onDestroy.next();
      this._onDestroy.complete();
    }

    clear(type) {
      let folio = this.f_drf_frm_via.FOLIO,
          fecha = this.f_drf_frm_via.FECHA;

      this.f_drf_frm_via = new F_DRF_FRM_VIA();
      this.p_drf_frm_via = new P_DRF_FRM_VIA();
      this.f_drf_frm_via_des = [];
      this.f_drf_frm_via_bol = [];

      this.names = new names();

      if(type == 'C') {
        this.f_drf_frm_via.FOLIO = folio;

        if(folio.length != 12) {
          this.wbService.getInformation('1', '').subscribe(response => {
            this.f_drf_frm_via.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
          })
        } else {
          this.f_drf_frm_via.FECHA = fecha;
        }
      } else {
        this.wbService.getInformation('1', '').subscribe(response => {
          this.f_drf_frm_via.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
        })
      }

      this.f_drf_frm_via.STTUS = '1',
      this.f_drf_frm_via.EJERC = sessionStorage.getItem('year'),
      this.f_drf_frm_via.USUAR = this.authService.user.user_name,
      this.f_drf_frm_via.REGIO = this.authService.user.office,
      this.f_drf_frm_via.ADSCR = this.authService.user.adscription,
      this.f_drf_frm_via.PUEST = this.authService.user.job,
      this.f_drf_frm_via.NIVEL = this.authService.user.level,
      this.names.NUEMP = this.authService.user.n_emp,
      this.names.SLNOM = this.authService.user.surname_first+' '+this.authService.user.surname_second+' '+this.authService.user.name;
      
      this.wbService.getInformation('10', 'PUEST=¯'+this.authService.user.job+'¯').subscribe(response => {
        if(response.data != null){
          this.names.SLPUE = response.data[0].DESCR;
        }
      })

      if(this.regiones != null) {
        this.f_drf_frm_via.CIUPR = this.regiones[0].CIUDA,
        this.f_drf_frm_via.ESTPR = this.regiones[0].ESTAD,
        this.f_drf_frm_via.PAIPR = this.regiones[0].PAISS,
        this.f_drf_frm_via.LATPR = this.regiones[0].LATIT,
        this.f_drf_frm_via.LNGPR = this.regiones[0].LONGI;
      }

      this.f_drf_frm_via_des.push({
        ID:1,
        ERROR:'',
        PLACE:'',
        LATDS:0,
        LNGDS:0,
        CIUDS:'',
        ESTDS:'',
        PAIDS:'',
        DISTA:0,
        ZONNA:'',
        PRINI:null,
        PRTER:null,
        DIASS:0,
        CUODI:0,
        IMPCM:0
      });

      this.f_drf_frm_via_bol.push({
        ID:1,
        ERROR:'',
        COMPA:'',
        VUELO:'',
        SALID:'',
        FECSL:null,
        HORSL:null,
        LLEGA:'',
        FECLL:null,
        HORLL:null,
        IMPOR:0
      });

      this.dataSourceDestinies  = new MatTableDataSource(this.f_drf_frm_via_des);
      this.dataSourceTickets  = new MatTableDataSource(this.f_drf_frm_via_bol);

      this.wbService.getInformation('32', 'USUAR=¯'+this.authService.user.user_name+'¯ AND EJERC=¯'+sessionStorage.getItem('year')+'¯').subscribe(response => {
        this.diasViaticados = response.data;

        this.names.TDIAS = this.diasViaticados != null ? this.diasViaticados[0].TTDIA:0;
      })
    }

    openDialog(): void {
      const dialogRef = this.dialog.open(DRFVIADialogRecover, {
        width: '1000px',
        height: '650px'
      });
  
      dialogRef.afterClosed().subscribe(result => {
        if(result && result.length > 0) {
          this.getInformation(result);
        }
      });
    }

    getInformation(fol: string) {
      this.wbService.getInformationForms(this.format, 'FOLIO=¯'+fol+'¯').subscribe(response => {
        let rec_drf_frm_via = response.F_DRF_FRM_VIA,
            rec_drf_frm_via_des = response.F_DRF_FRM_VIA_DES,
            rec_drf_frm_via_bol = response.F_DRF_FRM_VIA_BOL;
  
        if(rec_drf_frm_via && rec_drf_frm_via_des && rec_drf_frm_via_bol){
          this.f_drf_frm_via.FOLIO=rec_drf_frm_via[0].FOLIO,
          this.f_drf_frm_via.STTUS=rec_drf_frm_via[0].STTUS,
          this.f_drf_frm_via.EJERC=rec_drf_frm_via[0].EJERC,
          this.f_drf_frm_via.USUAR=rec_drf_frm_via[0].USUAR,
          this.f_drf_frm_via.FECHA=rec_drf_frm_via[0].FOLIO.length != 12 ? this.f_drf_frm_via.FECHA:this.globalService.getDateUTC(rec_drf_frm_via[0].FECHA),
          this.f_drf_frm_via.REGIO=rec_drf_frm_via[0].REGIO,
          this.f_drf_frm_via.ADSCR=rec_drf_frm_via[0].ADSCR,
          this.f_drf_frm_via.PUEST=rec_drf_frm_via[0].PUEST,
          this.f_drf_frm_via.NIVEL=rec_drf_frm_via[0].NIVEL,
          this.f_drf_frm_via.LATPR=rec_drf_frm_via[0].LATPR,
          this.f_drf_frm_via.LNGPR=rec_drf_frm_via[0].LNGPR,
          this.f_drf_frm_via.CIUPR=rec_drf_frm_via[0].CIUPR,
          this.f_drf_frm_via.ESTPR=rec_drf_frm_via[0].ESTPR,
          this.f_drf_frm_via.PAIPR=rec_drf_frm_via[0].PAIPR,
          this.f_drf_frm_via.TPVIA=rec_drf_frm_via[0].TPVIA,
          this.f_drf_frm_via.PAQUE=rec_drf_frm_via[0].PAQUE,
          this.f_drf_frm_via.MDPAG=rec_drf_frm_via[0].MDPAG,
          this.f_drf_frm_via.TTSOL=rec_drf_frm_via[0].TTSOL,
          this.f_drf_frm_via.TTDIA=rec_drf_frm_via[0].TTDIA,
          this.f_drf_frm_via.TTVIA=rec_drf_frm_via[0].TTVIA,
          this.f_drf_frm_via.BANCO=rec_drf_frm_via[0].BANCO,
          this.f_drf_frm_via.SUCUR=rec_drf_frm_via[0].SUCUR,
          this.f_drf_frm_via.CLABE=rec_drf_frm_via[0].CLABE,
          this.f_drf_frm_via.MDTRA=rec_drf_frm_via[0].MDTRA,
          this.f_drf_frm_via.GSYPE=rec_drf_frm_via[0].GSYPE,
          this.f_drf_frm_via.CZPDF=rec_drf_frm_via[0].CZPDF,
          this.f_drf_frm_via.AUPDF=rec_drf_frm_via[0].AUPDF,
          this.f_drf_frm_via.TTBOL=rec_drf_frm_via[0].TTBOL,
          this.f_drf_frm_via.OBCOM=rec_drf_frm_via[0].OBCOM,
          this.f_drf_frm_via.JUSTI=rec_drf_frm_via[0].JUSTI,
          this.f_drf_frm_via.OBSER=rec_drf_frm_via[0].OBSER,
          this.f_drf_frm_via.AUTOR=rec_drf_frm_via[0].AUTOR;
  
          if(this.f_drf_frm_via.STTUS == '1' && this.regiones != null) {
            this.f_drf_frm_via.CIUPR = this.regiones[0].CIUDA,
            this.f_drf_frm_via.ESTPR = this.regiones[0].ESTAD,
            this.f_drf_frm_via.PAIPR = this.regiones[0].PAISS,
            this.f_drf_frm_via.LATPR = this.regiones[0].LATIT,
            this.f_drf_frm_via.LNGPR = this.regiones[0].LONGI;
          }

          this.p_drf_frm_via.AUPDF=rec_drf_frm_via[0].AUPDF ? 100:0;
          this.p_drf_frm_via.CZPDF=rec_drf_frm_via[0].CZPDF ? 100:0;

          this.f_drf_frm_via_des = [];
          this.f_drf_frm_via_bol = [];
  
          for(let x = 0; x < rec_drf_frm_via_des.length; x++){
            this.f_drf_frm_via_des.push({
              ID:rec_drf_frm_via_des[x].ID,
              ERROR:rec_drf_frm_via_des[x].ERROR,
              PLACE:rec_drf_frm_via_des[x].PLACE,
              LATDS:rec_drf_frm_via_des[x].LATDS,
              LNGDS:rec_drf_frm_via_des[x].LNGDS,
              CIUDS:rec_drf_frm_via_des[x].CIUDS,
              ESTDS:rec_drf_frm_via_des[x].ESTDS,
              PAIDS:rec_drf_frm_via_des[x].PAIDS,
              DISTA:rec_drf_frm_via_des[x].DISTA,
              ZONNA:rec_drf_frm_via_des[x].ZONNA,
              PRINI:rec_drf_frm_via_des[x].PRINI ? this.globalService.getDateUTC(rec_drf_frm_via_des[x].PRINI):null,
              PRTER:rec_drf_frm_via_des[x].PRTER ? this.globalService.getDateUTC(rec_drf_frm_via_des[x].PRTER):null,
              DIASS:rec_drf_frm_via_des[x].DIASS,
              CUODI:rec_drf_frm_via_des[x].CUODI,
              IMPCM:rec_drf_frm_via_des[x].IMPCM
            });
          }

          for(let x = 0; x < rec_drf_frm_via_bol.length; x++){
            this.f_drf_frm_via_bol.push({
              ID:rec_drf_frm_via_bol[x].ID,
              ERROR:rec_drf_frm_via_bol[x].ERROR,
              COMPA:rec_drf_frm_via_bol[x].COMPA,
              VUELO:rec_drf_frm_via_bol[x].VUELO,
              SALID:rec_drf_frm_via_bol[x].SALID,
              FECSL:rec_drf_frm_via_bol[x].FECSL ? this.globalService.getDateUTC(rec_drf_frm_via_bol[x].FECSL):null,
              HORSL:rec_drf_frm_via_bol[x].HORSL,
              LLEGA:rec_drf_frm_via_bol[x].LLEGA,
              FECLL:rec_drf_frm_via_bol[x].FECLL ? this.globalService.getDateUTC(rec_drf_frm_via_bol[x].FECLL):null,
              HORLL:rec_drf_frm_via_bol[x].HORLL,
              IMPOR:rec_drf_frm_via_bol[x].IMPOR  
            });
          }
          
          this.dataSourceDestinies  = new MatTableDataSource(this.f_drf_frm_via_des);
          this.dataSourceTickets  = new MatTableDataSource(this.f_drf_frm_via_bol);

          for(let x = 0; x < this.f_drf_frm_via_des.length; x++){
            this.calcDistancia(x);
          }

          this.transporteSelect();
          this.puesto(this.f_drf_frm_via.AUTOR, 'AUPUE');

        }else{
          this.globalService.messageSwal('Información no encontrada', 'La información con el folio '+fol+ ' no se encontro en el servidor', 'error');
        }
      })
    }

    addRowD() {
      let index = this.f_drf_frm_via_des.length+1;
  
      this.f_drf_frm_via_des.push({
        ID:index,
        ERROR:'',
        PLACE:'',
        LATDS:0,
        LNGDS:0,
        CIUDS:'',
        ESTDS:'',
        PAIDS:'',
        DISTA:0,
        ZONNA:'',
        PRINI:null,
        PRTER:null,
        DIASS:0,
        CUODI:0,
        IMPCM:0
      });
  
      this.dataSourceDestinies  = new MatTableDataSource(this.f_drf_frm_via_des);
    }

    removeRowD(index) {
      this.f_drf_frm_via_des.splice(index,1);
  
      this.f_drf_frm_via_des.forEach(function(element,index) {
        element.ID = index +1;
      })
  
      this.dataSourceDestinies  = new MatTableDataSource(this.f_drf_frm_via_des);
      
      this.updateComision();
    }

    addRowT() {
      let index = this.f_drf_frm_via_bol.length+1;
  
      this.f_drf_frm_via_bol.push({
        ID:index,
        ERROR:'',
        COMPA:'',
        VUELO:'',
        SALID:'',
        FECSL:null,
        HORSL:null,
        LLEGA:'',
        FECLL:null,
        HORLL:null,
        IMPOR:0
      });
  
      this.dataSourceTickets  = new MatTableDataSource(this.f_drf_frm_via_bol);
    }

    removeRowT(index) {
      this.f_drf_frm_via_bol.splice(index,1);
  
      this.f_drf_frm_via_bol.forEach(function(element,index) {
        element.ID = index +1;
      })
  
      this.dataSourceTickets  = new MatTableDataSource(this.f_drf_frm_via_bol);
      this.updateTickets();
    }

    infoAddress(address: any, index) {
      let ad = this.globalService.getAddressJSON(address);

      this.f_drf_frm_via_des[index].PLACE = ad.address;
      this.f_drf_frm_via_des[index].LATDS = ad.latitude;
      this.f_drf_frm_via_des[index].LNGDS = ad.longitud;
      this.f_drf_frm_via_des[index].CIUDS = ad.city;
      this.f_drf_frm_via_des[index].ESTDS = ad.state;
      this.f_drf_frm_via_des[index].PAIDS = ad.country;

      this.calcDistancia(index);

      let res = this.zme.find( z => z.PAISS === ad.country && z.ESTAD === ad.state && z.CIUDA === ad.city );

      if(res) {
        this.f_drf_frm_via_des[index].ZONNA = '-e';
      } else {
        this.f_drf_frm_via_des[index].ZONNA = '+e';
      }

      this.cuota(index);
    }

    calcDistancia(index) {
      let origen = new google.maps.LatLng(this.f_drf_frm_via.LATPR,this.f_drf_frm_via.LNGPR);
      let destiny = new google.maps.LatLng(this.f_drf_frm_via_des[index].LATDS,this.f_drf_frm_via_des[index].LNGDS);

      let distancia = google.maps.geometry.spherical.computeDistanceBetween(origen, destiny);

      this.f_drf_frm_via_des[index].DISTA = (distancia/1000);
    }

    changePackage() {
      for(let x = 0; x < this.f_drf_frm_via_des.length; x++) {
        if(this.f_drf_frm_via_des[x].ZONNA) {
          this.cuota(x);
        }
      }
    }

    cuota(index) {
      if(this.tabulador[0]) {
        let cuota: number = this.f_drf_frm_via_des[index].ZONNA == '-e' ? this.tabulador[0].MNSEC:this.tabulador[0].MASEC;

        if(this.f_drf_frm_via.PAQUE) {
          let rs = this.paquetes.find( p => p.PAQUE === this.f_drf_frm_via.PAQUE );
          this.f_drf_frm_via_des[index].CUODI = (cuota * (rs.PORCE/100));
        } else {
          this.f_drf_frm_via_des[index].CUODI = cuota;
        }
      } else {
        this.f_drf_frm_via_des[index].CUODI = 0;
      }

      this.impComision(index);
    }
    
    diffDays(index) {
      if(this.f_drf_frm_via_des[index].PRINI && this.f_drf_frm_via_des[index].PRTER) {
        let fi = moment(this.f_drf_frm_via_des[index].PRINI),
            ff = moment(this.f_drf_frm_via_des[index].PRTER);

        this.f_drf_frm_via_des[index].DIASS = ff.diff(fi, 'days') + .5;

        this.impComision(index);
      }
    }

    impComision(index) {
      this.f_drf_frm_via_des[index].IMPCM = this.f_drf_frm_via_des[index].DIASS * this.f_drf_frm_via_des[index].CUODI;

      this.updateComision();
    }

    updateComision() {
      let total: number = 0,
          dias: number = 0;
  
      this.f_drf_frm_via_des.forEach(function(element) {
        total += element.IMPCM;
        dias += element.DIASS;
      })
      
      this.f_drf_frm_via.TTVIA = total;
      this.f_drf_frm_via.TTDIA = dias;

      this.updateSolicitado();
    }

    updateSolicitado() {
      this.f_drf_frm_via.TTSOL = this.f_drf_frm_via.TTVIA + this.f_drf_frm_via.GSYPE;
    }

    addressTickets(address: any, index, column) {
      let ad = this.globalService.getAddressJSON(address);

      this.f_drf_frm_via_bol[index][column] = ad.address;
    }

    updateTickets() {
      let total: number = 0;
  
      this.f_drf_frm_via_bol.forEach(function(element) {
        total += element.IMPOR;
      })
      
      this.f_drf_frm_via.TTBOL = total;
    }

    protected filterAU() {
      if (!this.funcionarios) {
        return;
      }
      
      let search = this.AUFilterCtrl.value;
      if (!search) {
        this.filteredAU.next(this.funcionarios.slice());
        return;
      } else {
        search = search.toLowerCase();
      }
      
      this.filteredAU.next(
        this.funcionarios.filter(funcionario => funcionario.DESCR.toLowerCase().indexOf(search) > -1)
      );
    }

    puesto(func: string, puest: string) {
      let res = this.funcionarios.find( funcionario => funcionario.CUSER === func );
      
      this.names[puest] = res.DSCPU;
    }

    transporteSelect() {
      let res = this.medioTransportes.find( mt => mt.MDTRA === this.f_drf_frm_via.MDTRA );

      this.names.GSYPE = res.GSYPE,
      this.names.DTVUE = res.DTVUE;

      if(!this.names.DTVUE) {
        this.f_drf_frm_via_bol = [];

        this.f_drf_frm_via_bol.push({
          ID:1,
          ERROR:'',
          COMPA:'',
          VUELO:'',
          SALID:'',
          FECSL:null,
          HORSL:null,
          LLEGA:'',
          FECLL:null,
          HORLL:null,
          IMPOR:0
        });

        this.dataSourceTickets  = new MatTableDataSource(this.f_drf_frm_via_bol);
        this.updateTickets();
      }
    }

    openInput(fieldName: string) {
      if(this.f_drf_frm_via.STTUS == '1') {
        if(this.f_drf_frm_via.FOLIO) {
          document.getElementById(fieldName).click();
        } else {
          this.saveInformation('SI', fieldName);
        }
      }
    }

    fileChange(files: File[], field: string) {
      let folio: string = this.f_drf_frm_via.FOLIO;
      this.p_drf_frm_via[field] = 0;
  
      if (files.length > 0) {
        if(files[0].name.toLowerCase().endsWith('.pdf')) {
          this.f_drf_frm_via[field] = files[0].name;
    
          this.wbService.uploadFile(files[0], this.format, folio).subscribe(
            event => {
              if(event.type === HttpEventType.UploadProgress) {
                this.p_drf_frm_via[field] = Math.round((event.loaded/event.total)*100);
              } else if(event.type === HttpEventType.Response) {
                let response:any = event.body;
                this.globalService.messageSwal('Subida Archivo', 'Tu archivo se ha subido con éxito: ' + files[0].name, 'success');
              }
            },
            error => {
              if(error.error){
                if(error.error.file == 'prohibitus'){
                  this.f_drf_frm_via[field] = error.error.file;
                  this.globalService.messageSwal('Archivo Bloqueado', error.error.message, 'error');
                }else{
                  this.globalService.messageSwal('Error', error.error.message, 'error');
                }
              } else{
                console.log(error);
              }
            })
        } else {
          this.f_drf_frm_via[field] = '';
          this.globalService.messageSwal('Extensión Invalida', 'Archivo adjunto no se pudo cargar, solo se permiten archivos pdf', 'warning');
        }
      } else {
        this.f_drf_frm_via[field] = '';
      }
    }

    showFile(fieldName: string) {
      let folio: string = this.f_drf_frm_via.FOLIO;
  
      this.wbService.showFile(this.f_drf_frm_via[fieldName] ,this.format, folio, 'application/pdf').subscribe(x => {
        var newBlob = new Blob([x], { type: 'application/pdf' });
  
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(newBlob);
            return;
        }
  
        const data = window.URL.createObjectURL(newBlob);
  
        window.open(data,'_blank');
      });
    }

    showAUPDF() {
      return this.names.TDIAS + this.f_drf_frm_via.TTDIA > 48;
    }

    showGSYPE() {
      return this.names.GSYPE;
    }

    showCZPDF() {
      return this.names.DTVUE;
    }

    valDias() {
      if(this.f_drf_frm_via.STTUS=='1') {
        if(this.groupF && this.groupF.form) {
          if(this.names.TDIAS + this.f_drf_frm_via.TTDIA > 48 && !this.f_drf_frm_via.AUPDF) {
              this.groupF.form.setErrors({'dias': true});
              return true;
          } else{
              this.groupF.form.setErrors(null);
              return false;
          }
        } else {
          return false;
        }
      } else {
        return false;
      }
    }

    valGnr(column) {
      if(this.f_drf_frm_via.STTUS == '1') {
        if(this.groupF && this.groupF.controls[column]) {
          if(column == 'BANCO' || column == 'SUCUR' || column == 'CLABE') {
            if(this.f_drf_frm_via.MDPAG == 'D' && !this.f_drf_frm_via[column]) {
              this.groupF.controls[column].setErrors({'db': true});
              return true;
            } else {
              this.groupF.controls[column].setErrors(null);
              return false;
            }
          } else if(column == 'GSYPE') {
            if(this.names.GSYPE && this.f_drf_frm_via[column] <= 0) {
              this.groupF.controls[column].setErrors({'gp': true});
              return true;
            } else {
              this.groupF.controls[column].setErrors(null);
              return false;
            }
          } else if(column == 'CZPDF') {
            if(this.showCZPDF() && (this.f_drf_frm_via[column].indexOf('prohibitus') >= 0 || this.f_drf_frm_via[column].length <= 0)) {
              this.groupF.controls[column].setErrors({'incorrect': true});
              return true;
            } else {
              this.groupF.controls[column].setErrors(null);
              return false;
            }
          } else if(column == 'AUPDF') {
            if(this.showAUPDF() && (this.f_drf_frm_via[column].indexOf('prohibitus') >= 0 || this.f_drf_frm_via[column].length <= 0)) {
              this.groupF.controls[column].setErrors({'incorrect': true});
              return true;
            } else {
              this.groupF.controls[column].setErrors(null);
              return false;
            }
          }
        } else{
          return false;
        }
      } else {
        return false;
      }
    }

    valDes(item, element, column, index) {
      if(this.f_drf_frm_via.STTUS == '1') {
        if(element.PRINI && element.PRTER && element.PRTER >= element.PRINI) {
          element.ERROR = 'ok';
        } else {
          element.ERROR = '';
        }

        if(this.groupF && this.groupF.controls[column+index]) {
          if(column == 'PRINI') {
            if(item) {
              this.groupF.controls[column+index].setErrors(null);
              return false;
            } else {
              this.groupF.controls[column+index].setErrors({'date': true});
              return true;
            }
          } else if(column == 'PRTER') {
            if(item) {
              if(item >= element.PRINI) {
                this.groupF.controls[column+index].setErrors(null);
                return false;
              } else {
                this.groupF.controls[column+index].setErrors({'date': true});
                return true;
              }
            } else {
              this.groupF.controls[column+index].setErrors({'date': true});
              return true;
            }
          } else if(column == 'DIASS') {
            if(item > 0) {
              this.groupF.controls[column+index].setErrors(null);
              return false;
            } else {
              this.groupF.controls[column+index].setErrors({'dias': true});
              return true;
            }
          }
        } else {
          return false;
        }
      } else {
        element.ERROR = 'ok';
        return false;
      }
    }

    valTick(item, element, column, index) {
      if(this.f_drf_frm_via.STTUS == '1' && this.names.DTVUE) {
        if(element.FECSL && element.FECLL && element.FECLL >= element.FECSL) {
          element.ERROR = 'ok';
        } else {
          element.ERROR = '';
        }

        if(this.groupF && this.groupF.controls[column+index]) {
          if(column == 'COMPA' || column == 'SALID' || column == 'FECSL' || column == 'HORSL' || column == 'LLEGA' || column == 'HORLL') {
            if(item) {
              this.groupF.controls[column+index].setErrors(null);
              return false;
            } else {
              this.groupF.controls[column+index].setErrors({'field': true});
              return true;
            }
          } else if(column == 'FECLL') {
            if(item) {
              if(item >= element.FECSL) {
                this.groupF.controls[column+index].setErrors(null);
                return false;
              } else {
                this.groupF.controls[column+index].setErrors({'date': true});
                return true;
              }
            } else {
              this.groupF.controls[column+index].setErrors({'date': true});
              return true;
            }
          } else if(column == 'IMPOR') {
            if(item > 0) {
              this.groupF.controls[column+index].setErrors(null);
              return false;
            } else {
              this.groupF.controls[column+index].setErrors({'importe': true});
              return true;
            }
          } 
        } else {
          return false;
        }
      } else {
        element.ERROR = 'ok';
        return false;
      }
    }

    saveInformation(type: string, option: string) {
      this._fuseSplashScreenService.show();

      let folio: string = this.f_drf_frm_via.FOLIO,
          sttus: string = this.f_drf_frm_via.STTUS;
  
      let information = { 'F_DRF_FRM_VIA': this.f_drf_frm_via,
                          'F_DRF_FRM_VIA_DES': this.f_drf_frm_via_des,
                          'F_DRF_FRM_VIA_BOL': this.f_drf_frm_via_bol};
  
      this.wbService.saveInformationForms(type, this.format, folio, sttus, JSON.stringify(information)).subscribe(
        response => {
          let old_folio = this.f_drf_frm_via.FOLIO;
  
          this.f_drf_frm_via.FOLIO = response.FOLIO;
  
          if(!folio) {
            this.globalService.messageSwal('Información Almacenada', 'Tu folio temporal es: ' + response.FOLIO, 'success');
          } else {
            if(type == 'CF'){
              if(old_folio != this.f_drf_frm_via.FOLIO) {
                this.globalService.messageSwal('Información Almacenada', 'Tu información ha sido almacenada con el folio: ' + response.FOLIO, 'success');
              } else {
                this.globalService.messageSwal('Información Actualizada', 'Tu información con el folio ' + response.FOLIO + ' ha sido actualizada', 'success');
              }
            } else {
              this.globalService.messageSwal('Información Actualizada', 'Tu información con el folio ' + response.FOLIO + ' ha sido actualizada', 'success');
            }
          }

          if(option == 'VP' || option == 'OK'){
            this.createFile(option);
          } else if(option.length >= 5) {
            document.getElementById(option).click();
          }
  
          if(type == 'CF'){
            this.f_drf_frm_via.STTUS = '2';
          }

          this._fuseSplashScreenService.hide();
        },
        error => {
          this._fuseSplashScreenService.hide();
          this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
          console.log(error);
        })
    }

    createFile(option: string) {
      let folio: string = this.f_drf_frm_via.FOLIO;
  
      this.wbService.createFile(option ,this.format, folio).subscribe(x => {
  
        var newBlob = new Blob([x], { type: 'application/pdf' });
  
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(newBlob);
            return;
        }
  
        const data = window.URL.createObjectURL(newBlob);
  
        var link = document.createElement('a');
        link.href = data;
        link.download = folio + '-' + option + '.pdf';
  
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
  
        setTimeout(function () {
            window.URL.revokeObjectURL(data);
            link.remove();
        }, 100);
      });
    }

    downloadFile(option: string) {
      let folio: string = this.f_drf_frm_via.FOLIO;
  
      this.wbService.downloadFile(option ,this.format, folio).subscribe(x => {
  
        var newBlob = new Blob([x], { type: 'application/pdf' });
  
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(newBlob);
            return;
        }
  
        const data = window.URL.createObjectURL(newBlob);
  
        var link = document.createElement('a');
        link.href = data;
        link.download = folio + '-' + option + '.pdf';
  
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
  
        setTimeout(function () {
            window.URL.revokeObjectURL(data);
            link.remove();
        }, 100);
      });
    }

    disabled() {
      if(this.f_drf_frm_via.STTUS == '1') {
        return false;
      } else {
        return true;
      }
    }
}

@Component({
  selector: 'drf_via_dialog_recover',
  templateUrl: 'via.dialog.recover.html',
})
export class DRFVIADialogRecover implements OnInit {
    private gridApi: any;
    public rowSelection = "single";
    public format: string;
    public color = 'accent';
    public mode = 'determinate';
    public value = 0;
    public bufferValue = 100;

    columnDefs = [
        {headerName: 'Folio', field: 'FOLIO', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Fecha', field: 'FECHA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Estatus', field: 'STDSC', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Tipo de Viáticos', field: 'DSTPV', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Paquete', field: 'DSPAQ', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Solicitado', field: 'TTSOL', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true, cellStyle: {'text-align': 'right'} }
    ];

    rowData: any;

  constructor(private authService: AuthService,
              private wbService: WBService,
              private globalService: GlobalService,
              public dialogRef: MatDialogRef<DRFVIADialogRecover>) {
        this.format = 'DRF-FRM-VIA';
    }

    ngOnInit() {
      this.loadData();
    }

    loadData() {
      this.wbService.getInformation('29', 'EJERC='+sessionStorage.getItem('year')+' AND USUAR=¯' + this.authService.user.user_name + '¯').subscribe(response => {
        this.rowData = response.data;
      })
    }

    onGridReady(params) {
      this.gridApi = params;    
    }

    onNoClick() {
      this.dialogRef.close();
    }

    recoverData() {
      let selectedNodes = this.gridApi.api.getSelectedNodes();

      if(selectedNodes.length <= 0){
        this.globalService.messageSwal('Advertencia', 'Selecciona un folio para recuperar', 'warning');
        return
      }

      this.dialogRef.close(selectedNodes[0].data.FOLIO);
    }

    showAdjuntar() {
      if(this.gridApi && this.gridApi.api){
        let selectedNodes = this.gridApi.api.getSelectedNodes();
        if(selectedNodes.length > 0){
          if(selectedNodes[0].data.STTUS == 2){
            return true;
          }
        }
      }
      
      return false;
    }

    adjuntarPDF() {
      this.value = 0;
      document.getElementById('PDFFR').click();
    }

    fileChange(files: File[], field: string) {
      let selectedNodes = this.gridApi.api.getSelectedNodes();

      let folio: string = selectedNodes[0].data.FOLIO;
  
      if (files.length > 0) {
        if(files[0].name.toLowerCase().endsWith('.pdf')) {
          this.wbService.uploadForm(files[0], this.format, folio).subscribe(
            event => {
              if(event.type === HttpEventType.UploadProgress) {
                this.value = Math.round((event.loaded/event.total)*100);
              } else if(event.type === HttpEventType.Response) {
                let response:any = event.body;

                this.gridApi.api.deselectAll();
                this.loadData();
                this.value = 0;

                this.globalService.messageSwal('Subida Archivo', 'Tu archivo se ha subido con éxito: ' + files[0].name, 'success');
              }
            },
            error => {
              this.value = 0;

              if(error.error){
                if(error.error.file == 'prohibitus'){
                  this.globalService.messageSwal('Archivo Bloqueado', error.error.message, 'error');
                }else{
                  this.globalService.messageSwal('Error', error.error.message, 'error');
                }
              } else{
                console.log(error);
              }
            })
        } else {
          this.globalService.messageSwal('Extensión Invalida', 'Archivo adjunto no se pudo cargar, solo se permiten archivos pdf', 'warning');
        }
      }
    }
}