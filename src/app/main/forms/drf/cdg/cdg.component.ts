import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { MatTableDataSource } from '@angular/material';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { HttpEventType } from '@angular/common/http';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import { AuthService } from '../../../../services/auth.service';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';

import { F_DRF_FRM_CDG, F_DRF_FRM_CDG_COM, P_DRF_FRM_CDG_COM, names } from '../../../../models/forms/DRF/FRM/CDG/drf.frm.cdg.model';
import { CustomErrorStateMatcher } from '../../../../models/global/error';

@Component({
    selector     : 'drf_cdg',
    templateUrl  : './cdg.component.html',
    styleUrls  : ['./cdg.component.scss'],
    providers: [
      { provide: MAT_DATE_LOCALE, useValue: 'es-MX' },
      { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
      { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
    ]
})
export class DRFCDGComponent implements OnInit, OnDestroy {
    @ViewChild('groupForm', {static: false}) groupF: NgForm;

    public format: string;
    public error = new CustomErrorStateMatcher();

    public f_drf_frm_cdg: F_DRF_FRM_CDG = new F_DRF_FRM_CDG();
    public f_drf_frm_cdg_com: F_DRF_FRM_CDG_COM[] = [];
    public p_drf_frm_cdg_com: P_DRF_FRM_CDG_COM[] = [];
    public names: names = new names();

    public funcionarios: Array<any>;
    public filteredAU: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public AUFilterCtrl: FormControl = new FormControl();

    public regiones: Array<any>;
    public partidas: Array<any>;
    public gastosComprobar: Array<any>;

    protected _onDestroy = new Subject<void>();

    columnsToDisplay = ['ERROR', 'ACTIONS', 'ID', 'XXMLL', 'PPDFF', 'NMDOC', 'FCDOC', 'RRFCC', 'CONCE', 'TOTFC'];
    dataSource;
    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     */
    constructor(private authService: AuthService,
                private wbService: WBService,
                private globalService: GlobalService,
                public dialog: MatDialog,
                private cdr: ChangeDetectorRef,
                private _fuseSplashScreenService: FuseSplashScreenService) {

        this.format = 'DRF-FRM-CDG';
        this.f_drf_frm_cdg.GUARD = false;

        this.f_drf_frm_cdg.STTUS = '1',
        this.f_drf_frm_cdg.EJERC = sessionStorage.getItem('year'),
        this.f_drf_frm_cdg.USUAR = this.authService.user.user_name,
        this.f_drf_frm_cdg.REGIO = this.authService.user.office,
        this.names.SLNOM = this.authService.user.surname_first+' '+this.authService.user.surname_second+' '+this.authService.user.name;
        
        this.wbService.getInformation('10', 'PUEST=¯'+this.authService.user.job+'¯').subscribe(response => {
          if(response.data != null){
            this.names.SLPUE = response.data[0].DESCR;
          }
        })

        this.f_drf_frm_cdg_com.push({
          ID:1,
          ERROR:'',
          XXMLL:'',
          PPDFF:'',
          NMDOC:'',
          FCDOC:null,
          RRFCC:'',
          NMRZS:'',
          CONCE:'',
          TOTFC:0
        });

        this.p_drf_frm_cdg_com.push({
          XXMLL:0,
          PPDFF:0
        });

        this.dataSource  = new MatTableDataSource(this.f_drf_frm_cdg_com);

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    ngAfterContentChecked() {
      this.cdr.detectChanges();
    }

    ngOnInit() {
      this.wbService.getInformation('1', '').subscribe(response => {
        this.f_drf_frm_cdg.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
      })

      this.wbService.getInformation('5', '').subscribe(response => {
        this.funcionarios = response.data;
        this.filteredAU.next(this.funcionarios)
      })

      this.AUFilterCtrl.valueChanges
        .pipe(takeUntil(this._onDestroy))
        .subscribe(() => {
          this.filterAU();
      })

      this.wbService.getInformation('3', 'REGIO=¯'+this.authService.user.office+'¯').subscribe(response => {
        this.regiones = response.data;
      })

      this.wbService.getInformation('38', 'EJERC=¯'+sessionStorage.getItem('year')+'¯ AND STTUS=¯3¯ AND REGIO=¯'+this.authService.user.office+'¯ AND FOLIO NOT IN (SELECT FOLIO FROM F_DRF_FRM_CDG)').subscribe(response => {
        this.gastosComprobar = response.data;
      })

      this.wbService.getInformation('9', '').subscribe(response => {
        this.partidas = response.data;
      })
    }

    ngOnDestroy() {
      this._onDestroy.next();
      this._onDestroy.complete();
    }

    clear(type) {
      let folio = this.f_drf_frm_cdg.FOLIO,
          fecha = this.f_drf_frm_cdg.FECHA;

      this.f_drf_frm_cdg = new F_DRF_FRM_CDG();

      this.f_drf_frm_cdg_com = [];
      this.p_drf_frm_cdg_com = [];

      this.names = new names();

      if(type == 'C') {
        this.f_drf_frm_cdg.FOLIO = folio;
        this.f_drf_frm_cdg.GUARD = true;

        this.f_drf_frm_cdg.FECHA = fecha;
      } else {
        this.wbService.getInformation('1', '').subscribe(response => {
          this.f_drf_frm_cdg.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
        })
      }

      this.f_drf_frm_cdg.STTUS = '1',
      this.f_drf_frm_cdg.EJERC = sessionStorage.getItem('year'),
      this.f_drf_frm_cdg.USUAR = this.authService.user.user_name,
      this.f_drf_frm_cdg.REGIO = this.authService.user.office,
      this.names.SLNOM = this.authService.user.surname_first+' '+this.authService.user.surname_second+' '+this.authService.user.name;
      
      this.wbService.getInformation('10', 'PUEST=¯'+this.authService.user.job+'¯').subscribe(response => {
        if(response.data != null){
          this.names.SLPUE = response.data[0].DESCR;
        }
      })

      this.dataSource  = new MatTableDataSource(this.f_drf_frm_cdg_com);

      if(this.f_drf_frm_cdg.FOLIO) {
        this.selectFolio();
      }

      this.f_drf_frm_cdg_com.push({
        ID:1,
        ERROR:'',
        XXMLL:'',
        PPDFF:'',
        NMDOC:'',
        FCDOC:null,
        RRFCC:'',
        NMRZS:'',
        CONCE:'',
        TOTFC:0
      });

      this.p_drf_frm_cdg_com.push({
        XXMLL:0,
        PPDFF:0
      });
    }

    openDialog(): void {
      const dialogRef = this.dialog.open(DRFCDGDialogRecover, {
        width: '1000px',
        height: '650px'
      });
  
      dialogRef.afterClosed().subscribe(result => {
        if(result) {
          this.getInformation(result);
        }
      });
    }

    getInformation(data) {
      this.wbService.getInformationForms(this.format, 'FOLIO=¯'+data.FOLIO+'¯').subscribe(response => {
        let rec_drf_frm_cdg = response.F_DRF_FRM_CDG,
            rec_drf_frm_cdg_com = response.F_DRF_FRM_CDG_COM;
  
        if(rec_drf_frm_cdg && rec_drf_frm_cdg_com){
          this.f_drf_frm_cdg.FOLIO=rec_drf_frm_cdg[0].FOLIO,
          this.f_drf_frm_cdg.STTUS=rec_drf_frm_cdg[0].STTUS,
          this.f_drf_frm_cdg.EJERC=rec_drf_frm_cdg[0].EJERC,
          this.f_drf_frm_cdg.USUAR=rec_drf_frm_cdg[0].USUAR,
          this.f_drf_frm_cdg.FECHA=rec_drf_frm_cdg[0].FOLIO.length != 12 ? this.f_drf_frm_cdg.FECHA:this.globalService.getDateUTC(rec_drf_frm_cdg[0].FECHA),
          this.f_drf_frm_cdg.GUARD=rec_drf_frm_cdg[0].GUARD,
          this.f_drf_frm_cdg.REGIO=rec_drf_frm_cdg[0].REGIO,
          this.f_drf_frm_cdg.COMFS=rec_drf_frm_cdg[0].COMFS,
          this.f_drf_frm_cdg.TOTCM=rec_drf_frm_cdg[0].TOTCM,
          this.f_drf_frm_cdg.DIFER=rec_drf_frm_cdg[0].DIFER,
          this.f_drf_frm_cdg.NMRIN=rec_drf_frm_cdg[0].NMRIN,
          this.f_drf_frm_cdg.MNRIN=rec_drf_frm_cdg[0].MNRIN,
          this.f_drf_frm_cdg.PAGCM=rec_drf_frm_cdg[0].PAGCM,
          this.f_drf_frm_cdg.RMGAC=rec_drf_frm_cdg[0].RMGAC,
          this.f_drf_frm_cdg.OBSER=rec_drf_frm_cdg[0].OBSER,
          this.f_drf_frm_cdg.AUTOR=rec_drf_frm_cdg[0].AUTOR;

          this.f_drf_frm_cdg_com = [];
          this.p_drf_frm_cdg_com = [];
  
          for(let x = 0; x < rec_drf_frm_cdg_com.length; x++){
            this.f_drf_frm_cdg_com.push({
              ID:rec_drf_frm_cdg_com[x].ID,
              ERROR:rec_drf_frm_cdg_com[x].ERROR,
              XXMLL:rec_drf_frm_cdg_com[x].XXMLL,
              PPDFF:rec_drf_frm_cdg_com[x].PPDFF,
              NMDOC:rec_drf_frm_cdg_com[x].NMDOC,
              FCDOC:rec_drf_frm_cdg_com[x].FCDOC ? this.globalService.getDateUTC(rec_drf_frm_cdg_com[x].FCDOC):null,
              RRFCC:rec_drf_frm_cdg_com[x].RRFCC,
              NMRZS:rec_drf_frm_cdg_com[x].NMRZS,
              CONCE:rec_drf_frm_cdg_com[x].CONCE,
              TOTFC:rec_drf_frm_cdg_com[x].TOTFC,
            });

            this.p_drf_frm_cdg_com.push({
              XXMLL:rec_drf_frm_cdg_com[x].XXMLL ? 100:0,
              PPDFF:rec_drf_frm_cdg_com[x].PPDFF ? 100:0
            });
          }
          
          this.dataSource  = new MatTableDataSource(this.f_drf_frm_cdg_com);

          this.selectFolio();
          
          if(this.f_drf_frm_cdg.AUTOR) {
            this.puesto(this.f_drf_frm_cdg.AUTOR, 'AUPUE');
          }
        }else{
          this.globalService.messageSwal('Información no encontrada', 'La información de la comproción '+data.FOLIO+'-'+data.COMPR+ ' no se encontro en el servidor', 'error');
        }
      })
    }

    protected filterAU() {
      if (!this.funcionarios) {
        return;
      }
      
      let search = this.AUFilterCtrl.value;
      if (!search) {
        this.filteredAU.next(this.funcionarios.slice());
        return;
      } else {
        search = search.toLowerCase();
      }
      
      this.filteredAU.next(
        this.funcionarios.filter(funcionario => funcionario.DESCR.toLowerCase().indexOf(search) > -1)
      );
    }

    addRow() {
      let index = this.f_drf_frm_cdg_com.length+1;
  
      this.f_drf_frm_cdg_com.push({
        ID:index,
        ERROR:'',
        XXMLL:'',
        PPDFF:'',
        NMDOC:'',
        FCDOC:null,
        RRFCC:'',
        NMRZS:'',
        CONCE:'',
        TOTFC:0
      });

      this.p_drf_frm_cdg_com.push({
        XXMLL:0,
        PPDFF:0
      });
  
      this.dataSource  = new MatTableDataSource(this.f_drf_frm_cdg_com);
    }

    removeRow(index) {
      this.f_drf_frm_cdg_com.splice(index,1);
      this.p_drf_frm_cdg_com.splice(index,1);
  
      this.f_drf_frm_cdg_com.forEach(function(element,index) {
        element.ID = index +1;
      })
  
      this.dataSource  = new MatTableDataSource(this.f_drf_frm_cdg_com);
      
      this.updateAmount();
    }

    updateAmount() {
      let total: number = 0;
  
      this.f_drf_frm_cdg_com.forEach(function(element) {
        total += element.TOTFC;
      })
      
      this.f_drf_frm_cdg.TOTCM = total;

      this.updateTotal();
    }

    updateTotal() {
      this.f_drf_frm_cdg.DIFER = this.names.IMPOR - this.f_drf_frm_cdg.TOTCM;
      this.f_drf_frm_cdg.RMGAC = this.f_drf_frm_cdg.DIFER > 0 ? this.f_drf_frm_cdg.DIFER - this.f_drf_frm_cdg.MNRIN:0;
    }

    selectFolio() {
      this.wbService.getInformation('38', 'FOLIO=¯'+this.f_drf_frm_cdg.FOLIO+'¯').subscribe(response => {
        if(response.data != null) {
          this.names.CTRPD=response.data[0].CTRPD,
          this.names.REQUI=response.data[0].REQUI,
          this.names.PRTSP=response.data[0].PRTSP,
          this.names.RRFCC=response.data[0].RRFCC,
          this.names.RZSOC=response.data[0].RZSOC,
          this.names.UTILI=response.data[0].UTILI,
          this.names.IMPOR=response.data[0].MONTO;

          this.updateAmount();
        }
      })
    }

    puesto(func: string, puest: string) {
      let res = this.funcionarios.find( funcionario => funcionario.CUSER === func );
      
      this.names[puest] = res.DSCPU;
    }

    saveInformation(type: string, option: string) {
      this._fuseSplashScreenService.show();

      let folio: string = this.f_drf_frm_cdg.FOLIO,
          sttus: string = this.f_drf_frm_cdg.STTUS;
  
      let information = { 'F_DRF_FRM_CDG': this.f_drf_frm_cdg,
                          'F_DRF_FRM_CDG_COM': this.f_drf_frm_cdg_com} ;
  
      this.wbService.saveInformationForms(type, this.format, folio, sttus, JSON.stringify(information)).subscribe(
        response => {
          this.f_drf_frm_cdg.GUARD = true;
          
          let old_folio = this.f_drf_frm_cdg.FOLIO;
  
          this.f_drf_frm_cdg.FOLIO = response.FOLIO;
  
          if(!folio) {
            this.globalService.messageSwal('Información Almacenada', 'Tu folio temporal es: ' + response.FOLIO, 'success');
          } else {
            if(type == 'CF'){
              if(old_folio != this.f_drf_frm_cdg.FOLIO) {
                this.globalService.messageSwal('Información Almacenada', 'Tu información ha sido almacenada con el folio: ' + response.FOLIO, 'success');
              } else {
                this.globalService.messageSwal('Información Actualizada', 'Tu información con el folio ' + response.FOLIO + ' ha sido actualizada', 'success');
              }
            } else {
              this.globalService.messageSwal('Información Actualizada', 'Tu información con el folio ' + response.FOLIO + ' ha sido actualizada', 'success');
            }
          }

          if(option == 'VP' || option == 'OK'){
            this.createFile(option);
          } else if(option.length >= 5) {
            document.getElementById(option).click();
          }
  
          if(type == 'CF'){
            this.f_drf_frm_cdg.STTUS = '2';
          }

          this._fuseSplashScreenService.hide();
        },
        error => {
          this._fuseSplashScreenService.hide();
          this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
          console.log(error);
        })
    }

    createFile(option: string) {
      let folio: string = this.f_drf_frm_cdg.FOLIO;
  
      this.wbService.createFile(option ,this.format, folio).subscribe(x => {
  
        var newBlob = new Blob([x], { type: 'application/pdf' });
  
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(newBlob);
            return;
        }
  
        const data = window.URL.createObjectURL(newBlob);
  
        var link = document.createElement('a');
        link.href = data;
        link.download = folio + '-' + option + '.pdf';
  
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
  
        setTimeout(function () {
            window.URL.revokeObjectURL(data);
            link.remove();
        }, 100);
      });
    }

    downloadFile(option: string) {
      let folio: string = this.f_drf_frm_cdg.FOLIO;
  
      this.wbService.downloadFile(option ,this.format, folio).subscribe(x => {
  
        var newBlob = new Blob([x], { type: 'application/pdf' });
  
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(newBlob);
            return;
        }
  
        const data = window.URL.createObjectURL(newBlob);
  
        var link = document.createElement('a');
        link.href = data;
        link.download = folio + '-' + option + '.pdf';
  
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
  
        setTimeout(function () {
            window.URL.revokeObjectURL(data);
            link.remove();
        }, 100);
      });
    }

    openInput(fieldName: string) {
      if(this.f_drf_frm_cdg.STTUS == '1' && this.f_drf_frm_cdg.FOLIO) {
        if(this.f_drf_frm_cdg.GUARD && this.f_drf_frm_cdg.FOLIO) {
          document.getElementById(fieldName).click();
        } else {
          this.saveInformation('SI', fieldName);
        }
      }
    }

    fileChange(files: File[], field: string, index: number) {
      let folio: string = this.f_drf_frm_cdg.FOLIO;
      
      this.p_drf_frm_cdg_com[index][field] = 0;
  
      if (files.length > 0) {
        let extension = files[0].name.toLowerCase();
        
        if((field=='PPDFF' && extension.endsWith('.pdf')) || (field=='XXMLL' && extension.endsWith('.xml'))) {

          this.f_drf_frm_cdg_com[index][field] = files[0].name;
    
          this.wbService.uploadFile(files[0], this.format, folio).subscribe(
            event => {
              if(event.type === HttpEventType.UploadProgress) {
                this.p_drf_frm_cdg_com[index][field] = Math.round((event.loaded/event.total)*100);
              } else if(event.type === HttpEventType.Response) {
                let response:any = event.body;
                this.globalService.messageSwal('Subida Archivo', 'Tu archivo se ha subido con éxito: ' + files[0].name, 'success');

                if(field=='XXMLL' && extension.endsWith('.xml')){
                  this.wbService.getXML(files[0].name, this.format, folio).subscribe(response => {
                    this.infoXML(index, response);
                  })
                }
              }
            },
            error => {
              if(error.error){
                if(error.error.file == 'prohibitus'){
                  this.f_drf_frm_cdg_com[index][field] = error.error.file;
                  this.globalService.messageSwal('Archivo Bloqueado', error.error.message, 'error');
                }else{
                  this.globalService.messageSwal('Error', error.error.message, 'error');
                }
              } else{
                console.log(error);
              }
            })
        } else {
          this.f_drf_frm_cdg_com[index][field] = '';
          if(field=='PPDFF'){
            this.globalService.messageSwal('Extensión Invalida', 'Archivo adjunto no se pudo cargar, solo se permiten archivos pdf', 'warning');
          } else if(field=='XXMLL'){
            this.globalService.messageSwal('Extensión Invalida', 'Archivo adjunto no se pudo cargar, solo se permiten archivos xml', 'warning');
          }
        }
      } else {
        this.f_drf_frm_cdg_com[index][field] = '';
      }
    }

    infoXML(index: number, result: any){
      if(result["cfdi:Comprobante"]){console.log(result["cfdi:Comprobante"]);
        if(result["cfdi:Comprobante"]["cfdi:Complemento"] && result["cfdi:Comprobante"]["cfdi:Complemento"][1] && result["cfdi:Comprobante"]["cfdi:Complemento"][1]["tfd:TimbreFiscalDigital"]){
          this.f_drf_frm_cdg_com[index].NMDOC = result["cfdi:Comprobante"]["cfdi:Complemento"][1]["tfd:TimbreFiscalDigital"]["UUID"];

          var fecha = result["cfdi:Comprobante"]["cfdi:Complemento"][1]["tfd:TimbreFiscalDigital"]["FechaTimbrado"].substring(0,10).split('-');

          this.f_drf_frm_cdg_com[index].FCDOC = this.globalService.getDateUTC(fecha[2]+'/'+fecha[1]+'/'+fecha[0]);
        }else{
          this.f_drf_frm_cdg_com[index].NMDOC = '';
        }

        if(result["cfdi:Comprobante"]["cfdi:Emisor"] && result["cfdi:Comprobante"]["cfdi:Emisor"]["Rfc"]){
          this.f_drf_frm_cdg_com[index].RRFCC = result["cfdi:Comprobante"]["cfdi:Emisor"]["Rfc"];
        }else{
          this.f_drf_frm_cdg_com[index].RRFCC = '';
        }

        if(result["cfdi:Comprobante"]["cfdi:Emisor"] && result["cfdi:Comprobante"]["cfdi:Emisor"]["Nombre"]){
          this.f_drf_frm_cdg_com[index].NMRZS = result["cfdi:Comprobante"]["cfdi:Emisor"]["Nombre"];
        }else{
          this.f_drf_frm_cdg_com[index].NMRZS = '';
        }

        if(result["cfdi:Comprobante"]["Total"]){
          this.f_drf_frm_cdg_com[index].TOTFC = result["cfdi:Comprobante"]["Total"];
        }else{
          this.f_drf_frm_cdg_com[index].TOTFC = 0;
        }
      }

      this.updateAmount();
    }

    showFile(fieldName: string, index: number) {
      let folio: string = this.f_drf_frm_cdg.FOLIO;

      this.wbService.showFile(this.f_drf_frm_cdg_com[index][fieldName] ,this.format, folio, 'application/pdf').subscribe(x => {
        var newBlob = new Blob([x], { type: 'application/pdf' });
  
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(newBlob);
            return;
        }
  
        const data = window.URL.createObjectURL(newBlob);
  
        window.open(data,'_blank');
      });
    }

    valGnr(item, column) {
      if(this.groupF && this.groupF.controls[column]) {
          if(column == 'RMGAC') {
            if(item > 0) {
                this.groupF.controls[column].setErrors({'remanente': true});
                this.groupF.form.setErrors({'remanente': true});
                return true;
            } else{
                this.groupF.controls[column].setErrors(null);
                this.groupF.form.setErrors(null);
                return false;
            }
          } else if(column == 'AUTOR') {
            if(this.f_drf_frm_cdg.PAGCM) {
              if(item) {
                this.groupF.controls[column].setErrors(null);
                return true;
              } else {
                this.groupF.controls[column].setErrors({'autor': true});
                return false;
              }
            } else {
              this.groupF.controls[column].setErrors(null);
              return false;
            }
          }
      } else {
        return false;
      }
  }

  valCom(item, element, column, index) {
    if(this.f_drf_frm_cdg.STTUS == '1') {
      if(this.f_drf_frm_cdg.COMFS) {
        element.ERROR = 'ok';
      } else {
        if(element.FCDOC) {
          element.ERROR = 'ok';
        } else {
          element.ERROR = '';
        }
      }

      if(this.groupF && this.groupF.controls[column+index]) {
        if(column == 'XXMLL') {
          if(this.f_drf_frm_cdg.COMFS && ((item.substring(item.lastIndexOf("."))).toLowerCase())!='.xml') {
            this.groupF.controls[column+index].setErrors({'extension': true});
            return true;
          } else{
            this.groupF.controls[column+index].setErrors(null);
            return false;
          }
        } else if(column == 'PPDFF') {
          if(item === 'prohibitus' || ((item.substring(item.lastIndexOf("."))).toLowerCase())!='.pdf') {
            this.groupF.controls[column+index].setErrors({'extension': true});
            return true;
          } else{
            this.groupF.controls[column+index].setErrors(null);
            return false;
          }
        } else if(column == 'NMDOC' || column == 'FCDOC') {
          if(this.f_drf_frm_cdg.COMFS || item) {
            this.groupF.controls[column+index].setErrors(null);
            return false;
          } else {
            this.groupF.controls[column+index].setErrors({'field': true});
            return true;
          }
        } else if(column == 'TOTFC') {
          if(this.f_drf_frm_cdg.COMFS || item > 0) {
            this.groupF.controls[column+index].setErrors(null);
            return false;
          } else {
            this.groupF.controls[column+index].setErrors({'dias': true});
            return true;
          }
        }
      } else {
        return false;
      }
    } else {
      element.ERROR = 'ok';
      return false;
    }
  }
    disabledSave() {
      if(this.f_drf_frm_cdg.STTUS == '1') {
        if(this.groupF && this.groupF.controls['FOLIO']) {
          if(this.f_drf_frm_cdg.FOLIO.length <= 0) {
            this.groupF.controls['FOLIO'].setErrors({'folio': true});
            return true;
          } else {
            this.groupF.controls['FOLIO'].setErrors(null);
            return false;
          }
        } else {
          return false;
        }
      } else {
        return false;
      }
    }

    disabled() {
      if(this.f_drf_frm_cdg.STTUS == '1') {
        return false;
      } else {
        return true;
      }
    }
}

@Component({
  selector: 'drf_cdg_dialog_recover',
  templateUrl: 'cdg.dialog.recover.html',
})
export class DRFCDGDialogRecover implements OnInit {
    private gridApi: any;
    public rowSelection = "single";
    public format: string;
    public color = 'accent';
    public mode = 'determinate';
    public value = 0;
    public bufferValue = 100;

    columnDefs = [
      {headerName: 'Folio', field: 'FOLIO', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
      {headerName: 'Fecha', field: 'FECHA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
      {headerName: 'Estatus', field: 'STDSC', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
      {headerName: 'Contrato', field: 'CTRPD', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
      {headerName: 'Proveedor', field: 'RZSOC', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
      {headerName: 'Solicitado', field: 'IMPOR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true, cellStyle: {'text-align': 'right'} },
      {headerName: 'Comprobado', field: 'TOTCM', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true, cellStyle: {'text-align': 'right'} }
    ];

    rowData: any;

  constructor(private authService: AuthService,
              private wbService: WBService,
              private globalService: GlobalService,
              public dialogRef: MatDialogRef<DRFCDGDialogRecover>) {
        this.format = 'DRF-FRM-CDG';
    }

    ngOnInit() {
      this.loadData();
    }

    loadData() {
      this.wbService.getInformation('42', 'EJERC='+sessionStorage.getItem('year')+' AND USUAR=¯' + this.authService.user.user_name + '¯').subscribe(response => {
        this.rowData = response.data;
      })
    }

    onGridReady(params) {
      this.gridApi = params;    
    }

    onNoClick() {
      this.dialogRef.close();
    }

    recoverData() {
      let selectedNodes = this.gridApi.api.getSelectedNodes();

      if(selectedNodes.length <= 0){
        this.globalService.messageSwal('Advertencia', 'Selecciona un folio para recuperar', 'warning');
        return
      }

      this.dialogRef.close(selectedNodes[0].data);
    }

    showAdjuntar() {
      if(this.gridApi && this.gridApi.api){
        let selectedNodes = this.gridApi.api.getSelectedNodes();
        if(selectedNodes.length > 0){
          if(selectedNodes[0].data.STTUS == 2){
            return true;
          }
        }
      }
      
      return false;
    }

    adjuntarPDF() {
      this.value = 0;
      document.getElementById('PDFFR').click();
    }

    fileChange(files: File[], field: string) {
      let selectedNodes = this.gridApi.api.getSelectedNodes();

      let folio: string = selectedNodes[0].data.FOLIO;
  
      if (files.length > 0) {
        if(files[0].name.toLowerCase().endsWith('.pdf')) {
          this.wbService.uploadForm(files[0], this.format, folio).subscribe(
            event => {
              if(event.type === HttpEventType.UploadProgress) {
                this.value = Math.round((event.loaded/event.total)*100);
              } else if(event.type === HttpEventType.Response) {
                let response:any = event.body;

                this.gridApi.api.deselectAll();
                this.loadData();
                this.value = 0;

                this.globalService.messageSwal('Subida Archivo', 'Tu archivo se ha subido con éxito: ' + files[0].name, 'success');
              }
            },
            error => {
              this.value = 0;

              if(error.error){
                if(error.error.file == 'prohibitus'){
                  this.globalService.messageSwal('Archivo Bloqueado', error.error.message, 'error');
                }else{
                  this.globalService.messageSwal('Error', error.error.message, 'error');
                }
              } else{
                console.log(error);
              }
            })
        } else {
          this.globalService.messageSwal('Extensión Invalida', 'Archivo adjunto no se pudo cargar, solo se permiten archivos pdf', 'warning');
        }
      }
    }
}