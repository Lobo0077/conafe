import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { MatTableDataSource } from '@angular/material';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { HttpEventType } from '@angular/common/http';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import { AuthService } from '../../../../services/auth.service';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';

import { F_DRF_FRM_PAP, F_DRF_FRM_PAP_COM, P_DRF_FRM_PAP_COM, names } from '../../../../models/forms/DRF/FRM/PAP/drf.frm.pap.model';
import { CustomErrorStateMatcher } from '../../../../models/global/error';

@Component({
    selector     : 'drf_pap',
    templateUrl  : './pap.component.html',
    styleUrls  : ['./pap.component.scss'],
    animations: [
      trigger('detailExpand', [
        state('collapsed', style({height: '0px', minHeight: '0'})),
        state('expanded', style({height: '*'})),
        transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
      ]),
    ],
    providers: [
      { provide: MAT_DATE_LOCALE, useValue: 'es-MX' },
      { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
      { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
    ]
})
export class DRFPAPComponent implements OnInit, OnDestroy {
    @ViewChild('groupForm', {static: false}) groupF: NgForm;

    public format: string;
    public error = new CustomErrorStateMatcher();

    public f_drf_frm_pap: F_DRF_FRM_PAP = new F_DRF_FRM_PAP();
    public f_drf_frm_pap_com: F_DRF_FRM_PAP_COM[] = [];
    public p_drf_frm_pap_com: P_DRF_FRM_PAP_COM[] = [];
    public names: names = new names();

    public regiones: Array<any>;

    public ctrpd: Array<any>;
    public filteredCP: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public CPFilterCtrl: FormControl = new FormControl();

    public requisiciones: Array<any>;
    public partidas: Array<any>;

    public funcionarios: Array<any>;
    public filteredVB: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public VBFilterCtrl: FormControl = new FormControl();
    public filteredAU: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public AUFilterCtrl: FormControl = new FormControl();

    public autorizado: number = 0;
    public disponible: number = 0;

    protected _onDestroy = new Subject<void>();

    columnsToDisplay = ['ACTIONS', 'ID', 'NMDOC', 'SUBTT', 'IIVAA', 'TOTAL', 'RTISR', 'RTIVA', 'TOTFC'];
    dataSource;
    
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     */
    constructor(private authService: AuthService,
                private wbService: WBService,
                private globalService: GlobalService,
                public dialog: MatDialog,
                private cdr: ChangeDetectorRef,
                private _fuseSplashScreenService: FuseSplashScreenService) {

        this.format = 'DRF-FRM-PAP';
        this.f_drf_frm_pap.STTUS = '1',
        this.f_drf_frm_pap.EJERC = sessionStorage.getItem('year'),
        this.f_drf_frm_pap.USUAR = this.authService.user.user_name,
        this.f_drf_frm_pap.REGIO = this.authService.user.office,
        this.names.ELNOM = this.authService.user.surname_first+' '+this.authService.user.surname_second+' '+this.authService.user.name;
        
        this.wbService.getInformation('10', 'PUEST=¯'+this.authService.user.job+'¯').subscribe(response => {
          if(response.data != null){
            this.names.ELPUE = response.data[0].DESCR;
          }
        })

        this.f_drf_frm_pap_com.push({
          ID:1,
          ERROR:'',
          XXMLL:'',
          PPDFF:'',
          NMDOC:'',
          FCDOC:null,
          RRFCC:'',
          NMRZS:'',
          CONCE:'',
          SUBTT:0,
          IIVAA:0,
          TOTAL:0,
          RTISR:0,
          RTIVA:0,
          TOTFC:0
        });

        this.p_drf_frm_pap_com.push({
          XXMLL:0,
          PPDFF:0
        });

        this.dataSource  = new MatTableDataSource(this.f_drf_frm_pap_com);
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    ngAfterContentChecked() {
      this.cdr.detectChanges();
    }

    ngOnInit() {
      this.wbService.getInformation('1', '').subscribe(response => {
        this.f_drf_frm_pap.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
      })

      this.wbService.getInformation('3', 'REGIO=¯'+this.authService.user.office+'¯').subscribe(response => {
        this.regiones = response.data;
      })

      this.wbService.getInformation('14', 'STTUS=3 AND REGIO=¯'+this.authService.user.office+'¯').subscribe(response => {
        this.ctrpd = response.data;

        this.ctrpd.splice(0, 0, {CTRPD:'SG',DESCR:'Servicio General'});

        this.filteredCP.next(this.ctrpd)
      })

      this.CPFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterCP();
      })

      this.wbService.getInformation('9', '').subscribe(response => {
        this.partidas = response.data;
      })

      this.wbService.getInformation('5', '').subscribe(response => {
        this.funcionarios = response.data;
        this.filteredVB.next(this.funcionarios)
        this.filteredAU.next(this.funcionarios)
      })
  
    this.VBFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterVB();
      })
  
    this.AUFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterAU();
      })
    }

    ngOnDestroy() {
      this._onDestroy.next();
      this._onDestroy.complete();
    }

    nuevo() {
      this.f_drf_frm_pap = new F_DRF_FRM_PAP();
      this.f_drf_frm_pap_com = [];
      this.p_drf_frm_pap_com = [];

      this.names = new names();

      this.f_drf_frm_pap.STTUS = '1',
      this.f_drf_frm_pap.EJERC = sessionStorage.getItem('year'),
      this.f_drf_frm_pap.USUAR = this.authService.user.user_name,
      this.f_drf_frm_pap.REGIO = this.authService.user.office,
      this.names.ELNOM = this.authService.user.surname_first+' '+this.authService.user.surname_second+' '+this.authService.user.name;
        
      this.wbService.getInformation('10', 'PUEST=¯'+this.authService.user.job+'¯').subscribe(response => {
        if(response.data != null){
          this.names.ELPUE = response.data[0].DESCR;
        }
      })

      this.wbService.getInformation('1', '').subscribe(response => {
        this.f_drf_frm_pap.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
      })

      this.f_drf_frm_pap_com.push({
        ID:1,
        ERROR:'',
        XXMLL:'',
        PPDFF:'',
        NMDOC:'',
        FCDOC:null,
        RRFCC:'',
        NMRZS:'',
        CONCE:'',
        SUBTT:0,
        IIVAA:0,
        TOTAL:0,
        RTISR:0,
        RTIVA:0,
        TOTFC:0
      });

      this.p_drf_frm_pap_com.push({
        XXMLL:0,
        PPDFF:0
      });

      this.dataSource  = new MatTableDataSource(this.f_drf_frm_pap_com);  
      
      this.autorizado = 0;
      this.disponible = 0;
    }

    openDialog(): void {
      const dialogRef = this.dialog.open(DRFPAPDialogRecover, {
        width: '1000px',
        height: '650px'
      });
  
      dialogRef.afterClosed().subscribe(result => {
        if(result && result.length > 0) {
          this.getInformation(result);
        }
      });
    }

    getInformation(fol: string) {
      this.wbService.getInformationForms(this.format, 'FOLIO=¯'+fol+'¯').subscribe(response => {
        let rec_drf_frm_pap = response.F_DRF_FRM_PAP,
            rec_drf_frm_pap_com = response.F_DRF_FRM_PAP_COM;
  
        if(rec_drf_frm_pap && rec_drf_frm_pap_com){
          this.f_drf_frm_pap.FOLIO=rec_drf_frm_pap[0].FOLIO,
          this.f_drf_frm_pap.STTUS=rec_drf_frm_pap[0].STTUS,
          this.f_drf_frm_pap.EJERC=rec_drf_frm_pap[0].EJERC,
          this.f_drf_frm_pap.USUAR=rec_drf_frm_pap[0].USUAR,
          this.f_drf_frm_pap.FECHA=rec_drf_frm_pap[0].FOLIO.length != 12 ? this.f_drf_frm_pap.FECHA:this.globalService.getDateUTC(rec_drf_frm_pap[0].FECHA),
          this.f_drf_frm_pap.CTRPD=rec_drf_frm_pap[0].CTRPD,
          this.f_drf_frm_pap.REQUI=rec_drf_frm_pap[0].REQUI,
          this.f_drf_frm_pap.PRTSP=rec_drf_frm_pap[0].PRTSP,
          this.f_drf_frm_pap.DTBAN=rec_drf_frm_pap[0].DTBAN,
          this.f_drf_frm_pap.TTSUB=rec_drf_frm_pap[0].TTSUB,
          this.f_drf_frm_pap.TTIVA=rec_drf_frm_pap[0].TTIVA,
          this.f_drf_frm_pap.TTTOT=rec_drf_frm_pap[0].TTTOT,
          this.f_drf_frm_pap.TTRIS=rec_drf_frm_pap[0].TTRIS,
          this.f_drf_frm_pap.TTRIV=rec_drf_frm_pap[0].TTRIV,
          this.f_drf_frm_pap.TOTAL=rec_drf_frm_pap[0].TOTAL,
          this.f_drf_frm_pap.OBSER=rec_drf_frm_pap[0].OBSER,
          this.f_drf_frm_pap.VSTBO=rec_drf_frm_pap[0].VSTBO,
          this.f_drf_frm_pap.AUTOR=rec_drf_frm_pap[0].AUTOR;
  
          this.f_drf_frm_pap_com = [];
          this.p_drf_frm_pap_com = [];
  
          for(let x = 0; x < rec_drf_frm_pap_com.length; x++){
            this.f_drf_frm_pap_com.push({
              ID:rec_drf_frm_pap_com[x].ID,
              ERROR:rec_drf_frm_pap_com[x].ERROR,
              XXMLL:rec_drf_frm_pap_com[x].XXMLL,
              PPDFF:rec_drf_frm_pap_com[x].PPDFF,
              NMDOC:rec_drf_frm_pap_com[x].NMDOC,
              FCDOC:rec_drf_frm_pap_com[x].FCDOC ? this.globalService.getDateUTC(rec_drf_frm_pap_com[x].FCDOC):null,
              RRFCC:rec_drf_frm_pap_com[x].RRFCC,
              NMRZS:rec_drf_frm_pap_com[x].NMRZS,
              CONCE:rec_drf_frm_pap_com[x].CONCE,
              SUBTT:rec_drf_frm_pap_com[x].SUBTT,
              IIVAA:rec_drf_frm_pap_com[x].IIVAA,
              TOTAL:rec_drf_frm_pap_com[x].TOTAL,
              RTISR:rec_drf_frm_pap_com[x].RTISR,
              RTIVA:rec_drf_frm_pap_com[x].RTIVA,
              TOTFC:rec_drf_frm_pap_com[x].TOTFC
            });

            this.p_drf_frm_pap_com.push({
              XXMLL:rec_drf_frm_pap_com[x].XXMLL.length > 0 ? 100:0,
              PPDFF:rec_drf_frm_pap_com[x].PPDFF.length > 0 ? 100:0
            });
          }
    
          this.dataSource  = new MatTableDataSource(this.f_drf_frm_pap_com);

          this.names.ELNOM = this.authService.user.surname_first+' '+this.authService.user.surname_second+' '+this.authService.user.name;
        
          this.wbService.getInformation('10', 'PUEST=¯'+this.authService.user.job+'¯').subscribe(response => {
            if(response.data != null){
              this.names.ELPUE = response.data[0].DESCR;
            }
          })

          this.puesto(this.f_drf_frm_pap.VSTBO, 'VBPUE');
          this.puesto(this.f_drf_frm_pap.AUTOR, 'AUPUE');

          this.getReq(this.f_drf_frm_pap.CTRPD);
          this.infoReq(this.f_drf_frm_pap.REQUI);
        }else{
          this.globalService.messageSwal('Información no encontrada', 'La información con el folio '+fol+ ' no se encontro en el servidor', 'error');
        }
      })
    }

    getReq(ctrpd: string) {
      this.f_drf_frm_pap.PRTSP = '';

      this.wbService.getInformation('23', 'REGIO=¯'+this.authService.user.office+'¯ AND CTRPD=¯'+ctrpd+'¯').subscribe(response => {
        this.requisiciones = response.data;
      })
    }

    infoReq(req: string) {
      this.f_drf_frm_pap.PRTSP='',
      this.autorizado=0,
      this.disponible=0;

      this.wbService.getInformation('23', 'REGIO=¯'+this.authService.user.office+'¯ AND CTRPD=¯'+this.f_drf_frm_pap.CTRPD+'¯ AND FOLIO =¯'+req+'¯').subscribe(response => {
          if(response.data != null) {
            this.f_drf_frm_pap.PRTSP = response.data[0].PRTSP,
            this.autorizado=response.data[0].AUTOR,
            this.disponible=response.data[0].DISPO;
          }
      })
    }

    addRow() {
      let index = this.f_drf_frm_pap_com.length+1;
  
      this.f_drf_frm_pap_com.push({
        ID:index,
        ERROR:'',
        XXMLL:'',
        PPDFF:'',
        NMDOC:'',
        FCDOC:null,
        RRFCC:'',
        NMRZS:'',
        CONCE:'',
        SUBTT:0,
        IIVAA:0,
        TOTAL:0,
        RTISR:0,
        RTIVA:0,
        TOTFC:0
      });

      this.p_drf_frm_pap_com.push({
        XXMLL:0,
        PPDFF:0
      });
  
      this.dataSource  = new MatTableDataSource(this.f_drf_frm_pap_com);
    }

    removeRow(index) {
      this.f_drf_frm_pap_com.splice(index,1);
  
      this.f_drf_frm_pap_com.forEach(function(element,index) {
        element.ID = index +1;
      })

      this.p_drf_frm_pap_com.splice(index,1);
  
      this.dataSource  = new MatTableDataSource(this.f_drf_frm_pap_com);
      this.updateAmounts();
    }

    updateAmounts(){
      let ttsub: number = 0,
          ttiva: number = 0,
          tttot: number = 0,
          ttris: number = 0,
          ttriv: number = 0,
          total: number = 0;
  
      this.f_drf_frm_pap_com.forEach(function(element) {
        ttsub += element.SUBTT;
        ttiva += element.IIVAA;
        tttot += element.TOTAL;
        ttris += element.RTISR;
        ttriv += element.RTIVA;
        total += element.TOTFC;
      })
      
      this.f_drf_frm_pap.TTSUB = ttsub;
      this.f_drf_frm_pap.TTIVA = ttiva;
      this.f_drf_frm_pap.TTTOT = tttot;
      this.f_drf_frm_pap.TTRIS = ttris;
      this.f_drf_frm_pap.TTRIV = ttriv;
      this.f_drf_frm_pap.TOTAL = total;
    }

    openInput(fieldName: string) {
      if(this.f_drf_frm_pap.STTUS == '1') {
        if(this.f_drf_frm_pap.FOLIO) {
          document.getElementById(fieldName).click();
        } else {
          this.saveInformation('SI', fieldName);
        }
      }
    }

    fileChange(files: File[], field: string, index: number) {
      let folio: string = this.f_drf_frm_pap.FOLIO;
      this.p_drf_frm_pap_com[index][field] = 0;
  
      if (files.length > 0) {
        let extension = files[0].name.toLowerCase();
        
        if((field=='PPDFF' && extension.endsWith('.pdf')) || (field=='XXMLL' && extension.endsWith('.xml'))) {
          this.f_drf_frm_pap_com[index][field] = files[0].name;
    
          this.wbService.uploadFile(files[0], this.format, folio).subscribe(
            event => {
              if(event.type === HttpEventType.UploadProgress) {
                this.p_drf_frm_pap_com[index][field] = Math.round((event.loaded/event.total)*100);
              } else if(event.type === HttpEventType.Response) {
                let response:any = event.body;
                this.globalService.messageSwal('Subida Archivo', 'Tu archivo se ha subido con éxito: ' + files[0].name, 'success');

                if(field=='XXMLL' && extension.endsWith('.xml')){
                  this.wbService.getXML(files[0].name, this.format, folio).subscribe(response => {
                    this.infoXML(index, response);
                  })
                }
              }
            },
            error => {
              if(error.error){
                if(error.error.file == 'prohibitus'){
                  this.f_drf_frm_pap_com[index][field] = error.error.file;
                  this.globalService.messageSwal('Archivo Bloqueado', error.error.message, 'error');
                }else{
                  this.globalService.messageSwal('Error', error.error.message, 'error');
                }
              } else{
                console.log(error);
              }
            })
        } else {
          this.f_drf_frm_pap_com[index][field] = '';
          if(field=='PPDFF'){
            this.globalService.messageSwal('Extensión Invalida', 'Archivo adjunto no se pudo cargar, solo se permiten archivos pdf', 'warning');
          } else if(field=='XXMLL'){
            this.globalService.messageSwal('Extensión Invalida', 'Archivo adjunto no se pudo cargar, solo se permiten archivos xml', 'warning');
          }
        }
      } else {
        this.f_drf_frm_pap_com[index][field] = '';
      }
    }

    infoXML(index: number, result: any){
      if(result["cfdi:Comprobante"]){console.log(result["cfdi:Comprobante"]);
        if(result["cfdi:Comprobante"]["cfdi:Complemento"] && result["cfdi:Comprobante"]["cfdi:Complemento"][1] && result["cfdi:Comprobante"]["cfdi:Complemento"][1]["tfd:TimbreFiscalDigital"]){
          this.f_drf_frm_pap_com[index].NMDOC = result["cfdi:Comprobante"]["cfdi:Complemento"][1]["tfd:TimbreFiscalDigital"]["UUID"];

          var fecha = result["cfdi:Comprobante"]["cfdi:Complemento"][1]["tfd:TimbreFiscalDigital"]["FechaTimbrado"].substring(0,10).split('-');

          this.f_drf_frm_pap_com[index].FCDOC = this.globalService.getDateUTC(fecha[2]+'/'+fecha[1]+'/'+fecha[0]);
        }else{
          this.f_drf_frm_pap_com[index].NMDOC = '';
        }

        if(result["cfdi:Comprobante"]["cfdi:Emisor"] && result["cfdi:Comprobante"]["cfdi:Emisor"]["Rfc"]){
          this.f_drf_frm_pap_com[index].RRFCC = result["cfdi:Comprobante"]["cfdi:Emisor"]["Rfc"];
        }else{
          this.f_drf_frm_pap_com[index].RRFCC = '';
        }

        if(result["cfdi:Comprobante"]["cfdi:Emisor"] && result["cfdi:Comprobante"]["cfdi:Emisor"]["Nombre"]){
          this.f_drf_frm_pap_com[index].NMRZS = result["cfdi:Comprobante"]["cfdi:Emisor"]["Nombre"];
        }else{
          this.f_drf_frm_pap_com[index].NMRZS = '';
        }

        if(result["cfdi:Comprobante"]["SubTotal"]){
          this.f_drf_frm_pap_com[index].SUBTT = result["cfdi:Comprobante"]["SubTotal"];
        }else{
          this.f_drf_frm_pap_com[index].SUBTT = 0;
        }

        if(result["cfdi:Comprobante"]["cfdi:Impuestos"] && result["cfdi:Comprobante"]["cfdi:Impuestos"]["TotalImpuestosTrasladados"]){
          this.f_drf_frm_pap_com[index].IIVAA = result["cfdi:Comprobante"]["cfdi:Impuestos"]["TotalImpuestosTrasladados"];
        }else{
          this.f_drf_frm_pap_com[index].IIVAA = 0;
        }

        this.f_drf_frm_pap_com[index].TOTAL = this.f_drf_frm_pap_com[index].SUBTT + this.f_drf_frm_pap_com[index].IIVAA;

        this.f_drf_frm_pap_com[index].RTISR = 0;
        this.f_drf_frm_pap_com[index].RTIVA = 0;

        if(result["cfdi:Comprobante"]["cfdi:Impuestos"] && result["cfdi:Comprobante"]["cfdi:Impuestos"]["cfdi:Retenciones"] && result["cfdi:Comprobante"]["cfdi:Impuestos"]["cfdi:Retenciones"]["cfdi:Retencion"]){
          if(result["cfdi:Comprobante"]["cfdi:Impuestos"]["cfdi:Retenciones"]["cfdi:Retencion"][0]){
            for(var x = 0; x < result["cfdi:Comprobante"]["cfdi:Impuestos"]["cfdi:Retenciones"]["cfdi:Retencion"].length; x++){
              if(result["cfdi:Comprobante"]["cfdi:Impuestos"]["cfdi:Retenciones"]["cfdi:Retencion"][x]["Impuesto"]){
                if(result["cfdi:Comprobante"]["cfdi:Impuestos"]["cfdi:Retenciones"]["cfdi:Retencion"][x]["Impuesto"] == '001'){
                  this.f_drf_frm_pap_com[index].RTISR = result["cfdi:Comprobante"]["cfdi:Impuestos"]["cfdi:Retenciones"]["cfdi:Retencion"][x]["Importe"];
                } else if(result["cfdi:Comprobante"]["cfdi:Impuestos"]["cfdi:Retenciones"]["cfdi:Retencion"][x]["Impuesto"] == '002'){
                  this.f_drf_frm_pap_com[index].RTIVA = result["cfdi:Comprobante"]["cfdi:Impuestos"]["cfdi:Retenciones"]["cfdi:Retencion"][x]["Importe"];
                }
              }
            }
          } else if(result["cfdi:Comprobante"]["cfdi:Impuestos"]["cfdi:Retenciones"]["cfdi:Retencion"]){
            if(result["cfdi:Comprobante"]["cfdi:Impuestos"]["cfdi:Retenciones"]["cfdi:Retencion"]["Impuesto"]){
              if(result["cfdi:Comprobante"]["cfdi:Impuestos"]["cfdi:Retenciones"]["cfdi:Retencion"]["Impuesto"] == '001'){
                this.f_drf_frm_pap_com[index].RTISR = result["cfdi:Comprobante"]["cfdi:Impuestos"]["cfdi:Retenciones"]["cfdi:Retencion"]["Importe"];
              } else if(result["cfdi:Comprobante"]["cfdi:Impuestos"]["cfdi:Retenciones"]["cfdi:Retencion"]["Impuesto"] == '002'){
                this.f_drf_frm_pap_com[index].RTIVA = result["cfdi:Comprobante"]["cfdi:Impuestos"]["cfdi:Retenciones"]["cfdi:Retencion"]["Importe"];
              }
            }
          }
        }

        if(result["cfdi:Comprobante"]["Total"]){
          this.f_drf_frm_pap_com[index].TOTFC = result["cfdi:Comprobante"]["Total"];
        }else{
          this.f_drf_frm_pap_com[index].TOTFC = 0;
        }
      }

      this.updateAmounts();
    }

    protected filterCP() {
      if (!this.ctrpd) {
        return;
      }
      
      let search = this.CPFilterCtrl.value;
      if (!search) {
        this.filteredCP.next(this.ctrpd.slice());
        return;
      } else {
        search = search.toLowerCase();
      }
      
      this.filteredCP.next(
        this.ctrpd.filter(cp => cp.DESCR.toLowerCase().indexOf(search) > -1)
      );
    }

    protected filterVB() {
      if (!this.funcionarios) {
        return;
      }
      
      let search = this.VBFilterCtrl.value;
      if (!search) {
        this.filteredVB.next(this.funcionarios.slice());
        return;
      } else {
        search = search.toLowerCase();
      }
      
      this.filteredVB.next(
        this.funcionarios.filter(funcionario => funcionario.DESCR.toLowerCase().indexOf(search) > -1)
      );
    }

    protected filterAU() {
      if (!this.funcionarios) {
        return;
      }
      
      let search = this.AUFilterCtrl.value;
      if (!search) {
        this.filteredAU.next(this.funcionarios.slice());
        return;
      } else {
        search = search.toLowerCase();
      }
      
      this.filteredAU.next(
        this.funcionarios.filter(funcionario => funcionario.DESCR.toLowerCase().indexOf(search) > -1)
      );
    }

    puesto(func: string, puest: string) {
      let res = this.funcionarios.find( funcionario => funcionario.CUSER === func );
      
      this.names[puest] = res.DSCPU;
    }

    showFile(fieldName: string, index: number) {
      let folio: string = this.f_drf_frm_pap.FOLIO;
  
      this.wbService.showFile(this.f_drf_frm_pap_com[index][fieldName] ,this.format, folio, 'application/pdf').subscribe(x => {
        var newBlob = new Blob([x], { type: 'application/pdf' });
  
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(newBlob);
            return;
        }
  
        const data = window.URL.createObjectURL(newBlob);
  
        window.open(data,'_blank');
      });
    }

    saveInformation(type: string, option: string) {
      this._fuseSplashScreenService.show();

      let folio: string = this.f_drf_frm_pap.FOLIO,
          sttus: string = this.f_drf_frm_pap.STTUS;
  
      let information = { 'F_DRF_FRM_PAP': this.f_drf_frm_pap,
                          'F_DRF_FRM_PAP_COM': this.f_drf_frm_pap_com} ;
  
      this.wbService.saveInformationForms(type, this.format, folio, sttus, JSON.stringify(information)).subscribe(
        response => {
          let old_folio = this.f_drf_frm_pap.FOLIO;
  
          this.f_drf_frm_pap.FOLIO = response.FOLIO;
  
          if(!folio) {
            this.globalService.messageSwal('Información Almacenada', 'Tu folio temporal es: ' + response.FOLIO, 'success');
          } else {
            if(type == 'CF'){
              if(old_folio != this.f_drf_frm_pap.FOLIO) {
                this.globalService.messageSwal('Información Almacenada', 'Tu información ha sido almacenada con el folio: ' + response.FOLIO, 'success');
              } else {
                this.globalService.messageSwal('Información Actualizada', 'Tu información con el folio ' + response.FOLIO + ' ha sido actualizada', 'success');
              }
            } else {
              this.globalService.messageSwal('Información Actualizada', 'Tu información con el folio ' + response.FOLIO + ' ha sido actualizada', 'success');
            }
          }

          if(option == 'VP' || option == 'OK'){
            this.createFile(option);
          } else if(option.length >= 5) {
            document.getElementById(option).click();
          }
  
          if(type == 'CF'){
            this.f_drf_frm_pap.STTUS = '2';
          }

          this._fuseSplashScreenService.hide();
        },
        error => {
          this._fuseSplashScreenService.hide();
          this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
          console.log(error);
        })
    }

    createFile(option: string) {
      let folio: string = this.f_drf_frm_pap.FOLIO;
  
      this.wbService.createFile(option ,this.format, folio).subscribe(x => {
  
        var newBlob = new Blob([x], { type: 'application/pdf' });
  
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(newBlob);
            return;
        }
  
        const data = window.URL.createObjectURL(newBlob);
  
        var link = document.createElement('a');
        link.href = data;
        link.download = folio + '-' + option + '.pdf';
  
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
  
        setTimeout(function () {
            window.URL.revokeObjectURL(data);
            link.remove();
        }, 100);
      });
    }

    downloadFile(option: string) {
      let folio: string = this.f_drf_frm_pap.FOLIO;
  
      this.wbService.downloadFile(option ,this.format, folio).subscribe(x => {
  
        var newBlob = new Blob([x], { type: 'application/pdf' });
  
        if (window.navigator && window.navigator.msSaveOrOpenBlob) {
            window.navigator.msSaveOrOpenBlob(newBlob);
            return;
        }
  
        const data = window.URL.createObjectURL(newBlob);
  
        var link = document.createElement('a');
        link.href = data;
        link.download = folio + '-' + option + '.pdf';
  
        link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
  
        setTimeout(function () {
            window.URL.revokeObjectURL(data);
            link.remove();
        }, 100);
      });
    }

    valDis() {
      if(this.f_drf_frm_pap.STTUS=='1') {
        if(this.groupF && this.groupF.form) {
          if(this.f_drf_frm_pap.TOTAL > this.disponible) {
              this.groupF.form.setErrors({'disponible': true});
              return true;
          } else{
              this.groupF.form.setErrors(null);
              return false;
          }
        } else {
          return false;
        }
      } else {
        return false;
      }
    }

    valDet(item, column, index) {
      if(this.groupF && this.groupF.controls[column+index]) {
        if(column == 'XXMLL') {
          if(((item.substring(item.lastIndexOf("."))).toLowerCase())!='.xml') {
            this.groupF.controls[column+index].setErrors({'extension': true});
            return true;
          } else{
            this.groupF.controls[column+index].setErrors(null);
            return false;
          }
        } else if(column == 'PPDFF') {
          if(item === 'prohibitus' || ((item.substring(item.lastIndexOf("."))).toLowerCase())!='.pdf') {
            this.groupF.controls[column+index].setErrors({'extension': true});
            return true;
          } else{
            this.groupF.controls[column+index].setErrors(null);
            return false;
          }
        }
      } else {
        return false;
      }
    }

    clean() {
      let folio = this.f_drf_frm_pap.FOLIO,
          fecha = this.f_drf_frm_pap.FECHA;
  
      this.f_drf_frm_pap = new F_DRF_FRM_PAP();
      this.f_drf_frm_pap_com = [];
      this.p_drf_frm_pap_com = [];
  
      this.f_drf_frm_pap.STTUS = '1',
      this.f_drf_frm_pap.FOLIO = folio,
      this.f_drf_frm_pap.EJERC = sessionStorage.getItem('year'),
      this.f_drf_frm_pap.USUAR = this.authService.user.user_name,
      this.f_drf_frm_pap.REGIO = this.authService.user.office,
      this.names.ELNOM = this.authService.user.surname_first+' '+this.authService.user.surname_second+' '+this.authService.user.name,
      this.names.ELPUE = this.authService.user.job;

      if(folio.length != 12) {
        this.wbService.getInformation('1', '').subscribe(response => {
          this.f_drf_frm_pap.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
        })
      } else {
        this.f_drf_frm_pap.FECHA = fecha;
      }

      this.f_drf_frm_pap_com.push({
        ID:1,
        ERROR:'',
        XXMLL:'',
        PPDFF:'',
        NMDOC:'',
        FCDOC:null,
        RRFCC:'',
        NMRZS:'',
        CONCE:'',
        SUBTT:0,
        IIVAA:0,
        TOTAL:0,
        RTISR:0,
        RTIVA:0,
        TOTFC:0
      });

      this.p_drf_frm_pap_com.push({
        XXMLL:0,
        PPDFF:0
      });
  
      this.dataSource  = new MatTableDataSource(this.f_drf_frm_pap_com);
    }

    disabled() {
      if(this.f_drf_frm_pap.STTUS == '1') {
        return false;
      } else {
        return true;
      }
    }
}

@Component({
  selector: 'drf_pap_dialog_recover',
  templateUrl: 'pap.dialog.recover.html',
})
export class DRFPAPDialogRecover implements OnInit {
    private gridApi: any;
    public rowSelection = "single";
    public format: string;
    public color = 'accent';
    public mode = 'determinate';
    public value = 0;
    public bufferValue = 100;

    columnDefs = [
        {headerName: 'Folio', field: 'FOLIO', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Fecha', field: 'FECHA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Estatus', field: 'STDSC', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Contrato', field: 'CTRPD', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Monto', field: 'TOTAL', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true, cellStyle: {'text-align': 'right'} }
    ];

    rowData: any;

  constructor(private authService: AuthService,
              private wbService: WBService,
              private globalService: GlobalService,
              public dialogRef: MatDialogRef<DRFPAPDialogRecover>) {
        this.format = 'DRF-FRM-PAP';
    }

    ngOnInit() {
      this.loadData();
    }

    loadData() {
      this.wbService.getInformation('11', 'EJERC='+sessionStorage.getItem('year')+' AND USUAR=¯' + this.authService.user.user_name + '¯').subscribe(response => {
        this.rowData = response.data;
      })
    }

    onGridReady(params) {
      this.gridApi = params;
      this.gridApi.api.sizeColumnsToFit();      
    }

    onNoClick() {
      this.dialogRef.close();
    }

    recoverData() {
      let selectedNodes = this.gridApi.api.getSelectedNodes();

      if(selectedNodes.length <= 0){
        this.globalService.messageSwal('Advertencia', 'Selecciona un folio para recuperar', 'warning');
        return
      }

      this.dialogRef.close(selectedNodes[0].data.FOLIO);
    }

    showAdjuntar() {
      if(this.gridApi && this.gridApi.api){
        let selectedNodes = this.gridApi.api.getSelectedNodes();
        if(selectedNodes.length > 0){
          if(selectedNodes[0].data.STTUS == 2){
            return true;
          }
        }
      }
      
      return false;
    }

    adjuntarPDF() {
      this.value = 0;
      document.getElementById('PDFFR').click();
    }

    fileChange(files: File[], field: string) {
      let selectedNodes = this.gridApi.api.getSelectedNodes();

      let folio: string = selectedNodes[0].data.FOLIO;
  
      if (files.length > 0) {
        if(files[0].name.toLowerCase().endsWith('.pdf')) {
          this.wbService.uploadForm(files[0], this.format, folio).subscribe(
            event => {
              if(event.type === HttpEventType.UploadProgress) {
                this.value = Math.round((event.loaded/event.total)*100);
              } else if(event.type === HttpEventType.Response) {
                let response:any = event.body;

                this.gridApi.api.deselectAll();
                this.loadData();
                this.value = 0;

                this.globalService.messageSwal('Subida Archivo', 'Tu archivo se ha subido con éxito: ' + files[0].name, 'success');
              }
            },
            error => {
              this.value = 0;

              if(error.error){
                if(error.error.file == 'prohibitus'){
                  this.globalService.messageSwal('Archivo Bloqueado', error.error.message, 'error');
                }else{
                  this.globalService.messageSwal('Error', error.error.message, 'error');
                }
              } else{
                console.log(error);
              }
            })
        } else {
          this.globalService.messageSwal('Extensión Invalida', 'Archivo adjunto no se pudo cargar, solo se permiten archivos pdf', 'warning');
        }
      }
    }
}