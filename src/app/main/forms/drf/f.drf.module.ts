import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRippleModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { NgxDnDModule } from '@swimlane/ngx-dnd';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { CurrencyMaskModule } from "ngx-currency-mask";
import { NgCircleProgressModule } from 'ng-circle-progress';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

import { AgGridModule } from 'ag-grid-angular';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';

import { DRFRICComponent, DRFRICDialogRecover } from './ric/ric.component';
import { DRFPAPComponent, DRFPAPDialogRecover } from './pap/pap.component';
import { DRFGACComponent, DRFGACDialogRecover } from './gac/gac.component';
import { DRFCDGComponent, DRFCDGDialogRecover } from './cdg/cdg.component';
import { DRFVIAComponent, DRFVIADialogRecover } from './via/via.component';
import { DRFCDVComponent, DRFCDVDialogRecover } from './cdv/cdv.component';
import { DRFFRVComponent, DRFFRVDialogRecover } from './frv/frv.component';

import { RoleGuard } from '../../../guards/role.guard';

const routes: Routes = [
    {
        path:      'ric',
        component: DRFRICComponent,
        canActivate: [RoleGuard],
        data:{role: 'FORMS:DRF-FRM-RIC'}
    },
    {
        path:      'pap',
        component: DRFPAPComponent,
        canActivate: [RoleGuard],
        data:{role: 'FORMS:DRF-FRM-PAP'}
    },
    {
        path:      'gac',
        component: DRFGACComponent,
        canActivate: [RoleGuard],
        data:{role: 'FORMS:DRF-FRM-GAC'}
    },
    {
        path:      'cdg',
        component: DRFCDGComponent,
        canActivate: [RoleGuard],
        data:{role: 'FORMS:DRF-FRM-CDG'}
    },
    {
        path:      'via',
        component: DRFVIAComponent,
        canActivate: [RoleGuard],
        data:{role: 'FORMS:DRF-FRM-VIA'}
    },
    {
        path:      'cdv',
        component: DRFCDVComponent,
        canActivate: [RoleGuard],
        data:{role: 'FORMS:DRF-FRM-CDV'}
    },
    {
        path:      'frv',
        component: DRFFRVComponent,
        canActivate: [RoleGuard],
        data:{role: 'FORMS:DRF-FRM-FRV'}
    }
];

@NgModule({
    entryComponents: [
        DRFRICDialogRecover,
        DRFPAPDialogRecover,
        DRFGACDialogRecover,
        DRFCDGDialogRecover,
        DRFFRVDialogRecover,
        DRFVIADialogRecover,
        DRFCDVDialogRecover
    ],
    declarations: [
        DRFRICComponent,
        DRFRICDialogRecover,
        DRFPAPComponent,
        DRFPAPDialogRecover,
        DRFGACComponent,
        DRFGACDialogRecover,
        DRFCDGComponent,
        DRFCDGDialogRecover,
        DRFVIAComponent,
        DRFVIADialogRecover,
        DRFCDVComponent,
        DRFCDVDialogRecover,
        DRFFRVComponent,
        DRFFRVDialogRecover
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSelectModule,
        MatTableModule,
        MatPaginatorModule,
        MatSortModule,
        
        MatCardModule,
        MatToolbarModule,
        MatTooltipModule,
        MatDialogModule,
        MatProgressBarModule,
        MatAutocompleteModule,
        
        NgxDnDModule,
        NgxMatSelectSearchModule,
        NgxMaterialTimepickerModule,
        CurrencyMaskModule,
        NgCircleProgressModule.forRoot({}),
        GooglePlaceModule,

        FuseSharedModule,
        FuseSidebarModule,

        AgGridModule.withComponents([])
    ]
})
export class ForDRFModule
{
}
