import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { HttpEventType } from '@angular/common/http';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import { AuthService } from '../../../../services/auth.service';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';

import { F_DRF_FRM_RIC, P_DRF_FRM_RIC, names } from '../../../../models/forms/DRF/FRM/RIC/drf.frm.ric.model';
import { CustomErrorStateMatcher } from '../../../../models/global/error';

@Component({
    selector     : 'drf_ric',
    templateUrl  : './ric.component.html',
    styleUrls  : ['./ric.component.scss'],
    providers: [
      { provide: MAT_DATE_LOCALE, useValue: 'es-MX' },
      { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
      { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
    ]
})
export class DRFRICComponent implements OnInit, OnDestroy {
    @ViewChild('groupForm', {static: false}) groupF: NgForm;

    public format: string;
    public error = new CustomErrorStateMatcher();

    public f_drf_frm_ric: F_DRF_FRM_RIC = new F_DRF_FRM_RIC();
    public p_drf_frm_ric: P_DRF_FRM_RIC = new P_DRF_FRM_RIC();
    public names: names = new names();
    
    public regiones: Array<any>;
    public tipoIngreso: Array<any>;
    public realizadoPor: Array<any>;
    public mediosPago: Array<any>;

    public funcionarios: Array<any>;
    public filteredRC: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public RCFilterCtrl: FormControl = new FormControl();

    protected _onDestroy = new Subject<void>();

    // Private
    private _unsubscribeAll: Subject<any>;


    columnsTo = ['REGIO', 'DESCR', 'CIUDA'];
    dataSource;

    columnsTo2 = ['CIUDA', 'ESTAD', 'LATIT'];
    dataSource2;

    /**
     * Constructor
     *
     */
    constructor(private authService: AuthService,
                private wbService: WBService,
                private globalService: GlobalService,
                public dialog: MatDialog,
                private cdr: ChangeDetectorRef,
                private _fuseSplashScreenService: FuseSplashScreenService) {

        this.format = 'DRF-FRM-RIC';
        this.f_drf_frm_ric.STTUS = '1',
        this.f_drf_frm_ric.EJERC = sessionStorage.getItem('year'),
        this.f_drf_frm_ric.USUAR = this.authService.user.user_name,
        this.f_drf_frm_ric.REGIO = this.authService.user.office;

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    ngOnInit() {
        this.wbService.getInformation('1', '').subscribe(response => {
            this.f_drf_frm_ric.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
        })

        this.wbService.getInformation('3', 'REGIO=¯'+this.authService.user.office+'¯').subscribe(response => {
            this.regiones = response.data;
        })

        this.wbService.getInformation('43', '').subscribe(response => {
            this.tipoIngreso = response.data;
        })

        this.wbService.getInformation('44', '').subscribe(response => {
            this.realizadoPor = response.data;
        })

        this.wbService.getInformation('5', '').subscribe(response => {
            this.funcionarios = response.data;
            this.filteredRC.next(this.funcionarios)
        })
      
        this.RCFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                this.filterRC();
        })

        this.wbService.getInformation('27', '').subscribe(response => {
            this.mediosPago = response.data;
        })
    }

    ngOnDestroy() {
        this._onDestroy.next();
        this._onDestroy.complete();
    }

    clear(type) {
        let folio = this.f_drf_frm_ric.FOLIO,
            fecha = this.f_drf_frm_ric.FECHA;
  
        this.f_drf_frm_ric = new F_DRF_FRM_RIC();
        this.p_drf_frm_ric = new P_DRF_FRM_RIC();
  
        this.names = new names();
  
        if(type == 'C') {
          this.f_drf_frm_ric.FOLIO = folio;
  
          if(folio.length == 30) {
            this.wbService.getInformation('1', '').subscribe(response => {
              this.f_drf_frm_ric.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
            })
          } else {
            this.f_drf_frm_ric.FECHA = fecha;
          }
        } else {
          this.wbService.getInformation('1', '').subscribe(response => {
            this.f_drf_frm_ric.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
          })
        }
  
        this.f_drf_frm_ric.STTUS = '1',
        this.f_drf_frm_ric.EJERC = sessionStorage.getItem('year'),
        this.f_drf_frm_ric.USUAR = this.authService.user.user_name,
        this.f_drf_frm_ric.REGIO = this.authService.user.office;
    }

    protected filterRC() {
        if (!this.funcionarios) {
          return;
        }
        
        let search = this.RCFilterCtrl.value;
        if (!search) {
          this.filteredRC.next(this.funcionarios.slice());
          return;
        } else {
          search = search.toLowerCase();
        }
        
        this.filteredRC.next(
          this.funcionarios.filter(funcionario => funcionario.DESCR.toLowerCase().indexOf(search) > -1)
        );
    }

    openDialog(): void {
        const dialogRef = this.dialog.open(DRFRICDialogRecover, {
          width: '1000px',
          height: '650px'
        });
    
        dialogRef.afterClosed().subscribe(result => {
          if(result) {
            this.getInformation(result);
          }
        });
      }
  
      getInformation(data) {
        this.wbService.getInformationForms(this.format, 'FOLIO=¯'+data.FOLIO+'¯').subscribe(response => {
          let rec_drf_frm_ric = response.F_DRF_FRM_RIC;
    
          if(rec_drf_frm_ric){
            this.f_drf_frm_ric.FOLIO=rec_drf_frm_ric[0].FOLIO,
            this.f_drf_frm_ric.STTUS=rec_drf_frm_ric[0].STTUS,
            this.f_drf_frm_ric.EJERC=rec_drf_frm_ric[0].EJERC,
            this.f_drf_frm_ric.USUAR=rec_drf_frm_ric[0].USUAR,
            this.f_drf_frm_ric.FECHA=rec_drf_frm_ric[0].FOLIO.length != 12 ? this.f_drf_frm_ric.FECHA:this.globalService.getDateUTC(rec_drf_frm_ric[0].FECHA),
            this.f_drf_frm_ric.REGIO=rec_drf_frm_ric[0].REGIO,
            this.f_drf_frm_ric.TIDIN=rec_drf_frm_ric[0].TIDIN,
            this.f_drf_frm_ric.INRLZ=rec_drf_frm_ric[0].INRLZ,
            this.f_drf_frm_ric.CONCE=rec_drf_frm_ric[0].CONCE,
            this.f_drf_frm_ric.IMPOR=rec_drf_frm_ric[0].IMPOR,
            this.f_drf_frm_ric.MDPAG=rec_drf_frm_ric[0].MDPAG,
            this.f_drf_frm_ric.NMPAG=rec_drf_frm_ric[0].NMPAG,
            this.f_drf_frm_ric.FCPAG=rec_drf_frm_ric[0].FCPAG ? this.globalService.getDateUTC(rec_drf_frm_ric[0].FCPAG):null,
            this.f_drf_frm_ric.CMPAG=rec_drf_frm_ric[0].CMPAG,
            this.f_drf_frm_ric.NMRZS=rec_drf_frm_ric[0].NMRZS,
            this.f_drf_frm_ric.RRFCC=rec_drf_frm_ric[0].RRFCC,
            this.f_drf_frm_ric.DOMIC=rec_drf_frm_ric[0].DOMIC,
            this.f_drf_frm_ric.CDPST=rec_drf_frm_ric[0].CDPST,
            this.f_drf_frm_ric.TELEF=rec_drf_frm_ric[0].TELEF,
            this.f_drf_frm_ric.RECIB=rec_drf_frm_ric[0].RECIB;

            this.p_drf_frm_ric.CMPAG=rec_drf_frm_ric[0].CMPAG ? 100:0;
  
            this.puesto(this.f_drf_frm_ric.RECIB, 'RCPUE');
          }else{
            this.globalService.messageSwal('Información no encontrada', 'La información de la comproción '+data.FOLIO+'-'+data.COMPR+ ' no se encontro en el servidor', 'error');
          }
        })
    }

    puesto(func: string, puest: string) {
      if(func) {
        let res = this.funcionarios.find( funcionario => funcionario.CUSER === func );
        
        this.names[puest] = res.DSCPU;
      }
    }

    openInput(fieldName: string) {
        if(this.f_drf_frm_ric.STTUS == '1') {
          if(this.f_drf_frm_ric.FOLIO) {
            document.getElementById(fieldName).click();
          } else {
            this.saveInformation('SI', fieldName);
          }
        }
    }
  
      fileChange(files: File[], field: string) {
        let folio: string = this.f_drf_frm_ric.FOLIO;
        this.p_drf_frm_ric[field] = 0;
    
        if (files.length > 0) {
          if(files[0].name.toLowerCase().endsWith('.pdf')) {
            this.f_drf_frm_ric[field] = files[0].name;
      
            this.wbService.uploadFile(files[0], this.format, folio).subscribe(
              event => {
                if(event.type === HttpEventType.UploadProgress) {
                  this.p_drf_frm_ric[field] = Math.round((event.loaded/event.total)*100);
                } else if(event.type === HttpEventType.Response) {
                  let response:any = event.body;
                  this.globalService.messageSwal('Subida Archivo', 'Tu archivo se ha subido con éxito: ' + files[0].name, 'success');
                }
              },
              error => {
                if(error.error){
                  if(error.error.file == 'prohibitus'){
                    this.f_drf_frm_ric[field] = error.error.file;
                    this.globalService.messageSwal('Archivo Bloqueado', error.error.message, 'error');
                  }else{
                    this.globalService.messageSwal('Error', error.error.message, 'error');
                  }
                } else{
                  console.log(error);
                }
              })
          } else {
            this.f_drf_frm_ric[field] = '';
            this.globalService.messageSwal('Extensión Invalida', 'Archivo adjunto no se pudo cargar, solo se permiten archivos pdf', 'warning');
          }
        } else {
          this.f_drf_frm_ric[field] = '';
        }
    }

    showFile(fieldName: string) {
        let folio: string = this.f_drf_frm_ric.FOLIO;
    
        this.wbService.showFile(this.f_drf_frm_ric[fieldName] ,this.format, folio, 'application/pdf').subscribe(x => {
          var newBlob = new Blob([x], { type: 'application/pdf' });
    
          if (window.navigator && window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveOrOpenBlob(newBlob);
              return;
          }
    
          const data = window.URL.createObjectURL(newBlob);
    
          window.open(data,'_blank');
        });
    }

    saveInformation(type: string, option: string) {
        this._fuseSplashScreenService.show();
  
        let folio: string = this.f_drf_frm_ric.FOLIO,
            sttus: string = this.f_drf_frm_ric.STTUS;
    
        let information = { 'F_DRF_FRM_RIC': this.f_drf_frm_ric} ;
    
        this.wbService.saveInformationForms(type, this.format, folio, sttus, JSON.stringify(information)).subscribe(
          response => {
            let old_folio = this.f_drf_frm_ric.FOLIO;
    
            this.f_drf_frm_ric.FOLIO = response.FOLIO;
    
            if(!folio) {
              this.globalService.messageSwal('Información Almacenada', 'Tu folio temporal es: ' + response.FOLIO, 'success');
            } else {
              if(type == 'CF'){
                if(old_folio != this.f_drf_frm_ric.FOLIO) {
                  this.globalService.messageSwal('Información Almacenada', 'Tu información ha sido almacenada con el folio: ' + response.FOLIO, 'success');
                } else {
                  this.globalService.messageSwal('Información Actualizada', 'Tu información con el folio ' + response.FOLIO + ' ha sido actualizada', 'success');
                }
              } else {
                this.globalService.messageSwal('Información Actualizada', 'Tu información con el folio ' + response.FOLIO + ' ha sido actualizada', 'success');
              }
            }
  
            if(option == 'VP' || option == 'OK'){
              this.createFile(option);
            } else if(option.length >= 5) {
              document.getElementById(option).click();
            }
    
            if(type == 'CF'){
              this.f_drf_frm_ric.STTUS = '2';
            }
  
            this._fuseSplashScreenService.hide();
          },
          error => {
            this._fuseSplashScreenService.hide();
            this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
            console.log(error);
          })
      }
  
      createFile(option: string) {
        let folio: string = this.f_drf_frm_ric.FOLIO;
    
        this.wbService.createFile(option ,this.format, folio).subscribe(x => {
    
          var newBlob = new Blob([x], { type: 'application/pdf' });
    
          if (window.navigator && window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveOrOpenBlob(newBlob);
              return;
          }
    
          const data = window.URL.createObjectURL(newBlob);
    
          var link = document.createElement('a');
          link.href = data;
          link.download = folio + '-' + option + '.pdf';
    
          link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
    
          setTimeout(function () {
              window.URL.revokeObjectURL(data);
              link.remove();
          }, 100);
        });
      }
  
      downloadFile(option: string) {
        let folio: string = this.f_drf_frm_ric.FOLIO;
    
        this.wbService.downloadFile(option ,this.format, folio).subscribe(x => {
    
          var newBlob = new Blob([x], { type: 'application/pdf' });
    
          if (window.navigator && window.navigator.msSaveOrOpenBlob) {
              window.navigator.msSaveOrOpenBlob(newBlob);
              return;
          }
    
          const data = window.URL.createObjectURL(newBlob);
    
          var link = document.createElement('a');
          link.href = data;
          link.download = folio + '-' + option + '.pdf';
    
          link.dispatchEvent(new MouseEvent('click', { bubbles: true, cancelable: true, view: window }));
    
          setTimeout(function () {
              window.URL.revokeObjectURL(data);
              link.remove();
          }, 100);
        });
    }

    valGnr(column) {
        if(this.f_drf_frm_ric.STTUS == '1') {
          if(this.groupF && this.groupF.controls[column]) {
            if(column == 'IMPOR') {
              if(this.f_drf_frm_ric[column] <= 0) {
                this.groupF.controls[column].setErrors({'importe': true});
                return true;
              } else {
                this.groupF.controls[column].setErrors(null);
                return false;
              }
            } else if(column == 'NMPAG') {
                if(this.f_drf_frm_ric.MDPAG != '' && this.f_drf_frm_ric.MDPAG != 'E' && !this.f_drf_frm_ric.NMPAG) {
                  this.groupF.controls[column].setErrors({'pago': true});
                  return true;
                } else {
                  this.groupF.controls[column].setErrors(null);
                  return false;
                }
            } else if(column == 'FCPAG') {
                if(!this.f_drf_frm_ric.FCPAG) {
                  this.groupF.controls[column].setErrors({'fecha': true});
                  this.groupF.form.setErrors({'fecha': true});
                  return true;
                } else {
                  this.groupF.controls[column].setErrors(null);
                  this.groupF.form.setErrors(null);
                  return false;
                }
            } else if(column == 'NMRZS' || column == 'RRFCC' || column == 'DOMIC' || column == 'CDPST' || column == 'TELEF') {
                if(this.f_drf_frm_ric.INRLZ=='E' && !this.f_drf_frm_ric[column]) {
                  this.groupF.controls[column].setErrors({'valor': true});
                  return true;
                } else {
                  this.groupF.controls[column].setErrors(null);
                  return false;
                }
            } else if(column == 'CMPAG') {
              if(this.f_drf_frm_ric.MDPAG!='E' && (this.f_drf_frm_ric[column].indexOf('prohibitus') >= 0 || this.f_drf_frm_ric[column].length <= 0)) {
                this.groupF.controls[column].setErrors({'incorrect': true});
                return true;
              } else {
                this.groupF.controls[column].setErrors(null);
                return false;
              }
            }
          } else{
            return false;
          }
        } else {
          return false;
        }
      }

    disabled() {
        if(this.f_drf_frm_ric.STTUS == '1') {
          return false;
        } else {
          return true;
        }
    }


    sum() {
      return this.dataSource2 ? this.dataSource2.data.map(t => t.LATIT).reduce((acc, value) => acc + value, 0):0;
    }
}

@Component({
    selector: 'drf_ric_dialog_recover',
    templateUrl: 'ric.dialog.recover.html',
  })
  export class DRFRICDialogRecover implements OnInit {
      private gridApi: any;
      public rowSelection = "single";
      public format: string;
      public color = 'accent';
      public mode = 'determinate';
      public value = 0;
      public bufferValue = 100;
  
      columnDefs = [
        {headerName: 'Folio', field: 'FOLIO', checkboxSelection: true, suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Fecha', field: 'FECHA', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Estatus', field: 'STDSC', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Ingreso', field: 'TIDIN', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Importe', field: 'IMPOR', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true, cellStyle: {'text-align': 'right'} },
        {headerName: 'Medio de Pago', field: 'MDPAG', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Nº de Pago', field: 'NMPAG', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true },
        {headerName: 'Fecha de Pago', field: 'FCPAG', suppressMenu: true, filter: 'agTextColumnFilter', sortable: true }
      ];
  
      rowData: any;
  
    constructor(private authService: AuthService,
                private wbService: WBService,
                private globalService: GlobalService,
                public dialogRef: MatDialogRef<DRFRICDialogRecover>) {
        this.format = 'DRF-FRM-RIC';
      }
  
      ngOnInit() {
        this.loadData();
      }
  
      loadData() {
        this.wbService.getInformation('45', 'EJERC='+sessionStorage.getItem('year')+' AND USUAR=¯' + this.authService.user.user_name + '¯').subscribe(response => {
          this.rowData = response.data;
        })
      }
  
      onGridReady(params) {
        this.gridApi = params;    
      }
  
      onNoClick() {
        this.dialogRef.close();
      }
  
      recoverData() {
        let selectedNodes = this.gridApi.api.getSelectedNodes();
  
        if(selectedNodes.length <= 0){
          this.globalService.messageSwal('Advertencia', 'Selecciona un folio para recuperar', 'warning');
          return
        }
  
        this.dialogRef.close(selectedNodes[0].data);
      }
  
      showAdjuntar() {
        if(this.gridApi && this.gridApi.api){
          let selectedNodes = this.gridApi.api.getSelectedNodes();
          if(selectedNodes.length > 0){
            if(selectedNodes[0].data.STTUS == 2){
              return true;
            }
          }
        }
        
        return false;
      }
  
      adjuntarPDF() {
        this.value = 0;
        document.getElementById('PDFFR').click();
      }
  
      fileChange(files: File[], field: string) {
        let selectedNodes = this.gridApi.api.getSelectedNodes();
  
        let folio: string = selectedNodes[0].data.FOLIO;
    
        if (files.length > 0) {
          if(files[0].name.toLowerCase().endsWith('.pdf')) {
            this.wbService.uploadForm(files[0], this.format, folio).subscribe(
              event => {
                if(event.type === HttpEventType.UploadProgress) {
                  this.value = Math.round((event.loaded/event.total)*100);
                } else if(event.type === HttpEventType.Response) {
                  let response:any = event.body;
  
                  this.gridApi.api.deselectAll();
                  this.loadData();
                  this.value = 0;
  
                  this.globalService.messageSwal('Subida Archivo', 'Tu archivo se ha subido con éxito: ' + files[0].name, 'success');
                }
              },
              error => {
                this.value = 0;
  
                if(error.error){
                  if(error.error.file == 'prohibitus'){
                    this.globalService.messageSwal('Archivo Bloqueado', error.error.message, 'error');
                  }else{
                    this.globalService.messageSwal('Error', error.error.message, 'error');
                  }
                } else{
                  console.log(error);
                }
              })
          } else {
            this.globalService.messageSwal('Extensión Invalida', 'Archivo adjunto no se pudo cargar, solo se permiten archivos pdf', 'warning');
          }
        }
      }
  }