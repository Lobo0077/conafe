import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { MatTableDataSource } from '@angular/material';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { HttpEventType } from '@angular/common/http';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import { AuthService } from '../../../../services/auth.service';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';

import { F_ADM_FRM_EFL } from '../../../../models/forms/ADM/FRM/EFL/adm.frm.efl.model';
import { CustomErrorStateMatcher } from '../../../../models/global/error';

@Component({
    selector     : 'adm_efl',
    templateUrl  : './efl.component.html',
    styleUrls  : ['./efl.component.scss'],
    providers: [
      { provide: MAT_DATE_LOCALE, useValue: 'es-MX' },
      { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
      { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
    ]
})
export class ADMEFLComponent implements OnInit, OnDestroy {
    @ViewChild('groupForm', {static: false}) groupF: NgForm;

    public format: string;
    public error = new CustomErrorStateMatcher();
    public type: number;
    public baja: boolean;
    public pass: string;

    public f_adm_frm_efl: F_ADM_FRM_EFL = new F_ADM_FRM_EFL();

    public formatos: Array<any>;
    public folios: Array<any>;
    public estatus: Array<any>;

    protected _onDestroy = new Subject<void>();

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     */
    constructor(private authService: AuthService,
                private wbService: WBService,
                private globalService: GlobalService,
                public dialog: MatDialog,
                private cdr: ChangeDetectorRef,
                private _fuseSplashScreenService: FuseSplashScreenService) {

        this.format = 'ADM-FRM-EFL';
        this.f_adm_frm_efl.STTUS = '1',
        this.f_adm_frm_efl.EJERC = sessionStorage.getItem('year'),
        this.f_adm_frm_efl.USUAR = this.authService.user.user_name;

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    ngAfterContentChecked() {
        this.cdr.detectChanges();
    }
    
    ngOnInit() {
        this.wbService.getInformation('1', '').subscribe(response => {
            this.f_adm_frm_efl.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
        })

        this.wbService.getInformation('46', 'MODUL IN (¯DRF¯,¯DRM¯)').subscribe(response => {
            this.formatos = response.data;
        })

        this.wbService.getInformation('47', '').subscribe(response => {
            this.estatus = response.data;
        })
    }

    ngOnDestroy() {
        this._onDestroy.next();
        this._onDestroy.complete();
    }

    getFolios(forma) {
        this.wbService.getInformation('48', 'EJERC=¯'+sessionStorage.getItem('year')+'¯ AND FORMA=¯'+forma+'¯').subscribe(response => {
            this.folios = response.data;
          })
    }

    saveInformation() {
        this._fuseSplashScreenService.show();
  
        let folio: string = this.f_adm_frm_efl.UUIDD,
            sttus: string = this.f_adm_frm_efl.STTUS;
        
        let information = { 'F_ADM_FRM_EFL': this.f_adm_frm_efl} ;

        this.wbService.saveInformationForms('SI', this.format, folio, sttus, JSON.stringify(information)).subscribe(
          response => {    
            this.globalService.messageSwal('Estatus Cambiado', 'El folio ' + this.f_adm_frm_efl.FOLIO + ' ha cambiado de estatus', 'success');

            this.f_adm_frm_efl = new F_ADM_FRM_EFL();

            this.f_adm_frm_efl.STTUS = '1',
            this.f_adm_frm_efl.EJERC = sessionStorage.getItem('year'),
            this.f_adm_frm_efl.USUAR = this.authService.user.user_name;

            this._fuseSplashScreenService.hide();
          },
          error => {
            this._fuseSplashScreenService.hide();
            this.globalService.messageSwal('Error!!', 'No se pudo realizar el cambio de estatus, favor de intentarlo nuevamente', 'error');
            console.log(error);
          })
    }
}
