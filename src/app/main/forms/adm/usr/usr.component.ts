import { Component, OnInit, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { MatTableDataSource } from '@angular/material';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { HttpEventType } from '@angular/common/http';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';

import { AuthService } from '../../../../services/auth.service';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';

import { DialogRecover } from '../../../../custom/dialogs/recover/recover.component';

import { F_ADM_FRM_USR, F_ADM_FRM_USR_ACS, aditional } from '../../../../models/forms/ADM/FRM/USR/adm.frm.usr.model';
import { CustomErrorStateMatcher } from '../../../../models/global/error';

@Component({
    selector: 'adm_usr',
    templateUrl: './usr.component.html',
    styleUrls: ['./usr.component.scss'],
    providers: [
        { provide: MAT_DATE_LOCALE, useValue: 'es-MX' },
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
    ]
})
export class ADMUSRComponent implements OnInit, OnDestroy {
    @ViewChild('groupForm', { static: false }) groupF: NgForm;

    public format: string;
    public error = new CustomErrorStateMatcher();
    public pass: string;
    public type: number;

    public f_adm_frm_usr: F_ADM_FRM_USR = new F_ADM_FRM_USR();
    public f_adm_frm_usr_acs: F_ADM_FRM_USR_ACS[] = [];
    public aditional: aditional = new aditional();

    public funcionarios: Array<any>;
    public filteredF: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
    public FFilterCtrl: FormControl = new FormControl();

    public perfiles: Array<any>;

    public countUP: Number = 0;

    protected _onDestroy = new Subject<void>();

    columnsToDisplay = ['ID', 'DESCR', 'ACCES'];
    dataSource;

    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     */
    constructor(private authService: AuthService,
        private wbService: WBService,
        private globalService: GlobalService,
        public dialog: MatDialog,
        private cdr: ChangeDetectorRef,
        private _fuseSplashScreenService: FuseSplashScreenService) {

        this.format = 'ADM-FRM-USR';
        this.f_adm_frm_usr.STTUS = '1',
        this.f_adm_frm_usr.USUAR = this.authService.user.user_name;

        this.type = 0;
        this.pass = '';

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------
    ngAfterContentChecked() {
        this.cdr.detectChanges();
    }

    ngOnInit() {
        this.wbService.getInformation('1', '').subscribe(response => {
            this.f_adm_frm_usr.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
        })

        this.wbService.getCount('41', 'STTUS=¯4¯ AND LEN(EMAIL) = 0 AND LEN(CAUBA) = 0').subscribe(response => {
            if (response.count) {
                this.countUP = response.count;
            } else {
                this.countUP = 0;
            }
        })

        this.wbService.getInformation('41', '').subscribe(response => {
            this.funcionarios = response.data;
            this.filteredF.next(this.funcionarios)
        })

        this.FFilterCtrl.valueChanges
            .pipe(takeUntil(this._onDestroy))
            .subscribe(() => {
                this.filterF();
            })

        this.wbService.getInformation('39', '').subscribe(response => {
            this.perfiles = response.data;
        })
    }

    ngOnDestroy() {
        this._onDestroy.next();
        this._onDestroy.complete();
    }

    protected filterF() {
        if (!this.funcionarios) {
            return;
        }

        let search = this.FFilterCtrl.value;
        if (!search) {
            this.filteredF.next(this.funcionarios.slice());
            return;
        } else {
            search = search.toLowerCase();
        }

        this.filteredF.next(
            this.funcionarios.filter(f => (f.EMAIL + ' ' + f.NOMBR).toLowerCase().indexOf(search) > -1)
        );
    }

    openDialog(option, type) {
        if (option == 'R') {
            const dialogRef = this.dialog.open(DialogRecover, {
                width: '1000px',
                height: '650px',
                data: { type: type, format: this.format }
            });

            dialogRef.afterClosed().subscribe(result => {
                if (result && result.EMAIL) {
                    this.getInformation(type, result);
                }
            });
        }
    }

    getInformation(type, data) {
        this.type = type == 'UP' ? 1:2,
        this.pass = '',
        this.f_adm_frm_usr.EMAIL = '',
        this.f_adm_frm_usr.CUSER = '',
        this.f_adm_frm_usr.ENABL = true,
        this.f_adm_frm_usr.PERFL = '',
        this.f_adm_frm_usr.PASSW = '';
        
        this.aditional = new aditional(data);

        if (this.type == 2) {
            this.f_adm_frm_usr.EMAIL = data.EMAIL;
            this.getUser();
        } else {
            this.f_adm_frm_usr_acs = [];
            this.dataSource = new MatTableDataSource(this.f_adm_frm_usr_acs);
        }
    }

    valUser(user: string) {
        if (user) {
            let email = user+'@conafe.gob.mx';

            this.f_adm_frm_usr.EMAIL = email;

            this.wbService.getInformation('41', 'EMAIL=¯' + email + '¯').subscribe(response => {
                if(response.data != null){
                    this.groupF.controls['CUSER'].setErrors({'exists': true});
                } else {
                    this.groupF.controls['CUSER'].setErrors(null);
                    this.getAccess(user);
                }
            })
        } else {
            this.f_adm_frm_usr.EMAIL = '';
        }
    }

    showErrorCUSER() {
        if(this.groupF && this.groupF.controls['CUSER']) {
            return this.groupF.controls['CUSER'].hasError('exists');
        } else {
            return false;
        }
    }

    getUser() {
        let email = this.f_adm_frm_usr.EMAIL;

        this.f_adm_frm_usr.CUSER = '';

        if (this.f_adm_frm_usr_acs.length > 0) {
            this.f_adm_frm_usr_acs.splice(0, this.f_adm_frm_usr_acs.length);
            this.dataSource = new MatTableDataSource(this.f_adm_frm_usr_acs);
        }

        if (email) {
            if (email.indexOf('@') >= 0 && email.length > email.indexOf('@') + 1) {
                let user = email.substring(0, email.indexOf('@'));

                this.f_adm_frm_usr.CUSER = user;

                this.wbService.getInformation('49', 'CUSER=¯' + user + '¯').subscribe(response => {
                    if (response.data != null) {
                        this.f_adm_frm_usr.ENABL = response.data[0].ENABL,
                        this.f_adm_frm_usr.PERFL = response.data[0].PERFL,
                        this.f_adm_frm_usr.FECHA = response.data[0].FECHA ? this.globalService.getDateUTC(response.data[0].FECHA) : this.f_adm_frm_usr.FECHA,
                        this.f_adm_frm_usr.KEYUI = response.data[0].KEYUI;

                        this.pass = response.data[0].PASSW;
                    }
                    
                    this.getAccess(user);
                })
            }
        }
    }

    getAccess(user) {
        this.f_adm_frm_usr_acs = [];
        
        this.wbService.getInformation('0', '', '¯' + user + '¯').subscribe(response => {

            if (response.data != null) {
                for (let x = 0; x < response.data.length; x++) {
                    this.f_adm_frm_usr_acs.push({
                        ID: response.data[x].SEC,
                        GRUPO: response.data[x].GRUPO,
                        CLAVE: response.data[x].ID,
                        TICON: response.data[x].TICON,
                        DESCR: response.data[x].TITLE,
                        NIVEL: response.data[x].NIVEL,
                        ACCES: response.data[x].ACCES
                    });
                }
            }

            this.f_adm_frm_usr_acs[0].ACCES = true;
            this.dataSource = new MatTableDataSource(this.f_adm_frm_usr_acs);
        })
    }

    selectItems(element, index: number) {
        index += 1;

        for (let i = index; i < this.f_adm_frm_usr_acs.length; i++) {
            if (this.f_adm_frm_usr_acs[i].NIVEL > element.NIVEL) {
                this.f_adm_frm_usr_acs[i].ACCES = element.ACCES;
            } else {
                break;
            }
        }

        this.dataSource = new MatTableDataSource(this.f_adm_frm_usr_acs);
    }

    valGnr(item, column) {
        if (this.groupF && this.groupF.controls[column]) {
            if (column == 'PASSW') {
                if (!item && this.type == 1) {
                    this.groupF.controls[column].setErrors({ 'password': true });
                    return true;
                } else {
                    this.groupF.controls[column].setErrors(null);
                    return false;
                }
            } else if(column == 'CUSER') {
                return this.groupF.controls['CUSER'].hasError('exists');
            }
        } else {
            return false;
        }
    }

    saveInformation() {
        this._fuseSplashScreenService.show();

        let folio: string = this.f_adm_frm_usr.CUSER,
            sttus: string = this.f_adm_frm_usr.STTUS;

        let access = [];

        for (let x = 0; x < this.f_adm_frm_usr_acs.length; x++) {
            access.push({
                ID: this.f_adm_frm_usr_acs[x].ID,
                GRUPO: this.f_adm_frm_usr_acs[x].GRUPO,
                CLAVE: this.f_adm_frm_usr_acs[x].CLAVE,
                DESCR: this.f_adm_frm_usr_acs[x].DESCR,
                NIVEL: this.f_adm_frm_usr_acs[x].NIVEL,
                ACCES: this.f_adm_frm_usr_acs[x].ACCES
            })
        }

        this.f_adm_frm_usr.PASSW = this.f_adm_frm_usr.PASSW.length > 0 ? this.f_adm_frm_usr.PASSW : this.pass;

        let information = {
            'F_ADM_FRM_USR': this.f_adm_frm_usr,
            'F_ADM_FRM_USR_ACS': access
        };

        this.wbService.saveInformationForms('SI', this.format, folio, sttus, JSON.stringify(information)).subscribe(
            response => {
                this.globalService.messageSwal('Información Almacenada', 'El usuario ' + response.FOLIO + ' ha sido almacenado', 'success');
                
                if (this.type == 1) {
                    this.sendInformation(this.f_adm_frm_usr.EMAIL);
                }

                this.wbService.getInformation('49', 'CUSER=¯' + this.f_adm_frm_usr.CUSER + '¯').subscribe(response => {
                    if (response.data != null) {
                        this.f_adm_frm_usr.KEYUI = response.data[0].KEYUI;

                        this.type = 2;
                        this.pass = response.data[0].PASSW;
                        this.f_adm_frm_usr.PASSW = '';

                        this._fuseSplashScreenService.hide();
                    }
                })
            },
            error => {
                this._fuseSplashScreenService.hide();
                this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
                console.log(error);
            })
    }

    sendInformation(email) {
        let information;

        information = [{ EMAIL: email }]

        this.wbService.updateInformationForms('DRH-FRM-PER', '1', 'REGIO=¯' + this.aditional.REGIO + '¯ AND FOLIO=¯' + this.aditional.FOLIO + '¯', JSON.stringify(information)).subscribe(
            response => {
                if (response.message) {
                    this.wbService.getCount('41', 'STTUS=¯4¯ AND LEN(EMAIL) = 0 AND LEN(CAUBA) = 0').subscribe(resp => {
                        if (resp.count) {
                            this.countUP = resp.count;
                        } else {
                            this.countUP = 0;
                        }
                    })
                }
            })
    }

    clear() {
        this.aditional = new aditional();;
        this.f_adm_frm_usr = new F_ADM_FRM_USR();

        this.f_adm_frm_usr.STTUS = '1',
        this.f_adm_frm_usr.USUAR = this.authService.user.user_name;

        this.wbService.getInformation('1', '').subscribe(response => {
            this.f_adm_frm_usr.FECHA = this.globalService.getDateUTC(response.data[0].FCACT);
        })

        this.type = 1;
        this.pass = '';

        this.f_adm_frm_usr_acs = [];
        this.dataSource = new MatTableDataSource(this.f_adm_frm_usr_acs);
    }
}