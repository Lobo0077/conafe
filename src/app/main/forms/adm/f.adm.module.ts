import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRippleModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';

import { NgxDnDModule } from '@swimlane/ngx-dnd';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { CurrencyMaskModule } from "ngx-currency-mask";
import { NgCircleProgressModule } from 'ng-circle-progress';

import { AgGridModule } from 'ag-grid-angular';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';

import { ADMEFLComponent } from './efl/efl.component';
import { ADMUSRComponent } from './usr/usr.component';

import { RoleGuard } from '../../../guards/role.guard';

const routes: Routes = [
    {
        path:      'usr',
        component: ADMUSRComponent,
        canActivate: [RoleGuard],
        data:{role: 'FORMS:ADM-FRM-USR'}
    },
    {
        path:      'efl',
        component: ADMEFLComponent,
        canActivate: [RoleGuard],
        data:{role: 'FORMS:ADM-FRM-EFL'}
    }
];

@NgModule({
    entryComponents: [],
    declarations: [
        ADMUSRComponent,
        ADMEFLComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatBadgeModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSelectModule,
        MatTableModule,
        MatCardModule,
        MatToolbarModule,
        MatTooltipModule,
        MatDialogModule,
        
        NgxDnDModule,

        NgxMatSelectSearchModule,
        CurrencyMaskModule,
        NgCircleProgressModule.forRoot({}),

        FuseSharedModule,
        FuseSidebarModule,

        AgGridModule.withComponents([])
    ]
})
export class ForADMModule
{
}
