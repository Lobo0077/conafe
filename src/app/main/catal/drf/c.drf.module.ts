import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRippleModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { NgxDnDModule } from '@swimlane/ngx-dnd';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { CurrencyMaskModule } from "ngx-currency-mask";
import { NgCircleProgressModule } from 'ng-circle-progress';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

import { AgGridModule } from 'ag-grid-angular';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';
import { CustomGridModule } from '../../../custom/grid/customGrid.module';

import { CATDRFFNDComponent, CATDRFFNDAction } from './fnd/fnd.component';

import { RoleGuard } from '../../../guards/role.guard';

const routes: Routes = [
    {
        path:      'fnd',
        component: CATDRFFNDComponent,
        canActivate: [RoleGuard],
        data:{role: 'CATAL:DRF-FND'}
    },
    
];

@NgModule({
    entryComponents: [CATDRFFNDAction],
    declarations: [
        CATDRFFNDComponent,
        CATDRFFNDAction
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSelectModule,
        MatTableModule,
        MatCardModule,
        MatToolbarModule,
        MatTooltipModule,
        MatDialogModule,
        MatProgressBarModule,
        MatAutocompleteModule,
        
        NgxDnDModule,
        NgxMatSelectSearchModule,
        NgxMaterialTimepickerModule,
        CurrencyMaskModule,
        NgCircleProgressModule.forRoot({}),
        GooglePlaceModule,

        FuseSharedModule,
        FuseSidebarModule,
        CustomGridModule,

        AgGridModule.withComponents([])
    ]
})
export class CatDRFModule
{
}
