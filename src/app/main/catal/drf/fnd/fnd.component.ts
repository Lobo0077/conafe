import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { FormControl, NgForm } from '@angular/forms';
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { CustomErrorStateMatcher } from '../../../../models/global/error';

import { AuthService } from '../../../../services/auth.service';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';
import { buttonGrid } from "../../../../custom/grid/button/button.grid.component";
import { currencyGrid } from "../../../../custom/grid/label/currency.grid.component";

import swal from 'sweetalert2';

import "ag-grid-enterprise";

@Component({
  selector: 'cat_drf_fnd',
  templateUrl: './fnd.component.html'
})
export class CATDRFFNDComponent implements OnInit {
  public catalog: string = 'DRF-FND';
  public regiones: Array<any>;
  public fondos: Array<any>;
  public funcionarios: Array<any>;

  public groupDefaultExpanded;
  public getDataPath;
  public autoGroupColumnDef;
  public rowData = [];
  private gridApi: any;

  public columnDefs = [
    { headerName: "Descripción", field: "DESCR", filter: 'agTextColumnFilter', suppressMenu: true },
    { headerName: "Monto Fondo Revolvente", field: "MNDFD", filter: 'agNumberColumnFilter', suppressMenu: true, cellStyle: { 'text-align': 'right' }, cellRendererFramework: currencyGrid },
    { headerName: "Encargado", field: "NAMEE", filter: 'agTextColumnFilter', suppressMenu: true },
    { headerName: "", field: "ACTIO", suppressMenu: true, cellRendererFramework: buttonGrid, cellRendererParams: { options: "action", onClick: this.onBtnAction.bind(this) } }
  ];

  constructor(private authService: AuthService,
    private wbService: WBService,
    private globalService: GlobalService,
    public dialog: MatDialog) {

    this.getDataPath = function (data) {
      return data.GROUP.split('|');
    };

    this.groupDefaultExpanded = -1;

    this.autoGroupColumnDef = {
      headerName: 'Región/Clave',
      filter: 'agTextColumnFilter',
      suppressMenu: true,
      cellRendererParams: { suppressCount: true }
    };
  }

  ngOnInit() {
    let object = ['3', '16', '5'];
    let find = ['', '', ''];
    let param = ['', '', ''];

    this.wbService.getInformationParallel(object, find, param).subscribe(response => {
      const reg = response[0].data;
      const fon = response[1].data;
      this.funcionarios = response[2].data;

      this.regiones = reg.map(x => {
        return {
          SHWBA: true,
          SHWBM: false,
          SHWBD: false,
          GROUP: x.DESCR,
          REGIO: x.REGIO
        }
      })

      this.fondos = fon.map(x => {
        return {
          SHWBA: false,
          SHWBM: true,
          SHWBD: true,
          GROUP: x.RGDSC + '|' + x.FONDO,
          REGIO: x.REGIO,
          FONDO: x.FONDO,
          DESCR: x.DESCR,
          MNDFD: x.MNDFD,
          ENCAR: x.ENCAR,
          NAMEE: this.name(x.ENCAR)
        }
      })

      this.rowData = this.regiones.concat(this.fondos);
    })
  }

  name(func: string) {
    let res = this.funcionarios.find(funcionario => funcionario.CUSER === func);

    return res ? res.DESCR:'';
  }

  onGridReady(params) {
    this.gridApi = params;
    this.gridApi.api.sizeColumnsToFit();
    console.clear();
  }

  onBtnAction(e) {
    let type: string = e.option;
    let title: string;

    switch (type) {
      case 'A':
        title = 'Agregar Registro';
        break;
      case 'M':
        title = 'Modificar Registro';
        break;
      case 'E':
        title = 'Eliminar Registro';
        break;
      default:
        title = '';
        break;
    }

    if (type == 'A' || type == 'M') {
      const dialogRef = this.dialog.open(CATDRFFNDAction, {
        width: '1000px',
        height: '450px',
        data: { type: type, title: title, data: e.rowData, index: e.index }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.refreshInformation(result);
        }
      });
    } else if (type == 'E') {
      swal.fire({
        type: 'warning',
        title: '¿Estas seguro de eliminar el registro?',
        text: '¡Esta acción no se podra revertir!',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Si',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.value) {
          if (result.value) {
            let folio = e.rowData.REGIO+'|'+e.rowData.FONDO;
  
            this.wbService.deleteCatalogs(this.catalog, folio).subscribe(
              response => {
                this.globalService.messageSwal('¡Borrado!', 'El registro fue eliminado', 'success');
                this.gridApi.api.updateRowData({ remove: [e.rowData] });
              },
              error => {
                this.globalService.messageSwal('Error!!', 'El registro no pudo ser eliminado favor de intentarlo nuevamente', 'error');
                console.log(error);
              })
          }
        }
      })
    }
  }

  refreshInformation(data) {
    if (data.type == 'A') {
      this.gridApi.api.updateRowData({ add: [data.data] });
    } else if (data.type == 'M') {
      data.previous.DESCR = data.data.DESCR;
      data.previous.MNDFD = data.data.MNDFD;
      data.previous.ENCAR = data.data.ENCAR;
      data.previous.NAMEE = data.data.NAMEE;

      this.gridApi.api.updateRowData({ update: [data.previous] });
    }
  }
}

@Component({
  selector: 'cat_drf_fnd_action',
  templateUrl: 'action.dialog.html',
})
export class CATDRFFNDAction implements OnInit {
  @ViewChild('groupForm', { static: false }) groupF: NgForm;

  public catalog: string = 'DRF-FND';
  public regiones: Array<any>;
  public row = { REGIO: '', FONDO: '', DESCR: '', MNDFD: 0, ENCAR: '' };

  public funcionarios: Array<any>;
  public filteredEN: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
  public ENFilterCtrl: FormControl = new FormControl();

  protected _onDestroy = new Subject<void>();

  public error = new CustomErrorStateMatcher();

  constructor(private wbService: WBService,
    private globalService: GlobalService,
    public dialogRef: MatDialogRef<CATDRFFNDAction>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    if (data.type == 'A') {
      this.row = { REGIO: data.data.REGIO, FONDO: '', DESCR: '', MNDFD: 0, ENCAR: '' };
    } else {
      this.row = { REGIO: data.data.REGIO, FONDO: data.data.FONDO, DESCR: data.data.DESCR, MNDFD: data.data.MNDFD, ENCAR: data.data.ENCAR };
    }
  }

  ngOnInit() {
    this.wbService.getInformation('3', '').subscribe(response => {
      this.regiones = response.data;
    })

    this.wbService.getInformation('5', '').subscribe(response => {
      this.funcionarios = response.data;
      this.filteredEN.next(this.funcionarios)
    })

    this.ENFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEN();
      })
  }

  protected filterEN() {
    if (!this.funcionarios) {
      return;
    }

    let search = this.ENFilterCtrl.value;
    if (!search) {
      this.filteredEN.next(this.funcionarios.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredEN.next(
      this.funcionarios.filter(funcionario => funcionario.DESCR.toLowerCase().indexOf(search) > -1)
    );
  }

  name(func: string) {
    let res = this.funcionarios.find(funcionario => funcionario.CUSER === func);

    return res.DESCR;
  }

  close() {
    this.dialogRef.close();
  }

  spaces(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;

    if (charCode == 32) {
      return false;
    }
    return true;

  }

  folioFondo() {
    this.row['FONDO'] = this.row['FONDO'].toUpperCase();
  }

  valGnr(item, column) {
    if (this.groupF && this.groupF.controls[column]) {
      if (column == 'MNDFD') {
        if (item <= 0) {
          this.groupF.controls[column].setErrors({ 'monto': true });
          this.groupF.form.setErrors({ 'monto': true });
          return true;
        } else {
          this.groupF.controls[column].setErrors(null);
          this.groupF.form.setErrors(null);
          return false;
        }
      }
    } else {
      return false;
    }
  }

  valFondo() {
    if (this.row['FONDO']) {
      this.wbService.getInformation('16', 'REGIO=¯' + this.row['REGIO'] + '¯ AND FONDO=¯' + this.row['FONDO'] + '¯').subscribe(response => {
        if (response.data != null) {
          this.groupF.controls['FONDO'].setErrors({ 'exists': true });
        } else {
          this.groupF.controls['FONDO'].setErrors(null);
        }
      })
    }
  }

  showErrorFONDO() {
    if (this.groupF && this.groupF.controls['FONDO']) {
      return this.groupF.controls['FONDO'].hasError('exists');
    } else {
      return false;
    }
  }

  saveData() {
    let folio = this.row['FONDO'];

    let information = { 'DRF-FND': this.row } ;

    this.wbService.saveCatalogs(this.data.type, this.catalog, folio, '', JSON.stringify(information)).subscribe(
      response => {
        this.globalService.messageSwal('Información Almacenada', 'Tu información ha sido almacenada', 'success');

        if (this.data.type == 'A') {
          this.row['GROUP'] = this.data.data.GROUP + '|' + this.row['FONDO'];
        } else if (this.data.type == 'M') {
          this.row['GROUP'] = this.data.data.GROUP;
        }

        this.row['SHWBA'] = false;
        this.row['SHWBM'] = true;
        this.row['SHWBD'] = true;
        this.row['NAMEE'] = this.name(this.row['ENCAR']);

        let data = { previous: this.data.data, data: this.row, type: this.data.type, index: this.data.index };
        this.dialogRef.close(data);
      },
      error => {
        this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
        console.log(error);
      })
  }
}