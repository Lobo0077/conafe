import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CustomErrorStateMatcher } from '../../../../models/global/error';
import { FormControl, NgForm } from '@angular/forms';
import { buttonGrid } from "../../../../custom/grid/button/button.grid.component";
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import swal from 'sweetalert2';




@Component({
  selector: 'cat_drh_doc',
  templateUrl: './doc.component.html'
})

export class CATDRHDOCComponent {
  public catalog: string = 'DRH-DOC';
  public rowData = [];
  private gridApi: any;

  public columnDefs = [
    { headerName: "Documentos", field: "TPDOC", filter: 'agNumberColumnFilter', suppressMenu: true },
    { headerName: "Descripción", field: "DESCR", filter: 'agNumberColumnFilter', suppressMenu: true },
    { headerName: "", field: "ACTIO", suppressMenu: true, cellRendererFramework: buttonGrid, cellRendererParams: { options: "action", onClick: this.onBtnAction.bind(this) } }


  ];
  constructor(
    private wbService: WBService,
    private globalService: GlobalService,
    public dialog: MatDialog) {

  }

  ngOnInit() {
     this.dataDOC();
  }
  dataDOC(){
    this.wbService.getInformation('50', '').subscribe(response => {
      this.rowData = response.data.map(x => {
        x.SHWBA = false,
          x.SHWBM = true,
          x.SHWBD = true;

        return x
      })
    })

  }
  onGridReady(params) {
    this.gridApi = params;
    this.gridApi.api.sizeColumnsToFit();
    console.clear();
  }

  onBtnAction(e) {
    let type: string = e.option;
    let title: string;

    switch (type) {
      case 'A':
        title = 'Agregar Registro';
        break;
      case 'M':
        title = 'Modificar Registro';
        break;
      case 'E':
        title = 'Eliminar Registro';
        break;
      default:
        title = '';
        break;
    }

    if (type == 'M') {
      const dialogRef = this.dialog.open(CATDRHDOCAction, {
        width: '1000px',
        height: '450px',
        data: { type: type, title: title, data: e.rowData, index: e.index }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.refreshInformation(result);
        }
      })
    } else if (type == 'E') {
      swal.fire({
        type: 'warning',
        title: '¿Estas seguro de eliminar el registro?',
        text: '¡Esta acción no se podra revertir!',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Si',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.value) {
          if (result.value) {
            let folio = e.rowData.TPDOC;
  
            this.wbService.deleteCatalogs(this.catalog, folio).subscribe(
              response => {
                this.globalService.messageSwal('¡Borrado!', 'El registro fue eliminado', 'success');
                this.dataDOC();
              },
              error => {
                this.globalService.messageSwal('Error!!', 'El registro no pudo ser eliminado favor de intentarlo nuevamente', 'error');
                console.log(error);
              })
          }
        }
      })
    }

  }

  refreshInformation(data) {
    if (data.type == 'A') {
      this.dataDOC();
    } else if (data.type == 'M') {
      data.previous.TPDOC = data.data.TPDOC;
      data.previous.DESCR = data.data.DESCR;
      
      this.gridApi.api.updateRowData({ update: [data.previous] });
    }

  }
  openDialog() {
    let title: string = 'Agregar Registro';

    const dialogRef = this.dialog.open(CATDRHDOCAction, {
      width: '1000px',
      height: '450px',
      data: { type: 'A', title: title, data: {}, index: null }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.refreshInformation(result);
      }
    })

  }
}

@Component({
  selector: 'cat_drh_doc_action',
  templateUrl: 'action.dialog.html', 
})



export class CATDRHDOCAction implements OnInit {
  @ViewChild('groupForm', { static: false }) groupF: NgForm;

  public catalog: string = 'DRH-DOC';
  public row = { TPDOC: '', DESCR: '',};

  public funcionarios: Array<any>;
  public filteredEN: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
  public ENFilterCtrl: FormControl = new FormControl();

  protected _onDestroy = new Subject<void>();

  options = {
    componentRestrictions: { country: 'MX' }
  }

  public error = new CustomErrorStateMatcher();

  constructor(private wbService: WBService,
    private globalService: GlobalService,
    public dialogRef: MatDialogRef<CATDRHDOCAction>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    if (data.type == 'A') {
      this.row = { TPDOC: '', DESCR: ''};
    } else {
      this.row = { TPDOC: data.data.TPDOC, DESCR: data.data.DESCR };
    }
  }
  protected filterEN() {
    if (!this.funcionarios) {
      return;
    }

    let search = this.ENFilterCtrl.value;
    if (!search) {
      this.filteredEN.next(this.funcionarios.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredEN.next(
      this.funcionarios.filter(funcionario => funcionario.DESCR.toLowerCase().indexOf(search) > -1)
    );
  }
  ngOnInit() {

    this.wbService.getInformation('50', '').subscribe(response => {
      this.funcionarios = response.data;
      this.filteredEN.next(this.funcionarios)
    })

    this.ENFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEN();
      })
  }

  spaces(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;

    if (charCode == 32) {
      return false;
    }
    return true;

  }

  folio() {
    this.row['TPDOC'] = this.row['TPDOC'].toUpperCase();
  }

  valDoc() {
    if (this.row['TPDOC']) {
      this.wbService.getInformation('50', 'TPDOC=¯' + this.row['TPDOC'] + '¯').subscribe(response => {
        if (response.data != null) {
          this.groupF.controls['TPDOC'].setErrors({ 'exists': true });
        } else {
          this.groupF.controls['TPDOC'].setErrors(null);
        }
      })
    }
  }

  showErrorDoc() {
    if (this.groupF && this.groupF.controls['TPDOC']) {
      return this.groupF.controls['TPDOC'].hasError('exists');
    } else {
      return false;
    }
  }

  close() {
    this.dialogRef.close();
  }

  saveData() {
    let folio = this.row['TPDOC'];

    let information = { 'DRH-DOC': this.row };

    this.wbService.saveCatalogs(this.data.type, this.catalog, folio, '', JSON.stringify(information)).subscribe(
      response => {
        this.globalService.messageSwal('Información Almacenada', 'Tu información ha sido almacenada', 'success');

        let data = { previous: this.data.data, data: this.row, type: this.data.type, index: this.data.index };
        this.dialogRef.close(data);
      },
      error => {
        this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
        console.log(error);
      })
  }

}
