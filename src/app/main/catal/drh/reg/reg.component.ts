import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Console } from 'console';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { buttonGrid } from "../../../../custom/grid/button/button.grid.component";
import { FormControl, NgForm } from '@angular/forms';
import { CustomErrorStateMatcher } from '../../../../models/global/error';

@Component({
  selector: 'cat_drh_reg',
  templateUrl: './reg.component.html'
})
export class CATDRHREGComponent implements OnInit {
  public rowData = [];
  private gridApi: any;

  public columnDefs = [
    { headerName: "Region", field: "REGIO", filter: 'agTextColumnFilter', suppressMenu: true },
    { headerName: "Descripcion", field: "DESCR", filter: 'agNumberColumnFilter', suppressMenu: true },
    { headerName: "Abreviatura", field: "ABREV", filter: 'agTextColumnFilter', suppressMenu: true },
    { headerName: "Cuidad", field: "CIUDA", filter: 'agTextColumnFilter', suppressMenu: true },
    { headerName: "Estado", field: "ESTAD", filter: 'agTextColumnFilter', suppressMenu: true },
    { headerName: "Ubicacion", field: "PLACE", filter: 'agTextColumnFilter', suppressMenu: true },
    { headerName: "", field: "ACTIO", suppressMenu: true, cellRendererFramework: buttonGrid, cellRendererParams: { options: "action", onClick: this.onBtnAction.bind(this) } }

  ];
  

  constructor(
    private wbService: WBService,
    private globalService: GlobalService,
    public dialog: MatDialog) {

  }

  ngOnInit() {
    this.wbService.getInformation('3', '').subscribe(response => {
      this.rowData = response.data.map(x => {
        x.SHWBA = false,
          x.SHWBM = true,
          x.SHWBD = false;

        return x
      })
    })
  }

  onGridReady(params) {
    this.gridApi = params;
    this.gridApi.api.sizeColumnsToFit();
    console.clear();
  }
  onBtnAction(e) {
    console.log(e)
    let type: string = e.option;
    let title: string;

    switch (type) {
      case 'A':
        title = 'Agregar Registro';
        break;
      case 'M':
        title = 'Modificar Registro';
        break;
      case 'E':
        title = 'Eliminar Registro';
        break;
      default:
        title = '';
        break;
    }

    if (type == 'M') {
      const dialogRef = this.dialog.open(CATDRHREGAction, {
        width: '1000px',
        height: '450px',
        data: { type: type, title: title, data: e.rowData, index: e.index }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.refreshInformation(result);
        }
      })
    }

  }refreshInformation(data) {
    
    
      data.previous.DESCR = data.data.DESCR;
      data.previous.ABREV = data.data.ABREV;
      data.previous.CIUDA = data.data.CIUDA;
      data.previous.ESTAD = data.data.ESTAD;
      data.previous.PAISS = data.data.PAISS;
      data.previous.PLACE = data.data.PLACE;
      data.previous.LATIT = data.data.LATIT;
      data.previous.LONGI = data.data.LONGI;

      this.gridApi.api.updateRowData({ update: [data.previous] });
    
  }

}

@Component({
  selector: 'cat_drh_reg_action',
  templateUrl: 'action.dialog.html',
})
export class CATDRHREGAction {
  @ViewChild('groupForm', { static: false }) groupF: NgForm;

  public catalog: string = 'DRH-REG';
  public row = { REGIO: '', DESCR: '', ABREV: '', CIUDA: '', ESTAD: '',PAISS: '', LATIT: 0, LONGI: 0, PLACE: '', ZONNA: 0 };

  options = {
    componentRestrictions: { country: 'MX' }
    
  }
  public error = new CustomErrorStateMatcher();
  

  constructor(private wbService: WBService,
    private globalService: GlobalService,
    public dialogRef: MatDialogRef<CATDRHREGAction>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    
      this.row = { REGIO: data.data.REGIO, DESCR: data.data.DESCR, ABREV: data.data.ABREV, CIUDA: data.data.CIUDA, ESTAD: data.data.ESTAD, PAISS: data.data.PAISS, LATIT: data.data.LATIT, LONGI: data.data.LONGI, PLACE: data.data.PLACE, ZONNA: data.data.ZONNA};
    }
   
    infoAddress(address: any) {
      let ad = this.globalService.getAddressJSON(address);

      this.row.PLACE = ad.address;
      this.row.LONGI= ad.latitude;
      this.row.LATIT = ad.longitud;
      this.row.CIUDA = ad.city;
      this.row.ESTAD = ad.state;
      this.row.PAISS = ad.country;
    }

    close() {
      this.dialogRef.close();
    }
    
    saveData() {
      let folio = this.row['REGIO'];
  
      let information = { 'DRH-REG': this.row } ;
  
      this.wbService.saveCatalogs(this.data.type, this.catalog, folio, '', JSON.stringify(information)).subscribe(
        response => {
          this.globalService.messageSwal('Información Almacenada', 'Tu información ha sido almacenada', 'success');
  
          let data = { previous: this.data.data, data: this.row, type: this.data.type, index: this.data.index };
          this.dialogRef.close(data);
        },
        error => {
          this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
          console.log(error);
        })
    }
  }


