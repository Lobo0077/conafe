import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRippleModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatAutocompleteModule } from '@angular/material/autocomplete';

import { NgxDnDModule } from '@swimlane/ngx-dnd';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import { NgxMaterialTimepickerModule } from 'ngx-material-timepicker';
import { CurrencyMaskModule } from "ngx-currency-mask";
import { NgCircleProgressModule } from 'ng-circle-progress';
import { GooglePlaceModule } from "ngx-google-places-autocomplete";

import { AgGridModule } from 'ag-grid-angular';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule } from '@fuse/components';
import { CustomGridModule } from '../../../custom/grid/customGrid.module';

import { CATDRHREGComponent, CATDRHREGAction } from './reg/reg.component';
import { CATDRHADSComponent, CATDRHADSAction } from './ads/ads.component';
import { CATDRHGRJComponent, CATDRHGRJAction } from './grj/grj.component';
import { CATDRHPUEComponent, CATDRHPUEAction } from './pue/pue.component';
import { CATDRHPFLComponent, CATDRHPFLAction } from './pfl/pfl.component';
import { CATDRHPLAComponent, CATDRHPLAAction } from './pla/pla.component';
import { CATDRHDOCComponent, CATDRHDOCAction } from './doc/doc.component';
import { CATDRHTITComponent, CATDRHTITAction } from './tnm/tit.component';
import { CATDRHECIComponent, CATDRHECIAction } from './eci/eci.component';
import { CATDRHGENComponent, CATDRHGENAction } from './gen/gen.component';
import { CATDRHESCComponent, CATDRHESCAction } from './esc/esc.component';
import { CATDRHTPLComponent, CATDRHTPLAction } from './tpl/tpl.component';




import { RoleGuard } from '../../../guards/role.guard';



const routes: Routes = [
    {
        path: 'reg',
        component: CATDRHREGComponent,
        canActivate: [RoleGuard],
        data: { role: 'CATAL:DRF-FND' }
    },
    {
        path: 'ads',
        component: CATDRHADSComponent,
        canActivate: [RoleGuard],
        data: { role: 'CATAL:DRH-ADS' }
    },
    {
        path: 'grj',
        component: CATDRHGRJComponent,
        canActivate: [RoleGuard],
        data: { role: 'CATAL:DRH-GRJ' }
    },
    {
        path: 'pue',
        component: CATDRHPUEComponent,
        canActivate: [RoleGuard],
        data: { role: 'CATAL:DRH-PUE' }
    },
    {
        path: 'pfl',
        component: CATDRHPFLComponent,
        canActivate: [RoleGuard],
        data: { role: 'CATAL:DRH-PFL' }
    },

    {
        path: 'pla',
        component: CATDRHPLAComponent,
        canActivate: [RoleGuard],
        data: { role: 'CATAL:DRH-PLA' }
    },
    {
        path: 'doc',
        component: CATDRHDOCComponent,
        canActivate: [RoleGuard],
        data: { role: 'CATAL:DRH-DOC' }
    },
    {
        path: 'tit',
        component: CATDRHTITComponent,
        canActivate: [RoleGuard],
        data: { role: 'CATAL:DRH-TIT' }
    },
    {
        path: 'eci',
        component: CATDRHECIComponent,
        canActivate: [RoleGuard],
        data: { role: 'CATAL:DRH-ECI' },
    },

    {
        path: 'gen',
        component: CATDRHGENComponent,
        canActivate: [RoleGuard],
        data: { role: 'CATAL:DRH-GEN' },
    },
    {
        path: 'esc',
        component: CATDRHESCComponent,
        canActivate: [RoleGuard],
        data: { role: 'CATAL:DRH-ESC' },
    },
    {
        path: 'tpl',
        component: CATDRHTPLComponent,
        canActivate: [RoleGuard],
        data: { role: 'CATAL:DRH-TPL' },
    },


];

@NgModule({
    entryComponents: [
        CATDRHREGAction,
        CATDRHADSAction,
        CATDRHGRJAction,
        CATDRHPUEAction,
        CATDRHPFLAction,
        CATDRHPLAAction,
        CATDRHDOCAction,
        CATDRHTITAction,
        CATDRHECIAction,
        CATDRHGENAction,
        CATDRHESCAction,
        CATDRHTPLAction

    ],
    declarations: [
        CATDRHREGComponent,
        CATDRHREGAction,
        CATDRHADSComponent,
        CATDRHADSAction,
        CATDRHGRJComponent,
        CATDRHGRJAction,
        CATDRHPUEComponent,
        CATDRHPUEAction,
        CATDRHPFLComponent,
        CATDRHPFLAction,
        CATDRHPLAComponent,
        CATDRHPLAAction,
        CATDRHDOCComponent,
        CATDRHDOCAction,
        CATDRHTITComponent,
        CATDRHTITAction,
        CATDRHECIComponent,
        CATDRHECIAction,
        CATDRHGENComponent,
        CATDRHGENAction,
        CATDRHESCComponent,
        CATDRHESCAction,
        CATDRHTPLComponent,
        CATDRHTPLAction
    ],
    imports: [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSelectModule,
        MatTableModule,
        MatCardModule,
        MatToolbarModule,
        MatTooltipModule,
        MatDialogModule,
        MatProgressBarModule,
        MatAutocompleteModule,

        NgxDnDModule,
        NgxMatSelectSearchModule,
        NgxMaterialTimepickerModule,
        CurrencyMaskModule,
        NgCircleProgressModule.forRoot({}),
        GooglePlaceModule,

        FuseSharedModule,
        FuseSidebarModule,
        CustomGridModule,

        AgGridModule.withComponents([])
    ]
})
export class CatDRHModule {
}
