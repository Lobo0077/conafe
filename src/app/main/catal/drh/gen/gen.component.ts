import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CustomErrorStateMatcher } from '../../../../models/global/error';
import { FormControl, NgForm } from '@angular/forms';
import { buttonGrid } from "../../../../custom/grid/button/button.grid.component";
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import swal from 'sweetalert2';




@Component({
  selector: 'cat_drh_gen',
  templateUrl: './gen.component.html'
})

export class CATDRHGENComponent {
  public catalog: string = 'DRH-GEN';
  public rowData = [];
  private gridApi: any;

  public columnDefs = [
    { headerName: "Género", field: "GENER", filter: 'agNumberColumnFilter', suppressMenu: true },
    { headerName: "Descripción", field: "DESCR", filter: 'agNumberColumnFilter', suppressMenu: true },
    { headerName: "", field: "ACTIO", suppressMenu: true, cellRendererFramework: buttonGrid, cellRendererParams: { options: "action", onClick: this.onBtnAction.bind(this) } }


  ];
  constructor(
    private wbService: WBService,
    private globalService: GlobalService,
    public dialog: MatDialog) {

  }

  ngOnInit() {
     this.dataGEN();
  }
  dataGEN(){
    this.wbService.getInformation('52', '').subscribe(response => {
      this.rowData = response.data.map(x => {
        x.SHWBA = false,
          x.SHWBM = true,
          x.SHWBD = true;

        return x
      })
    })

  }


  onGridReady(params) {
    this.gridApi = params;
    this.gridApi.api.sizeColumnsToFit();
    console.clear();
  }

  onBtnAction(e) {
    let type: string = e.option;
    let title: string;

    switch (type) {
      case 'A':
        title = 'Agregar Registro';
        break;
      case 'M':
        title = 'Modificar Registro';
        break;
      case 'E':
        title = 'Eliminar Registro';
        break;
      default:
        title = '';
        break;
    }

    if (type == 'M') {
      const dialogRef = this.dialog.open(CATDRHGENAction, {
        width: '1000px',
        height: '450px',
        data: { type: type, title: title, data: e.rowData, index: e.index }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.refreshInformation(result);
        }
      })
    } else if (type == 'E') {
      swal.fire({
        type: 'warning',
        title: '¿Estas seguro de eliminar el registro?',
        text: '¡Esta acción no se podra revertir!',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Si',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.value) {
          if (result.value) {
            let folio = e.rowData.GENER;
  
            this.wbService.deleteCatalogs(this.catalog, folio).subscribe(
              response => {
                this.globalService.messageSwal('¡Borrado!', 'El registro fue eliminado', 'success');
                this.dataGEN
                ();
              },
              error => {
                this.globalService.messageSwal('Error!!', 'El registro no pudo ser eliminado favor de intentarlo nuevamente', 'error');
                console.log(error);
              })
          }
        }
      })
    }

  }

  refreshInformation(data) {
    if (data.type == 'A') {
      this.dataGEN();
    } else if (data.type == 'M') {
      data.previous.GENER = data.data.GENER;
      data.previous.DESCR = data.data.DESCR;
      
      this.gridApi.api.updateRowData({ update: [data.previous] });
    }

  }
  openDialog() {
    let title: string = 'Agregar Registro';

    const dialogRef = this.dialog.open(CATDRHGENAction, {
      width: '1000px',
      height: '450px',
      data: { type: 'A', title: title, data: {}, index: null }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.refreshInformation(result);
      }
    })

  }
}

@Component({
  selector: 'cat_drh_gen_action',
  templateUrl: 'action.dialog.html', 
})



export class CATDRHGENAction implements OnInit {
  @ViewChild('groupForm', { static: false }) groupF: NgForm;

  public catalog: string = 'DRH-GEN';
  public row = { GENER: '', DESCR: ''};

  public funcionarios: Array<any>;
  public filteredEN: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
  public ENFilterCtrl: FormControl = new FormControl();

  protected _onDestroy = new Subject<void>();

  options = {
    componentRestrictions: { country: 'MX' }
  }

  public error = new CustomErrorStateMatcher();

  constructor(private wbService: WBService,
    private globalService: GlobalService,
    public dialogRef: MatDialogRef<CATDRHGENAction>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    if (data.type == 'A') {
      this.row = { GENER: '', DESCR: ''};
    } else {
      this.row = { GENER: data.data.GENER, DESCR: data.data.DESCR };
    }
  }
  protected filterEN() {
    if (!this.funcionarios) {
      return;
    }

    let search = this.ENFilterCtrl.value;
    if (!search) {
      this.filteredEN.next(this.funcionarios.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredEN.next(
      this.funcionarios.filter(funcionario => funcionario.DESCR.toLowerCase().indexOf(search) > -1)
    );
  }
  ngOnInit() {

    this.wbService.getInformation('52', '').subscribe(response => {
      this.funcionarios = response.data;
      this.filteredEN.next(this.funcionarios)
    })

    this.ENFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEN();
      })
  }

  spaces(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;

    if (charCode == 32) {
      return false;
    }
    return true;

  }

  folio() {
    this.row['GENER'] = this.row['GENER'].toUpperCase();
  }

  valGENER() {
    if (this.row['GENER']) {
      this.wbService.getInformation('52', 'GENER=¯' + this.row['GENER'] + '¯').subscribe(response => {
        if (response.data != null) {
          this.groupF.controls['GENER'].setErrors({ 'exists': true });
        } else {
          this.groupF.controls['GENER'].setErrors(null);
        }
      })
    }
  }

  showErrorGENER() {
    if (this.groupF && this.groupF.controls['GENER']) {
      return this.groupF.controls['GENER'].hasError('exists');
    } else {
      return false;
    }
  }

  close() {
    this.dialogRef.close();
  }

  saveData() {
    let folio = this.row['GENER'];

    let information = { 'DRH-GEN': this.row };

    this.wbService.saveCatalogs(this.data.type, this.catalog, folio, '', JSON.stringify(information)).subscribe(
      response => {
        this.globalService.messageSwal('Información Almacenada', 'Tu información ha sido almacenada', 'success');

        let data = { previous: this.data.data, data: this.row, type: this.data.type, index: this.data.index };
        this.dialogRef.close(data);
      },
      error => {
        this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
        console.log(error);
      })
  }

}
