import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { GlobalService } from '../../../../services/global.service';
import { WBService } from '../../../../services/wb.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { CustomErrorStateMatcher } from '../../../../models/global/error';
import { FormControl, NgForm } from '@angular/forms';
import { buttonGrid } from "../../../../custom/grid/button/button.grid.component";
import { ReplaySubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import swal from 'sweetalert2';




@Component({
  selector: 'cat_drh_ads',
  templateUrl: './ads.component.html'
})

export class CATDRHADSComponent {
  public catalog: string = 'DRH-ADS';
  public rowData = [];
  private gridApi: any;

  public columnDefs = [
    { headerName: "Adscripción", field: "ADSCR", filter: 'agNumberColumnFilter', suppressMenu: true },
    { headerName: "Descripción", field: "DESCR", filter: 'agNumberColumnFilter', suppressMenu: true },
    { headerName: "Titular de Area", field: "NOMCP", filter: 'agTextColumnFilter', suppressMenu: true },
    { headerName: "", field: "ACTIO", suppressMenu: true, cellRendererFramework: buttonGrid, cellRendererParams: { options: "action", onClick: this.onBtnAction.bind(this) } }


  ];
  constructor(
    private wbService: WBService,
    private globalService: GlobalService,
    public dialog: MatDialog) {

  }

  ngOnInit() {
     this.dataADS();
  }
  dataADS(){
    this.wbService.getInformation('4', '').subscribe(response => {
      this.rowData = response.data.map(x => {
        x.SHWBA = false,
          x.SHWBM = true,
          x.SHWBD = true;

        return x
      })
    })

  }


  onGridReady(params) {
    this.gridApi = params;
    this.gridApi.api.sizeColumnsToFit();
    console.clear();
  }

  onBtnAction(e) {
    let type: string = e.option;
    let title: string;

    switch (type) {
      case 'A':
        title = 'Agregar Registro';
        break;
      case 'M':
        title = 'Modificar Registro';
        break;
      case 'E':
        title = 'Eliminar Registro';
        break;
      default:
        title = '';
        break;
    }

    if (type == 'M') {
      const dialogRef = this.dialog.open(CATDRHADSAction, {
        width: '1000px',
        height: '450px',
        data: { type: type, title: title, data: e.rowData, index: e.index }
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.refreshInformation(result);
        }
      })
    } else if (type == 'E') {
      swal.fire({
        type: 'warning',
        title: '¿Estas seguro de eliminar el registro?',
        text: '¡Esta acción no se podra revertir!',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Si',
        cancelButtonColor: '#d33',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if (result.value) {
          if (result.value) {
            let folio = e.rowData.ADSCR;
  
            this.wbService.deleteCatalogs(this.catalog, folio).subscribe(
              response => {
                this.globalService.messageSwal('¡Borrado!', 'El registro fue eliminado', 'success');
                this.dataADS();
              },
              error => {
                this.globalService.messageSwal('Error!!', 'El registro no pudo ser eliminado favor de intentarlo nuevamente', 'error');
                console.log(error);
              })
          }
        }
      })
    }
    

  }

  refreshInformation(data) {
    if (data.type == 'A') {
      this.dataADS();
    } else if (data.type == 'M') {
      data.previous.DESCR = data.data.DESCR;
      data.previous.ADSCR = data.data.ADSCR;
      data.previous.TITUL = data.data.TITUL;
      this.gridApi.api.updateRowData({ update: [data.previous] });
    }

  }
  openDialog() {
    let title: string = 'Agregar Registro';

    const dialogRef = this.dialog.open(CATDRHADSAction, {
      width: '1000px',
      height: '450px',
      data: { type: 'A', title: title, data: {}, index: null }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.refreshInformation(result);
      }
    })

  }
}

@Component({
  selector: 'cat_drh_ads_action',
  templateUrl: 'action.dialog.html', 
})



export class CATDRHADSAction implements OnInit {
  @ViewChild('groupForm', { static: false }) groupF: NgForm;

  public catalog: string = 'DRH-ADS';
  public row = { DESCR: '', ADSCR: '', TITUL: '', };

  public funcionarios: Array<any>;
  public filteredEN: ReplaySubject<string[]> = new ReplaySubject<string[]>(1);
  public ENFilterCtrl: FormControl = new FormControl();

  protected _onDestroy = new Subject<void>();

  options = {
    componentRestrictions: { country: 'MX' }
  }

  public error = new CustomErrorStateMatcher();

  constructor(private wbService: WBService,
    private globalService: GlobalService,
    public dialogRef: MatDialogRef<CATDRHADSAction>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
    if (data.type == 'A') {
      this.row = { ADSCR: '', DESCR: '', TITUL: '' };
    } else {
      this.row = { ADSCR: data.data.ADSCR, DESCR: data.data.DESCR, TITUL: data.data.TITUL };
    }
  }
  protected filterEN() {
    if (!this.funcionarios) {
      return;
    }

    let search = this.ENFilterCtrl.value;
    if (!search) {
      this.filteredEN.next(this.funcionarios.slice());
      return;
    } else {
      search = search.toLowerCase();
    }

    this.filteredEN.next(
      this.funcionarios.filter(funcionario => funcionario.DESCR.toLowerCase().indexOf(search) > -1)
    );
  }
  ngOnInit() {

    this.wbService.getInformation('5', '').subscribe(response => {
      this.funcionarios = response.data;
      this.filteredEN.next(this.funcionarios)
    })

    this.ENFilterCtrl.valueChanges
      .pipe(takeUntil(this._onDestroy))
      .subscribe(() => {
        this.filterEN();
      })
  }

  spaces(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;

    if (charCode == 32) {
      return false;
    }
    return true;

  }

  folio() {
    this.row['ADSCR'] = this.row['ADSCR'].toUpperCase();
  }

  valAdscripcion() {
    if (this.row['ADSCR']) {
      this.wbService.getInformation('4', 'ADSCR=¯' + this.row['ADSCR'] + '¯').subscribe(response => {
        if (response.data != null) {
          this.groupF.controls['ADSCR'].setErrors({ 'exists': true });
        } else {
          this.groupF.controls['ADSCR'].setErrors(null);
        }
      })
    }
  }

  showErrorADSCR() {
    if (this.groupF && this.groupF.controls['ADSCR']) {
      return this.groupF.controls['ADSCR'].hasError('exists');
    } else {
      return false;
    }
  }

  close() {
    this.dialogRef.close();
  }

  saveData() {
    let folio = this.row['ADSCR'];

    let information = { 'DRH-ADS': this.row };

    this.wbService.saveCatalogs(this.data.type, this.catalog, folio, '', JSON.stringify(information)).subscribe(
      response => {
        this.globalService.messageSwal('Información Almacenada', 'Tu información ha sido almacenada', 'success');

        let data = { previous: this.data.data, data: this.row, type: this.data.type, index: this.data.index };
        this.dialogRef.close(data);
      },
      error => {
        this.globalService.messageSwal('Error!!', 'Tu información no pudo ser almacenada favor de intentarlo nuevamente', 'error');
        console.log(error);
      })
  }

}