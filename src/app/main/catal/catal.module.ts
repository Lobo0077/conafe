import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';

const routes = [
    {
        path        : 'drf',
        loadChildren: './drf/c.drf.module#CatDRFModule'
    },
    {
        path        : 'drh',
        loadChildren: './drh/c.drh.module#CatDRHModule',
          
    }
];

@NgModule({
    imports     : [
        RouterModule.forChild(routes),
        FuseSharedModule
    ]
})
export class CatalModule
{
}
