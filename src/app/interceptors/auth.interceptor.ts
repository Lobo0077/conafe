import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import swal from 'sweetalert2';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor(private _authService: AuthService,
        private _router: Router){}

    intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {        
        return next.handle(req).pipe(
            catchError(e => {
                if(e.status == 401) {
                    if(this._authService.user) {
                        swal.fire('Sesión Expirada', 'Hola '+this._authService.user.user_name+' tu sesión ha expirado', 'warning');
                        this._router.navigate(['/pages/auth/login']);
                        sessionStorage.clear();
                    }
                }

                return throwError(e);
            })
        );
    }
}