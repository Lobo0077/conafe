import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    constructor(private _authService: AuthService){}

    intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {
        let token = this._authService.token;
        let array = ['/upload/file','/show/file','/create/file','/download/file/','/upload/form'];

        if(token != null) {
            let str = req.url;

            if (new RegExp(array.join("|")).test(str)) {
                const authReq = req.clone({
                    headers: req.headers.set('Authorization', 'Bearer ' + token)
                });
    
                return next.handle(authReq);
            } else {
                const authReq = req.clone({
                    headers: req.headers.set('Authorization', 'Bearer ' + token).set('Content-Type','application/json; charset=utf-8')
                });
    
                return next.handle(authReq);
            }
        }
        
        return next.handle(req);
    }
}