import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation, Input, NgZone } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subject } from 'rxjs';
import { delay, filter, take, takeUntil } from 'rxjs/operators';
import swal from 'sweetalert2';
import { WBService } from '../../../../../services/wb.service';
import { AuthService } from '../../../../../services/auth.service';
import { SessionStorageService } from 'ngx-store';

import { FuseConfigService } from '@fuse/services/config.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { FusePerfectScrollbarDirective } from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';

@Component({
    selector     : 'navbar-vertical-style-1',
    templateUrl  : './style-1.component.html',
    styleUrls    : ['./style-1.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class NavbarVerticalStyle1Component implements OnInit, OnDestroy {
    fuseConfig: any;
    navigation: any;

    // Private
    private _fusePerfectScrollbar: FusePerfectScrollbarDirective;
    private _unsubscribeAll: Subject<any>;

    // Public 
    public year: number;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseNavigationService} _fuseNavigationService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {Router} _router
     */
    constructor(private wbService: WBService,
                private authService: AuthService,
                private sessionStorageService: SessionStorageService,
                private zone: NgZone,
                private _fuseConfigService: FuseConfigService,
                private _fuseNavigationService: FuseNavigationService,
                private _fuseSidebarService: FuseSidebarService,
                private _router: Router) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Accessors
    // -----------------------------------------------------------------------------------------------------

    // Directive
    @ViewChild(FusePerfectScrollbarDirective, {static: true})
    set directive(theDirective: FusePerfectScrollbarDirective) {
        if ( !theDirective )
        {
            return;
        }

        this._fusePerfectScrollbar = theDirective;

        // Update the scrollbar on collapsable item toggle
        this._fuseNavigationService.onItemCollapseToggled
            .pipe(
                delay(500),
                takeUntil(this._unsubscribeAll)
            )
            .subscribe(() => {
                this._fusePerfectScrollbar.update();
            });

        // Scroll to the active item position
        this._router.events
            .pipe(
                filter((event) => event instanceof NavigationEnd),
                take(1)
            )
            .subscribe(() => {
                    setTimeout(() => {
                        this._fusePerfectScrollbar.scrollToElement('navbar .nav-link.active', -120);
                    });
                }
            );
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit() {
        this.sessionStorageService.observe()
        .subscribe((event) => {
          if(event.key == 'year'){
            if(!sessionStorage.getItem('year')){
              this.year = null;
              this.selectYear('storage');
            }          
          }else if(event.key == 'token' || event.key == 'user'){
            if(!sessionStorage.getItem('token') || !sessionStorage.getItem('user')){
              sessionStorage.clear();
              swal.fire('Login', 'No se encuentran los datos de tu usuario vuelve a ingresar tus credenciales', 'error');
              this._router.navigate(['/pages/auth/login']);
            }          
          }else if(!event.key){
            if(!sessionStorage.getItem('token') || !sessionStorage.getItem('user')){
              sessionStorage.clear();
              swal.fire('Login', 'No se encuentran los datos de tu usuario vuelve a ingresar tus credenciales', 'error');
              this._router.navigate(['/pages/auth/login']);
            }else{
              if(!sessionStorage.getItem('year')){
                this.year = null;
                this.selectYear('storage');
              } 
            }
          }
        });
  
        if(!sessionStorage.getItem('year')){
          this.selectYear('init');
        }else{
          this.year = Number(sessionStorage.getItem('year'));
        }

        this._router.events
            .pipe(
                filter((event) => event instanceof NavigationEnd),
                takeUntil(this._unsubscribeAll)
            )
            .subscribe(() => {
                    if ( this._fuseSidebarService.getSidebar('navbar') )
                    {
                        this._fuseSidebarService.getSidebar('navbar').close();
                    }
                }
            );

        // Subscribe to the config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((config) => {
                this.fuseConfig = config;
            });

        // Get current navigation
        this._fuseNavigationService.onNavigationChanged
            .pipe(
                filter(value => value !== null),
                takeUntil(this._unsubscribeAll)
            )
            .subscribe(() => {
                this.navigation = this._fuseNavigationService.getCurrentNavigation();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy() {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle sidebar opened status
     */
    toggleSidebarOpened() {
        this._fuseSidebarService.getSidebar('navbar').toggleOpen();
    }

    /**
     * Toggle sidebar folded status
     */
    toggleSidebarFolded() {
        this._fuseSidebarService.getSidebar('navbar').toggleFold();
    }

    selectYear(type){
      if(this.authService.user) {
        this.wbService.getInformation('2','').subscribe(response => {
          const options = {};
  
          for(let x = 0; x < response.data.length; x++){
              let vREGISTRY_ID: string = response.data[x].EJERC;
              let vACCOUNT_NAME: string = response.data[x].EJERC;
  
              options[vREGISTRY_ID] = vACCOUNT_NAME;
          }
          
          swal.fire({
              title: 'Seleccionar ejercicio fiscal',
              input: 'select',
              inputOptions: options,
              inputPlaceholder: 'Selecciona',
              showCancelButton: false,
              allowOutsideClick: false,
              allowEscapeKey: false,
              inputValidator: (value) => {
                  return new Promise((resolve) => {
                      if (value != '') {
                          if(type == 'change'){
                            if(sessionStorage.getItem('year') != value){
                              swal.fire({
                                title: 'Cambiar ejercicio?',
                                text: "Si continuas se actualizara la pagina y los cambios no guardados se perderan",
                                type: 'warning',
                                showCancelButton: true,
                                confirmButtonColor: '#28a745',
                                cancelButtonColor: '#dc3545',
                                confirmButtonText: 'Si',
                                cancelButtonText: 'No'
                              }).then((result) => {
                                if (result.value) {
                                  sessionStorage.setItem('year', value);
                                  this.year = Number(sessionStorage.getItem('year'));
                                  
                                  this.zone.runOutsideAngular(() => {
                                    location.reload();
                                  });
                                }
                              })
                            }else {
                              sessionStorage.setItem('year', value);
                              this.year = Number(sessionStorage.getItem('year'));
                              resolve();
                            }
                          } else if(type == 'storage'){
                            sessionStorage.setItem('year', value);
                            this.year = Number(sessionStorage.getItem('year'));
                            resolve();
  
                            this.zone.runOutsideAngular(() => {
                              location.reload();
                            });
                          } else if(type == 'init'){
                            sessionStorage.setItem('year', value);
                            this.year = Number(sessionStorage.getItem('year'));
                            resolve();
                          }
                      } else {
                          resolve('No se puede continuar si no seleccionas un ejercicio fiscal')
                      }
                  })
              }
          })
        });
      }
    }
}
