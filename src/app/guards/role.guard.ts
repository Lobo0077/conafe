import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import swal from 'sweetalert2';
import { AuthService } from '../services/auth.service';

@Injectable({
    providedIn: 'root'
})
export class RoleGuard implements CanActivate {

    constructor(private _authService: AuthService, 
                private _router: Router){}

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {

            let role = next.data['role'] as string;

            if(this._authService.tokenExpired()) {
                swal.fire('Sesión Expirada', 'Hola '+this._authService.user.user_name+' tu sesión ha expirado', 'warning');
                this._router.navigate(['/pages/auth/login']);
                sessionStorage.clear();
                return false;
            }

            if(this._authService.hasRole(role)){
                return true;
            }

            swal.fire('Acceso Denegado', 'Hola '+this._authService.user.user_name+' no tienes acceso a este recurso!', 'warning');
            this._router.navigate(['init']);
            return false;
        }
}