export class recover{
    format: string;
    type: string;
    title: string;
    object: string;
    columns: Array<any>;
    filter: string;

    constructor() {
        this.format = '',
        this.type = '',
        this.title = '',
        this.object = '',
        this.columns = [],
        this.filter = '';
    }
}

