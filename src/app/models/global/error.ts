import { ErrorStateMatcher } from '@angular/material/core';
import { FormControl } from '@angular/forms';

export class CustomErrorStateMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null): boolean {
      let error: boolean = false;
      
      if(control.errors != null){
        error = true;
      }

      return (control.invalid || error);
    }
}