export class address{
    address: string;
    place: boolean;
    placeId: string;
    geometry: boolean;
    latitude: number;
    longitud: number;
    components: boolean;
    streetNumber: string;
    street: string;
    sublocality: string;
    city: string;
    state: string;
    stateShort: string;
    country: string;
    countryShort: string;
    district: string;
    postalCode: string;

    constructor() {
        this.place = false,
        this.placeId = '',
        this.geometry = false,
        this.latitude = 0,
        this.longitud = 0,
        this.components = false,
        this.streetNumber = '',
        this.street = '',
        this.sublocality = '',
        this.city = '',
        this.state = '',
        this.stateShort = '',
        this.country = '',
        this.countryShort = '',
        this.district = '',
        this.postalCode = '';
    }
}

