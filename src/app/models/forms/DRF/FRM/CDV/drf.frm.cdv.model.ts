export class F_DRF_FRM_CDV {
    FOLIO:string;
    COMPR:string;
    STTUS:string;
    EJERC:string;
    USUAR:string;
    FECHA:Date;
    REGIO:string;
    ADSCR:string;
    PUEST:string;
    NIVEL:string;
    LUGAR:number;
    NMPAG:string;
    GSCCM:number;
    GSSCM:number;
    TTGTS:number;
    DIFER:number;
    NMRIN:string;
    MNRIN:number;
    RMCMS:number;
    OBSER:string;
    DIRIA:string;
    PROPO:string;
    ACTIV:string;
    RESUL:string;
    CONTR:string;
  
    constructor(){
        this.FOLIO='',
        this.COMPR='',
        this.STTUS='',
        this.EJERC='',
        this.USUAR='',
        this.FECHA=null,
        this.REGIO='',
        this.ADSCR='',
        this.PUEST='',
        this.NIVEL='',
        this.LUGAR=null,
        this.NMPAG='',
        this.GSCCM=0,
        this.GSSCM=0,
        this.TTGTS=0,
        this.DIFER=0,
        this.NMRIN='',
        this.MNRIN=0,
        this.RMCMS=0,
        this.OBSER='',
        this.DIRIA='',
        this.PROPO='',
        this.ACTIV='',
        this.RESUL='',
        this.CONTR='';
    }
  }
  
  export class F_DRF_FRM_CDV_GCC {
    ID:number;
    ERROR:boolean;
    XXMLL:string;
    PPDFF:string;
    FACDV:number;
    NMDOC:string;
    FCDOC:Date;
    RRFCC:string;
    NMRZS:string;
    CNGCC:string;
    TOTFC:number;
    IMACM:number;
  
    constructor(){
      this.ID=0,
      this.ERROR=true,
      this.XXMLL='',
      this.PPDFF='',
      this.FACDV=1,
      this.NMDOC='',
      this.FCDOC=null,
      this.RRFCC='',
      this.NMRZS='',
      this.CNGCC='',
      this.TOTFC=0,
      this.IMACM=0;
    }
  }
  
  export class F_DRF_FRM_CDV_GSC {
    ID:number;
    ERROR:boolean;
    PPDFF:string;
    FECGS:Date;
    CNGSC:string;
    IMPOR:number;
  
    constructor(){
      this.ID=0,
      this.ERROR=false,
      this.PPDFF='',
      this.FECGS=null,
      this.CNGSC='',
      this.IMPOR=0;
    }
  }

  export class P_DRF_FRM_CDV_GCC {
    XXMLL: number;
    PPDFF: number;
  
    constructor(){
      this.XXMLL=0,
      this.PPDFF=0;
    }
  }

  export class P_DRF_FRM_CDV_GSC {
    PPDFF: number;
  
    constructor(){
      this.PPDFF=0;
    }
  }
  
  export class names {
    NUEMP:string;
    SLNOM:string;
    SLPUE:string;
    PRINI:Date;
    PRTER:Date;
    TPVIA:string;
    MDPAG:string;
    LBLMN:string;
    TTVIA:number;
    INFOR:boolean;
    RCNOM:string;
    RCPUE:string;
  
    constructor(){
      this.NUEMP='',
      this.SLNOM='',
      this.SLPUE='',
      this.PRINI=null,
      this.PRTER=null,
      this.TPVIA='',
      this.MDPAG='',
      this.LBLMN='',
      this.TTVIA=0,
      this.INFOR=false,
      this.RCNOM='',
      this.RCPUE='';
    }
  }
  