export class F_DRF_FRM_GAC {
  FOLIO: string;
  STTUS: string;
  EJERC: string;
  USUAR: string;
  FECHA: Date;
  REGIO: string;
  CTRPD: string;
  REQUI: string;
  PRTSP: string;
  RRFCC: string;
  RZSOC: string;
  IMPOR: number;
  UTILI: string;
  VSTBO: string;
  AUTOR: string;

  constructor(){
    this.FOLIO='',
    this.STTUS='',
    this.EJERC='',
    this.USUAR='',
    this.FECHA=null,
    this.REGIO='',
    this.CTRPD='',
    this.REQUI='',
    this.PRTSP='',
    this.RRFCC='',
    this.RZSOC='',
    this.IMPOR=0,
    this.UTILI='',
    this.VSTBO='',
    this.AUTOR='';
  }
}

export class names {
  ELNOM:string;
  ELPUE:string;
  VBPUE:string;
  AUPUE:string;

  constructor(){
      this.ELNOM='',
      this.ELPUE='',
      this.VBPUE='',
      this.AUPUE='';
  }
}