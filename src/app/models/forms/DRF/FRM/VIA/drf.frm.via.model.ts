export class F_DRF_FRM_VIA {
    FOLIO:string;
    STTUS:string;
    EJERC:string;
    USUAR:string;
    FECHA:Date;
    REGIO:string;
    ADSCR:string;
    PUEST:string;
    NIVEL:string;
    LATPR:number;
    LNGPR:number;
    CIUPR:string;
    ESTPR:string;
    PAIPR:string;
    TPVIA:string;
    PAQUE:string;
    MDPAG:string;
    TTSOL:number;
    TTDIA:number;
    TTVIA:number;
    BANCO:string;
    SUCUR:string;
    CLABE:string;
    MDTRA:string;
    GSYPE:number;
    CZPDF:string;
    AUPDF:string;
    TTBOL:number;
    OBCOM:string;
    JUSTI:string;
    OBSER:string;
    AUTOR:string;
  
    constructor(){
        this.FOLIO='',
        this.STTUS='',
        this.EJERC='',
        this.USUAR='',
        this.FECHA=null,
        this.REGIO='',
        this.ADSCR='',
        this.PUEST='',
        this.NIVEL='',
        this.LATPR=0,
        this.LNGPR=0,
        this.CIUPR='',
        this.ESTPR='',
        this.PAIPR='',
        this.TPVIA='',
        this.PAQUE='',
        this.MDPAG='',
        this.TTSOL=0,
        this.TTDIA=0,
        this.TTVIA=0,
        this.BANCO='',
        this.SUCUR='',
        this.CLABE='',
        this.MDTRA='',
        this.GSYPE=0,
        this.CZPDF='',
        this.AUPDF='',
        this.TTBOL=0,
        this.OBCOM='',
        this.JUSTI='',
        this.OBSER='',
        this.AUTOR='';
    }
  }
  
  export class F_DRF_FRM_VIA_DES {
    ID:number;
    ERROR:string;
    PLACE:string;
    LATDS:number;
    LNGDS:number;
    CIUDS:string;
    ESTDS:string;
    PAIDS:string;
    DISTA:number;
    ZONNA:string;
    PRINI:Date;
    PRTER:Date;
    DIASS:number;
    CUODI:number;
    IMPCM:number;
  
    constructor(){
        this.ID=0,
        this.ERROR='',
        this.PLACE='',
        this.LATDS=0,
        this.LNGDS=0,
        this.CIUDS='',
        this.ESTDS='',
        this.PAIDS='',
        this.DISTA=0,
        this.ZONNA='',
        this.PRINI=null,
        this.PRTER=null,
        this.DIASS=0,
        this.CUODI=0,
        this.IMPCM=0;
    }
  }
  
  export class F_DRF_FRM_VIA_BOL {
    ID:number;
    ERROR:string;
    COMPA:string;
    VUELO:string;
    SALID:string;
    FECSL:Date;
    HORSL:string;
    LLEGA:string;
    FECLL:Date;
    HORLL:string;
    IMPOR:number;
  
    constructor(){
        this.ID=0,
        this.ERROR='',
        this.COMPA='',
        this.VUELO='',
        this.SALID='',
        this.FECSL=null,
        this.HORSL='',
        this.LLEGA='',
        this.FECLL=null,
        this.HORLL='',
        this.IMPOR=0;
    }
  }

  export class P_DRF_FRM_VIA {
    CZPDF: number;
    AUPDF: number;
  
    constructor(){
      this.CZPDF=0,
      this.AUPDF=0;
    }
  }
  
  export class names {
    NUEMP:string;
    SLNOM:string;
    SLPUE:string;
    AUPUE:string;
    TDIAS:number;
    GSYPE:boolean;
    DTVUE:boolean;
  
    constructor(){
        this.NUEMP='',
        this.SLNOM='',
        this.SLPUE='',
        this.AUPUE='',
        this.TDIAS=0,
        this.GSYPE=false,
        this.DTVUE=false;
    }
  }
  