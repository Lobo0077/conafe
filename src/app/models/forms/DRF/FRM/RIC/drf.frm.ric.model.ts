export class F_DRF_FRM_RIC {
  FOLIO: string;
  STTUS: string;
  EJERC: string;
  USUAR: string;
  FECHA: Date;
  REGIO: string;
  TIDIN: string;
  INRLZ: string;
  CONCE: string;
  IMPOR: number;
  MDPAG: string;
  NMPAG: string;
  FCPAG: Date;
  CMPAG: string;
  NMRZS: string;
  RRFCC: string;
  DOMIC: string;
  CDPST: string;
  TELEF: string;
  RECIB: string;

  constructor(){
    this.FOLIO='',
    this.STTUS='',
    this.EJERC='',
    this.USUAR='',
    this.FECHA=null,
    this.REGIO='',
    this.TIDIN='',
    this.INRLZ='',
    this.CONCE='',
    this.IMPOR=0,
    this.MDPAG='',
    this.NMPAG='',
    this.FCPAG=null,
    this.CMPAG='',
    this.NMRZS='',
    this.RRFCC='',
    this.DOMIC='',
    this.CDPST='',
    this.TELEF='',
    this.RECIB='';
  }
}

export class P_DRF_FRM_RIC {
  CMPAG: number;

  constructor(){
    this.CMPAG=0;
  }
}

export class names {
  RCPUE:string;

  constructor(){
      this.RCPUE='';
  }
}