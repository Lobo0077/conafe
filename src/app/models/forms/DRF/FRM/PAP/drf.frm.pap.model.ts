export class F_DRF_FRM_PAP {
  FOLIO: string;
  STTUS: string;
  EJERC: string;
  USUAR: string;
  FECHA: Date;
  REGIO: string;
  CTRPD: string;
  REQUI: string;
  PRTSP: string;
  DTBAN: string;
  TTSUB: number;
  TTIVA: number;
  TTTOT: number;
  TTRIS: number;
  TTRIV: number;
  TOTAL: number;
  OBSER: string;
  VSTBO: string;
  AUTOR: string;

  constructor(){
    this.FOLIO='',
    this.STTUS='',
    this.EJERC='',
    this.USUAR='',
    this.FECHA=null,
    this.REGIO='',
    this.CTRPD='',
    this.REQUI='',
    this.PRTSP='',
    this.DTBAN='',
    this.TTSUB=0,
    this.TTIVA=0,
    this.TTTOT=0,
    this.TTRIS=0,
    this.TTRIV=0,
    this.TOTAL=0,
    this.OBSER='',
    this.VSTBO='',
    this.AUTOR='';
  }
}

export class F_DRF_FRM_PAP_COM {
  ID: number;
  ERROR: string;
  XXMLL: string;
  PPDFF: string;
  NMDOC: string;
  FCDOC: Date;
  RRFCC: string;
  NMRZS: string;
  CONCE: string;
  SUBTT: number;
  IIVAA: number;
  TOTAL: number;
  RTISR: number;
  RTIVA: number;
  TOTFC: number;

  constructor(){
    this.ID=0,
    this.ERROR='',
    this.XXMLL='',
    this.PPDFF='',
    this.NMDOC='',
    this.FCDOC=null,
    this.RRFCC='',
    this.NMRZS='',
    this.CONCE='',
    this.SUBTT=0,
    this.IIVAA=0,
    this.TOTAL=0,
    this.RTISR=0,
    this.RTIVA=0,
    this.TOTFC=0;
  }
}

export class P_DRF_FRM_PAP_COM {
  XXMLL: number;
  PPDFF: number;

  constructor(){
    this.XXMLL=0,
    this.PPDFF=0;
  }
}

export class names {
  ELNOM:string;
  ELPUE:string;
  VBPUE:string;
  AUPUE:string;

  constructor(){
      this.ELNOM='',
      this.ELPUE='',
      this.VBPUE='',
      this.AUPUE='';
  }
}