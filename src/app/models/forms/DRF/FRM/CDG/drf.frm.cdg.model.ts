export class F_DRF_FRM_CDG {
    FOLIO:string;
    STTUS:string;
    EJERC:string;
    USUAR:string;
    FECHA:Date;
    GUARD:boolean;
    REGIO:string;
    COMFS:boolean;
    TOTCM:number;
    DIFER:number;
    NMRIN:string;
    MNRIN:number;
    PAGCM:boolean;
    RMGAC:number;
    OBSER:string;
    AUTOR:string;
  
    constructor(){
      this.FOLIO='',
      this.STTUS='',
      this.EJERC='',
      this.USUAR='',
      this.FECHA=null,
      this.GUARD=false,
      this.REGIO='',
      this.COMFS=true,
      this.TOTCM=0,
      this.DIFER=0,
      this.NMRIN='',
      this.MNRIN=0,
      this.PAGCM=false,
      this.RMGAC=0,
      this.OBSER='',
      this.AUTOR='';
    }
  }
  
  export class F_DRF_FRM_CDG_COM {
    ID:number;
    ERROR:string;
    XXMLL:string;
    PPDFF:string;
    NMDOC:string;
    FCDOC:Date;
    RRFCC:string;
    NMRZS:string;
    CONCE:string;
    TOTFC:number;
  
    constructor(){
      this.ID=0,
      this.ERROR='',
      this.XXMLL='',
      this.PPDFF='',
      this.NMDOC='',
      this.FCDOC=null,
      this.RRFCC='',
      this.NMRZS='',
      this.CONCE='',
      this.TOTFC=0;
    }
  }
  
  export class P_DRF_FRM_CDG_COM {
    XXMLL: number;
    PPDFF: number;
  
    constructor(){
      this.XXMLL=0,
      this.PPDFF=0;
    }
  }
  
  export class names {
    SLNOM:string;
    SLPUE:string;
    CTRPD:string;
    REQUI:string;
    PRTSP:string;
    RRFCC:string;
    RZSOC:string;
    IMPOR:number;
    UTILI:string;
    AUNOM:string;
    AUPUE:string;
  
    constructor(){
      this.SLNOM='',
      this.SLPUE='',
      this.CTRPD='',
      this.REQUI='',
      this.PRTSP='',
      this.RRFCC='',
      this.RZSOC='',
      this.IMPOR=0,
      this.UTILI='',
      this.AUNOM='',
      this.AUPUE='';
    }
  }
  