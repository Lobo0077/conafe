export class F_DRF_FRM_FRV {
  FOLIO: string;
  STTUS: string;
  EJERC: string;
  USUAR: string;
  FECHA: Date;
  REGIO: string;
  FONDO: string;
  ENCAR: string;
  TPMVF: string;
  MNDFD: number;
  OFSOL: string;
  OFAPR: string;
  TOTAL: number;
  OBSER: string;

  constructor(){
    this.FOLIO='',
    this.STTUS='',
    this.EJERC='',
    this.USUAR='',
    this.FECHA=null,
    this.REGIO='',
    this.FONDO='',
    this.ENCAR='',
    this.TPMVF='',
    this.MNDFD=0,
    this.OFSOL='',
    this.OFAPR='',
    this.TOTAL=0,
    this.OBSER='';
  }
}

export class F_DRF_FRM_FRV_COM {
  ID: number;
  ERROR: string;
  XXMLL: string;
  PPDFF: string;
  NMDOC: string;
  FCDOC: Date;
  RRFCC: string;
  NMRZS: string;
  CONCE: string;
  TOTFC: number;
  ARRSP: string;
  PRTSP: string;

  constructor(){
    this.ID=0,
    this.ERROR='',
    this.XXMLL='',
    this.PPDFF='',
    this.NMDOC='',
    this.FCDOC=null,
    this.RRFCC='',
    this.NMRZS='',
    this.CONCE='',
    this.TOTFC=0,
    this.ARRSP='',
    this.PRTSP='';
  }
}

export class P_DRF_FRM_FRV {
  OFSOL: number;
  OFAPR: number;

  constructor(){
    this.OFSOL=0,
    this.OFAPR=0;
  }
}

export class P_DRF_FRM_FRV_COM {
  XXMLL: number;
  PPDFF: number;

  constructor(){
    this.XXMLL=0,
    this.PPDFF=0;
  }
}

export class names {
  ELNOM:string;
  ELPUE:string;
  EFNOM:string;
  EFPUE:string;

  constructor(){
      this.ELNOM='',
      this.ELPUE='',
      this.EFNOM='',
      this.EFPUE='';
  }
}
