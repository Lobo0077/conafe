export class F_DRM_FRM_ACP {
    CTRPD:string;
    STTUS:string;
    EJERC:string;
    USUAR:string;
    FECHA:Date;
    REGIO:string;
    REQUI:string;
    TPDCT:string;
    CTPDM:string;
    PRTSP:string;
    TPDAD:string;
    CTDCT:string;
    MDDCT:string;
    RRFCC:string;
    RZSOC:string;
    FCINI:Date;
    FCTER:Date;
    TEMPO:string;
    MNMIN:number;
    MNMAX:number;
    CTZC1:string;
    CTZC2:string;
    CTZC3:string;
    DESCR:string;
  
    constructor(){
        this.CTRPD='',
        this.STTUS='',
        this.EJERC='',
        this.USUAR='',
        this.FECHA=null,
        this.REGIO='',
        this.REQUI='',
        this.TPDCT='',
        this.CTPDM='',
        this.PRTSP='',
        this.TPDAD='',
        this.CTDCT='',
        this.MDDCT='',
        this.RRFCC='',
        this.RZSOC='',
        this.FCINI=null,
        this.FCTER=null,
        this.TEMPO='',
        this.MNMIN=0,
        this.MNMAX=0,
        this.CTZC1='',
        this.CTZC2='',
        this.CTZC3='',
        this.DESCR='';
    }
}

export class P_DRM_FRM_ACP {
    CTZC1: number;
    CTZC2: number;
    CTZC3: number;
  
    constructor(){
      this.CTZC1=0,
      this.CTZC2=0,
      this.CTZC3=0;
    }
}
