export class F_ADM_FRM_EFL {
  UUIDD:string;
  STTUS:string;
  USUAR:string;
  FECHA:Date;
  EJERC:string;
  FORMA:string;
  FOLIO:string;
  STSCH:number;
  MOTIV:string;

  constructor(){
    this.UUIDD='',
    this.STTUS='',
    this.USUAR='',
    this.FECHA=null,
    this.EJERC='',
    this.FORMA='',
    this.FOLIO='',
    this.STSCH=null,
    this.MOTIV='';
  }
}