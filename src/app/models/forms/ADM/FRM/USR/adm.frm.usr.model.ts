export class F_ADM_FRM_USR {
    CUSER:string;
    STTUS:string;
    USUAR:string;
    KEYUI:string;
    FECHA:Date;
    ENABL:boolean;
    PASSW:string;
    PERFL:string;
    EMAIL:string;
  
    constructor(){
      this.CUSER='',
      this.STTUS='',
      this.USUAR='',
      this.KEYUI='',
      this.FECHA=null,
      this.ENABL=true,
      this.PASSW='',
      this.PERFL='',
      this.EMAIL='';
    }
  }
  
  export class F_ADM_FRM_USR_ACS {
    ID:number;
    GRUPO:string;
    CLAVE:string;
    TICON:string;
    DESCR:string;
    NIVEL:number;
    ACCES:boolean;
  
    constructor(){
      this.ID=0,
      this.GRUPO='',
      this.CLAVE='',
      this.TICON='',
      this.DESCR='',
      this.NIVEL=0,
      this.ACCES=true;
    }
  }

  export class aditional {
    DSREG:string;
    ADSCR:string;
    PUEST:string;
    NAMEE:string;
    PRIAP:string;
    SEGAP:string;
    FOLIO:string;
    REGIO:string;
  
    constructor(data?){
      this.DSREG=data ? data.DSREG:'',
      this.ADSCR=data ? data.ADSCR:'',
      this.PUEST=data ? data.PUEST:'',
      this.NAMEE=data ? data.NAMEE:'',
      this.PRIAP=data ? data.PRIAP:'',
      this.SEGAP=data ? data.SEGAP:'',
      this.FOLIO=data ? data.FOLIO:'',
      this.REGIO=data ? data.REGIO:'';
    }
  }