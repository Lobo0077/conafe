export class F_DRH_FRM_MPR {
  FOLIO: string;
  STTUS: string;
  EJERC: number;
  USUAR: string;
  KEYUI: string;
  FECHA: Date;
  REGIO: string;
  FCMOV: Date;
  TIPIN: string;
  NUEMP: string;
  PRIAP: string;
  SEGAP: string;
  NAMEE: string;
  ADSSA: string;
  REGSA: string;
  PUESA: string;
  PLASA: boolean;
  SUSSA: string;
  PLAZA: number;
  DESSA: string;
  DESS2: string;
  NIVSA: string;
  TIPSA: string;
  NOMSA: string;
  SUMSA: number;
  COMSA: number;
  SMESA: number;
  SINSA: boolean;
  HORSA: boolean;
  HODSA: string;
  HOASA: string;
  HOPSA: string;
  HPPSA: string;
  ADSPR: string;
  REGPR: string;
  PUEPR: string;
  PLAPR: boolean;
  SUSPR: string;
  PLZPR: number;
  DESPR: string;
  DEPR2: string;
  NIVPR: string;
  TIPPR: string;
  NOMPR: string;
  SUMPR: number;
  COMPR: number;
  SMEPR: number;
  SINPR: boolean;
  HORPR: boolean;
  HODPR: string;
  HOAPR: string;
  HOPPR: string;
  HPPPR: string;
  FCINI: Date;
  FCFIN: Date;
  PERAC: number;
  PERIN: number;
  PERPR: number;
  OBSER: string;
  FUNCI: string;
  NOMAU: string;
  PUEAU: string;

  constructor() {
    this.FOLIO = '',
    this.STTUS = '',
    this.EJERC = 0,
    this.USUAR = '',
    this.KEYUI = '',
    this.FECHA = null,
    this.REGIO = '',
    this.FCMOV = null,
    this.TIPIN = '',
    this.NUEMP = '',
    this.PRIAP = '',
    this.SEGAP = '',
    this.NAMEE = '',
    this.ADSSA = '',
    this.REGSA = '',
    this.PUESA = '',
    this.PLASA = false,
    this.SUSSA = '',
    this.PLAZA = 0,
    this.DESSA = '',
    this.DESS2 = '',
    this.NIVSA = '',
    this.TIPSA = '',
    this.NOMSA = '',
    this.SUMSA = 0,
    this.COMSA = 0,
    this.SMESA = 0,
    this.SINSA = false,
    this.HORSA = false,
    this.HODSA = '',
    this.HOASA = '',
    this.HOPSA = '',
    this.HPPSA = '',
    this.ADSPR = '',
    this.REGPR = '',
    this.PUEPR = '',
    this.PLAPR = false,
    this.SUSPR = '',
    this.PLZPR = 0,
    this.DESPR = '',
    this.DEPR2 = '',
    this.NIVPR = '',
    this.TIPPR = '',
    this.NOMPR = '',
    this.SUMPR = 0,
    this.COMPR = 0,
    this.SMEPR = 0,
    this.SINPR = false,
    this.HORPR = false,
    this.HODPR = '',
    this.HOAPR = '',
    this.HOPPR = '',
    this.HPPPR = '',
    this.FCINI = null,
    this.FCFIN = null,
    this.PERAC = 0,
    this.PERIN = 0,
    this.PERPR = 0,
    this.OBSER = '',
    this.FUNCI = '',
    this.NOMAU = '',
    this.PUEAU = '';
  }
}

export class aditional {
  ELPUE: string;
  VBPUE: string;
  AUPUE: string;
  SUMEN: number;
  COMGA: number;
  SMEIN: number;

  constructor() {
    this.ELPUE = '',
      this.VBPUE = '',
      this.AUPUE = '',
      this.SUMEN = 0,
      this.COMGA = 0,
      this.SMEIN = 0;
  }
}