export class F_DRH_FRM_BPR {
  FOLIO: string;
  STTUS: string;
  EJERC: number;
  USUAR: string;
  KEYUI: string;
  FECHA: Date;
  REGIO: string;
  NUEMP: string;
  ADSCR: string;
  REGOP: string;
  PUEST: string;
  PLAZA: number;
  DESCR: string;
  DESC2: string;
  NIVEL: string;
  TIPPL: string;
  NOMBR: string;
  SUMEN: number;
  COMGA: number;
  SMEIN: number;
  SINDI: boolean;
  FCBAJ: Date;
  CAUBA: string;
  JUSTI: string;
  OBSER: string;
  VBHPU: string;
  VBHPF: Date;
  VBHPR: string;
  VBHPC: string;
  VBHPK: string;
  AUTOU: string;
  AUTOF: Date;
  AUTOR: string;
  AUTOC: string;
  AUTOK: string;

  constructor() {
    this.FOLIO = '',
    this.STTUS = '',
    this.EJERC = 0,
    this.USUAR = '',
    this.KEYUI = '',
    this.FECHA = null,
    this.REGIO = '',
    this.NUEMP = '',
    this.ADSCR = '',
    this.REGOP = '',
    this.PUEST = '',
    this.PLAZA = 0,
    this.DESCR = '',
    this.DESC2 = '',
    this.NIVEL = '',
    this.TIPPL = '',
    this.NOMBR = '',
    this.SUMEN = 0,
    this.COMGA = 0,
    this.SMEIN = 0,
    this.SINDI = false,
    this.FCBAJ = null,
    this.CAUBA = '',
    this.JUSTI = '',
    this.OBSER = '',
    this.VBHPU = '',
    this.VBHPF = null,
    this.VBHPR = '',
    this.VBHPC = '',
    this.VBHPK = '',
    this.AUTOU = '',
    this.AUTOF = null,
    this.AUTOR = '',
    this.AUTOC = '',
    this.AUTOK = '';
  }
}

export class aditional {
  ELPUE: string;
  VBPUE: string;
  AUPUE: string;

  constructor() {
    this.ELPUE = '',
    this.VBPUE = '',
    this.AUPUE = '';
  }
}