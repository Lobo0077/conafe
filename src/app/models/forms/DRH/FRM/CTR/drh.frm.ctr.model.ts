export class F_DRH_FRM_CTR {
  FOLIO: string;
  STTUS: string;
  EJERC: string;
  USUAR: string;
  KEYUI: string;
  FECHA: Date;
  REGIO: string;
  SOLIC: string;
  TIPCT: string;
  MESCT: number;
  FIRAU: boolean;
  NUEMP: string;
  TITUL: string;
  NAMEE: string;
  PRIAP: string;
  SEGAP: string;
  FCNAC: Date;
  ESCOL: string;
  CARRE: string;
  SITAC: string;
  INSED: string;
  PINED: string;
  GENER: string;
  ESCIV: string;
  ESCON: string;
  RRFCC: string;
  CCURP: string;
  NACIO: string;
  PAINA: string;
  ENTNA: string;
  NMNSS: string;
  CALLE: string;
  NMEXT: string;
  NMINT: string;
  COLOC: string;
  CODPO: string;
  MUDEL: string;
  ENTFE: string;
  FOINE: string;
  ADSCR: string;
  REGOP: string;
  PUEST: string;
  PLAEX: boolean;
  SUSTI: string;
  PLAZA: number;
  DESCR: string;
  DESC2: string;
  NIVEL: string;
  TIPPL: string;
  NOMBR: string;
  DRCTR: number;
  NUMPG: number;
  MONPR: number;
  MONTT: number;
  SUMEN: number;
  COMGA: number;
  SMEIN: number;
  SINDI: boolean;
  JORLB: string;
  HORAR: boolean;
  HORDA: string;
  HORAA: string;
  HORDP: string;
  HORAP: string;
  EMAIL: string;
  FUNCI: string;
  FCING: Date;
  FCINI: Date;
  FCTER: Date;
  OBSER: string;
  DIAPA: string;
  TIPAG: string;
  MDPAG: string;
  AUTOU: string;

  constructor() {
    this.FOLIO = '',
    this.STTUS = '',
    this.EJERC = '',
    this.USUAR = '',
    this.KEYUI = '',
    this.FECHA = null,
    this.REGIO = '',
    this.SOLIC = '',
    this.TIPCT = '',
    this.MESCT = 0,
    this.FIRAU = false,
    this.NUEMP = '',
    this.TITUL = '',
    this.NAMEE = '',
    this.PRIAP = '',
    this.SEGAP = '',
    this.FCNAC = null,
    this.ESCOL = '',
    this.CARRE = '',
    this.SITAC = '',
    this.INSED = '',
    this.PINED = '',
    this.GENER = '',
    this.ESCIV = '',
    this.ESCON = '',
    this.RRFCC = '',
    this.CCURP = '',
    this.NACIO = '',
    this.PAINA = '',
    this.ENTNA = '',
    this.NMNSS = '',
    this.CALLE = '',
    this.NMEXT = '',
    this.NMINT = '',
    this.COLOC = '',
    this.CODPO = '',
    this.MUDEL = '',
    this.ENTFE = '',
    this.FOINE = '',
    this.ADSCR = '',
    this.REGOP = '',
    this.PUEST = '',
    this.PLAEX = false,
    this.SUSTI = '',
    this.PLAZA = 0,
    this.DESCR = '',
    this.DESC2 = '',
    this.NIVEL = '',
    this.TIPPL = '',
    this.NOMBR = '',
    this.DRCTR = 0,
    this.NUMPG = 0,
    this.MONPR = 0,
    this.MONTT = 0,
    this.SUMEN = 0,
    this.COMGA = 0,
    this.SMEIN = 0,
    this.SINDI = false,
    this.JORLB = '',
    this.HORAR = false,
    this.HORDA = '',
    this.HORAA = '',
    this.HORDP = '',
    this.HORAP = '',
    this.EMAIL = '',
    this.FUNCI = '',
    this.FCING = null,
    this.FCINI = null,
    this.FCTER = null,
    this.OBSER = '',
    this.DIAPA = '',
    this.TIPAG = '',
    this.MDPAG = '',
    this.AUTOU = '';
  }
}

export class aditional {
  ELPUE: string;
  AUPUE: string;
  EDADS: number;

  constructor() {
      this.ELPUE = '',
      this.AUPUE = '',
      this.EDADS = 0;
  }
}