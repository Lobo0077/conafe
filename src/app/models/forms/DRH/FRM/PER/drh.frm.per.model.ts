export class F_DRH_FRM_PER {
  NUEMP: string;
  STTUS: string;
  USUAR: string;
  KEYUI: string;
  FECHA: Date;
  REGIO: string;
  TITUL: string;
  NAMEE: string;
  PRIAP: string;
  SEGAP: string;
  FCNAC: Date;
  ESCOL: string;
  CARRE: string;
  SITAC: string;
  TTGRC: string;
  INSED: string;
  PINED: string;
  COMPU: string;
  GENER: string;
  ESCIV: string;
  ESCON: string;
  MADRE: boolean;
  NMHIJ: number;
  RRFCC: string;
  CCURP: string;
  NACIO: string;
  PAINA: string;
  ENTNA: string;
  NMNSS: string;
  CALLE: string;
  NMEXT: string;
  NMINT: string;
  COLOC: string;
  CODPO: string;
  MUDEL: string;
  ENTFE: string;
  TELEF: string;
  TECEL: string;
  TEEME: string;
  EMAIP: string;
  FOINE: string;
  BANCO: string;
  CTABA: string;
  ENFCR: boolean;
  ENFER: string;
  DISCA: string;
  ADSCR: string;
  REGOP: string;
  PUEST: string;
  PLAEX: boolean;
  SUSTI: string;
  PLAZA: number;
  DESCR: string;
  DESC2: string;
  NIVEL: string;
  TIPPL: string;
  NOMBR: string;
  DRCTR: number;
  NUMPG: number;
  MONPR: number;
  MONTT: number;
  SUMEN: number;
  COMGA: number;
  SMEIN: number;
  SINDI: boolean;
  JORLB: string;
  HORAR: boolean;
  HORDA: string;
  HORAA: string;
  HORDP: string;
  HORAP: string;
  EMAIL: string;
  EXTEN: string;
  JEFEI: string;
  FUNCI: string;
  FCING: Date;
  FCINI: Date;
  FCMOV: Date;
  FCMOD: Date;
  FCTER: Date;
  FCBAJ: Date;
  CAUBA: string;
  OBSER: string;
  DIAPA: string;
  TIPAG: string;
  MDPAG: string;
  PAGAD: string;
  CRAMO: string;
  CLACO: string;
  SUBAI: number;
  SUBAS: number;
  REMUT: number;
  IMAGN: string;
  VBHPU: string;
  VBHPF: Date;
  VBHPR: string;
  VBHPC: string;
  VBHPK: string;
  AUTOU: string;
  AUTOF: Date;
  AUTOR: string;
  AUTOC: string;
  AUTOK: string;

  constructor() {
    this.NUEMP = '',
    this.STTUS = '',
    this.USUAR = '',
    this.KEYUI = '',
    this.FECHA = null,
    this.REGIO = '',
    this.TITUL = '',
    this.NAMEE = '',
    this.PRIAP = '',
    this.SEGAP = '',
    this.FCNAC = null,
    this.ESCOL = '',
    this.CARRE = '',
    this.SITAC = '',
    this.TTGRC = '',
    this.INSED = '',
    this.PINED = '',
    this.COMPU = '',
    this.GENER = '',
    this.ESCIV = '',
    this.ESCON = '',
    this.MADRE = false,
    this.NMHIJ = 0,
    this.RRFCC = '',
    this.CCURP = '',
    this.NACIO = '',
    this.PAINA = '',
    this.ENTNA = '',
    this.NMNSS = '',
    this.CALLE = '',
    this.NMEXT = '',
    this.NMINT = '',
    this.COLOC = '',
    this.CODPO = '',
    this.MUDEL = '',
    this.ENTFE = '',
    this.TELEF = '',
    this.TECEL = '',
    this.TEEME = '',
    this.EMAIP = '',
    this.FOINE = '',
    this.BANCO = '',
    this.CTABA = '',
    this.ENFCR = false,
    this.ENFER = '',
    this.DISCA = '',
    this.ADSCR = '',
    this.REGOP = '',
    this.PUEST = '',
    this.PLAEX = false,
    this.SUSTI = '',
    this.PLAZA = 0,
    this.DESCR = '',
    this.DESC2 = '',
    this.NIVEL = '',
    this.TIPPL = '',
    this.NOMBR = '',
    this.DRCTR = 0,
    this.NUMPG = 0,
    this.MONPR = 0,
    this.MONTT = 0,
    this.SUMEN = 0,
    this.COMGA = 0,
    this.SMEIN = 0,
    this.SINDI = false,
    this.JORLB = '',
    this.HORAR = false,
    this.HORDA = null,
    this.HORAA = null,
    this.HORDP = null,
    this.HORAP = null,
    this.EMAIL = '',
    this.EXTEN = '',
    this.JEFEI = '',
    this.FUNCI = '',
    this.FCING = null,
    this.FCINI = null,
    this.FCMOV = null,
    this.FCMOD = null,
    this.FCTER = null,
    this.FCBAJ = null,
    this.CAUBA = '',
    this.OBSER = '',
    this.DIAPA = '',
    this.TIPAG = '',
    this.MDPAG = '',
    this.PAGAD = '',
    this.CRAMO = '',
    this.CLACO = '',
    this.SUBAI = 0,
    this.SUBAS = 0,
    this.REMUT = 0,
    this.IMAGN = '',
    this.VBHPU = '',
    this.VBHPF = null,
    this.VBHPR = '',
    this.VBHPC = '',
    this.VBHPK = '',
    this.AUTOU = '',
    this.AUTOF = null,
    this.AUTOR = '',
    this.AUTOC = '',
    this.AUTOK = '';
  }
}

export class F_DRH_FRM_PER_DFM {
  ID: number;
  ERROR: string;
  NAMEE: string;
  PRIAP: string;
  SEGAP: string;
  FCNAC: Date;
  EDADS: number;
  CCURP: string;
  PDFCR: string;
  PORCR: number;
  PDFAN: string;
  PORAN: number;

  constructor() {
    this.ID = 0,
    this.ERROR = '',
    this.NAMEE = '',
    this.PRIAP = '',
    this.SEGAP = '',
    this.FCNAC = null,
    this.EDADS = 0,
    this.CCURP = '',
    this.PDFCR = '',
    this.PORCR = 0,
    this.PDFAN = '',
    this.PORAN = 0;
  }
}

export class F_DRH_FRM_PER_DOC {
  ID: number;
  ERROR: string;
  TPDOC: string;
  ENTRE: boolean;
  PPDFF: string;
  REQUI: boolean;

  constructor() {
    this.ID = 0,
    this.ERROR = '',
    this.TPDOC = '',
    this.ENTRE = false,
    this.PPDFF = '',
    this.REQUI = false;
  }
}

export class P_DRH_FRM_PER_DOC {
  PPDFF: number;

  constructor() {
    this.PPDFF = 0;
  }
}

export class F_DRH_FRM_PER_TLB {
  ID: number;
  ERROR: string;
  PAITL: string;
  ENTTL: string;
  EMPRE: string;
  PUEST: string;
  CAMEX: string;
  FCINI: Date;
  FCTER: Date;

  constructor() {
    this.ID = 0,
    this.ERROR = '',
    this.PAITL = '',
    this.ENTTL = '',
    this.EMPRE = '',
    this.PUEST = '',
    this.CAMEX = '',
    this.FCINI = null,
    this.FCTER = null;
  }
}

export class F_DRH_FRM_PER_DCC {
  ID: number;
  ERROR: string;
  PAIDC: string;
  ENTDC: string;
  MATER: string;
  AREGN: string;
  INSTI: string;
  FCINI: Date;
  FCTER: Date;
  NVLES: string;

  constructor() {
    this.ID = 0,
    this.ERROR = '',
    this.PAIDC = '',
    this.ENTDC = '',
    this.MATER = '',
    this.AREGN = '',
    this.INSTI = '',
    this.FCINI = null,
    this.FCTER = null,
    this.NVLES = '';
  }
}

export class F_DRH_FRM_PER_IDI {
  ID: number;
  ERROR: string;
  IDIOM: string;
  REDAC: string;
  CONVE: string;
  COMPR: string;

  constructor() {
    this.ID = 0,
    this.ERROR = '',
    this.IDIOM = '',
    this.REDAC = '',
    this.CONVE = '',
    this.COMPR = '';
  }
}

export class aditional {
  ELPUE: string;
  VBPUE: string;
  AUPUE: string;
  EDADS: number;
  ADSCR: string;
  PUEST: string;
  PLAZA: number;
  DESCR: string;
  DESC2: string;
  NIVEL: string;
  TIPPL: string;
  NOMBR: string;
  SUMEN: number;
  COMGA: number;
  SMEIN: number;

  constructor() {
    this.ELPUE = '',
      this.VBPUE = '',
      this.AUPUE = '',
      this.EDADS = 0,
      this.ADSCR = '',
      this.PUEST = '',
      this.PLAZA = 0,
      this.DESCR = '',
      this.DESC2 = '',
      this.NIVEL = '',
      this.TIPPL = '',
      this.NOMBR = '',
      this.SUMEN = 0,
      this.COMGA = 0,
      this.SMEIN = 0;
  }
}