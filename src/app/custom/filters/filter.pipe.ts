import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'filter'})

export class FilterPipe implements PipeTransform {
        transform(items: any[], fields: object): any[] {
            if(!fields) return [];

            let itemsTemp = items;

            Object.keys(fields).forEach(function (key) {
                if(fields[key] && itemsTemp) {
                    itemsTemp = itemsTemp.filter(it => it[key].toLowerCase().indexOf(fields[key].toLowerCase()) != -1);
                } else {
                    itemsTemp = [];
                }
            })

            return itemsTemp;
        }
}