import { Component } from "@angular/core";
import { ICellRendererAngularComp } from 'ag-grid-angular';
import { WBService } from '../../../services/wb.service';

@Component({
    selector: 'child-cell',
    template: `
    <button mat-icon-button (click)="downloadClick($event)" *ngIf="params.data && type=='download' && ((option=='1' && params.data[field] && params.data[field].length < 30) || (option=='2' && params.data[column]) || (option=='3' && params.data[column]))">
        <mat-icon class="s-18" matTooltip="{{tooltip}}" [ngStyle]="{'color': buttonStyle.modify}" (mouseenter)="buttonStyle.modify='#007bff'" (mouseleave)="buttonStyle.modify='gray'"><i class="{{icon}}"></i></mat-icon>
    </button>
    <button mat-icon-button (click)="onClick($event,'A')" *ngIf="params.data && type=='action' && params.data.SHWBA">
        <mat-icon class="s-18" matTooltip="Agregar Registro" [ngStyle]="{'color': buttonStyle.add}" (mouseenter)="buttonStyle.add='#28a745'" (mouseleave)="buttonStyle.add='gray'"><i class="fas fa-plus"></i></mat-icon>
    </button>
    <button mat-icon-button (click)="onClick($event,'M')" *ngIf="params.data && type=='action' && params.data.SHWBM">
        <mat-icon class="s-18" matTooltip="Modificar Registro" [ngStyle]="{'color': buttonStyle.modify}" (mouseenter)="buttonStyle.modify='#007bff'" (mouseleave)="buttonStyle.modify='gray'"><i class="far fa-edit"></i></mat-icon>
    </button>
    <button mat-icon-button (click)="onClick($event,'E')" *ngIf="params.data && type=='action' && params.data.SHWBD">
        <mat-icon class="s-18" matTooltip="Eliminar Registro" [ngStyle]="{'color': buttonStyle.delete}" (mouseenter)="buttonStyle.delete='#dc3545'" (mouseleave)="buttonStyle.delete='gray'"><i class="fas fa-trash"></i></mat-icon>
    </button>
    `,
    styles: [
        `.btn {
            line-height: 0.5
        }`
    ]
})
export class buttonGrid implements ICellRendererAngularComp {
    public params: any;
    public type: string;
    public icon: string;
    public tooltip: string;
    public option: string;
    public field: string;
    public column: string;
    public buttonStyle = {add: 'gray', modify: 'gray', delete: 'gray', download: 'gray'};

    constructor(private wbService: WBService) { }

    agInit(params: any): void {
        let options = params.options.split(',');

        if (options[0]) {
            this.type = options[0];
        }

        if (options[1]) {
            this.icon = options[1];
        }

        if (options[2]) {
            this.tooltip = options[2];
        }

        if (options[3]) {
            this.option = options[3];
        }

        if (options[4]) {
            this.field = options[4];
        }

        if (params.colDef) {
            this.column = params.colDef.field;
        }

        this.params = params;
    }

    downloadClick($event) {
        if (this.params.onClick instanceof Function) {
            // put anything into params u want pass into parents component
            const params = {
                event: $event,
                rowData: this.params.node.data,
                index: this.params.rowIndex,
                option: this.option,
                field: this.field,
                column: this.column
                // ...something
            }
            this.params.onClick(params);

        }
    }

    onClick($event, option) {
        if (this.params.onClick instanceof Function) {
            // put anything into params u want pass into parents component
            const params = {
                event: $event,
                rowData: this.params.node.data,
                index: this.params.rowIndex,
                option: option
                // ...something
            }
            this.params.onClick(params);

        }
    }

    refresh(): boolean {
        return false;
    }


}