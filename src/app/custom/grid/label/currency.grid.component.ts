import { Component } from "@angular/core";
import { ICellRendererAngularComp } from 'ag-grid-angular';

@Component({
    selector: 'currency-cell',
    template: `
    <span>{{ value | currency}}</span>
    `
})
export class currencyGrid implements ICellRendererAngularComp {
    public params: any;
    public value;
    public column: string;

    constructor() { }

    agInit(params: any): void {
        if (params.colDef) {
            this.column = params.colDef.field;
        }

        this.params = params;

        if(this.params.data) {
            if(!isNaN(this.params.data[this.column])) {
                this.value = this.params.data[this.column];
            } else {
                this.value = '';
            }
        } else {
            this.value = '';
        }
    }

    refresh(): boolean {
        return false;
    }
}