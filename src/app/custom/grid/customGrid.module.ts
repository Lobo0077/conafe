import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common'; 
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';

import { buttonGrid } from './button/button.grid.component';
import { currencyGrid } from './label/currency.grid.component';

@NgModule({
  entryComponents: [buttonGrid,
                    currencyGrid],
  declarations: [buttonGrid,
                  currencyGrid],
  exports: [buttonGrid,
            currencyGrid],
  imports: [
    CommonModule,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule
  ]
})
export class CustomGridModule {
}