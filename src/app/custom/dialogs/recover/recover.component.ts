import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { AuthService } from '../../../services/auth.service';
import { GlobalService } from '../../../services/global.service';
import { WBService } from '../../../services/wb.service';
import { RecoverService } from '../../../services/recover.service'

import { recover } from '../../../models/global/recover';

@Component({
    selector: 'dialog_recover',
    templateUrl: 'recover.component.html',
})
export class DialogRecover implements OnInit {
    private gridApi: any;
    public rowSelection = "single";
    public recoverInf: recover = new recover();

    public columnDefs = []

    rowData: any;

    constructor(private authService: AuthService,
        private wbService: WBService,
        private globalService: GlobalService,
        private recoverService: RecoverService,
        public dialogRef: MatDialogRef<DialogRecover>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
            if (this.data.title) {
                this.recoverInf = this.data
            } else {
                this.recoverInf = this.recoverService.getRecover(this.data.format, this.data.type, this.authService.user.user_name);
            }
        this.columnDefs = this.recoverInf.columns;
    }

    ngOnInit() {
        this.loadData();
    }

    loadData() {
        if (this.recoverInf.object) {
            this.wbService.getInformation(this.recoverInf.object, this.recoverInf.filter).subscribe(response => {
                this.rowData = response.data;
            })
        }
    }

    onGridReady(params) {
        this.gridApi = params;
    }

    onNoClick() {
        this.dialogRef.close();
    }

    recoverData() {
        let selectedNodes = this.gridApi.api.getSelectedNodes();

        if (selectedNodes.length <= 0) {
            this.globalService.messageSwal('Advertencia', 'Selecciona el registro a recuperar', 'warning');
            return
        }

        this.dialogRef.close(selectedNodes[0].data);
    }
}