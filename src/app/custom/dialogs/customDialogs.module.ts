import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatRippleModule } from '@angular/material/core';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatBadgeModule } from '@angular/material/badge';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatDialogModule } from '@angular/material/dialog';

import { DialogRecover } from './recover/recover.component';
import { DialogConfirm } from './confirm/confirm.component';

import { AgGridModule } from 'ag-grid-angular';

@NgModule({
    entryComponents: [
        DialogRecover,
        DialogConfirm
    ],
    declarations: [
        DialogRecover,
        DialogConfirm
    ],
    exports: [
        DialogRecover,
        DialogConfirm
    ],
    imports: [
        CommonModule,
        FormsModule,
        
        MatButtonModule,
        MatCheckboxModule,
        MatDatepickerModule,
        MatFormFieldModule,
        MatIconModule,
        MatBadgeModule,
        MatInputModule,
        MatMenuModule,
        MatRippleModule,
        MatSelectModule,
        MatTableModule,
        MatCardModule,
        MatToolbarModule,
        MatTooltipModule,
        MatDialogModule,
        
        AgGridModule.withComponents([])
    ]
})
export class CustomDialogModule {
}