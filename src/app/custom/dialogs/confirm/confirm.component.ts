import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { CustomErrorStateMatcher } from '../../../models/global/error';

@Component({
    selector: 'dialog_confirm',
    templateUrl: 'confirm.component.html',
    styleUrls: ['./confirm.component.scss']
  })
  export class DialogConfirm {
    public error = new CustomErrorStateMatcher();
    public option: string;
    public title: string;
    public response: string;
    public status: string;
    public label: string;
    public commit: string = '';
  
    constructor(public dialogRef: MatDialogRef<DialogConfirm>,
      @Inject(MAT_DIALOG_DATA) public data: any) {
      this.option = this.data.option;
      this.response = this.data.response;
  
      if (this.response == 'A' && this.option == 'V') {
        this.label = 'Comentario';
        this.status = '3';
        this.title = 'Confirmar Visto Bueno';
      } else if (this.response == 'A' && this.option == 'A') {
        this.label = 'Comentario';
        this.status = '4';
        this.title = 'Confirmar Autorización';
      } else if (this.response == 'R') {
        this.label = 'Observaciones';
        this.status = '1';
        this.title = 'Confirmar Devolución al Usuario para Corrección';
      } else if (this.response == 'C') {
        this.label = 'Motivo';
        this.status = '5';
        this.title = 'Confirmar Cancelación del Folio';
      }
    }
  
    onNoClick() {
      this.dialogRef.close();
    }
  
    sendData() {
      let response = { option: this.option, response: this.response, commit: this.commit, status: this.status }
      this.dialogRef.close(response);
    }
  }