(function (window) {
  window.__env = window.__env || {};

  var hostname = window.location.hostname;

  if(hostname == 'localhost') {
    window.__env.env = 'D';
    window.__env.url = 'http://sysclick.ddns.net/serviceCSA/';
    //window.__env.url = 'http://localhost:8080/';
  } else if(hostname == 'sysclick.ddns.net') {
    window.__env.env = 'D';
    window.__env.url = 'http://sysclick.ddns.net/serviceCSA/';
  }

}(this));
